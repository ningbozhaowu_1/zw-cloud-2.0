package com.zhaowu.cloud.application.client;

import com.zhaowu.cloud.application.client.form.ApplicationChannelQueryForm;
import com.zhaowu.cloud.application.client.form.ApplicationPayQueryForm;
import com.zhaowu.cloud.application.client.vo.ApplicationPayVO;
import com.zhaowu.cloud.application.client.form.ApplicationChannelAuthorizeForm;
import com.zhaowu.cloud.application.client.vo.ApplicationChannelVO;
import com.zhaowu.cloud.framework.base.protocol.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author xxp
 */
@FeignClient(name = "app-service")
public interface ApplicationClient {

    @PostMapping(value = "/app/public/applicationChannel/get")
    ApplicationChannelVO getApplicationChannel(@RequestBody ApplicationChannelQueryForm applicationChannelQueryForm);

    @PostMapping(value = "/app/applicationPay/get")
    ApplicationPayVO getApplicationPay(@RequestBody ApplicationPayQueryForm applicationPayQueryForm);

    @PostMapping(value = "/app/public/applicationChannel/getByAuthorizerAppId")
    ApplicationChannelVO getApplicationChannelByAuthorizerAppId(@RequestBody ApplicationChannelQueryForm applicationChannelQueryForm);

    @PostMapping(value = "/app/public/applicationChannel/authorizeUpdate")
    Result authorizeUpdate(@RequestBody ApplicationChannelAuthorizeForm applicationChannelAuthorizeForm);

    @PostMapping(value = "/app/public/applicationPay/authorizeAdd")
    Result authorizeAdd(@RequestBody ApplicationChannelAuthorizeForm applicationChannelAuthorizeForm);
}
