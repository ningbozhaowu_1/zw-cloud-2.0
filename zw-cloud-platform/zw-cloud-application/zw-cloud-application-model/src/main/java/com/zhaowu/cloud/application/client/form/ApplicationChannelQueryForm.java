package com.zhaowu.cloud.application.client.form;

import lombok.Data;

@Data
public class ApplicationChannelQueryForm {

    private Long channelId;

    private String applicationParty;

    private String authorizerAppId;
}
