package com.zhaowu.cloud.application.client.form;

import lombok.Data;

@Data
public class ApplicationChannelAuthorizeForm {

    private String codeType;

    private String code;

    private String authorizerAppid;

    private String accessToken;

    private String refreshToken;

    private String authCode;

    private String applicationParty;

    private String type;
}
