package com.zhaowu.cloud.application.client.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xxp
 **/
@Data
public class ApplicationVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;

    private String id;

    private String parentId;

    private String name;

    private String status;

    private Date effectiveTime;

    private Date expireTime;
}
