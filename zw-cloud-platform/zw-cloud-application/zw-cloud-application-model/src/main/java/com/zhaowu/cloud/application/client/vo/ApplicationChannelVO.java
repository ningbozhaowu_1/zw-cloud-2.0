package com.zhaowu.cloud.application.client.vo;


import lombok.Data;

/**
 * @author xxp
 **/
@Data
public class ApplicationChannelVO {

    private static final long serialVersionUID = 1L;

    private String id;

    private String applicationParty;

    private String authorizerAppId;

    private String name;

    private String introduction;

    private String avatar;

    private String type;

    private String secret;

    private String token;

    private String aesKey;

    private String accessToken;

    private String refreshToken;

    private String authToken;

}
