package com.zhaowu.cloud.application.client.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApplicationPayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String merchantId;

    private String applicationParty;

    private String applicationChannelId;

    private String merchantKey;

    private String merchantCertKey;

    private String merchantType;
}
