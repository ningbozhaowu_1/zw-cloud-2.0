package com.zhaowu.cloud.application.client.form;

import lombok.Data;

@Data
public class ApplicationPayQueryForm {

    private String applicationParty;

    private String applicationChannelId;
}
