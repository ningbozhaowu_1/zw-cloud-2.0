--公用中心
CREATE DATABASE IF NOT EXISTS `zw-common` DEFAULT CHARACTER SET = utf8mb4;
CREATE USER `common`@'%' IDENTIFIED BY 'common123';
GRANT ALL PRIVILEGES ON `zw-common`.* TO `common`;
GRANT ALL PRIVILEGES ON `zw-common`.* TO `common`;

USE `zw-common`;

DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application_id` bigint(20) NOT NULL COMMENT '应用编号',
  `channel_id` bigint(20) NOT NULL COMMENT '应用渠道编号',
  `file_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '文件上传时的名称',
  `content_type` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '文件mime类型',
  `url_path` varchar(2048) CHARACTER SET utf8 DEFAULT NULL COMMENT '文件存储路径',
  `full_url_path` varchar(2048) CHARACTER SET utf8 DEFAULT NULL COMMENT '文件访问路径',
  `size` bigint(20) DEFAULT NULL COMMENT '文件大小,单位字节',
  `create_by` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '上传人',
  `create_time` datetime DEFAULT NULL COMMENT '上传时间',
  `biz_id` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '业务类型 IMAGE',
  `biz_code` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '业务标识 PRODUCT',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='附件表';

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `type` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '日志类型:登录日志;系统日志;',
  `name` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '业务名称',
  `request_url` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求地址',
  `request_type` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求方法类型',
  `request_class` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '调用类',
  `request_method` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '调用方法',
  `request_ip` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求IP',
  `user_id` bigint(20) DEFAULT NULL COMMENT '创建用户Id',
  `user_name` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '创建用户名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `time` int(11) DEFAULT NULL COMMENT '消耗时间',
  `success` int(11) DEFAULT NULL COMMENT '是否成功',
  `exception` varchar(2250) COLLATE utf8_bin DEFAULT NULL COMMENT '异常信息',
  `request_parameter` varbinary(2250) DEFAULT NULL COMMENT '调用参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='系统日志表';