package com.zhaowu.cloud.common.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.common.entity.po.City;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhaowu.cloud.common.entity.po.Province;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 行政区域地州市信息表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface CityService{

    @Cached(name="cityCache::", key="#provinceId", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<City> selectListByProvinceId(String provinceId);

    @Cached(name="cityCache::", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<City> selectList();
}
