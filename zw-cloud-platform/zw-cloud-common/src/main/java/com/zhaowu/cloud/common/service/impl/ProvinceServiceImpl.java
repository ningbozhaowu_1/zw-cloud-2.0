package com.zhaowu.cloud.common.service.impl;

import com.zhaowu.cloud.common.entity.po.Province;
import com.zhaowu.cloud.common.mapper.ProvinceMapper;
import com.zhaowu.cloud.common.service.ProvinceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 省份信息表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@Service
public class ProvinceServiceImpl extends ServiceImpl<ProvinceMapper,Province> implements ProvinceService {

    @Override
    public List<Province> selectList() {
        return this.list();
    }
}
