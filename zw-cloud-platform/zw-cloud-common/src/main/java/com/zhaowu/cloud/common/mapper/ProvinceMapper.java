package com.zhaowu.cloud.common.mapper;

import com.zhaowu.cloud.common.entity.po.Province;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 省份信息表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface ProvinceMapper extends BaseMapper<Province> {

}
