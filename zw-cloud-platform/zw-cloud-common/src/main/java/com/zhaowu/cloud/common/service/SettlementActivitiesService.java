package com.zhaowu.cloud.common.service;

import com.zhaowu.cloud.common.entity.po.SettlementActivities;

import java.util.List;

/**
 * <p>
 * 优惠活动规则信息 server
 * </p>
 *
 * @author shen
 * @since 2021-11-13
 */
public interface SettlementActivitiesService {
    List<SettlementActivities> activities();
}
