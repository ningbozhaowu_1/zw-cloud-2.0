package com.zhaowu.cloud.common.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.common.entity.po.Bank;
import com.zhaowu.cloud.common.entity.po.SettlementRule;

import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * <p>
 * 结算规则
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
public interface SettlementRuleService {

    @Cached(name="SettlementRuleCache::", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<SettlementRule> selectList();
}
