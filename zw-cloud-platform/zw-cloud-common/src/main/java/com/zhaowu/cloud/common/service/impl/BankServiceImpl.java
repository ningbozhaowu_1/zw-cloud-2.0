package com.zhaowu.cloud.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.common.entity.po.Area;
import com.zhaowu.cloud.common.entity.po.Bank;
import com.zhaowu.cloud.common.mapper.AreaMapper;
import com.zhaowu.cloud.common.mapper.BankMapper;
import com.zhaowu.cloud.common.service.BankService;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * <p>
 * 开户行
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
@Service
public class BankServiceImpl extends ServiceImpl<BankMapper, Bank> implements BankService {
    @Override
    public List<Bank> selectListByName(String name) {
        QueryWrapper<Bank> queryWrapper = new QueryWrapper();
        queryWrapper.like("name", name);
        queryWrapper.last("limit 200");
        return this.list(queryWrapper);
    }

    @Override
    public List<Bank> selectList() {
        QueryWrapper<Bank> queryWrapper = new QueryWrapper();
        queryWrapper.last("limit 200");
        return this.list(queryWrapper);
    }
}
