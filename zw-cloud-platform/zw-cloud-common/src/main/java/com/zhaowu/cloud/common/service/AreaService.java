package com.zhaowu.cloud.common.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.common.entity.po.Area;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhaowu.cloud.common.entity.po.City;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 行政区域县区信息表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface AreaService{

    @Cached(name="areaCache::", key="#cityId", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<Area> selectListByCityId(String cityId);

    @Cached(name="areaCache::", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<Area> selectList();
}
