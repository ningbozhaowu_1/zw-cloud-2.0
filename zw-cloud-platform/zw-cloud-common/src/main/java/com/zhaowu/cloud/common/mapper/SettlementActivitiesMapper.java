package com.zhaowu.cloud.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.common.entity.po.SettlementActivities;

/**
 * 优惠活动 SettlementActivities 接口
 */
public interface SettlementActivitiesMapper extends BaseMapper<SettlementActivities> {
}
