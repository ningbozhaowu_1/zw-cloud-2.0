package com.zhaowu.cloud.common.entity.po;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 结算规则
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
@Data
@Accessors(chain = true)
public class SettlementRule implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 结算规则ID
     */
    private String settlementId;
    /**
     * 主体类型
     */
    private String mainType;
    /**
     * 费率说明
     */
    private String rateDesc;
    /**
     * 行业范围
     */
    private String qualificationScope;
    /**
     * 行业类型
     */
    private String qualificationType;
    /**
     * 特殊资质
     */
    private String qualifications;

    /**
     * 是否必须 0：否；1：是
     */
    private String qualificationRequired;
    /**
     * 类型，normal：正常；secondary：电商二级商户入驻
     */
    private String type;
}
