package com.zhaowu.cloud.common.mapper;

import com.zhaowu.cloud.common.entity.po.Area;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 行政区域县区信息表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface AreaMapper extends BaseMapper<Area> {

}
