package com.zhaowu.cloud.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.common.entity.po.Area;
import com.zhaowu.cloud.common.entity.po.Bank;
/**
 * <p>
 * 开户银行
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
public interface BankMapper extends BaseMapper<Bank> {
}
