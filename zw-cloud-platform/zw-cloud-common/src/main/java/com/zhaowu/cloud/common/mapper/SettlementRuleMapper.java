package com.zhaowu.cloud.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.common.entity.po.Province;
import com.zhaowu.cloud.common.entity.po.SettlementRule;
/**
 * <p>
 * 结算规则
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
public interface SettlementRuleMapper  extends BaseMapper<SettlementRule> {
}
