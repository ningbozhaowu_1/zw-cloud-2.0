package com.zhaowu.cloud.common.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.common.entity.po.Area;
import com.zhaowu.cloud.common.entity.po.Bank;
import com.zhaowu.cloud.common.entity.po.City;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 开户行
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
public interface BankService {
    @Cached(name="bank::", key="#cityId", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<Bank> selectListByName(String name);

//    @Cached(name="bank::", expire = 360000, cacheType = CacheType.BOTH)
//    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
//    @CachePenetrationProtect
    List<Bank> selectList();
}
