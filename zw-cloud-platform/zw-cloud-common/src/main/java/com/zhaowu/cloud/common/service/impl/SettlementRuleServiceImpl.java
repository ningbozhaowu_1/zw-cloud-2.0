package com.zhaowu.cloud.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.common.entity.po.Area;
import com.zhaowu.cloud.common.entity.po.SettlementRule;
import com.zhaowu.cloud.common.mapper.AreaMapper;
import com.zhaowu.cloud.common.mapper.SettlementRuleMapper;
import com.zhaowu.cloud.common.service.SettlementRuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * <p>
 * 结算规则
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
@Service
public class SettlementRuleServiceImpl extends ServiceImpl<SettlementRuleMapper, SettlementRule> implements SettlementRuleService {

    @Override
    public List<SettlementRule> selectList() {
        return this.list();
    }
}
