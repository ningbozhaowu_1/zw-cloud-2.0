package com.zhaowu.cloud.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.common.entity.po.Area;
import com.zhaowu.cloud.common.entity.po.City;
import com.zhaowu.cloud.common.mapper.AreaMapper;
import com.zhaowu.cloud.common.service.AreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 行政区域县区信息表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper,Area> implements AreaService {

    @Override
    public List<Area> selectListByCityId(String cityId) {
        QueryWrapper<Area> queryWrapper = new QueryWrapper();
        queryWrapper.eq("city_id", cityId);
        return this.list(queryWrapper);
    }

    @Override
    public List<Area> selectList() {
        return this.list();
    }
}
