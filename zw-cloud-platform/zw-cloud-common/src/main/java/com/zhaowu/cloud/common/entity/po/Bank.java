package com.zhaowu.cloud.common.entity.po;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 开户行
 * </p>
 *
 * @author shen
 * @since 2021-10-23
 */
@Data
@Accessors(chain = true)
public class Bank implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    private String code;

    private String name;

    private String remark;
}
