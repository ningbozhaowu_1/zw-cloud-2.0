package com.zhaowu.cloud.common.controller;


import com.google.common.collect.Maps;
import com.zhaowu.cloud.common.entity.po.*;
import com.zhaowu.cloud.common.service.*;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 行政区域县区信息表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@RestController
@RequestMapping("/common/v1")
@Api(tags = "公用服务 接口")
public class CommonController extends BaseController {

        @Autowired
        private AreaService areaService;

        @Autowired
        private CityService cityService;

        @Autowired
        private ProvinceService provinceService;

        @Autowired
        private BankService bankService;

        @Autowired
        private SettlementRuleService settlementRuleService;

        @Resource
        private SettlementActivitiesService settlementActivitiesService;

        @GetMapping(value = "/provinceList")
        @ApiOperation("省份列表")
        public Result<Province> provinceList() {
            return this.success(provinceService.selectList());
        }

        @GetMapping(value = "/cityList")
        @ApiOperation("城市列表")
        public Result<City> cityList(@Valid @RequestParam String provinceId) {
            return this.success(cityService.selectListByProvinceId(provinceId));
        }

        @GetMapping(value = "/areaList")
        @ApiOperation("城市列表")
        public Result<Area> areaList(@Valid @RequestParam String cityId) {
                return this.success(areaService.selectListByCityId(cityId));
        }
        @GetMapping(value = "/bankList")
        @ApiOperation("开户行列表")
        public Result<Bank> bankList() {
                return this.success(bankService.selectList());
        }
        @GetMapping(value = "/bankList/{name}")
        @ApiOperation("根据名称查询开户行列表")
        public Result<Bank> bankListByName(@PathVariable String name) {
                return this.success(bankService.selectListByName(name));
        }
        @GetMapping(value = "/settlementRuleList")
        @ApiOperation("结算规则查询")
        public Result<SettlementRule> settlementRuleList() {
                return this.success(settlementRuleService.selectList());
        }
        @GetMapping(value = "/settlementActivities")
        @ApiOperation("优惠活动查询")
        public Result<List<SettlementActivities>> settlementActivities() {
                return this.success(settlementActivitiesService.activities());
        }
        @GetMapping(value = "/areaMap")
        @ApiOperation("所有地区数据")
        public Result<Map> areaMap() {

                Map map = Maps.newHashMap();
                map.put("province", provinceService.selectList());
                map.put("city", cityService.selectList());
                map.put("area", areaService.selectList());
                return this.success(map);
        }

}

