package com.zhaowu.cloud.common.mapper;

import com.zhaowu.cloud.common.entity.po.City;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 行政区域地州市信息表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface CityMapper extends BaseMapper<City> {

}
