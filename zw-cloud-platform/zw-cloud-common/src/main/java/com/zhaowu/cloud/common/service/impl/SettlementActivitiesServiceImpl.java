package com.zhaowu.cloud.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.common.entity.po.SettlementActivities;
import com.zhaowu.cloud.common.mapper.SettlementActivitiesMapper;
import com.zhaowu.cloud.common.service.SettlementActivitiesService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 优惠活动规则信息 server
 * </p>
 *
 * @author shen
 * @since 2021-11-13
 */
@Service
public class SettlementActivitiesServiceImpl extends ServiceImpl<SettlementActivitiesMapper, SettlementActivities> implements SettlementActivitiesService {
    @Override
    public List<SettlementActivities> activities() {
        return this.list();
    }
}
