package com.zhaowu.cloud.common.entity.po;


import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 优惠活动规则信息
 * </p>
 *
 * @author shen
 * @since 2021-11-13
 */
@Data
@Accessors(chain = true)
public class SettlementActivities implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 活动ID
     */
    private String activityId;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 优惠费率描述
     */
    private String activityRateDesc;
    /**
     * 过期时间
     */
    private String expireTime;
    /**
     * 活动规则描述
     */
    private String activityRulesDesc;
    /**
     * 需要对应的结算费率ID
     */
    private String rules;

}
