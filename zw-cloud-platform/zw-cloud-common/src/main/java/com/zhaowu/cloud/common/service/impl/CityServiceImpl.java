package com.zhaowu.cloud.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.common.entity.po.City;
import com.zhaowu.cloud.common.mapper.CityMapper;
import com.zhaowu.cloud.common.service.CityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 行政区域地州市信息表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper,City> implements CityService {

    @Override
    public List<City> selectListByProvinceId(String provinceId) {

        QueryWrapper<City> queryWrapper = new QueryWrapper();
        queryWrapper.eq("province_id", provinceId);
        return this.list(queryWrapper);
    }

    @Override
    public List<City> selectList() {
        return this.list();
    }
}
