package com.zhaowu.cloud.common.entity.po;

import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 * 行政区域地州市信息表
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@Data
@Accessors(chain = true)
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String cityId;

    private String name;

    private String provinceId;



}
