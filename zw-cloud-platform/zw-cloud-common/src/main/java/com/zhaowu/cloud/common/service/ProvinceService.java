package com.zhaowu.cloud.common.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.common.entity.po.Province;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 省份信息表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface ProvinceService{

    @Cached(name="provinceCache::", expire = 360000, cacheType = CacheType.BOTH)
    @CacheRefresh(refresh = 360000, timeUnit = TimeUnit.SECONDS)
    @CachePenetrationProtect
    List<Province> selectList();
}
