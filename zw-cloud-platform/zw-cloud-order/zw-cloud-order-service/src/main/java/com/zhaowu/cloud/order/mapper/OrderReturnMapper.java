package com.zhaowu.cloud.order.mapper;

import com.zhaowu.cloud.order.entity.po.OrderReturn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单退款表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderReturnMapper extends BaseMapper<OrderReturn> {

}
