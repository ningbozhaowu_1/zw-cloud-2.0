package com.zhaowu.cloud.order.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.order.entity.param.OrderQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class OrderQueryByStatusForm extends BaseQueryForm<OrderQueryParam> {

    @ApiModelProperty("订单状态 WFP-待支付 PAID-已支付 SEND-已发货 FINISH-已完成 CLOSED-已关闭 RETURN-退款中 DEPOSIT-已下定 不送返回全部")
    private String status;

}
