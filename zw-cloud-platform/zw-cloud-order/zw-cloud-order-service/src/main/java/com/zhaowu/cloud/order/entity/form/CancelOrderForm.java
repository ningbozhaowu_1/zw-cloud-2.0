package com.zhaowu.cloud.order.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.order.entity.po.Order;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
public class CancelOrderForm extends BaseForm<Order> {

    @ApiModelProperty("订单编号")
    @NotNull(message = "订单编号不能为空")
    private String id;

    @ApiModelProperty("取消原因")
    @NotNull(message = "取消原因")
    private String reason;

}
