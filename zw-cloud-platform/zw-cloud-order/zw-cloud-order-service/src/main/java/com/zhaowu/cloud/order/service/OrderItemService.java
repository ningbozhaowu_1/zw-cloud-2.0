package com.zhaowu.cloud.order.service;

import com.zhaowu.cloud.order.entity.po.Order;
import com.zhaowu.cloud.order.entity.po.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单子表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderItemService{

    void add(OrderItem orderItem);

    List<OrderItem> selectByOrderId(String orderId);
}
