package com.zhaowu.cloud.order.entity.po;

import com.baomidou.mybatisplus.annotation.*;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 * 订单主表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@Accessors(chain = true)
@TableName("t_order")
public class Order extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 应用编号
     */
    @TableField(value = "application_party",fill = FieldFill.INSERT)
    private String applicationParty;

    /**
     * 参与者渠道编号
     */
    @TableField(value = "party_channel_id",fill = FieldFill.INSERT)
    private String partyChannelId;
    /**
     * 用户编号
     */
    @TableField(value = "user_id",fill = FieldFill.INSERT)
    private String userId;
    /**
     * 店铺编号
     */
    @TableField(value = "shop_id")
    private String shopId;
    /**
     * 店铺名称
     */
    @TableField(value = "shop_name")
    private String shopName;

    /**
     * 总金额
     */
    private BigDecimal total;

    /**
     * 优惠金额
     */
    private BigDecimal discount;
    /**
     * 实际支付总金额
     */
    private BigDecimal pay;
    /**
     * 退款金额
     */
    private BigDecimal refund;
    /**
     * 邮费
     */
    private BigDecimal freight;
    /**
     * 状态 0-待支付 1-已支付 2-已发货 3-已完成 4-已关闭 5-退款中 6-已下定
     */
    private String status;

    /**
     * 自定义数据
     */
    private String attach;

    /**
     * 发货时间
     */
    @TableField(value = "send_time")
    private Date sendTime;

    /**
     * 完成时间
     */
    @TableField(value = "end_time")
    private Date endTime;

    /**
     * 关闭时间
     */
    @TableField(value = "close_time")
    private Date closeTime;

    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    private Date payTime;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 内部流水号
     */
    @TableField(value = "transaction_id")
    private String transactionId;

    private String deliveryType;

    private String cancelReason;

    private String routingKey;
}
