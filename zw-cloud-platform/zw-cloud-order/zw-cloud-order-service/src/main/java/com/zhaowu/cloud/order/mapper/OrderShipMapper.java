package com.zhaowu.cloud.order.mapper;

import com.zhaowu.cloud.order.entity.po.OrderShip;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单物流表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderShipMapper extends BaseMapper<OrderShip> {

}
