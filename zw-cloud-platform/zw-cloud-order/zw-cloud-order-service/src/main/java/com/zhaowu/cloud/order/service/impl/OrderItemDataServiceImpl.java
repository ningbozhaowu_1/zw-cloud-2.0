package com.zhaowu.cloud.order.service.impl;

import com.zhaowu.cloud.order.entity.po.OrderItemData;
import com.zhaowu.cloud.order.mapper.OrderItemDataMapper;
import com.zhaowu.cloud.order.service.OrderItemDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单子表业务数据 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class OrderItemDataServiceImpl extends ServiceImpl<OrderItemDataMapper,OrderItemData> implements OrderItemDataService {

}
