package com.zhaowu.cloud.order.service;

import com.zhaowu.cloud.order.entity.vo.WxPayMpOrderResult;

import java.math.BigDecimal;

public interface PayService {

    byte[] wxScanCode(String transactionId,String ip, BigDecimal totalFee, String openId, String attach, String body, String notifyUrl);

    byte[] alipayScanCode(String transactionId, BigDecimal totalFee, String body, String notifyUrl);

    WxPayMpOrderResult wxMiniAppPay(String transactionId, String ip, BigDecimal totalFee, String openId, String attach, String body, String notifyUrl);
}
