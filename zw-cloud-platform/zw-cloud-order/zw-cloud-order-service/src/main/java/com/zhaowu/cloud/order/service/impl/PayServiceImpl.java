package com.zhaowu.cloud.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.github.binarywang.utils.qrcode.QrcodeUtils;
import com.jpay.alipay.AliPayApi;
import com.jpay.ext.kit.PaymentKit;
import com.jpay.ext.kit.StrKit;
import com.jpay.util.StringUtils;
import com.jpay.weixin.api.WxPayApi;
import com.jpay.weixin.api.WxPayApiConfig;
import com.jpay.weixin.api.WxPayApiConfigKit;
import com.zhaowu.cloud.order.entity.vo.WxPayMpOrderResult;
import com.zhaowu.cloud.order.service.PayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class PayServiceImpl implements PayService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public byte[] wxScanCode(String transactionId, String ip, BigDecimal totalFee, String openId, String attach, String body, String notifyUrl) {

//        if (StrKit.isBlank(openId)) {
//            throw new RuntimeException("openId不能为空");
//        }
        if (totalFee==null) {
            throw new RuntimeException("支付金额不能为空");
        }

        WxPayApiConfigKit.getWxPayApiConfig()
                .setAttach(attach)
                .setBody(body)
                .setSpbillCreateIp(ip)
                .setTotalFee(totalFee.toString())
                .setTradeType(WxPayApi.TradeType.NATIVE)
                .setNotifyUrl(notifyUrl)
                .setOutTradeNo(transactionId);

//        if(WxPayApiConfig.PayModel.BUSINESSMODEL.equals(WxPayApiConfigKit.getWxPayApiConfig().getPayModel())){
//            WxPayApiConfigKit.getWxPayApiConfig().setOpenId(openId);
//        }else if(WxPayApiConfig.PayModel.SERVICEMODE.equals(WxPayApiConfigKit.getWxPayApiConfig().getPayModel())){
//            WxPayApiConfigKit.getWxPayApiConfig().setSubOpenId(openId);
//        }

        Map<String, String> params = WxPayApiConfigKit.getWxPayApiConfig().build();
        log.info("最新支付配置信息："+WxPayApiConfigKit.getWxPayApiConfig());
        log.info("最新小程序支付的请求参数:" + params);

        String xmlResult = WxPayApi.pushOrder(false,params);

        log.info(xmlResult);
        Map<String, String> resultMap = PaymentKit.xmlToMap(xmlResult);

        String return_code = resultMap.get("return_code");
        String return_msg = resultMap.get("return_msg");
        if (!PaymentKit.codeIsOK(return_code)) {
            throw new RuntimeException(return_msg);
        }
        String result_code = resultMap.get("result_code");
        if (!PaymentKit.codeIsOK(result_code)) {
            throw new RuntimeException(return_msg);
        }
        String qrCodeUrl = resultMap.get("code_url");
        byte[] qr= QrcodeUtils.createQrcode(qrCodeUrl,500,null);
        return qr;
    }

    @Override
    public byte[] alipayScanCode(String transactionId, BigDecimal totalFee, String body, String notifyUrl) {

        AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
		model.setSubject(body);
		model.setTotalAmount(totalFee.toString());
//		model.setStoreId(storeId);
		model.setTimeoutExpress("5m");
		model.setOutTradeNo(transactionId);
		try {
			String resultStr = AliPayApi.tradePrecreatePay(model, notifyUrl);
			JSONObject jsonObject = JSONObject.parseObject(resultStr);
            byte[] qr= QrcodeUtils.createQrcode(jsonObject.getJSONObject("alipay_trade_precreate_response").getString("qr_code"),500,null);
			return qr;
		} catch (Exception e) {
		    e.printStackTrace();
			log.error(e.getMessage());
		}
		return null;
    }

    @Override
    public WxPayMpOrderResult wxMiniAppPay(String transactionId, String ip, BigDecimal totalFee, String openId, String attach, String body, String notifyUrl) {

        if (StrKit.isBlank(openId)) {
            throw new RuntimeException("openId不能为空");
        }
        if (totalFee==null) {
            throw new RuntimeException("支付金额不能为空");
        }

        WxPayApiConfigKit.getWxPayApiConfig()
                .setAttach(attach)
                .setBody(body)
                .setSpbillCreateIp(ip)
                .setTotalFee(totalFee.toString())
//                .setSubOpenId(openId)
                .setTradeType(WxPayApi.TradeType.JSAPI)
                .setNotifyUrl(notifyUrl)
                .setOutTradeNo(transactionId);

        if(WxPayApiConfig.PayModel.BUSINESSMODEL.equals(WxPayApiConfigKit.getWxPayApiConfig().getPayModel())){
            WxPayApiConfigKit.getWxPayApiConfig().setOpenId(openId);
        }else if(WxPayApiConfig.PayModel.SERVICEMODE.equals(WxPayApiConfigKit.getWxPayApiConfig().getPayModel())){
            WxPayApiConfigKit.getWxPayApiConfig().setSubOpenId(openId);
        }

        Map<String, String> params = WxPayApiConfigKit.getWxPayApiConfig().build();

        log.info("最新小程序支付的请求参数:" + params);

        String xmlResult =  WxPayApi.pushOrder(false,params);

        log.info(xmlResult);
        Map<String, String> resultMap = PaymentKit.xmlToMap(xmlResult);

        String return_code = resultMap.get("return_code");
        String return_msg = resultMap.get("return_msg");
        if (!PaymentKit.codeIsOK(return_code)) {
            throw new RuntimeException(return_msg);
        }
        String result_code = resultMap.get("result_code");
        if (!PaymentKit.codeIsOK(result_code)) {
            throw new RuntimeException(return_msg);
        }
        // 以下字段在return_code 和result_code都为SUCCESS的时候有返回
        String prepay_id = resultMap.get("prepay_id");
        Map<String, String> packageParams = new HashMap<String, String>();

        if(WxPayApiConfig.PayModel.BUSINESSMODEL.equals(WxPayApiConfigKit.getWxPayApiConfig().getPayModel())){
            packageParams.put("appId", WxPayApiConfigKit.getWxPayApiConfig().getAppId());
        }else if(WxPayApiConfig.PayModel.SERVICEMODE.equals(WxPayApiConfigKit.getWxPayApiConfig().getPayModel())){
            packageParams.put("appId", WxPayApiConfigKit.getWxPayApiConfig().getSubAppId());
//            packageParams.put("partnerid", WxPayApiConfigKit.getWxPayApiConfig().getMchId());
        }

        packageParams.put("timeStamp", System.currentTimeMillis() / 1000 + "");
        packageParams.put("nonceStr", System.currentTimeMillis() + "");
        packageParams.put("package", "prepay_id="+prepay_id);
        packageParams.put("signType", "MD5");
        String packageSign = PaymentKit.createSign(packageParams, WxPayApiConfigKit.getWxPayApiConfig().getPaternerKey());

        WxPayMpOrderResult wxPayMpOrderResult = WxPayMpOrderResult.builder().paySign(packageSign)
                .signType("MD5")
                .appId(packageParams.get("appId"))
                .packageValue("prepay_id="+prepay_id)
                .timeStamp(packageParams.get("timeStamp"))
                .nonceStr(packageParams.get("nonceStr"))
                .build();
        log.info("最新返回小程序支付的参数:"+wxPayMpOrderResult);
        return wxPayMpOrderResult;
    }
}
