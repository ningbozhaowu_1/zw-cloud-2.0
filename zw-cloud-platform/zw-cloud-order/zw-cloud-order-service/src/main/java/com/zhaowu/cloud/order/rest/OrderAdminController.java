package com.zhaowu.cloud.order.rest;


import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.common.web.audit.EnableAudit;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.order.entity.form.*;
import com.zhaowu.cloud.order.entity.po.Order;
import com.zhaowu.cloud.order.entity.po.OrderItem;
import com.zhaowu.cloud.order.entity.vo.OrderItemVO;
import com.zhaowu.cloud.order.entity.vo.OrderVOExt;
import com.zhaowu.cloud.order.service.*;
import com.zhaowu.cloud.uc.client.UserClient;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 订单主表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/order/admin/v1")
@Api(tags = "订单主表 接口")
@EnableAudit
public class OrderAdminController extends BaseController {

        private Logger log = LoggerFactory.getLogger(this.getClass());

        @Autowired
        private OrderService orderService;

        @Autowired
        private OrderItemService orderItemService;

        @Autowired
        private OrderPayService orderPayService;

        @Autowired
        private OrderShipService orderShipService;

        @Autowired
        private UserClient userClient;

        @PostMapping(value = "/getOrder")
        @ApiIgnore
        public Result<OrderVOExt> getOrder(@Valid @RequestBody OrderQueryForm orderQueryForm){

            OrderVOExt orderVO = new OrderVOExt();
            Order order = orderService.selectById(orderQueryForm.getOrderId());
            BeanUtils.copyProperties(order, orderVO);

            List<OrderItem> orderItemList = orderItemService.selectByOrderId(orderQueryForm.getOrderId());

            List<OrderItemVO> orderItemVOList = JSON.parseArray(JSON.toJSONString(orderItemList), OrderItemVO.class);

            orderVO.setOrderItemVOList(orderItemVOList);
            return this.success(orderVO);
        }

        @PostMapping(value = "/deleveryOrder")
        @ApiIgnore
        public Result deleveryOrder(@Valid @RequestBody SendOrderForm sendOrderForm){

            return this.success();
        }

        @PostMapping(value = "/cancelOrder")
        @ApiIgnore
        public Result cancelOrder(@Valid @RequestBody OrderQueryForm orderQueryForm){

            return this.success();
        }

        @PostMapping(value = "/orderReturn")
        @ApiIgnore
        public Result orderReturn(@Valid @RequestBody OrderQueryForm orderQueryForm){

            return this.success();
        }

        @PostMapping(value = "/orderAfterSalesList")
        @ApiIgnore
        public Result orderAfterSalesList(@Valid @RequestBody OrderQueryForm orderQueryForm){

            return this.success();
        }
}

