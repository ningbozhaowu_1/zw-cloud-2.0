package com.zhaowu.cloud.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.order.constant.OrderConstant;
import com.zhaowu.cloud.order.entity.param.CancelOrderParam;
import com.zhaowu.cloud.order.entity.param.OrderQueryParam;
import com.zhaowu.cloud.order.entity.po.Order;
import com.zhaowu.cloud.order.entity.po.OrderPay;
import com.zhaowu.cloud.order.mapper.OrderMapper;
import com.zhaowu.cloud.order.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 订单主表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper,Order> implements OrderService {

    @Override
    public Order add(Order order) {

        this.save(order);
        return order;
    }

    @Override
    public Order modify(Order order) {

        this.updateById(order);
        return order;
    }

//    @Override
//    public List<Order> selectByOrderPayId(Long orderPayId) {
//
//        QueryWrapper<Order> queryWrapper = new QueryWrapper<Order>();
//        queryWrapper.eq("order_pay_id", orderPayId);
//        return this.list(queryWrapper);
//    }

    @Override
    public Order selectById(String id) {
        return this.getById(id);
    }

    @Override
    public List<Order> selectByTransactionId(String transactionId) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<Order>();
        queryWrapper.eq("transaction_id", transactionId);
        return this.list(queryWrapper);
    }

    @Override
    public IPage<Order> pageList(Page<Order> page, OrderQueryParam OrderQueryParam) {

        QueryWrapper<Order> queryWrapper = OrderQueryParam.build();
        queryWrapper.select("id");
        queryWrapper.eq("application_party", OrderQueryParam.getApplicationParty());
        queryWrapper.eq("user_id", OrderQueryParam.getUserId());
        if(!StringUtils.isEmpty(OrderQueryParam.getStatus())){
            queryWrapper.eq("status", OrderQueryParam.getStatus());
        }
        queryWrapper.orderByDesc("create_time");
        return this.page(page, queryWrapper);
    }

    @Override
    public void cancelOrder(CancelOrderParam cancelOrderParam) {

        QueryWrapper<Order> queryWrapper = cancelOrderParam.build();
        queryWrapper.select("id");
        queryWrapper.eq("application_party", cancelOrderParam.getApplicationParty());
        queryWrapper.eq("user_id", cancelOrderParam.getUserId());
        queryWrapper.eq("status", OrderConstant.ORDER_PAY_WAIT_FOR_PAY);
        Order orderExsit = this.getOne(queryWrapper);
        if(orderExsit==null){
            Assert.isTrue(false,"订单不存在");
        }
        orderExsit.setStatus(OrderConstant.ORDER_CLOSED);
        orderExsit.setCancelReason(cancelOrderParam.getReason());
        this.updateById(orderExsit);
    }
}
