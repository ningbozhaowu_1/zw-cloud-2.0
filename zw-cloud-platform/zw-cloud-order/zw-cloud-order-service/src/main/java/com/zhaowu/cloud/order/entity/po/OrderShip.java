package com.zhaowu.cloud.order.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * <p>
 * 订单物流表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderShip extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    @TableField(value = "user_id",fill = FieldFill.INSERT)
    private String userId;

    /**
     * 订单编号
     */
    @TableField(value = "order_id")
    private String orderId;

    /**
     * 物流编号
     */
    private String shipCode;

    /**
     * 提货点编号
     */
    private String pickUpId;

    /**
     * 收货人姓名
     */
    private String shipName;

    /**
     * 收货人手机号
     */
    private String shipPhone;

    /**
     * 收货人省份
     */
    private String shipProvince;

    /**
     * 收货人城市
     */
    private String shipCity;

    /**
     * 收货人街道
     */
    private String shipDistrict;

    /**
     * 收货人详细地址
     */
    private String shipAddr;

    /**
     * 期望送货时间
     */
    private Date requireTime;
}
