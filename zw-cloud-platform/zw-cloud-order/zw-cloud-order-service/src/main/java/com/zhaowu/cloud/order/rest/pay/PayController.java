//package com.zhaowu.cloud.order.controller.pay;
//
//import com.alicp.jetcache.anno.CachePenetrationProtect;
//import com.alicp.jetcache.anno.CacheType;
//import com.alicp.jetcache.anno.CreateCache;
//import com.jpay.alipay.AliPayApiConfig;
//import com.jpay.ext.kit.IpKit;
//import com.jpay.ext.kit.PaymentKit;
//import com.jpay.ext.kit.StrKit;
//import com.jpay.weixin.api.WxPayApi;
//import com.jpay.weixin.api.WxPayApiConfig;
//import com.zhaowu.cloud.framework.base.constant.CommonConstant;
//import com.zhaowu.cloud.framework.base.controller.BaseController;
//import com.zhaowu.cloud.framework.base.exception.ServiceException;
//import com.zhaowu.cloud.framework.base.protocol.Result;
//import com.zhaowu.cloud.framework.pay.base.alipay.AliPayBaseController;
//import com.zhaowu.cloud.framework.pay.base.wx.WxPayBaseController;
//import com.zhaowu.cloud.framework.pay.entity.AliPayBean;
//import com.zhaowu.cloud.framework.pay.entity.WxPayBean;
//import com.zhaowu.cloud.framework.util.UserContextHolder;
//import com.zhaowu.cloud.uc.client.UserClient;
//import com.zhaowu.cloud.uc.entity.form.PartyChannelQueryForm;
//import com.zhaowu.cloud.uc.entity.form.PartyPayQueryForm;
//import com.zhaowu.cloud.uc.entity.vo.PartyChannelVO;
//import com.zhaowu.cloud.uc.entity.vo.PartyPayVO;
//import org.apache.catalina.User;
//import org.apache.commons.collections4.CollectionUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/pay/v1")
//public class PayController extends BaseController{
//
//	private Logger log = LoggerFactory.getLogger(this.getClass());
//
//	@Autowired
//	private UserClient userClient;
//
//	/**
//	 * 企业付款到零钱
//	 */
//	@RequestMapping(value = "/wx/transfers",method={RequestMethod.POST,RequestMethod.GET})
//	public Result transfers(HttpServletRequest request, @RequestParam("merchantId") String merchantId, @RequestParam("openId") String openId) {
//
////		openId = "oRMVFv8zhSH--EhJiu3z9G3kNX-o";
//
//		PartyChannelQueryForm applicationChannelQueryForm = new PartyChannelQueryForm();
//		applicationChannelQueryForm.setPartyChannelId(UserContextHolder.getInstance().getChannelId());
//		Result<PartyChannelVO> resultPC = userClient.getPartyChannel(applicationChannelQueryForm);
//		PartyChannelVO applicationChannelVO = resultPC.getData();
//
//		PartyPayQueryForm applicationPayQueryForm = new PartyPayQueryForm();
//		applicationPayQueryForm.setMerchantId(merchantId);
//		applicationPayQueryForm.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
//		Result<PartyPayVO> resultAP = userClient.getPartyPay(applicationPayQueryForm);
//
//		if(resultAP.getData()==null){
//			throw new ServiceException("商户不存在");
//		}
//
//		PartyPayVO applicationPayVO = resultAP.getData();
//
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("mch_appid", applicationChannelVO.getAuthorizerAppId());
//		params.put("mchid", applicationPayVO.getMerchantId());
//		String nonceStr = String.valueOf(System.currentTimeMillis());
//		params.put("nonce_str", nonceStr);
//		String partnerTradeNo = String.valueOf(System.currentTimeMillis());
//		params.put("partner_trade_no", partnerTradeNo);
//		params.put("openid", openId);
//		params.put("check_name", "NO_CHECK");
//		params.put("amount", "100");
//		params.put("desc", "IJPay提现测试-By Javen");
//		String ip = IpKit.getRealIp(request);
//		if (StrKit.isBlank(ip)) {
//			ip = "127.0.0.1";
//		}
//		params.put("spbill_create_ip", ip);
//
//		params.put("sign", PaymentKit.createSign(params, applicationPayVO.getMerchantKey()));
//		System.out.println("certPath>"+applicationPayVO.getMerchantCertKey());
//		// 提现
//		String transfers = WxPayApi.transfers(params, applicationPayVO.getMerchantCertKey(), applicationPayVO.getMerchantId());
//
//		log.info("提现结果:" + transfers);
//
//		Map<String, String> map = PaymentKit.xmlToMap(transfers);
//		String return_code = map.get("return_code");
//		String result_code = null;
//		if (("SUCCESS").equals(return_code)) {
//			result_code = map.get("result_code");
//			if (("SUCCESS").equals(result_code)) {
//				return this.success(map.get("payment_no"));
//			} else {
//				//提现失败
//				throw new ServiceException(map.get("err_code"), map.get("err_code_des"));
//			}
//		}
//		return this.success(map.get("payment_no"));
//	}
//}
