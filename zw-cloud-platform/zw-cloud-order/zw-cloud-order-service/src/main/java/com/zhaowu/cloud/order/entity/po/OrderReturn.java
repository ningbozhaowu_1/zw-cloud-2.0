package com.zhaowu.cloud.order.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * <p>
 * 订单退款表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderReturn extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    @TableField(value = "user_id",fill = FieldFill.INSERT)
    private String userId;

    /**
     * 订单编号
     */
    @TableField(value = "order_id")
    private String orderId;

    /**
     * 支付编号 第三方支付退款必输
     */
    private String thirdPayId;

    /**
     * 退款金额
     */
    private BigDecimal amount;

    /**
     * 退款类型 1-全额 2-部分
     */
    private String returnType;

    /**
     * 退款方式 1-余额 2-积分 3-代金卡 4-第三方支付
     */
    private String returnWay;

    /**
     * 状态 0-退款中 1-退款成功 -1-退款失败
     */
    private String status;

    /**
     * 退款原因
     */
    private String refundDesc;

    /**
     * 第三方退款单号
     */
    private String outRefundId;

    /**
     * 错误编码
     */
    private String errorCode;

    /**
     * 错误描述
     */
    private String errorDesc;
}
