package com.zhaowu.cloud.order.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.order.entity.po.Order;
import lombok.Data;

@Data
public class SendOrderParam extends BaseParam<Order> {

    private String id;

    private String reason;

    private String remark;
}
