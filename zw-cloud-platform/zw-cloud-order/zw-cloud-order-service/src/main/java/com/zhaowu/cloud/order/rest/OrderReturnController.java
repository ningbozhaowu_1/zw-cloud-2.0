package com.zhaowu.cloud.order.rest;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.order.service.OrderReturnService;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

/**
 * <p>
 * 订单退款表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/order/orderReturn")
@Api(tags = "订单退款表 接口")
public class OrderReturnController extends BaseController {

        @Autowired
        private OrderReturnService orderReturnService;


}

