package com.zhaowu.cloud.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.order.entity.param.OrderQueryParam;
import com.zhaowu.cloud.order.entity.po.Order;
import com.zhaowu.cloud.order.entity.po.OrderShip;
import com.zhaowu.cloud.order.mapper.OrderShipMapper;
import com.zhaowu.cloud.order.service.OrderShipService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单物流表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class OrderShipServiceImpl extends ServiceImpl<OrderShipMapper,OrderShip> implements OrderShipService {

    @Override
    public OrderShip selectByOrderId(String orderId) {
        QueryWrapper<OrderShip> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId);
        return this.getOne(queryWrapper);
    }

    @Override
    public void add(OrderShip orderShip) {
        this.save(orderShip);
    }
}
