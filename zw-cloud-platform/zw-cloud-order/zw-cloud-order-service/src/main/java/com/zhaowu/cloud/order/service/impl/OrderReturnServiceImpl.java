package com.zhaowu.cloud.order.service.impl;

import com.zhaowu.cloud.order.entity.po.OrderReturn;
import com.zhaowu.cloud.order.mapper.OrderReturnMapper;
import com.zhaowu.cloud.order.service.OrderReturnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单退款表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class OrderReturnServiceImpl extends ServiceImpl<OrderReturnMapper,OrderReturn> implements OrderReturnService {

}
