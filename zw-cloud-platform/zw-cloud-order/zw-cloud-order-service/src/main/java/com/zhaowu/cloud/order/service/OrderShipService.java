package com.zhaowu.cloud.order.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.order.entity.po.OrderShip;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单物流表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderShipService{

    @Cached(name="orderShipCache:", key="#orderId", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    OrderShip selectByOrderId(String orderId);

    void add(OrderShip orderShip);
}
