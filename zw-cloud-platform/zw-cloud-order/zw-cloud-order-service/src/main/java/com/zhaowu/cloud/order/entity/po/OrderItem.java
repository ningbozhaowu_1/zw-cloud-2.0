package com.zhaowu.cloud.order.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * <p>
 * 订单子表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderItem extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 订单编号
     */
    @TableField(value = "order_id")
    private String orderId;

    /**
     * 用户编号
     */
    @TableField(value = "user_id",fill = FieldFill.INSERT)
    private String userId;

    /**
     * 商品编号
     */
    @TableField(value = "goods_id")
    private String goodsId;

    @TableField(value = "goods_sku_id")
    private String goodsSkuId;
    /**
     * 总金额
     */
    private BigDecimal total;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 实际价格 秒杀价折扣价会员价
     */
    @TableField(value = "actual_price")
    private BigDecimal actualPrice;

    /**
     * 商品图片
     */
    @TableField(value = "goods_image")
    private String goodsImage;

    /**
     * 商品名称
     */
    @TableField(value = "goods_name")
    private String goodsName;

    /**
     * 商品规格
     */
    @TableField(value = "goods_skus")
    private String goodsSkus;
}
