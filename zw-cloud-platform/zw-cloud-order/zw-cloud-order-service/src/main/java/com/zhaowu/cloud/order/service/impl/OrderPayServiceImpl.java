package com.zhaowu.cloud.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.order.entity.po.Order;
import com.zhaowu.cloud.order.entity.po.OrderPay;
import com.zhaowu.cloud.order.mapper.OrderPayMapper;
import com.zhaowu.cloud.order.service.OrderPayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 订单支付表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class OrderPayServiceImpl extends ServiceImpl<OrderPayMapper,OrderPay> implements OrderPayService {

    @Override
    public OrderPay add(OrderPay orderPay) {

        this.save(orderPay);
        return orderPay;
    }

    @Override
    public void modify(OrderPay orderPay) {
        this.updateById(orderPay);
    }

    @Override
    public OrderPay selectById(String id) {
        return this.getById(id);
    }

    @Override
    public List<OrderPay> selectByOrderId(String orderId, String[] payType) {

        QueryWrapper<OrderPay> queryWrapper = new QueryWrapper<OrderPay>();
        queryWrapper.eq("order_id", orderId);
        if(payType!=null && payType.length>0){
            queryWrapper.in("pay_type", payType);
        }
        return this.list(queryWrapper);
    }
}
