package com.zhaowu.cloud.order.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CacheUpdate;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.order.entity.param.CancelOrderParam;
import com.zhaowu.cloud.order.entity.param.OrderQueryParam;
import com.zhaowu.cloud.order.entity.po.Order;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单主表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderService{

    Order add(Order order);

    @CacheUpdate(name="orderCache:",key = "#order.id", value="#result")
    Order modify(Order order);

//    List<Order> selectByOrderPayId(Long orderPayId);

    @Cached(name="orderCache:", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Order selectById(String id);

    @Cached(name="orderListCache:", key="#transactionId", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    List<Order> selectByTransactionId(String transactionId);

    IPage<Order> pageList(Page<Order> page, OrderQueryParam OrderQueryParam);

    void cancelOrder(CancelOrderParam cancelOrderParam);
}
