package com.zhaowu.cloud.order.service;

import com.zhaowu.cloud.order.entity.po.OrderPay;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单支付表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderPayService{

    OrderPay add(OrderPay orderPay);

    void modify(OrderPay orderPay);

    OrderPay selectById(String id);

    List<OrderPay> selectByOrderId(String orderId, String[] payType);
}

