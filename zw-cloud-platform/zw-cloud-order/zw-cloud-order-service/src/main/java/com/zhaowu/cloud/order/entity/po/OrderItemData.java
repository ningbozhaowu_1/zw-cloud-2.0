package com.zhaowu.cloud.order.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 订单子表业务数据
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderItemData extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    @TableField(value = "user_id",fill = FieldFill.INSERT)
    private String userId;

    /**
     * 订单编号
     */
    @TableField(value = "order_id")
    private String orderId;

    /**
     * 订单项编号
     */
    private String orderItemId;

    /**
     * 数据json
     */
    private String data;

}
