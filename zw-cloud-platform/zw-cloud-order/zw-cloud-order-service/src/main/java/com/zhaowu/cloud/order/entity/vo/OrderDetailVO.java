package com.zhaowu.cloud.order.entity.vo;

import com.zhaowu.cloud.order.entity.po.*;
import com.zhaowu.cloud.uc.entity.vo.PartyPickUpVO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;


/**
 * <p>
 * 订单主表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@Accessors(chain = true)
public class OrderDetailVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Order order;

    private List<OrderItem> orderItemList;

    private OrderShip orderShip;

    private PartyPickUpVO partyPickUpVO;

    private List<OrderPay> orderPayList;

}
