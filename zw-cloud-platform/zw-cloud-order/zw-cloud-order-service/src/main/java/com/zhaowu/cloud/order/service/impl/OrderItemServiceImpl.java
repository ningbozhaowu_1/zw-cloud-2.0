package com.zhaowu.cloud.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.order.entity.po.OrderItem;
import com.zhaowu.cloud.order.mapper.OrderItemMapper;
import com.zhaowu.cloud.order.service.OrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 订单子表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper,OrderItem> implements OrderItemService {

    @Override
    public void add(OrderItem orderItem) {
        this.save(orderItem);
    }

    @Override
    public List<OrderItem> selectByOrderId(String orderId) {
        QueryWrapper<OrderItem> queryWrapper = new QueryWrapper<OrderItem>();
        queryWrapper.eq("order_id", orderId);
        return this.list(queryWrapper);
    }
}
