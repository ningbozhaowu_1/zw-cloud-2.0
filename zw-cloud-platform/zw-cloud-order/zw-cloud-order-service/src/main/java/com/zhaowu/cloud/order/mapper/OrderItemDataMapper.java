package com.zhaowu.cloud.order.mapper;

import com.zhaowu.cloud.order.entity.po.OrderItemData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单子表业务数据 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface OrderItemDataMapper extends BaseMapper<OrderItemData> {

}
