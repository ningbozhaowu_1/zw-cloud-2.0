package com.zhaowu.cloud.order.constant;

/**
 *
 */
public class OrderConstant {

    /**
     * 订单支付状态 1-已支付，0-未支付
     */
    public static final String ORDER_PAY_WAIT_FOR_PAY = "WFP";
    public static final String ORDER_PAY_PAID = "PAID";

    /**
     * 0-待支付 1-已支付 2-已发货 3-已完成 4-已关闭 5-退款中 6-已下定
     */
    public static final String ORDER_WAIT_FOR_PAY = "WFP";
    public static final String ORDER_PAID = "PAID";
    public static final String ORDER_SEND = "SEND";
    public static final String ORDER_FINISH = "FINISH";
    public static final String ORDER_CLOSED = "CLOSED";
    public static final String ORDER_RETURN = "RETURN";
    public static final String ORDER_DEPOSIT = "DEPOSIT";
}
