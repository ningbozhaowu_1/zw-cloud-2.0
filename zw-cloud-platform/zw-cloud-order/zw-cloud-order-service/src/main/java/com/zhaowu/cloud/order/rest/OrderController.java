package com.zhaowu.cloud.order.rest;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.AutoReleaseLock;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jpay.alipay.AliPayApiConfig;
import com.jpay.ext.kit.IpKit;
import com.jpay.ext.kit.StrKit;
import com.jpay.weixin.api.WxPayApiConfig;
import com.zhaowu.cloud.common.web.audit.EnableAudit;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import com.zhaowu.cloud.framework.pay.base.alipay.AliPayBaseController;
import com.zhaowu.cloud.framework.pay.base.wx.WxPayBaseController;
import com.zhaowu.cloud.framework.pay.entity.AliPayBean;
import com.zhaowu.cloud.framework.pay.entity.WxPayBean;
import com.zhaowu.cloud.framework.util.DateUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.order.constant.OrderConstant;
import com.zhaowu.cloud.order.entity.form.*;
import com.zhaowu.cloud.order.entity.param.CancelOrderParam;
import com.zhaowu.cloud.order.entity.param.OrderQueryParam;
import com.zhaowu.cloud.order.entity.po.*;
import com.zhaowu.cloud.order.entity.vo.*;
import com.zhaowu.cloud.order.service.*;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.PartyChannelQueryForm;
import com.zhaowu.cloud.uc.entity.form.PartyPayQueryForm;
import com.zhaowu.cloud.uc.entity.vo.PartyChannelVO;
import com.zhaowu.cloud.uc.entity.vo.PartyPayVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * <p>
 * 订单主表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/order/v1")
@Api(tags = "订单主表 接口")
@EnableAudit
public class OrderController extends BaseController implements WxPayBaseController, AliPayBaseController {

        private Logger log = LoggerFactory.getLogger(this.getClass());

        @Autowired
        private WxPayBean wxPayBean;

        @Autowired
        private AliPayBean aliPayBean;

        @Autowired
        private OrderService orderService;

        @Autowired
        private OrderItemService orderItemService;

        @Autowired
        private OrderPayService orderPayService;

        @Autowired
        private OrderShipService orderShipService;

        @Autowired
        private PayService payService;

        @Autowired
        private UserClient userClient;

//        private String notify_url;

        @Autowired
        private BusSender busSender;

        @CreateCache(name = "payLockCache::", expire = 3600, cacheType = CacheType.REMOTE)
        private com.alicp.jetcache.Cache<String, Order> payLockCache;

        @CreateCache(name = "wxPayApiConfigCache::", expire = 3600, cacheType = CacheType.BOTH)
        @CachePenetrationProtect
        private com.alicp.jetcache.Cache<String, WxPayApiConfig> applicationPayCache;

        @Override
        public WxPayApiConfig getWxPayApiConfig() {
            String applicationParty = UserContextHolder.getInstance().getApplicationParty();
            String channelId = UserContextHolder.getInstance().getChannelId();
            String key = applicationParty.concat(":").concat(channelId);
            WxPayApiConfig wxPayApiConfig = applicationPayCache.get(key);
            if(wxPayApiConfig==null){
                PartyPayQueryForm applicationPayQueryForm = new PartyPayQueryForm();
                applicationPayQueryForm.setApplicationChannelId(channelId);
                applicationPayQueryForm.setApplicationParty(applicationParty);
                applicationPayQueryForm.setMercahtType("wechat");
                List<PartyPayVO> applicationPayVOList = userClient.getPartyPay(applicationPayQueryForm).getData();
                PartyPayVO applicationPayVO = applicationPayVOList.get(0);

                if(CommonConstant.MerchantType.WECHAT_BUSINESS.equalsIgnoreCase(applicationPayVO.getMerchantType())){//直连商户

                    if(StringUtils.isEmpty(applicationPayVO.getAppId())){
                        throw new ServiceException("商家未设置appid");
                    }

                    wxPayApiConfig = WxPayApiConfig.New()
                            .setAppId(applicationPayVO.getAppId())
                            .setMchId(applicationPayVO.getMerchantId())
                            .setPaternerKey(applicationPayVO.getMerchantKey())
                            .setPayModel(WxPayApiConfig.PayModel.BUSINESSMODEL);
                }else if(CommonConstant.MerchantType.WECHAT_SERVICE.equalsIgnoreCase(applicationPayVO.getMerchantType())){//平台特约商户
                    wxPayApiConfig =  WxPayApiConfig.New()
                            .setAppId(wxPayBean.getAppId())
                            .setSubAppId(applicationPayVO.getAppId())
                            .setSubMchId(applicationPayVO.getMerchantId())
                            .setMchId(wxPayBean.getMchId())
                            .setPaternerKey(wxPayBean.getPartnerKey())
                            .setPayModel(WxPayApiConfig.PayModel.SERVICEMODE);
                }
                applicationPayCache.put(key, wxPayApiConfig);
            }

            return wxPayApiConfig;
        }

        @Override
        public AliPayApiConfig getAliPayApiConfig() {

            return AliPayApiConfig.New()
                    .setAppId(aliPayBean.getAppId())
                    .setAlipayPublicKey(aliPayBean.getPublicKey())
                    .setCharset("UTF-8")
                    .setPrivateKey(aliPayBean.getPrivateKey())
                    .setServiceUrl(aliPayBean.getServerUrl())
                    .setSignType("RSA2")
                    .build();
        }

        @ApiIgnore
        @PostMapping(value = "/createOrder")
        @ApiOperation("下单 支持购物车模式")
        @Transactional
        public Result createOrder(HttpServletRequest request, @Valid @RequestBody CreateOrderForm createOrderForm) {

            BigDecimal ratio = BigDecimal.ZERO;

            String transactionId = com.jpay.util.StringUtils.getOutTradeNo();

            String payModel = "";

            PartyChannelQueryForm partyChannelQueryForm = new PartyChannelQueryForm();
            partyChannelQueryForm.setPartyChannelId(UserContextHolder.getInstance().getChannelId());
            partyChannelQueryForm.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            PartyChannelVO partyChannelVO = this.userClient.getPartyChannel(partyChannelQueryForm).getData();
            if(CommonConstant.ChannelType.PWEB.equals(partyChannelVO.getType())){
                payModel = CommonConstant.PayModel.QR;
            }else if(CommonConstant.ChannelType.WECHATMA.equals(partyChannelVO.getType())){
                payModel = CommonConstant.PayModel.MA;
            }

            boolean free = (createOrderForm.getTotal().compareTo(BigDecimal.ZERO)==0)?true:false;
            List<Order> orderList = new ArrayList<>();
            for(OrderForm orderForm : createOrderForm.getOrderFormList()){
                Order order = new Order();
                BeanUtils.copyProperties(orderForm, order);
                if(free){
                    order.setStatus(OrderConstant.ORDER_PAID);
                }else{
                    order.setStatus(OrderConstant.ORDER_WAIT_FOR_PAY);
                }
                order.setRefund(BigDecimal.ZERO);
                order.setPay(order.getTotal());
                order.setDiscount(BigDecimal.ZERO);
                order.setAttach(createOrderForm.getAttach());
                order.setTransactionId(transactionId);//订单流水号
                order.setDeliveryType(orderForm.getDeliveryType());
                order.setRoutingKey(createOrderForm.getRoutingKey());
                order = orderService.add(order);
                orderList.add(order);
                if(!free){
                    ratio = order.getTotal().divide(createOrderForm.getTotal(), 4, RoundingMode.UP);
                }
                if(createOrderForm.getOrderPayFormList()!=null){
                    for(OrderPayForm orderPayForm : createOrderForm.getOrderPayFormList()){
                        OrderPay orderPay = new OrderPay();
                        orderPay.setOrderId(order.getId());
                        orderPay.setRefund(BigDecimal.ZERO);
                        orderPay.setStatus(OrderConstant.ORDER_PAY_WAIT_FOR_PAY);
                        orderPay.setPay(orderPayForm.getPay().multiply(ratio));
                        orderPay.setPayModel(payModel);
                        orderPay.setPayType(orderPayForm.getPayType());
                        orderPayService.add(orderPay);
                    }
                }

                for(OrderItemForm orderItemForm : orderForm.getOrderItemFormList()){
                    OrderItem orderItem = new OrderItem();
                    BeanUtils.copyProperties(orderItemForm, orderItem);
                    orderItem.setOrderId(order.getId());
                    orderItem.setGoodsSkuId(orderItemForm.getGoodsSkuId());
                    orderItemService.add(orderItem);
                }

                //收货地址
                if(orderForm.getOrderShipForm()!=null){
                    OrderShip orderShip = new OrderShip();
                    BeanUtils.copyProperties(orderForm.getOrderShipForm(), orderShip);
                    orderShip.setOrderId(order.getId());
                    orderShipService.add(orderShip);
                }
            }

            String goodsName = createOrderForm.getOrderFormList().get(0).getOrderItemFormList().get(0).getGoodsName();
            String attach = "GOODS";

            if(!free && createOrderForm.getOrderPayFormList()!=null){
                for(OrderPayForm orderPayForm : createOrderForm.getOrderPayFormList()) {
                    if (CommonConstant.PayType.WECHAT.equals(orderPayForm.getPayType())) {

                        String notify_url = wxPayBean.getDomain().concat("/notify/wx");

                        if (orderPayForm.getPayModel() == null) {
                            return this.failure("支付模式不能为空");
                        }

                        Result wxPayResult = null;
                        String ip = IpKit.getRealIp(request);
                        if (StrKit.isBlank(ip)) {
                            ip = "127.0.0.1";
                        }

//                    ip = "127.0.0.1";

                        if (CommonConstant.PayModel.QR.equals(orderPayForm.getPayModel())) {
                            log.info(transactionId + " 微信支付-二维码: " + orderPayForm.getPay());
                            byte[] result = payService.wxScanCode(transactionId, ip, orderPayForm.getPay().multiply(BigDecimal.TEN).multiply(BigDecimal.TEN).setScale(0, BigDecimal.ROUND_HALF_UP), UserContextHolder.getInstance().getOpenId(), attach, goodsName, notify_url);
                            WebPayOrderResult webPayOrderResult = new WebPayOrderResult();
                            webPayOrderResult.setTransactionId(transactionId);
                            webPayOrderResult.setQrCode(result);
                            return this.success(webPayOrderResult);
                        } else if (CommonConstant.PayModel.MA.equals(orderPayForm.getPayModel())) {
                            log.info(transactionId + " 微信支付-小程序: " + orderPayForm.getPay());
                            WxPayMpOrderResult wxPayMpOrderResult = payService.wxMiniAppPay(transactionId, ip, orderPayForm.getPay().multiply(BigDecimal.TEN).multiply(BigDecimal.TEN).setScale(0, BigDecimal.ROUND_HALF_UP), UserContextHolder.getInstance().getOpenId(), attach, goodsName, notify_url);
                            wxPayMpOrderResult.setTransactionId(transactionId);
                            return this.success(wxPayMpOrderResult);
                        } else {
                            return this.failure("不支持的支付方式");
                        }
                    } else if (CommonConstant.PayType.ALIPAY.equals(orderPayForm.getPayType())) {

                        String notify_url =aliPayBean.getDomain().concat("/notify/alipay");

                        if (orderPayForm.getPayModel() == null) {
                            return this.failure("支付模式不能为空");
                        }

                        if (CommonConstant.PayModel.QR.equals(orderPayForm.getPayModel())) {
                            log.info(transactionId + " 支付宝支付-二维码: " + orderPayForm.getPay());
                            WebPayOrderResult webPayOrderResult = new WebPayOrderResult();
                            webPayOrderResult.setTransactionId(transactionId);
                            webPayOrderResult.setQrCode(payService.alipayScanCode(transactionId, orderPayForm.getPay(), goodsName, notify_url));
                            return this.success(webPayOrderResult);
                        } else if (CommonConstant.PayModel.MA.equals(orderPayForm.getPayModel())) {
                            return this.failure("不支持的支付方式");
                        }
                    }else {
                        return this.failure("不支持的支付方式");
                    }
                }
            }else{
                //通知业务系统支付结果
                for(Order order : orderList){
                    if(!com.alipay.api.internal.util.StringUtils.isEmpty(order.getRoutingKey())){
                        BusMessage message = new BusMessage();
                        message.setStandalone("Y");
                        message.setInfoType(CommonConstant.InfoType.ORDER);
                        JSONObject jsonObject = new JSONObject();
                        message.setJsonObject(jsonObject);
                        jsonObject.put("orderId", order.getId());
                        jsonObject.put("attach", order.getAttach());
                        jsonObject.put("success", true);
                        busSender.send(order.getRoutingKey(), message);
                    }
                }
            }
            return this.success();
        }

        @ApiIgnore
        @PostMapping(value = "/payOrder")
        @ApiOperation("单个订单支付")
        @Transactional
        public Result payOrder(HttpServletRequest request, @Valid @RequestBody PayOrderForm payOrderForm) {

            String[] payTypes = {"WECHAT","ALIPAY"};//微信 支付宝

            List<OrderPay> orderPayList = orderPayService.selectByOrderId(payOrderForm.getOrderId(), payTypes);

            if(CollectionUtils.isEmpty(orderPayList)){
                return this.failure("订单不存在未支付信息");
            }

            String payModel = "";

            PartyChannelQueryForm partyChannelQueryForm = new PartyChannelQueryForm();
            partyChannelQueryForm.setPartyChannelId(UserContextHolder.getInstance().getChannelId());
            partyChannelQueryForm.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            PartyChannelVO partyChannelVO = this.userClient.getPartyChannel(partyChannelQueryForm).getData();
            if(CommonConstant.ChannelType.PWEB.equals(partyChannelVO.getType())){
                payModel = CommonConstant.PayModel.QR;
            }else if(CommonConstant.ChannelType.WECHATMA.equals(partyChannelVO.getType())){
                payModel = CommonConstant.PayModel.MA;
            }

            OrderPay orderPay = orderPayList.get(0);

            boolean hasRun = false;

            try(AutoReleaseLock lock = payLockCache.tryLock(payOrderForm.getOrderId(), 300, TimeUnit.SECONDS)){
                if(lock != null){
                    String transactionId = com.jpay.util.StringUtils.getOutTradeNo();
                    Order order = orderService.selectById(payOrderForm.getOrderId());
                    order.setTransactionId(transactionId);
                    orderService.modify(order);
                    orderPay.setPayModel(payModel);
                    orderPayService.modify(orderPay);
                    List<OrderItem> orderItemList = orderItemService.selectByOrderId(payOrderForm.getOrderId());
                    String goodsName = orderItemList.get(0).getGoodsName();
                    String attach = "GOODS";

                    if(CommonConstant.PayType.WECHAT.equals(orderPay.getPayType())){

                        String notify_url = wxPayBean.getDomain().concat("/notify/wx");
//                        Result wxPayResult = null;
                        String ip = IpKit.getRealIp(request);
                        if (StrKit.isBlank(ip)) {
                            ip = "127.0.0.1";
                        }
//                        ip = "127.0.0.1";
                        if(CommonConstant.PayModel.QR.equals(payModel)) {
                            log.info(transactionId + " 微信支付-二维码: " + orderPay.getPay());
                            byte[] result = payService.wxScanCode(transactionId, ip, orderPay.getPay().multiply(BigDecimal.TEN).multiply(BigDecimal.TEN).setScale( 0, BigDecimal.ROUND_HALF_UP), UserContextHolder.getInstance().getOpenId(), attach, goodsName, notify_url);
                            return this.success(result);
                        } else if(CommonConstant.PayModel.MA.equals(payModel)){
                            log.info(transactionId + " 微信支付-小程序: " + orderPay.getPay());
                            WxPayMpOrderResult wxPayMpOrderResult = payService.wxMiniAppPay(transactionId, ip, orderPay.getPay().multiply(BigDecimal.TEN).multiply(BigDecimal.TEN).setScale( 0, BigDecimal.ROUND_HALF_UP), UserContextHolder.getInstance().getOpenId(), attach, goodsName, notify_url);
                            wxPayMpOrderResult.setTransactionId(transactionId);
                            return this.success(wxPayMpOrderResult);
                        } else{
                            return this.failure("不支持的支付方式");
                        }
                    }else if(CommonConstant.PayType.ALIPAY.equals(orderPay.getPayType())){

                        String notify_url =aliPayBean.getDomain().concat("/notify/alipay");
                        if(CommonConstant.PayModel.QR.equals(payModel)) {
                            log.info(transactionId + " 支付宝支付-二维码: " + orderPay.getPay());
                            return this.success(payService.alipayScanCode(transactionId, orderPay.getPay(), goodsName, notify_url));
                        }else if(CommonConstant.PayModel.MA.equals(payModel)){
                            return this.failure("不支持的支付方式");
                        }
                    }else{
                        return this.failure("不支持的支付方式");
                    }
                }else {
                    this.failure("当前订单正在被支付中");
                }
            };
            return this.success();
        }

        @PostMapping(value = "/orderPageListByStatus")
        @ApiOperation("应用端用户订单列表")
        public Result<Page<Order>> orderPageListByStatus(@Valid @RequestBody OrderQueryByStatusForm orderQueryByStatusForm) {

            OrderQueryParam orderQueryParam = orderQueryByStatusForm.toParam(OrderQueryParam.class);
            orderQueryParam.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            orderQueryParam.setUserId(UserContextHolder.getInstance().getUserId());
            IPage<Order> page = orderService.pageList(orderQueryByStatusForm.getPage(), orderQueryParam);
            List<Order> orderList = page.getRecords();
            IPage<OrderVOExt> result = new Page<OrderVOExt>();
            page.setRecords(null);
            BeanUtils.copyProperties(page, result);
            List<OrderVOExt> orderVOExtList = JSON.parseArray(JSON.toJSONString(orderList), OrderVOExt.class);
            result.setRecords(orderVOExtList);
            for(OrderVOExt orderVOExt : result.getRecords()){
                org.springframework.beans.BeanUtils.copyProperties(this.orderService.selectById(orderVOExt.getId()), orderVOExt);
                List<OrderItem> orderItemList = orderItemService.selectByOrderId(orderVOExt.getId());
                List<OrderItemVO> orderItemVOList = JSON.parseArray(JSON.toJSONString(orderItemList), OrderItemVO.class);
                orderVOExt.setOrderItemVOList(orderItemVOList);
            }
            return this.success(result);
        }

//        @PostMapping(value = "/getAllOrderList")
//        @ApiOperation("获取全部订单以及订单子项")
//        @Transactional
//        @ApiIgnore
//        public Result<OrderVOExt>> getAllOrderList(@Valid @RequestBody OrderQueryForm orderQueryForm) {
//
//            List<Order> orderList = new LinkedList<>();
//            switch(orderQueryForm.getScene()){
//                case "1":
//                    orderList = this.orderService.selectByOrderPayId(orderQueryForm.getOrderPayId());
//                    break;
//                case "2":
//                    orderList.add(this.orderService.selectById(orderQueryForm.getOrderId()));
//                    break;
//            }
//
//            List<OrderVOExt> orderVOExtList = JSON.parseArray(JSON.toJSONString(orderList), OrderVOExt.class);
//            for(OrderVOExt orderVOExt : orderVOExtList){
//                List<OrderItem> orderItemList = orderItemService.selectByOrderId(orderVOExt.getId());
//                List<OrderItemVO> orderItemVOList = JSON.parseArray(JSON.toJSONString(orderItemList), OrderItemVO.class);
//                orderVOExt.setOrderItemVOList(orderItemVOList);
//            }
//
//            return this.success(orderVOExtList);
//        }

        @GetMapping(value = "/orderDetail")
        @ApiOperation("获取订单详情")
        public Result<OrderDetailVO> orderDetail(@Valid @RequestParam("orderId") String orderId){

            OrderDetailVO orderDetailVO = new OrderDetailVO();
            Order order = orderService.selectById(orderId);
            orderDetailVO.setOrder(order);

            List<OrderItem> orderItemList = orderItemService.selectByOrderId(orderId);
            orderDetailVO.setOrderItemList(orderItemList);

            OrderShip orderShip = orderShipService.selectByOrderId(orderId);

            if(orderShip != null){
                orderDetailVO.setOrderShip(orderShip);
                if(!StringUtils.isEmpty(orderShip.getPickUpId())){
                    orderDetailVO.setPartyPickUpVO(userClient.getPartyPickUpById(orderShip.getPickUpId()).getData());
                }
            }

            List<OrderPay> orderPayList = orderPayService.selectByOrderId(order.getId(), null);
            orderDetailVO.setOrderPayList(orderPayList);

            return this.success(orderDetailVO);
        }

        @PostMapping(value = "/getOrder")
        @ApiIgnore
        public Result<OrderVOExt> getOrder(@Valid @RequestBody OrderQueryForm orderQueryForm){

            OrderVOExt orderVO = new OrderVOExt();
            Order order = orderService.selectById(orderQueryForm.getOrderId());
            BeanUtils.copyProperties(order, orderVO);

            List<OrderItem> orderItemList = orderItemService.selectByOrderId(orderQueryForm.getOrderId());

            List<OrderItemVO> orderItemVOList = JSON.parseArray(JSON.toJSONString(orderItemList), OrderItemVO.class);

            orderVO.setOrderItemVOList(orderItemVOList);
            return this.success(orderVO);
        }

        @PostMapping(value = "/cancelOrder")
        @ApiOperation("客户取消待付款订单")
        @Transactional
        public Result cancelOrder(@Valid @RequestBody CancelOrderForm cancelOrdeForm){

            CancelOrderParam cancelOrderParam = new CancelOrderParam();
            cancelOrderParam.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            BeanUtils.copyProperties(cancelOrdeForm, cancelOrderParam);
            orderService.cancelOrder(cancelOrderParam);
            return this.success();
        }

        @PostMapping(value = "/finishOrder")
        @ApiOperation("客户完成订单")
        public Result finishOrder(@Valid @RequestBody SendOrderForm sendOrderForm){

            return this.success();
        }

        @PostMapping(value = "/orderAfterSales")
        @ApiOperation("客户订单售后")
        public Result orderAfterSales(@Valid @RequestBody OrderQueryForm orderQueryForm){

            return this.success();
        }

}

