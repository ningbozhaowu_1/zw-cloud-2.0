package com.zhaowu.cloud.order.rest.pay;

import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.internal.util.StringUtils;
import com.jpay.alipay.AliPayApi;
import com.jpay.ext.kit.HttpKit;
import com.jpay.ext.kit.PaymentKit;
import com.jpay.weixin.api.WxPayApiConfigKit;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import com.zhaowu.cloud.framework.pay.entity.AliPayBean;
import com.zhaowu.cloud.framework.util.DateUtils;
import com.zhaowu.cloud.framework.util.GoeasyUtils;
import com.zhaowu.cloud.order.constant.OrderConstant;
import com.zhaowu.cloud.order.entity.po.Order;
import com.zhaowu.cloud.order.entity.po.OrderPay;
import com.zhaowu.cloud.order.service.*;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/notify")
public class NotifyController extends BaseController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AliPayBean aliPayBean;

	@Autowired
	private OrderPayService orderPayService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private BusSender busSender;

	@CreateCache(name = "orderLockCache:", expire = 3600, cacheType = CacheType.REMOTE)
	private com.alicp.jetcache.Cache<String, OrderPay> orderLockCache;

	@ApiIgnore
	@RequestMapping(value = "/wx",method={RequestMethod.POST, RequestMethod.GET})
	@ResponseBody
	@Transactional
	public String wx(HttpServletRequest request) {

		String xmlMsg = HttpKit.readData(request);
		log.info("微信支付通知="+xmlMsg);
		Map<String, String> params = PaymentKit.xmlToMap(xmlMsg);

		if(PaymentKit.verifyNotify(params, WxPayApiConfigKit.getWxPayApiConfig().getPaternerKey())) {

			String tradeState  = params.get("return_code");
			String attach      = params.get("attach");
			// 注意重复通知的情况，同一订单号可能收到多次通知，请注意一定先判断订单状态
			// 避免已经成功、关闭、退款的订单被再次更新
			// 微信支付订单号
			String outTransactionId = params.get("transaction_id");
			// 商户订单号
			String transactionId = params.get("out_trade_no");
			boolean hasRun = false;
			while(!hasRun){
				hasRun = orderLockCache.tryLockAndRun(transactionId,300, TimeUnit.SECONDS, () -> {
					if (("SUCCESS").equals(tradeState)) {
						log.info("更新订单支付成功:"+attach);
						updateOrderSuccess(transactionId, outTransactionId, CommonConstant.PayType.WECHAT);
					}else{
						String errorMsg = params.get("trade_state_desc");
						log.info("更新订单支付失败:"+attach);
						updateOrderFail(transactionId, CommonConstant.PayType.WECHAT, tradeState, errorMsg);
					}
				});
			}
			if(!hasRun){
				log.info("更新订单错误信息:"+attach);
				return renderFail("系统错误");
			}
		} else{
			return renderFail("验签失败");
		}
		return renderOk("4");
	}

	@ApiIgnore
	@RequestMapping(value = "/alipay",method={RequestMethod.POST, RequestMethod.GET})
	@ResponseBody
	@Transactional
	public String  alipay(HttpServletRequest request) {
		try {
//			Map<String, String> params = AliPayApi.toMap(request);
			String xmlMsg = HttpKit.readData(request);
			Map<String, String> params = new HashMap();
			String datas[] = xmlMsg.split("&");
			for(String data : datas){
				params.put(data.split("=")[0],URLDecoder.decode(data.split("=")[1]));
			}
			log.info("alipay支付通知="+params);

			boolean verify_result = AlipaySignature.rsaCheckV1(params, aliPayBean.getPublicKey(), "UTF-8",
					"RSA2");

			if (verify_result) {

				String tradeState = params.get("trade_status");
				String transactionId = params.get("out_trade_no");
				String outTransactionId = params.get("trade_no");

				boolean hasRun = false;
				while(!hasRun){
					hasRun = orderLockCache.tryLockAndRun(transactionId,300, TimeUnit.SECONDS, () -> {
						if (("TRADE_SUCCESS").equals(tradeState)) {
							log.info("更新订单支付失败");
							updateOrderSuccess(transactionId, outTransactionId, CommonConstant.PayType.ALIPAY);
						}else{
							String errorMsg = params.get("trade_state_desc");
							log.info("更新订单支付失败");
							updateOrderFail(transactionId, CommonConstant.PayType.ALIPAY, tradeState, errorMsg);
						}
					});
				}
				if(!hasRun){
					log.info("更新订单错误信息:");
					return renderFail("系统错误");
				}
			} else {
				return "failure";
			}
		} catch (AlipayApiException e) {
			log.error(e.getErrMsg());
			return "failure";
		}
		return renderOk("5");
	}

	String updateOrderFail(String transactionId, String payType, String errorCode, String errorMsg){

		List<Order> orderList = orderService.selectByTransactionId(transactionId);

		if(CollectionUtils.isEmpty(orderList)){
			throw new  RuntimeException(" 订单不存在");
		}

		for(Order order : orderList){
			List<OrderPay> orderPayList = orderPayService.selectByOrderId(order.getId(), (String[]) Arrays.asList(payType).toArray());
			if(CollectionUtils.isEmpty(orderPayList)){
				throw new  RuntimeException("子订单不存在");
			}

			OrderPay orderPay = orderPayList.get(0);
			orderPay.setErrorCode(errorCode);
			orderPay.setErrorDesc(errorMsg);
			orderPayService.modify(orderPay);

			//通知业务系统支付结果
			if(!StringUtils.isEmpty(order.getRoutingKey())){
				BusMessage message = new BusMessage();
				message.setStandalone("Y");
				message.setInfoType(CommonConstant.InfoType.ORDER);
				JSONObject jsonObject = new JSONObject();
				message.setJsonObject(jsonObject);
				jsonObject.put("orderId", order.getId());
				jsonObject.put("attach", order.getAttach());
				jsonObject.put("success", false);
				jsonObject.put("msg", errorMsg);
				busSender.send(order.getRoutingKey(), message);
			}
		}

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("return_code", errorCode);
		jsonObject.put("return_msg", errorMsg);
		GoeasyUtils.send(transactionId, jsonObject);

		return renderOk(payType);
	}

	String updateOrderSuccess(String transactionId, String outTransactionId, String payType){

		List<Order> orderList = orderService.selectByTransactionId(transactionId);

		if(CollectionUtils.isEmpty(orderList)){
			throw new  RuntimeException(" 订单不存在");
		}

		for(Order order : orderList){
			List<OrderPay> orderPayList = orderPayService.selectByOrderId(order.getId(), (String[]) Arrays.asList(payType).toArray());
			if(CollectionUtils.isEmpty(orderPayList)){
				throw new  RuntimeException("子订单不存在");
			}

			OrderPay orderPay = orderPayList.get(0);

			if(!OrderConstant.ORDER_PAY_WAIT_FOR_PAY.equalsIgnoreCase(orderPay.getStatus())){
				renderOk(payType);
			}

			if(CommonConstant.DefaultParty.PartyId.equals(order.getApplicationParty())){
				orderPay.setStatus(OrderConstant.ORDER_FINISH);
			}else{
				orderPay.setStatus(OrderConstant.ORDER_PAID);
			}
			orderPay.setOutTransactionId(outTransactionId);
			orderPayService.modify(orderPay);

			if(CommonConstant.DefaultParty.PartyId.equals(order.getApplicationParty())){
				order.setStatus(OrderConstant.ORDER_FINISH);
			}else{
				order.setStatus(OrderConstant.ORDER_PAID);
			}

			order.setPayTime(DateUtils.currentDate());
			orderService.modify(order);

			//通知业务系统支付结果
			if(!StringUtils.isEmpty(order.getRoutingKey())){
				BusMessage message = new BusMessage();
				message.setStandalone("Y");
				message.setInfoType(CommonConstant.InfoType.ORDER);
				JSONObject jsonObject = new JSONObject();
				message.setJsonObject(jsonObject);
				jsonObject.put("orderId", order.getId());
				jsonObject.put("attach", order.getAttach());
				jsonObject.put("success", true);
				busSender.send(order.getRoutingKey(), message);
			}
		}
		// TODO 通知用户中心扣除对应账户金额 余额账户 积分账户 代金卡账户


		// TODO 通知商品中心更新付款扣减库存


		//二维码模式通知前端跳转支付成功
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("return_code", "SUCCESS");
		jsonObject.put("return_msg", "OK");
		GoeasyUtils.send(transactionId, jsonObject);

		return renderOk(payType);
	}

	String renderOk(String payType){

		if("4".equalsIgnoreCase(payType)){
			Map<String, String> xml = new HashMap<String, String>();
			xml.put("return_code", "SUCCESS");
			xml.put("return_msg", "OK");
			return PaymentKit.toXml(xml);
		}else if("5".equalsIgnoreCase(payType)){
			return "success";
		}

		return "success";
	}

	String renderFail(String msg){

		Map<String, String> xml = new HashMap<String, String>();
		xml.put("return_code", "FAIL");
		xml.put("return_msg", msg);
		return PaymentKit.toXml(xml);
	}

//	public static void main(String[] args) throws AlipayApiException, UnsupportedEncodingException {
//		String xmlMsg = "gmt_create=2021-05-14+18%3A00%3A21, charset=UTF-8, seller_email=zhaowu888%40vmaisui.com, subject=%E5%A4%9A%E5%95%86%E6%88%B7%E5%BA%94%E7%94%A8, sign=NcZrgSZjMp3GpsPDoZWQr3bUKkY9tMk%2Fc%2BA06z5OGv5k33%2B432a7UG509v3nkhfl%2Bx3UzRMr7fQYiFnENkEZAEXLv0waDZoskEnHn22O1Oj4FXtgrr3z0SI0g5%2FaY5A%2BDHE8v9mEGv5TMXj1TXls1uZ3vE5Q4iAXNwtDK9V5m4z%2Bn%2FioQ8F4ht1%2Bvx4zOecXTkFnBOKYXcrqAkKKE7a2Tw3HUWj1tqoeK082fytAV10nirChHez5xKAjWL8K0zUHCd8qoG9ZfEuUnQL9mJTG9%2Fg7TPdUSczYo5Gl7UahTehf843l5W9j7C9VXbiCkAezeyLkFOImJ65QXEVPVcyCtQ%3D%3D, buyer_id=2088702888660335, invoice_amount=0.01, notify_id=2021051400222180027060331456884418, fund_bill_list=%5B%7B%22amount%22%3A%220.01%22%2C%22fundChannel%22%3A%22ALIPAYACCOUNT%22%7D%5D, notify_type=trade_status_sync, trade_status=TRADE_SUCCESS, receipt_amount=0.01, app_id=2021001164614975, buyer_pay_amount=0.01, sign_type=RSA2, seller_id=2088031862642840, gmt_payment=2021-05-14+18%3A00%3A26, notify_time=2021-05-14+18%3A00%3A27, version=1.0, out_trade_no=051418001816209, total_amount=0.01, trade_no=2021051422001460331403047522, auth_app_id=2021001164614975, buyer_logon_id=187****6223, point_amount=0.00";
//		Map<String, String> params = new HashMap();
//		String datas[] = xmlMsg.split(", ");
//		for(String data : datas){
//			params.put(data.split("=")[0],URLDecoder.decode(data.split("=")[1]));
//		}
//
//		boolean verify_result = AlipaySignature.rsaCheckV1(params, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkPOGVp9P/vHhR2gm9qgBDMwoQKrYY/m5qJelHTPfbV22GUrQkZUiZhPzXtgvLSYVNJizALkOCQqT4HnwDTO7ldNQ8tUA4xLKOxHVt7ex81/w6SlPqe3m+qMia2vHvO9Flw6tzonVfBYsHUxGsbvS/pZnzEefK9GnPqCNFmb2Y/1Pq/uVunveuwNXwoVQwLrHkGjWa8xNIgi21OsujggOnTGiGMHA1hccbujOqXfmcbq78cStd7feAJ171xn1wyEAjhEnfGtMljHcysqmKEHJxwdE3P7sUHNFhNEhlgb2/ror+bQjxlGD46RiOcSahfyFoJmQthHWoBsC3ArhEUx3FQIDAQAB", "UTF-8",
//				"RSA2");
////		String str = URLDecoder.decode("NcZrgSZjMp3GpsPDoZWQr3bUKkY9tMk%2Fc%2BA06z5OGv5k33%2B432a7UG509v3nkhfl%2Bx3UzRMr7fQYiFnENkEZAEXLv0waDZoskEnHn22O1Oj4FXtgrr3z0SI0g5%2FaY5A%2BDHE8v9mEGv5TMXj1TXls1uZ3vE5Q4iAXNwtDK9V5m4z%2Bn%2FioQ8F4ht1%2Bvx4zOecXTkFnBOKYXcrqAkKKE7a2Tw3HUWj1tqoeK082fytAV10nirChHez5xKAjWL8K0zUHCd8qoG9ZfEuUnQL9mJTG9%2Fg7TPdUSczYo5Gl7UahTehf843l5W9j7C9VXbiCkAezeyLkFOImJ65QXEVPVcyCtQ%3D%3D","UTF-8");
//		System.out.println(verify_result);
//	}
}
