package com.zhaowu.cloud.order.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * <p>
 * 订单支付表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@Accessors(chain = true)
public class OrderPay extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 账户编号
     */
    @TableField(value = "ac_id")
    private String acId;

    @TableField(value = "order_id")
    private String orderId;

    /**
     * 用户编号
     */
    @TableField(value = "user_id",fill = FieldFill.INSERT)
    private String userId;

    /**
     * 支付金额
     */
    private BigDecimal pay;
    /**
     * 退款金额
     */
    private BigDecimal refund;
    /**
     * 状态 0-未支付 1-支付成功
     */
    private String status;
    /**
     * 乐观锁
     */
    private Integer revision;
    /**
     * 支付模式 1:二维码 2:小程序 3:公众号 4:app 5:h5
     */
    @TableField(value = "pay_model")
    private String payModel;
    /**
     * 支付方式 -余额 2-积分 3-代金卡 4-微信 5-支付宝
     */
    @TableField(value = "pay_type")
    private String payType;
    /**
     * 外部流水号
     */
    @TableField(value = "out_transaction_id")
    private String outTransactionId;
    /**
     * 错误编码
     */
    @TableField(value = "error_code")
    private String errorCode;

    /**
     * 错误描述
     */
    @TableField(value = "error_desc")
    private String errorDesc;
}
