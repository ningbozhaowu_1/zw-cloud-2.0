package com.zhaowu.cloud.order.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.order.entity.po.Order;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CancelOrderParam extends BaseParam<Order> {

    private String id;

    private String reason;

    private String remark;
}
