package com.zhaowu.cloud.order.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.order.entity.po.Order;
import lombok.Data;

@Data
public class OrderQueryParam extends BaseParam<Order> {

    private String status;

    private String userId;

    private String applicationParty;
}
