--订单中心
CREATE DATABASE IF NOT EXISTS `zw-order` DEFAULT CHARACTER SET = utf8mb4;
CREATE USER `order`@'%' IDENTIFIED BY 'order123';
GRANT ALL PRIVILEGES ON `zw-order`.* TO `order`;
GRANT ALL PRIVILEGES ON `zw-order`.* TO `order`;

USE `zw-order`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


DROP TABLE IF EXISTS `t_order_0`;
CREATE TABLE `t_order_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `application_id` bigint(20) NOT NULL COMMENT '应用编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `shop_id` bigint(20) DEFAULT NULL COMMENT '店铺编号',
  `shop_name` varchar(256) DEFAULT NULL COMMENT '店铺名称',
  `total` decimal(32,2) DEFAULT NULL COMMENT '总金额',
  `pay` decimal(32,2) DEFAULT NULL COMMENT '实际支付金额',
  `discount` decimal(32,2) DEFAULT NULL COMMENT '折扣金额',
  `refund` decimal(32,2) NOT NULL COMMENT '退款金额',
  `freight` decimal(32,2) DEFAULT NULL COMMENT '邮费',
  `status` char(3) NOT NULL COMMENT '状态 0-待支付\n1-已支付\n2-已发货\n3-已完成\n4-已关闭\n5-退款中\n6-已下定',
  `attach` varchar(1024) DEFAULT NULL COMMENT '自定义数据',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `end_time` datetime DEFAULT NULL COMMENT '完成时间',
  `close_time` datetime DEFAULT NULL COMMENT '关闭时间',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `transaction_id` varchar(256) DEFAULT NULL COMMENT '内部商户订单流水号',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单主表';

DROP TABLE IF EXISTS `t_order_1`;
CREATE TABLE `t_order_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `application_id` bigint(20) NOT NULL COMMENT '应用编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `shop_id` bigint(20) DEFAULT NULL COMMENT '店铺编号',
  `shop_name` varchar(256) DEFAULT NULL COMMENT '店铺名称',
  `total` decimal(32,2) DEFAULT NULL COMMENT '总金额',
  `pay` decimal(32,2) DEFAULT NULL COMMENT '实际支付金额',
  `discount` decimal(32,2) DEFAULT NULL COMMENT '折扣金额',
  `refund` decimal(32,2) NOT NULL COMMENT '退款金额',
  `freight` decimal(32,2) DEFAULT NULL COMMENT '邮费',
  `status` char(3) NOT NULL COMMENT '状态 0-待支付\n1-已支付\n2-已发货\n3-已完成\n4-已关闭\n5-退款中\n6-已下定',
  `attach` varchar(1024) DEFAULT NULL COMMENT '自定义数据',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `end_time` datetime DEFAULT NULL COMMENT '完成时间',
  `close_time` datetime DEFAULT NULL COMMENT '关闭时间',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `transaction_id` varchar(256) DEFAULT NULL COMMENT '内部商户订单流水号',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单主表';

DROP TABLE IF EXISTS `order_item_0`;
CREATE TABLE `order_item_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品编号',
  `goods_sku_id` bigint(20) DEFAULT NULL COMMENT '商品sku编号',
  `total` decimal(32,2) DEFAULT NULL COMMENT '总金额',
  `price` decimal(32,2) DEFAULT NULL COMMENT '单价',
  `quantity` int(11) DEFAULT NULL COMMENT '数量',
  `actual_price` decimal(32,2) DEFAULT NULL COMMENT '实际价格 秒杀价折扣价会员价',
  `goods_image` varchar(2048) DEFAULT NULL COMMENT '商品图片',
  `goods_name` varchar(256) DEFAULT NULL COMMENT '商品名称',
  `goods_skus` varchar(256) DEFAULT NULL COMMENT '商品规格',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单子表';

DROP TABLE IF EXISTS `order_item_1`;
CREATE TABLE `order_item_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品编号',
  `goods_sku_id` bigint(20) DEFAULT NULL COMMENT '商品sku编号',
  `total` decimal(32,2) DEFAULT NULL COMMENT '总金额',
  `price` decimal(32,2) DEFAULT NULL COMMENT '单价',
  `quantity` int(11) DEFAULT NULL COMMENT '数量',
  `actual_price` decimal(32,2) DEFAULT NULL COMMENT '实际价格 秒杀价折扣价会员价',
  `goods_image` varchar(2048) DEFAULT NULL COMMENT '商品图片',
  `goods_name` varchar(256) DEFAULT NULL COMMENT '商品名称',
  `goods_skus` varchar(256) DEFAULT NULL COMMENT '商品规格',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单子表';

DROP TABLE IF EXISTS `order_item_data_0`;
CREATE TABLE `order_item_data_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `order_item_id` bigint(20) DEFAULT NULL COMMENT '订单项编号',
  `data` text COMMENT '数据json',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单子表业务数据';

DROP TABLE IF EXISTS `order_item_data_1`;
CREATE TABLE `order_item_data_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `order_item_id` bigint(20) DEFAULT NULL COMMENT '订单项编号',
  `data` text COMMENT '数据json',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单子表业务数据';

DROP TABLE IF EXISTS `order_return_0`;
CREATE TABLE `order_return_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) NOT NULL COMMENT '订单编号',
  `third_pay_id` bigint(20) DEFAULT NULL COMMENT '支付编号 第三方支付退款必输',
  `amount` decimal(32,2) NOT NULL COMMENT '退款金额',
  `return_type` char(3) NOT NULL COMMENT '退款类型 1-全额\n2-部分',
  `return_way` char(3) NOT NULL COMMENT '退款方式 1-余额\n2-积分\n3-代金卡\n4-第三方支付',
  `status` char(3) NOT NULL COMMENT '状态 0-退款中\n1-退款成功\n-1-退款失败',
  `refund_desc` varchar(256) DEFAULT NULL COMMENT '退款原因',
  `out_refund_id` varchar(100) DEFAULT NULL COMMENT '第三方退款单号',
  `error_code` varchar(50) DEFAULT NULL COMMENT '错误编码',
  `error_desc` varchar(256) DEFAULT NULL COMMENT '错误描述',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单退款表';

DROP TABLE IF EXISTS `order_return_1`;
CREATE TABLE `order_return_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) NOT NULL COMMENT '订单编号',
  `third_pay_id` bigint(20) DEFAULT NULL COMMENT '支付编号 第三方支付退款必输',
  `amount` decimal(32,2) NOT NULL COMMENT '退款金额',
  `return_type` char(3) NOT NULL COMMENT '退款类型 1-全额\n2-部分',
  `return_way` char(3) NOT NULL COMMENT '退款方式 1-余额\n2-积分\n3-代金卡\n4-第三方支付',
  `status` char(3) NOT NULL COMMENT '状态 0-退款中\n1-退款成功\n-1-退款失败',
  `refund_desc` varchar(256) DEFAULT NULL COMMENT '退款原因',
  `out_refund_id` varchar(100) DEFAULT NULL COMMENT '第三方退款单号',
  `error_code` varchar(50) DEFAULT NULL COMMENT '错误编码',
  `error_desc` varchar(256) DEFAULT NULL COMMENT '错误描述',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单退款表';


DROP TABLE IF EXISTS `order_ship_0`;
CREATE TABLE `order_ship_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `ship_code` varchar(100) DEFAULT NULL COMMENT '物流编号',
  `pick_up_id` bigint(20) DEFAULT NULL COMMENT '提货点编号',
  `ship_name` varchar(256) NOT NULL COMMENT '收货人姓名',
  `ship_phone` varchar(20) NOT NULL COMMENT '收货人手机号',
  `ship_province` varchar(32) DEFAULT NULL COMMENT '收货人省份',
  `ship_city` varchar(32) DEFAULT NULL COMMENT '收货人城市',
  `ship_district` varchar(32) DEFAULT NULL COMMENT '收货人街道',
  `ship_addr` varchar(256) DEFAULT NULL COMMENT '收货人详细地址',
  `require_time` datetime DEFAULT NULL COMMENT '期望送货时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单物流表';

DROP TABLE IF EXISTS `order_ship_1`;
CREATE TABLE `order_ship_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `ship_code` varchar(100) DEFAULT NULL COMMENT '物流编号',
  `pick_up_id` bigint(20) DEFAULT NULL COMMENT '提货点编号',
  `ship_name` varchar(256) NOT NULL COMMENT '收货人姓名',
  `ship_phone` varchar(20) NOT NULL COMMENT '收货人手机号',
  `ship_province` varchar(32) DEFAULT NULL COMMENT '收货人省份',
  `ship_city` varchar(32) DEFAULT NULL COMMENT '收货人城市',
  `ship_district` varchar(32) DEFAULT NULL COMMENT '收货人街道',
  `ship_addr` varchar(256) DEFAULT NULL COMMENT '收货人详细地址',
  `require_time` datetime DEFAULT NULL COMMENT '期望送货时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单物流表';


DROP TABLE IF EXISTS `order_pay_0`;
CREATE TABLE `order_pay_0` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(19) NOT NULL  COMMENT '用户编号',
  `order_id` bigint(19) NOT NULL COMMENT '订单编号',
  `ac_id` bigint(19) DEFAULT NULL COMMENT '账户编号',
  `pay` decimal(32,2) DEFAULT NULL COMMENT '支付金额',
  `refund` decimal(32,2) NOT NULL COMMENT '退款金额',
  `pay_type` char(3) NOT NULL COMMENT '支付方式 1-余额\n2-积分\n3-代金卡\n4-微信\n5-支付宝',
  `pay_model` char(3) NOT NULL COMMENT '支付模式 1:二维码 2:小程序 3:公众号 4:app 5:h5',
  `status` char(3) NOT NULL COMMENT '状态 0-未支付\n1-支付成功\n2-退款\n3-关闭',
  `out_transaction_id` varchar(100) DEFAULT NULL COMMENT '第三方订单号',
  `error_code` varchar(50) DEFAULT NULL COMMENT '错误编码',
  `error_desc` varchar(256) DEFAULT NULL COMMENT '错误描述',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `success_time` datetime DEFAULT NULL COMMENT '支付成功时间',
  `create_by` bigint(19) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(19) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单支付主表';

DROP TABLE IF EXISTS `order_pay_1`;
CREATE TABLE `order_pay_1` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(19) NOT NULL  COMMENT '用户编号',
  `order_id` bigint(19) NOT NULL COMMENT '订单编号',
  `ac_id` bigint(19) DEFAULT NULL COMMENT '账户编号',
  `pay` decimal(32,2) DEFAULT NULL COMMENT '支付金额',
  `refund` decimal(32,2) NOT NULL COMMENT '退款金额',
  `pay_type` char(3) NOT NULL COMMENT '支付方式 1-余额\n2-积分\n3-代金卡\n4-微信\n5-支付宝',
  `pay_model` char(3) NOT NULL COMMENT '支付模式 1:二维码 2:小程序 3:公众号 4:app 5:h5',
  `status` char(3) NOT NULL COMMENT '状态 0-未支付\n1-支付成功\n2-退款\n3-关闭',
  `out_transaction_id` varchar(100) DEFAULT NULL COMMENT '第三方订单号',
  `error_code` varchar(50) DEFAULT NULL COMMENT '错误编码',
  `error_desc` varchar(256) DEFAULT NULL COMMENT '错误描述',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `success_time` datetime DEFAULT NULL COMMENT '支付成功时间',
  `create_by` bigint(19) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(19) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单支付表';

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `type` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '日志类型:登录日志;系统日志;',
  `name` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '业务名称',
  `request_url` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求地址',
  `request_type` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求方法类型',
  `request_class` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '调用类',
  `request_method` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '调用方法',
  `request_ip` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求IP',
  `user_id` bigint(20) DEFAULT NULL COMMENT '创建用户Id',
  `user_name` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '创建用户名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `time` int(11) DEFAULT NULL COMMENT '消耗时间',
  `success` int(11) DEFAULT NULL COMMENT '是否成功',
  `exception` varchar(2250) COLLATE utf8_bin DEFAULT NULL COMMENT '异常信息',
  `request_parameter` varbinary(2250) DEFAULT NULL COMMENT '调用参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=476 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

SET FOREIGN_KEY_CHECKS = 1;
