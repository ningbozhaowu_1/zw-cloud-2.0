package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;

@ApiModel
@Slf4j
@Data
public class OrderQueryForm {

    @ApiModelProperty("订单编号")
    private String orderId;

}
