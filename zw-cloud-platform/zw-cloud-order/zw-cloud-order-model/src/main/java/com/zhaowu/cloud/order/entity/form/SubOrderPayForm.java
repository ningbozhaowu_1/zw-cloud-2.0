package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel
@Slf4j
@Data
public class SubOrderPayForm {

//    @ApiModelProperty("支付账户")
//    private Long acId;

    @NotNull(message="支付金额不能为空")
    @ApiModelProperty("支付金额")
    private BigDecimal pay;

//    @NotNull(message="支付方式不能为空")
    @ApiModelProperty("支付方式 1-余额 2-积分 3-代金卡 4-微信 5-支付宝")
    private String payType;

    @ApiModelProperty("支付模式 支付方式未微信或者支付宝时必输 1:二维码 2:小程序 3:公众号 4:app 5:h5")
    private String payModel;
}
