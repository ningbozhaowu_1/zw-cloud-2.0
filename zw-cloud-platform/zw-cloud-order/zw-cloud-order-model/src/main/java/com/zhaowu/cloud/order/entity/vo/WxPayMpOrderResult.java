package com.zhaowu.cloud.order.entity.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WxPayMpOrderResult {
    private String appId;
    private String partnerId;
    private String timeStamp;
    private String nonceStr;
    private String packageValue;
    private String signType;
    private String paySign;

    private String transactionId;
}