package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@ApiModel
@Slf4j
@Data
public class CreateOrderForm {

    private List<OrderForm> orderFormList;

    private List<OrderPayForm> orderPayFormList;

    private BigDecimal total;

    private String attach;

    /**
     * 通知结果参数
     * infotype 信息类型 值为 order
     * orderId 订单编号
     * attach 创建订单时传入的自定义数据 业务系统可以用该字段关联业务
     * success 订单状态
     * msg 失败原因 订单失败时返回
     */
    @ApiModelProperty("bus通知的routingKey *需要订单中心异步通知支付结果时必输 ")
    private String routingKey;
}
