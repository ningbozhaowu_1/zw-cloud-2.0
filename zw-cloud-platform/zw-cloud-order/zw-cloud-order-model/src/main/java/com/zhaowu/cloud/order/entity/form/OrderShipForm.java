package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ApiModel("收货信息")
public class OrderShipForm {

    @ApiModelProperty("提货点编号")
    private String pickUpId;

    @NotNull(message="收货人姓名不能为空")
    @ApiModelProperty("收货人姓名")
    private String shipName;

    @NotNull(message="收货人手机号不能为空")
    @ApiModelProperty("收货人手机号")
    private String shipPhone;

    @NotNull(message="收货人省份")
    @ApiModelProperty("收货人省份")
    private String shipProvince;

    @NotNull(message="收货人城市不能为空")
    @ApiModelProperty("收货人城市")
    private String shipCity;

    @ApiModelProperty("收货人街道")
    private String shipDistrict;

    @NotNull(message="收货人详细地址不能为空")
    @ApiModelProperty("收货人详细地址")
    private String shipAddr;

    @NotNull(message="期望收货时间")
    @ApiModelProperty("期望收货时间")
    private Date requireTime;
}
