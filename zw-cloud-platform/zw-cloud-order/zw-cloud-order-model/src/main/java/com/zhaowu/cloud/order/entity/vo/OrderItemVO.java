package com.zhaowu.cloud.order.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OrderItemVO {

    private static final long serialVersionUID = 1L;

    private String orderId;

    private String id;

    private String userId;

    private String goodsId;

    private String goodsSkuId;

    private BigDecimal total;

    private BigDecimal price;

    private Integer quantity;

    private BigDecimal actualPrice;

    private String goodsImage;

    private String goodsName;

    private String goodsSkus;
}
