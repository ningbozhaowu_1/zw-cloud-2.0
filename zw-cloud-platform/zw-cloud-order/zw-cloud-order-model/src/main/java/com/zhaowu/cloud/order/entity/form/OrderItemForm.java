package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@ApiModel
@Slf4j
@Data
public class OrderItemForm {

    @NotNull(message = "商品编号不能为空")
    @ApiModelProperty("商品编号")
    private String goodsId;

    @ApiModelProperty("商品sku编号")
    private String goodsSkuId;

    @NotNull(message = "总金额不能为空")
    @ApiModelProperty("总金额")
    private BigDecimal total;

    @NotNull(message = "单价不能为空")
    @ApiModelProperty("单价")
    private BigDecimal price;

    @NotNull(message = "数量不能为空")
    @ApiModelProperty("数量")
    private Integer quantity;

    @NotNull(message = "实际价格不能为空")
    @ApiModelProperty("实际价格 秒杀价折扣价会员价")
    private BigDecimal actualPrice;

    @NotNull(message = "商品图片不能为空")
    @ApiModelProperty("商品图片")
    @Size(max= 256, message="商品图片名称长度必须在{max}个字符内")
    private String goodsImage;

    @NotNull(message = "商品名称不能为空")
    @Size(max= 256, message="商品名称名称长度必须在{max}个字符内")
    @ApiModelProperty("商品名称")
    private String goodsName;

    @NotNull(message = "商品规格不能为空")
    @Size(max= 256, message="商品规格长度必须在{max}个字符内")
    @ApiModelProperty("商品规格 红色,L")
    private String goodsSkus;
}