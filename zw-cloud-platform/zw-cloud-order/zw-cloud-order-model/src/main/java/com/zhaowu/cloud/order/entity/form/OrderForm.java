package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@ApiModel
@Slf4j
@Data
public class OrderForm {

    @ApiModelProperty("店铺编号")
    private String shopId;

    @Size(max= 256, message="店铺名称长度必须在{max}个字符内")
    @ApiModelProperty("店铺名称")
    private String shopName;

    @NotNull(message="总金额不能为空")
    @ApiModelProperty("总金额")
    private BigDecimal total;

    @NotNull(message="运费不能为空")
    @ApiModelProperty("运费")
    private BigDecimal freight;

    @Size(max= 1024, message="自定义数据长度必须在{max}个字符内")
    @ApiModelProperty("自定义数据")
    private String attach;

    private List<OrderItemForm> orderItemFormList;

    private String deliveryType;

    @ApiModelProperty("自提点编号")
    private String pickUpId;

    private OrderShipForm orderShipForm;
}
