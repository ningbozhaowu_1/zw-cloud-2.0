package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel("支付信息")
@Slf4j
@Data
public class PayOrderForm {

    @ApiModelProperty("订单编号")
    @NotNull(message = "订单编号不能为空")
    private String orderId;

}
