package com.zhaowu.cloud.order.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@ApiModel("支付信息")
@Slf4j
@Data
public class OrderPayForm {

    @NotNull(message="支付金额不能为空")
    @ApiModelProperty("支付金额")
    private BigDecimal pay;

    @ApiModelProperty("支付类型 BAL-余额 INT-积分 CASH-代金卡 WECHAT-微信 ALIPAY-支付宝")
    private String payType;

    @ApiModelProperty("支付模式 支付方式微信或者支付宝时必输 QR:二维码 MA:小程序 WMA:公众号 APP:app H5:h5")
    private String payModel;
    
}
