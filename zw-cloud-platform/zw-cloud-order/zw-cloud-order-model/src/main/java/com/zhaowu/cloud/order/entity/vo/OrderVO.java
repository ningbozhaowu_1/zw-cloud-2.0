package com.zhaowu.cloud.order.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 * 订单主表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@Accessors(chain = true)
public class OrderVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String shopId;

    private String shopName;

    private BigDecimal total;

    private BigDecimal discount;

    private BigDecimal pay;

    private BigDecimal refund;

    private BigDecimal freight;

    /**
     * 状态 0-待支付 1-已支付 2-已发货 3-已完成 4-已关闭 5-退款中 6-已下定
     */
    private String status;

    /**
     * 自定义数据
     */
    private String attach;

    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 完成时间
     */
    private Date endTime;

    /**
     * 关闭时间
     */
    private Date closeTime;

    /**
     * 下单时间
     */
    private Date createTime;

    /**
     *  支付时间
     */
    private Date payTime;

}
