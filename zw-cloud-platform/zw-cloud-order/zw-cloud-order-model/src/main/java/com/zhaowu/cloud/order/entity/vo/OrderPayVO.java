package com.zhaowu.cloud.order.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 * 订单主表
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@Accessors(chain = true)
public class OrderPayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private BigDecimal pay;

    private BigDecimal freight;

    /**
     * 状态 0-待支付 1-已支付
     */
    private String status;

}
