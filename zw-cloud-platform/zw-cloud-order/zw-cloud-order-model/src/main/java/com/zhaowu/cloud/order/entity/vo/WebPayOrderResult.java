package com.zhaowu.cloud.order.entity.vo;

import lombok.Builder;
import lombok.Data;

@Data
public class WebPayOrderResult {
    private byte[] qrCode;
    private String transactionId;
}