package com.zhaowu.cloud.order.client;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.order.entity.form.CreateOrderForm;
import com.zhaowu.cloud.order.entity.form.OrderQueryForm;
import com.zhaowu.cloud.order.entity.form.PayOrderForm;
import com.zhaowu.cloud.order.entity.vo.OrderVOExt;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author xxp
 */
@FeignClient(name = "order-service")
public interface OrderClient {

    @PostMapping(value = "/order/v1/createOrder")
    Result createOrder(@RequestBody CreateOrderForm createOrderForm);

    @PostMapping(value = "/order/v1/payOrder")
    Result payOrder(@RequestBody PayOrderForm payOrderForm);

    @PostMapping(value = "/order/v1/getOrder")
    Result<OrderVOExt> getOrder(@RequestBody OrderQueryForm orderQueryForm);
}
