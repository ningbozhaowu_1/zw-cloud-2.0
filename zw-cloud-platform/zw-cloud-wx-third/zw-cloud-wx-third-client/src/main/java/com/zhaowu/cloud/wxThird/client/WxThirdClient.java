package com.zhaowu.cloud.wxThird.client;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.wx.third.entity.form.*;
import com.zhaowu.cloud.wx.third.entity.model.xcx.Jscode2SessionResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author xxp
 */
@FeignClient(name = "wx-third-service")
public interface WxThirdClient {

    @PostMapping("/auth/admin/v1/create")
    Result creat(@RequestBody CreateForm createForm);

    @PostMapping("/auth/admin/v1//setNickName")
    Result setNickName(@RequestBody ModifyNickNameForm modifyNickNameForm);

    @PostMapping("/auth/admin/v1//modifyHeadImage")
    Result modifyHeadImage(@RequestBody ModifyHeadImageForm modifyHeadImage);

    @PostMapping("/auth/admin/v1//modifySignature")
    Result modifySignature(@RequestBody ModifySignatureForm modifySignatureForm);

    @PostMapping("/login/jscode2Session")
    Result<Jscode2SessionResult> miniProgramJscode2Session(@RequestBody JscodeLoginForm jscodeLoginForm);
}
