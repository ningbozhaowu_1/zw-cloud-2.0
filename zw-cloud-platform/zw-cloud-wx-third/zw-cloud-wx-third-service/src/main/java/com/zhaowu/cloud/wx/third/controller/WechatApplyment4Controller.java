package com.zhaowu.cloud.wx.third.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.applyment.ApplymentStateQueryResult;
import com.github.binarywang.wxpay.bean.applyment.SettlementInfoResult;
import com.github.binarywang.wxpay.bean.applyment.WxPayApplyment4SubCreateRequest;
import com.github.binarywang.wxpay.bean.applyment.WxPayApplymentCreateResult;
import com.github.binarywang.wxpay.bean.media.ImageUploadResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.PayApplymentForm;
import com.zhaowu.cloud.uc.entity.vo.PayApplymentVO;
import com.zhaowu.cloud.wx.third.entity.form.CreateApplyForm;
import com.zhaowu.cloud.wx.third.entity.form.ModifySettlementForm;
import com.zhaowu.cloud.wx.third.service.WechatApplyment4Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.util.*;

/**
 * 支付商户号申请
 * @author shen
 * @date 2021/10/18
 */
@RestController
@RequestMapping("/pay/apply/v1")
@Api("微信开放平台接口")
public class WechatApplyment4Controller extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private WechatApplyment4Service wechatApplyment4Service;
    @Autowired
    private UserClient userClient;
    @PostMapping("/create/apply")
    @ApiOperation("创建申请单")
    public Result createApply(@RequestBody CreateApplyForm createApplyForm){
        try {
            logger.info("支付申请单入参：{}", JSONObject.toJSONString(createApplyForm));
            WxPayApplyment4SubCreateRequest request = createApplyForm.getWxPayApplyment4SubCreateRequest();
            String businessCode = new Date().getTime()+"";
            if(StringUtils.isNotBlank(createApplyForm.getWxPayApplyment4SubCreateRequest().getBusinessCode())){
                businessCode = createApplyForm.getWxPayApplyment4SubCreateRequest().getBusinessCode();
            }
            createApplyForm.getWxPayApplyment4SubCreateRequest().setBusinessCode(businessCode);
            ApplymentStateQueryResult applymentStateQueryResult = null;
            PayApplymentForm payApplymentForm = new PayApplymentForm();
            payApplymentForm.setStatus(0);
            Map result =new HashMap();
            if(createApplyForm.getIsSubmit() !=null && createApplyForm.getIsSubmit()){
                WxPayApplyment4SubCreateRequest createRequest = SerializationUtils.clone(createApplyForm.getWxPayApplyment4SubCreateRequest());
                WxPayApplymentCreateResult wxPayApplymentCreateResult = wechatApplyment4Service.createApply(createRequest);
                logger.info("支付申请单结果：{}", JSONObject.toJSONString(wxPayApplymentCreateResult));
                payApplymentForm.setStatus(1);
                payApplymentForm.setApplymentId(wxPayApplymentCreateResult.getApplymentId());
                applymentStateQueryResult=  wechatApplyment4Service.queryApplyStatusByBusinessCode(businessCode);
                result.put("payApplymentState",applymentStateQueryResult);
            }
            payApplymentForm.setBusinessCode(businessCode);
            payApplymentForm.setBusinessInfo(JSONObject.toJSONString(request.getBusinessInfo()));
            if(request.getAdditionInfo() !=null) {
                payApplymentForm.setAdditionInfo(JSONObject.toJSONString(request.getAdditionInfo()));
            }
            payApplymentForm.setBankAccountInfo(JSONObject.toJSONString(request.getBankAccountInfo()));
            payApplymentForm.setContactInfo(JSONObject.toJSONString(request.getContactInfo()));
            payApplymentForm.setMerchantName(JSONObject.toJSONString(request.getSubjectInfo().getBusinessLicenseInfo().getMerchantName()));
            payApplymentForm.setSettlementInfo(JSONObject.toJSONString(request.getSettlementInfo()));
            payApplymentForm.setSubjectInfo(JSONObject.toJSONString(request.getSubjectInfo()));
            userClient.saveOrUpdate(payApplymentForm);
            result.put("payApplymentVO",payApplymentForm);
            return this.success(result);
        } catch (WxPayException e) {
            logger.error("支付申请单",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        }catch (Exception e){
            logger.error("支付申请单",e);
            throw new RuntimeException(e);
        }
    }


    @GetMapping("/query/applyStatus/byBusinessCode/{businessCode}")
    @ApiOperation("通过业务申请编号查询申请信息及状态情况")
    public Result queryApplyStatusByBusinessCode(@PathVariable String businessCode) throws WxPayException {
        try {
            logger.info("支付申请单状态查询：{}", JSONObject.toJSONString(businessCode));
            Map result =new HashMap();
            PayApplymentVO payApplymentVO =  userClient.queryByBusinessCode(businessCode).getData();
            logger.info("支付申请单状态查询结果：{}", JSONObject.toJSONString(payApplymentVO));
            result.put("payApplymentVO",payApplymentVO);
            if(payApplymentVO !=null && StringUtils.isNotBlank(payApplymentVO.getApplymentId())) {
                ApplymentStateQueryResult applymentStateQueryResult = wechatApplyment4Service.queryApplyStatusByBusinessCode(businessCode);
                result.put("payApplymentState", applymentStateQueryResult);
            }
            logger.info("支付申请单状态返回结果：{}", JSONObject.toJSONString(result));
            return this.success(result);
        } catch (WxPayException e) {
            logger.error("支付申请单状态查询",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        }catch (Exception e){
            logger.error("支付申请单",e);
            throw new RuntimeException(e);
        }
    }
    @GetMapping("/list")
    @ApiOperation("查询申请列表信息")
    public Result list() {
        try {
            List<PayApplymentVO> payApplymentVOS =  userClient.list().getData();
            logger.info("查询申请列表信息结果：{}", JSONObject.toJSONString(payApplymentVOS));
            List resultList = new ArrayList();
            if(payApplymentVOS !=null && payApplymentVOS.size()>0){
                //补全状态信息
                for(PayApplymentVO payApplymentVO:payApplymentVOS){
                    Map payApplymentMap = null;
                    if(StringUtils.isNotBlank(payApplymentVO.getApplymentId())) {
                        payApplymentMap = (Map) this.queryApplyStatusByBusinessCode(payApplymentVO.getBusinessCode()).getData();
                        resultList.add(payApplymentMap);
                    }else {
                        payApplymentMap = new HashMap();
                        payApplymentMap.put("payApplymentVO",payApplymentVO);
                        resultList.add(payApplymentMap);
                    }
                }
            }
            logger.info("查询申请列表信息构建状态信息：{}", JSONObject.toJSONString(resultList));
            return this.success(resultList);
        } catch (WxPayException e) {
            logger.error("支付申请单状态查询",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        }catch (Exception e){
            logger.error("支付申请单",e);
            throw new RuntimeException(e);
        }
    }
    @GetMapping("/query/applyStatus/byApplymentId/{applymentId}")
    @ApiOperation("通过业务申请编号查询申请内容及状态信息")
    public Result queryApplyStatusByApplymentId(@PathVariable String applymentId) throws WxPayException {
        try {
            logger.info("支付申请单状态查询：{}", JSONObject.toJSONString(applymentId));
            ApplymentStateQueryResult applymentStateQueryResult=  wechatApplyment4Service.queryApplyStatusByApplymentId(applymentId);
            logger.info("支付申请单状态查询：{}", JSONObject.toJSONString(applymentStateQueryResult));
            return this.success(applymentStateQueryResult);
        } catch (WxPayException e) {
            logger.error("支付申请单状态查询",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        }catch (Exception e){
            logger.error("支付申请单",e);
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/query/{subMchId}/settlement")
    @ApiOperation("通过子商户号查询结账信息")
    public Result querySettlementBySubMchid(@PathVariable String subMchId) throws WxPayException {
        try {
            logger.info("申请单结账信息查询入参：{}", JSONObject.toJSONString(subMchId));
            SettlementInfoResult settlementInfoResult=  wechatApplyment4Service.querySettlementBySubMchid(subMchId);
            logger.info("申请单结账信息查询结果：{}", JSONObject.toJSONString(subMchId));
            return this.success(settlementInfoResult);
        } catch (WxPayException e) {
            logger.error("支付申请单结账信息",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        }catch (Exception e){
            logger.error("支付申请单结账信息",e);
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/modify/settlement")
    @ApiOperation("修改结算信息")
    public Result modifySettlement(@RequestBody ModifySettlementForm modifySettlementForm) throws WxPayException {
        try {
            logger.info("申请单结账信息查询入参：{}", JSONObject.toJSONString(modifySettlementForm));
            wechatApplyment4Service.modifySettlement(modifySettlementForm.getSubMchid(),modifySettlementForm.getModifySettlementRequest());
            return this.success(null,modifySettlementForm.getSubMchid());
        } catch (WxPayException e) {
            logger.error("支付申请单结账信息",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        }catch (Exception e){
            logger.error("支付申请单结账信息",e);
            throw new RuntimeException(e);
        }
    }
    /**
     * 上传素材文件
     */
    @PostMapping("/upload")
    @ApiOperation("上传附件")
    public Result<Map> upload(@RequestParam String path) {
        try {
            logger.info("支付申请文件上传:{}",path);
            ImageUploadResult imageUploadResult = wechatApplyment4Service.imageUploadV3(path);
            logger.info("支付申请微信文件上传:{}",imageUploadResult);
            Result result = userClient.mediaSave(imageUploadResult.getMediaId(),path);
            logger.info("支付申请media本地存储:{}",result);
            if(result.getSuccess()) {
                Map resultMap = new HashMap();
                resultMap.put("mediaId",imageUploadResult.getMediaId());
                resultMap.put("fullUrlPath",path);
                return this.success(null, result);
            }else {
                throw new RuntimeException(result.getMsg());
            }
        }catch (WxPayException e) {
            logger.error("支付申请文件上传失败",e);
            return Result.buildFailure(e.getErrCode(),e.getCustomErrorMsg());
        } catch (Exception e) {
            throw new RuntimeException("存储文件失败" + e);
        }
    }
    /**
     * 上传素材文件
     */
    @PostMapping("/uploadFile")
    @ApiOperation("上传附件")
    public Result uploadFile(@RequestPart("file") MultipartFile file) {
            logger.info("支付申请文件上传:{}",file.getOriginalFilename());
        try {
           File re= multipartFileToFile(file);
            logger.info("支付申请文件上传1:{}",re.exists());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.success(null,file.getOriginalFilename());
    }
    public static File multipartFileToFile(MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }

    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
