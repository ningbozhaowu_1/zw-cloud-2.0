package com.zhaowu.cloud.wx.third.service;

import com.github.binarywang.wxpay.bean.applyment.*;
import com.github.binarywang.wxpay.bean.media.ImageUploadResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 支付商户号申请服务
 * @author shen
 * @date 2021/10/18
 */
public interface WechatApplyment4Service {
    /**
     * 创建申请单
     * @return
     */
    WxPayApplymentCreateResult createApply(WxPayApplyment4SubCreateRequest wxPayApplyment4SubCreateRequest) throws WxPayException;


    /**
     * 通过业务申请编号查询申请状态
     */
    ApplymentStateQueryResult queryApplyStatusByBusinessCode(@PathVariable String businessCode) throws WxPayException;
    /**
     * 微信支付申请单号查询支付状态
     */
    ApplymentStateQueryResult  queryApplyStatusByApplymentId(String applymentId) throws WxPayException;
    /**
     * 通过子商户号查询结账信息
     * @param subMchId
     * @throws WxPayException
     */
    SettlementInfoResult querySettlementBySubMchid(@PathVariable String subMchId) throws WxPayException;

    /**
     * 修改结算信息
     * @throws WxPayException
     */
    void modifySettlement(String subMchid, ModifySettlementRequest request) throws WxPayException;

    /**
     * 商户图片素材上传
     * @param path
     * @return
     * @throws Exception
     */
    ImageUploadResult imageUploadV3(String path) throws Exception;
}
