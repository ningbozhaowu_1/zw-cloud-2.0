package com.zhaowu.cloud.wx.third.service;

import com.zhaowu.cloud.wx.third.error.WechatErrorException;
import com.zhaowu.cloud.wx.third.message.WechatThirdXmlMessage;

/**
 * @author xxp
 * @date 2019/12/6
 */
public interface WechatNotifyService {


	boolean checkSignature(String timestamp, String nonce, String signature);

	String route(WechatThirdXmlMessage wechatThirdXmlMessage) throws WechatErrorException;


}
