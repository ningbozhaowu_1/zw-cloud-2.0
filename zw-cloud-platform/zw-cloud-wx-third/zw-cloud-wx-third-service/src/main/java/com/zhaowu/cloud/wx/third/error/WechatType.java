package com.zhaowu.cloud.wx.third.error;

/**
 * <pre>
 *  微信类型枚举.
 * </pre>
 *
 */
public enum WechatType {
  /**
   * 企业微信
   */
  CP,
  /**
   * 微信公众号
   */
  MP,
  /**
   * 微信小程序
   */
  MiniApp,
  /**
   * 微信开放平台
   */
  Open,
  /**
   * 微信支付
   */
  Pay;
}
