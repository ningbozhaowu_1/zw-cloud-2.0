package com.zhaowu.cloud.wx.third.event;

import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.cache.entity.CacheMessageWithName;
import com.zhaowu.cloud.framework.cache.events.BusCacheReceiver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BusReceiver {

    @Autowired
    private BusCacheReceiver busCacheReceiver;

    public void handleMessage(BusMessage message) {
        log.info("Received Message:<{}>", message);

        if(CommonConstant.InfoType.CACHE_UPDATE.equals(message.getInfoType())){
            CacheMessageWithName cacheMessageWithName = JSON.parseObject(JSON.toJSONString(message.getJsonObject()),CacheMessageWithName.class);
            //添加到线程池进行处理
            busCacheReceiver.handleMessage(cacheMessageWithName);
        }
    }
}