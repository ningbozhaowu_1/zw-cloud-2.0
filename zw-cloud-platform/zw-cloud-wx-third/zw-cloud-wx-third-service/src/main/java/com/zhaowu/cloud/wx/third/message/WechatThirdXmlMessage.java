package com.zhaowu.cloud.wx.third.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zhaowu.cloud.framework.util.XmlUtil;
import com.zhaowu.cloud.framework.util.crypto.WechatCryptUtil;
import com.zhaowu.cloud.wx.third.properties.WechatThirdProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

/**
 * @author <a href="https://github.com/007gzs">007</a>
 */
@Data
@Slf4j
@XStreamAlias("xml")
public class WechatThirdXmlMessage implements Serializable {
  private static final long serialVersionUID = 1L;

  @XStreamAlias("appid")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String appId;

  @XStreamAlias("auth_code")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String authCode;

  @XStreamAlias("CreateTime")
  private Long createTime;

  @XStreamAlias("InfoType")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String infoType;

  @XStreamAlias("ComponentVerifyTicket")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String componentVerifyTicket;

  @XStreamAlias("AuthorizerAppid")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String authorizerAppid;

  @XStreamAlias("AuthorizationCode")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String authorizationCode;

  @XStreamAlias("AuthorizationCodeExpiredTime")
  private Long authorizationCodeExpiredTime;

  @XStreamAlias("PreAuthCode")
//  @XStreamConverter(value = XStreamCDataConverter.class)
  private String preAuthCode;

  @XStreamAlias("info")
  private WechatThirdXmlInfoMessage info;


  /**
   * 从加密字符串转换
   *
   * @param encryptedXml        密文
   * @param wechatThirdProperties 配置存储器对象
   * @param timestamp           时间戳
   * @param nonce               随机串
   * @param msgSignature        签名串
   */
  public static WechatThirdXmlMessage fromEncryptedXml(String encryptedXml, WechatThirdProperties wechatThirdProperties,
                                                       String timestamp, String nonce, String msgSignature) {
    WechatCryptUtil cryptUtil = new WechatCryptUtil(wechatThirdProperties.getComponentToken(), wechatThirdProperties.getComponentAesKey(), wechatThirdProperties.getComponentAppId());
    String plainText = cryptUtil.decrypt(msgSignature, timestamp, nonce, encryptedXml);
    log.info("解密后的原始xml消息内容：{}", plainText);
    return XmlUtil.toBean(plainText, WechatThirdXmlMessage.class);
  }


  public static WechatThirdXmlMessage fromEncryptedXml(InputStream is, WechatThirdProperties wechatThirdProperties,
                                                       String timestamp, String nonce, String msgSignature) {
    try {
      return fromEncryptedXml(IOUtils.toString(is, "UTF-8"),
              wechatThirdProperties, timestamp, nonce, msgSignature);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

//  public static void main(String[] args) {
//    String xml = "<xml><AppId><![CDATA[wx682aaad9d95794f8]]></AppId>\n" +
//            "<CreateTime>1621651929</CreateTime>\n" +
//            "<InfoType><![CDATA[notify_third_fasteregister]]></InfoType><status>0</status>\n" +
//            "<msg><![CDATA[OK]]></msg>\n" +
//            "<appid><![CDATA[wxb823cfab9c3070df]]></appid>\n" +
//            "<auth_code><![CDATA[queryauthcode@@@ScCZ6Qu4YUN_KOMzcEikJAS-VAt2uqqFTSGnv3oRg0lMB_jW4nnWxOPfz60zeIjp9AmMeh8Fm2AkMNwmpD_Veg]]></auth_code>\n" +
//            "<info>\n" +
//            "<name><![CDATA[台州市开源信息咨询有限公司]]></name>\n" +
//            "<code><![CDATA[91331001344035864D]]></code>\n" +
//            "<code_type>1</code_type>\n" +
//            "<legal_persona_wechat><![CDATA[fssamzhao]]></legal_persona_wechat>\n" +
//            "<legal_persona_name><![CDATA[赵桂森]]></legal_persona_name>\n" +
//            "<component_phone><![CDATA[13486880913]]></component_phone>\n" +
//            "</info>\n" +
//            "</xml>";
//    WechatThirdXmlMessage wechatThirdXmlMessage=XmlUtil.toBean(xml, WechatThirdXmlMessage.class);
//    System.out.println(wechatThirdXmlMessage.getInfo().getCode());
//  }
}
