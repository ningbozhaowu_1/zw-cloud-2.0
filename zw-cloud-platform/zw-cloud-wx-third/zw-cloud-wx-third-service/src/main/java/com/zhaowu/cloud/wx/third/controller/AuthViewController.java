package com.zhaowu.cloud.wx.third.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.PartyChannelAuthorizeForm;
import com.zhaowu.cloud.wx.third.entity.model.auth.ApiGetAuthorizerInfo;
import com.zhaowu.cloud.wx.third.error.WechatErrorException;
import com.zhaowu.cloud.wx.third.service.WechatThirdAuthService;
import com.zhaowu.cloud.wx.third.entity.model.auth.ApiQueryAuthInfo;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xxp
 * @date 2019/11/7
 * 微信授权流程
 */
@Controller
@RequestMapping("/view")
@ApiIgnore
public class AuthViewController extends BaseController {


	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WechatThirdAuthService wechatThirdAuthService;

	@Autowired
	private UserClient userClient;

	/**
	 * 一键授权按钮
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/goto_auth_url_show")
	@ApiOperation("一键授权")
	public String gotoPreAuthUrlShow(HttpServletResponse response) throws Exception{
		return "index";
	}

	@GetMapping("/applyShow")
	@ApiOperation("一键授权")
	public String applyShow(HttpServletResponse response) throws Exception{
		return "apply";
	}

	/**
	 * 授权注册页面扫码授权   显示二维码
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/goto_auth_url", method = RequestMethod.GET)
	@ApiOperation("授权注册页面扫码授权   显示二维码")
	public void gotoPreAuthUrl(HttpServletRequest request, HttpServletResponse response) throws WechatErrorException {

		String applicationParty = request.getParameter("applicationParty");
		String host = "cloud.zhaowu.cc";
		String url = "http://"+host+"/wx-third-service/view/jump";
		if(applicationParty==null){
			url +="?applicationParty=1";
		}else{
			url +="?applicationParty="+applicationParty;
		}
		try {
			url = wechatThirdAuthService.getPreAuthUrl(url, null, null);
			logger.info("获取授权跳转链接applicationParty:{},getPreAuthUrl:{}", applicationParty,url);
			response.sendRedirect(url);
		} catch (WechatErrorException | IOException e) {
			logger.error("gotoPreAuthUrl", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * 授权成功回调链接
	 * @param authorizationCode
	 * 这边可以直接 重定向自己前台的页面   也可判断权限是否全部勾选完成  授权完成后把授权小程序的信息保存好
	 * @return
	 */
	@GetMapping("/jump")
	public String jump(@RequestParam("applicationParty") String applicationParty,
                       @RequestParam("auth_code") String authorizationCode,
                       @RequestParam(value = "expires_in", required = false) Integer expiresIn, HttpServletResponse response){

//		System.out.println(applicationParty);
		try {
			ApiQueryAuthInfo apiQueryAuthInfo = wechatThirdAuthService.getApiQueryAuthInfo(authorizationCode);
			PartyChannelAuthorizeForm partyChannelAuthorizeForm = new PartyChannelAuthorizeForm();
			partyChannelAuthorizeForm.setApplicationParty(applicationParty);
			partyChannelAuthorizeForm.setType("wechatMA");
			partyChannelAuthorizeForm.setAuthCode(authorizationCode);
			partyChannelAuthorizeForm.setRefreshToken(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerRefreshToken());
			partyChannelAuthorizeForm.setAuthorizerAppid(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAppid());
			partyChannelAuthorizeForm.setAccessToken(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAccessToken());
			ApiGetAuthorizerInfo apiGetAuthorizerInfo = wechatThirdAuthService.apiGetAuthorizerInfo(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAppid());
			partyChannelAuthorizeForm.setNickName(apiGetAuthorizerInfo.getAuthorizerInfo().getNickName());
			partyChannelAuthorizeForm.setIntroduction(apiGetAuthorizerInfo.getAuthorizerInfo().getSignature());
			partyChannelAuthorizeForm.setAvatar(apiGetAuthorizerInfo.getAuthorizerInfo().getQrcodeUrl());
			this.logger.info("授权新增流程参数:{} ", partyChannelAuthorizeForm);
			userClient.authorizeAdd(partyChannelAuthorizeForm);
			return "success";
		} catch (WechatErrorException e) {
			logger.error("jump", e);
			throw new RuntimeException(e);
		}
	}

}
