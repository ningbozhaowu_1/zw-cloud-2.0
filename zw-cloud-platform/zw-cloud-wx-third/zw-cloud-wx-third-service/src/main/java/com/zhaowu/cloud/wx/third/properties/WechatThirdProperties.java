package com.zhaowu.cloud.wx.third.properties;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xxp
 * @date 2019/12/4
 */
@Data
@Component
@ConfigurationProperties(prefix = "wechat.third")
public class WechatThirdProperties {

	/**
	 * 设置微信三方平台的appid
	 */
	private String componentAppId;

	/**
	 * 设置微信三方平台的app secret
	 */
	private String componentSecret;

	/**
	 * 设置微信三方平台的token
	 */
	private String componentToken;

	/**
	 * 设置微信三方平台的EncodingAESKey
	 */
	private String componentAesKey;

	private List<String> webViewDomain;
	
	private List<String> sockceDomain;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
