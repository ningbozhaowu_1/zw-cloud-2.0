package com.zhaowu.cloud.wx.third.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.Applyment4SubService;
import com.github.binarywang.wxpay.service.MerchantMediaService;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.Applyment4SubServiceImpl;
import com.github.binarywang.wxpay.service.impl.MerchantMediaServiceImpl;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.zhaowu.cloud.framework.pay.entity.WxPayBean;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 集成微信支付进件开源实现
 * @author shen
 */
@Configuration
@ConditionalOnClass(Applyment4SubService.class)
@AllArgsConstructor
public class ApplySubConfiguration {

  @Autowired
  private WxPayBean wxPayBean;

  @Bean
  @ConditionalOnMissingBean
  public Applyment4SubService wxService() {
    Applyment4SubService applyment4SubService  = new Applyment4SubServiceImpl(getWxPayService());
    return applyment4SubService;
  }
  @Bean
  @ConditionalOnMissingBean
  public MerchantMediaService wxMediaService() {

    MerchantMediaService applyment4SubService  = new MerchantMediaServiceImpl(getWxPayService());
    return applyment4SubService;
  }
  private WxPayService getWxPayService(){
    WxPayConfig payConfig = new WxPayConfig();
    payConfig.setAppId(StringUtils.trimToNull(this.wxPayBean.getAppId()));
    payConfig.setMchId(StringUtils.trimToNull(this.wxPayBean.getMchId()));
    payConfig.setMchKey(StringUtils.trimToNull(this.wxPayBean.getPartnerKey()));
    payConfig.setSubAppId(StringUtils.trimToNull(this.wxPayBean.getSubAppId()));
    payConfig.setSubMchId(StringUtils.trimToNull(this.wxPayBean.getSubMchId()));
    payConfig.setKeyPath(StringUtils.trimToNull(this.wxPayBean.getCertPath()));
    payConfig.setPrivateKeyPath(StringUtils.trimToNull(this.wxPayBean.getPrivateKeyPath()));
    payConfig.setPrivateCertPath(StringUtils.trimToNull(this.wxPayBean.getPrivateCertPath()));
    payConfig.setApiV3Key(StringUtils.trimToNull(this.wxPayBean.getApiV3Key()));
    payConfig.setCertSerialNo(StringUtils.trimToNull(this.wxPayBean.getCertSerialNo()));


    // 可以指定是否使用沙箱环境
    payConfig.setUseSandboxEnv(false);

    WxPayService wxPayService = new WxPayServiceImpl();
    wxPayService.setConfig(payConfig);
    return wxPayService;
  }
}
