package com.zhaowu.cloud.wx.third.message;

import com.google.common.collect.Lists;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zhaowu.cloud.framework.util.XmlUtil;
import com.zhaowu.cloud.framework.util.crypto.WechatCryptUtil;
import com.zhaowu.cloud.wx.third.properties.WechatThirdProperties;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@XStreamAlias("xml")
@Data
@Slf4j
@Builder
public class WechatMpXmlOutMessage implements Serializable {
  private static final long serialVersionUID = 1L;

  @XStreamAlias("ToUserName")
  protected String toUserName;

  @XStreamAlias("FromUserName")
  protected String fromUserName;

  @XStreamAlias("CreateTime")
  protected Long createTime;

  @XStreamAlias("MsgType")
  protected String msgType;

  @XStreamAlias("Content")
  protected String content;

  public static String wechatMpOutXmlMessageToEncryptedXml(WechatMpXmlOutMessage message, WechatThirdProperties wechatThirdProperties) {
    String plainXml = XmlUtil.toXmlWithCData(message,Lists.newArrayList("CreateTime"));
    log.info("加密后的原始xml消息内容：{}", plainXml);
    WechatCryptUtil pc = new WechatCryptUtil(wechatThirdProperties.getComponentToken(), wechatThirdProperties.getComponentAesKey(), wechatThirdProperties.getComponentAppId());
    return pc.encrypt(plainXml);
  }


//  public static void main(String[] args){
//    Long createTime = Calendar.getInstance().getTimeInMillis() / 1000;
//    WechatMpXmlOutMessage wechatMpXmlOutMessage=WechatMpXmlOutMessage.builder()
//            .content("TESTCOMPONENT_MSG_TYPE_TEXT_callback")
//            .fromUserName("ozy4qt0Rsc9YJzR5nEeVAaTHg9DQ")
//            .toUserName("gh_3c884a361561")
//            .createTime(createTime)
//            .msgType("text")
//            .build();
//    WechatThirdProperties wechatThirdProperties=new WechatThirdProperties();
//    wechatThirdProperties.setComponentAppId("wxc063640eb0fd5cce");
//    wechatThirdProperties.setComponentSecret("09fe375e0dfca184c7fe62865a16054f");
//    wechatThirdProperties.setComponentAesKey("asasasasasasasasasasasasasasasasasasasasasa");
//    wechatThirdProperties.setComponentToken("RPQLqJadlaXcrhbt");
//    String wechatMpXmlMessage=wechatMpOutXmlMessageToEncryptedXml(wechatMpXmlOutMessage,wechatThirdProperties);
//    System.out.println(wechatMpXmlMessage.toString());
//  }

}
