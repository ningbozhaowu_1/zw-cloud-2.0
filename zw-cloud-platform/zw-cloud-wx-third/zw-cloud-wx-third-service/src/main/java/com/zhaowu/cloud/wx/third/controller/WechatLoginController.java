package com.zhaowu.cloud.wx.third.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.wx.third.error.WechatErrorException;
import com.zhaowu.cloud.wx.third.service.WechatThirdMiniProgramService;
import com.zhaowu.cloud.wx.third.entity.form.JscodeLoginForm;
import com.zhaowu.cloud.wx.third.entity.model.xcx.Jscode2SessionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * @author xxp
 * @date 2019/11/7
 * 微信授权流程
 */
@RestController
@RequestMapping("/login")
@ApiIgnore
public class WechatLoginController extends BaseController {


	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WechatThirdMiniProgramService wechatThirdMiniProgramService;

	/**
	 * 第三方体系登录
	 */
	@PostMapping("/jscode2Session")
	public Result<Jscode2SessionResult> miniProgramJscode2Session(@Valid @RequestBody JscodeLoginForm jscodeLoginForm){

		Jscode2SessionResult result = null;
		try {
			result = wechatThirdMiniProgramService.miniProgramJscode2Session(jscodeLoginForm.getAuthorizerAppid(), jscodeLoginForm.getJsCode());
		}catch (WechatErrorException e){
			logger.error(e.getMessage());
			return this.failure(e.getError().getErrCode().toString(),e.getError().getErrMsg());
		}
		return this.success(result);
	}

}
