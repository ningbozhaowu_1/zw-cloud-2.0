package com.zhaowu.cloud.wx.third;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
/**
 * @author xxp
 **/
@EnableApolloConfig
@EnableFeignClients("com.zhaowu.cloud")
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableMethodCache(basePackages = {"com.zhaowu.cloud", "com.baomidou"})
@EnableCreateCacheAnnotation
@ComponentScan(basePackages = {"com.zhaowu.cloud", "com.zhaowu.aspect", "com.alicp.jetcache.autoconfigure"})
public class WxThirdApplication {

    public static void main(String[] args){
        SpringApplication.run(WxThirdApplication.class, args);
    }

}
