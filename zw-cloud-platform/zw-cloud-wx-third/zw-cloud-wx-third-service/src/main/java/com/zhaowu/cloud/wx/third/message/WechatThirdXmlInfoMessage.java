package com.zhaowu.cloud.wx.third.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zhaowu.cloud.framework.util.XmlUtil;
import com.zhaowu.cloud.framework.util.crypto.WechatCryptUtil;
import com.zhaowu.cloud.wx.third.properties.WechatThirdProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * @author <a href="https://github.com/007gzs">007</a>
 */
@Data
@Slf4j
@XStreamAlias("xml")
public class WechatThirdXmlInfoMessage implements Serializable {

  private static final long serialVersionUID = 1L;

  @XStreamAlias("code_type")
  private String codeType;

  @XStreamAlias("code")
  private String code;
}
