package com.zhaowu.cloud.wx.third.controller;

import com.zhaowu.cloud.framework.util.FastJsonUtils;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.PartyChannelAuthorizeForm;
import com.zhaowu.cloud.wx.third.constant.WechatConstant;
import com.zhaowu.cloud.wx.third.entity.model.auth.ApiGetAuthorizerInfo;
import com.zhaowu.cloud.wx.third.entity.model.auth.ApiQueryAuthInfo;
import com.zhaowu.cloud.wx.third.error.WechatErrorException;
import com.zhaowu.cloud.wx.third.message.WechatMpXmlMessage;
import com.zhaowu.cloud.wx.third.message.WechatMpXmlOutMessage;
import com.zhaowu.cloud.wx.third.message.WechatThirdXmlMessage;
import com.zhaowu.cloud.wx.third.properties.WechatThirdProperties;
import com.zhaowu.cloud.wx.third.service.WechatNotifyService;
import com.zhaowu.cloud.wx.third.service.WechatThirdAuthService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xxp
 * @date 2019/12/4
 */
@RestController
@RequestMapping("/notify")
@ApiIgnore
public class WechatNotifyController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Autowired
	private WechatNotifyService wechatNotifyService;
	@Autowired
	private WechatThirdProperties wechatThirdProperties;
	@Autowired
	private WechatThirdAuthService wechatThirdAuthService;

	@Autowired
	private UserClient userClient;

	/**
	 * 微信授权事件的接收
	 * @param requestBody
	 * @param timestamp
	 * @param nonce
	 * @param signature
	 * @param encType
	 * @param msgSignature
	 * @return
	 */
	@RequestMapping("/receive_ticket")
	public Object receiveTicket(@RequestBody(required = false) String requestBody, @RequestParam("timestamp") String timestamp,
                                @RequestParam("nonce") String nonce, @RequestParam("signature") String signature,
                                @RequestParam(name = "encrypt_type", required = false) String encType,
                                @RequestParam(name = "msg_signature", required = false) String msgSignature) {
		this.logger.info(
				"\n接收微信请求：[signature=[{}], encType=[{}], msgSignature=[{}],"
						+ " timestamp=[{}], nonce=[{}], requestBody=[\n{}\n] ",
				signature, encType, msgSignature, timestamp, nonce, requestBody);

		if (!StringUtils.equalsIgnoreCase("aes", encType)
				|| !wechatNotifyService.checkSignature(timestamp, nonce, signature)) {
			throw new IllegalArgumentException("非法请求，可能属于伪造的请求！");
		}

		// aes加密的消息
		WechatThirdXmlMessage inMessage = WechatThirdXmlMessage.fromEncryptedXml(requestBody,
				wechatThirdProperties, timestamp, nonce, msgSignature);
		this.logger.info("消息解密后内容为:{} ", inMessage.toString());

		try {

			if(StringUtils.equals(inMessage.getInfoType(), WechatConstant.NOTIFY_THIRD_FASTEREGISTER)){
				ApiQueryAuthInfo apiQueryAuthInfo = wechatThirdAuthService.getApiQueryAuthInfo(inMessage.getAuthCode());
				PartyChannelAuthorizeForm modifyPartyChannelForm = new PartyChannelAuthorizeForm();
				modifyPartyChannelForm.setAccessToken(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAccessToken());
				modifyPartyChannelForm.setRefreshToken(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerRefreshToken());
				modifyPartyChannelForm.setAuthorizerAppid(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAppid());
				modifyPartyChannelForm.setCode(inMessage.getInfo().getCode());
				modifyPartyChannelForm.setCodeType(inMessage.getInfo().getCodeType());
				modifyPartyChannelForm.setAuthCode(inMessage.getAuthorizationCode());
				ApiGetAuthorizerInfo apiGetAuthorizerInfo = wechatThirdAuthService.apiGetAuthorizerInfo(apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAppid());
				modifyPartyChannelForm.setNickName(apiGetAuthorizerInfo.getAuthorizerInfo().getNickName());
				modifyPartyChannelForm.setIntroduction(apiGetAuthorizerInfo.getAuthorizerInfo().getSignature());
				modifyPartyChannelForm.setAvatar(apiGetAuthorizerInfo.getAuthorizerInfo().getQrcodeUrl());
				this.logger.info("授权更新流程参数:{} ", modifyPartyChannelForm);
				userClient.authorizeUpdate(modifyPartyChannelForm);
			}

			String out = wechatNotifyService.route(inMessage);
			this.logger.info("\n组装回复信息：{}", out);
		} catch (WechatErrorException e) {
			this.logger.error("receive_ticket", e);
		}


		return "success";
	}


	/**
	 * 消息与事件接收URL
	 * @param requestBody
	 * @param appId
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @param openid
	 * @param encType
	 * @param msgSignature
	 * @return
	 */
	@RequestMapping(value ="/{appId}/callback", method = RequestMethod.POST)
	public Object callback(@RequestBody(required = false) String requestBody,
						   @PathVariable("appId") String appId,
						   @RequestParam("signature") String signature,
						   @RequestParam("timestamp") String timestamp,
						   @RequestParam("nonce") String nonce,
						   @RequestParam("openid") String openid,
						   @RequestParam("encrypt_type") String encType,
						   @RequestParam("msg_signature") String msgSignature) {
		this.logger.info(
				"\n callback接收微信请求：[appId=[{}], openid=[{}], signature=[{}], encType=[{}], msgSignature=[{}],"
						+ " timestamp=[{}], nonce=[{}], requestBody=[\n{}\n] ",
				appId, openid, signature, encType, msgSignature, timestamp, nonce, requestBody);
		if (!StringUtils.equalsIgnoreCase("aes", encType)
				|| !wechatNotifyService.checkSignature(timestamp, nonce, signature)) {
			throw new IllegalArgumentException("非法请求，可能属于伪造的请求！");
		}
		String out = "";
		// aes加密的消息
		WechatMpXmlMessage wechatMpXmlMessage=WechatMpXmlMessage.fromEncryptedMpXml(requestBody, wechatThirdProperties,timestamp,nonce,msgSignature);
		this.logger.info("\n消息解密后内容为：\n{} ", wechatMpXmlMessage.toString());
		// 全网发布测试用例
		try {
			if (StringUtils.equals(wechatMpXmlMessage.getMsgType(), "text")) {
				/*返回普通文本信息*/
				if (StringUtils.equals(wechatMpXmlMessage.getContent(), "TESTCOMPONENT_MSG_TYPE_TEXT")) {
					String returnContent = wechatMpXmlMessage.getContent()+"_callback";
					Long createTime = Calendar.getInstance().getTimeInMillis() / 1000;
					WechatMpXmlOutMessage wechatMpXmlOutMessage= WechatMpXmlOutMessage.builder()
							.content(returnContent)
							.fromUserName(wechatMpXmlMessage.getToUser())
							.toUserName(wechatMpXmlMessage.getFromUser())
							.createTime(createTime)
							.msgType("text")
							.build();
					out= WechatMpXmlOutMessage.wechatMpOutXmlMessageToEncryptedXml(wechatMpXmlOutMessage, wechatThirdProperties);
				}else if (StringUtils.startsWith(wechatMpXmlMessage.getContent(), "QUERY_AUTH_CODE:")) {
					/*全网发布返回api文本信息*/
					String authCode=wechatMpXmlMessage.getContent().split(":")[1];
					ApiQueryAuthInfo apiQueryAuthInfo= wechatThirdAuthService.getApiQueryAuthInfo(authCode);
					Map<String,Object> obj = new HashMap<String,Object>();
					Map<String,Object> msgMap = new HashMap<String,Object>();
					String msg = authCode + "_from_api";
					msgMap.put("content", msg);
					obj.put("touser", wechatMpXmlMessage.getFromUser());
					obj.put("msgtype", "text");
					obj.put("text", msgMap);
					out = wechatThirdAuthService.sendMessage(FastJsonUtils.map2json(obj), apiQueryAuthInfo.getAuthorizationInfo().getAuthorizerAccessToken());
				}else {
					out = "SUCCESS";
				}
			} else if (StringUtils.equals(wechatMpXmlMessage.getMsgType(), "event")) {
				/*发送事件信息(返回文本信息)*/
				String content = wechatMpXmlMessage.getEvent() + "from_callback";
				Long createTime = Calendar.getInstance().getTimeInMillis() / 1000;
				WechatMpXmlOutMessage wechatMpXmlOutMessage= WechatMpXmlOutMessage.builder()
						.content(content)
						.fromUserName(wechatMpXmlMessage.getToUser())
						.toUserName(wechatMpXmlMessage.getFromUser())
						.createTime(createTime)
						.msgType("text")
						.build();
				out= WechatMpXmlOutMessage.wechatMpOutXmlMessageToEncryptedXml(wechatMpXmlOutMessage, wechatThirdProperties);
			}
		}catch (Exception e) {
			logger.error("callback", e);
		}
		logger.info("\n消息与事件接收URL：out=[{}] ",out);
		return out;
	}
}
