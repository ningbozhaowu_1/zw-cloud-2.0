package com.zhaowu.cloud.wx.third.controller;

import com.google.common.collect.Lists;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.PageListResult;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.FastJsonUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.PartyChannelQueryForm;
import com.zhaowu.cloud.uc.entity.vo.PartyChannelVO;
import com.zhaowu.cloud.wx.third.error.WechatErrorException;
import com.zhaowu.cloud.wx.third.properties.WechatThirdProperties;
import com.zhaowu.cloud.wx.third.service.WechatThirdAuthService;
import com.zhaowu.cloud.wx.third.service.WechatThirdMiniProgramService;
import com.zhaowu.cloud.wx.third.entity.form.CreateForm;
import com.zhaowu.cloud.wx.third.entity.form.ModifyHeadImageForm;
import com.zhaowu.cloud.wx.third.entity.form.ModifyNickNameForm;
import com.zhaowu.cloud.wx.third.entity.form.ModifySignatureForm;
import com.zhaowu.cloud.wx.third.entity.model.auth.ApiSetAuthorizerOption;
import com.zhaowu.cloud.wx.third.entity.model.xcx.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * @author xxp
 * @date 2019/11/7
 * 微信授权流程
 */
@RestController
@RequestMapping("/auth/admin/v1")
@Api("微信开放平台接口")
public class WechatThirdAuthController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Autowired
	private WechatThirdProperties wechatThirdProperties;
	@Autowired
	private WechatThirdAuthService wechatThirdAuthService;
	@Autowired
	private WechatThirdMiniProgramService wechatThirdMiniProgramService;

	@Autowired
	private UserClient userClient;

	@GetMapping("/getAllCategoryList")
	@ApiOperation("获取类目列表")
	public Map getAllCategoryList(@RequestParam("applicationParty") String applicationParty,
								  @RequestParam("authorizerAppId") String authorizerAppId){
		try {
			Map categoriesResultList = wechatThirdMiniProgramService.getAllCategoryList(applicationParty, authorizerAppId);
			logger.info("getAllCategoryList", categoriesResultList);
			return categoriesResultList;
		} catch (WechatErrorException e) {
			logger.error("getAllCategoryList", e);
			throw new RuntimeException(e);
		}
	}
	
	@GetMapping("/getSettedCategory")
	@ApiOperation("获取账号已经设置的所有类目")
	public Map getSettedCategory(@Valid @RequestParam("applicationParty") String applicationParty,
								 @Valid @RequestParam("authorizerAppId") String authorizerAppId){
		try {
			Map getSettedCategory = wechatThirdMiniProgramService.getSettedCategory(applicationParty, authorizerAppId);
			logger.info("getSettedCategory", getSettedCategory);
			return getSettedCategory;
		} catch (WechatErrorException e) {
			logger.error("getSettedCategory", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/addCategory")
	@ApiOperation("添加类目")
	public Result addCategory(@Valid @RequestParam("applicationParty") String applicationParty,
                              @Valid @RequestParam("authorizerAppId") String authorizerAppId,
                              @Valid @RequestBody List<Category> categories){
		try {
			Result result = wechatThirdMiniProgramService.addCategory(applicationParty, authorizerAppId, categories);
			logger.info("addCategory", result);
			return result;
		} catch (WechatErrorException e) {
			logger.error("addCategory", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/modifyCategory")
	@ApiOperation("修改类目")
	public Result modifyCategory(@Valid @RequestParam("applicationParty") String applicationParty,
								 @Valid @RequestParam("authorizerAppId") String authorizerAppId,
								 @Valid @RequestBody Category category){
		try {
			Result result = wechatThirdMiniProgramService.modifyCategory(applicationParty, authorizerAppId, category);
			logger.info("addCategory", result);
			return result;
		} catch (WechatErrorException e) {
			logger.error("addCategory", e);
			throw new RuntimeException(e);
		}
	}
	
	@GetMapping("/delCategory")
	@ApiOperation("删除类目")
	public Result delCategory(@Valid @RequestParam("applicationParty") String applicationParty,
							  @Valid @RequestParam("authorizerAppId") String authorizerAppId,
							  @Valid @RequestParam("first") String first,
							  @Valid @RequestParam("second") String second){
		try {
			Result result = wechatThirdMiniProgramService.delCategory(applicationParty, authorizerAppId, first, second);
			logger.info("delCategory", result);
			return result;
		} catch (WechatErrorException e) {
			logger.error("delCategory", e);
			throw new RuntimeException(e);
		}
	}

	@PostMapping("/setNickName")
	@ApiOperation("设置名称")
	public AuditSetNickNameResult setNickName(@Valid @RequestBody ModifyNickNameForm modifyNickNameForm){
		try {
			AuditSetNickNameResult auditSetNickNameResult = wechatThirdMiniProgramService.setNickName(modifyNickNameForm.getApplicationParty(), modifyNickNameForm.getAuthorizerAppId(), modifyNickNameForm.getNickName(), modifyNickNameForm.getLicense());
			logger.info("auditSetNickNameResult", auditSetNickNameResult);
			return auditSetNickNameResult;
		} catch (WechatErrorException e) {
			logger.error("setNickName", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/modifyHeadImage")
	@ApiOperation("修改头像")
	public Result modifyHeadImage(@Valid @RequestBody ModifyHeadImageForm modifyHeadImageForm){
		try {
			Result result = wechatThirdMiniProgramService.modifyHeadImage(modifyHeadImageForm.getApplicationParty(), modifyHeadImageForm.getAuthorizerAppId(), modifyHeadImageForm.getHeadImageMediaId(), modifyHeadImageForm.getX1(), modifyHeadImageForm.getY1(), modifyHeadImageForm.getX2(), modifyHeadImageForm.getY2());
			logger.info("result", result);
			return result;
		} catch (WechatErrorException e) {
			logger.error("modifyHeadImage", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/modifySignature")
	@ApiOperation("修改简介")
	public Result modifySignature(@Valid @RequestBody ModifySignatureForm modifySignatureForm){
		try {
			Result result = wechatThirdMiniProgramService.modifySignature(modifySignatureForm.getApplicationParty(), modifySignatureForm.getAuthorizerAppId(), modifySignatureForm.getSignature());
			logger.info("modifySignature:{}", result);
			return result;
		} catch (WechatErrorException e) {
			logger.error("modifySignature", e);
			throw new RuntimeException(e);
		}
	}
	
	@GetMapping("/upload")
	@ApiOperation("上传素材")
	public AuditUploadMediaResult upload(@RequestParam("applicationParty") String applicationParty,
                                         @RequestParam("authorizerAppId") String authorizerAppId,
                                         @RequestParam("path") String path,
                                         @RequestParam(value = "type") String type){
		try {
			AuditUploadMediaResult auditUploadMediaResult = wechatThirdMiniProgramService.uploadMedia(applicationParty, authorizerAppId, path, type);
			logger.info("upload", auditUploadMediaResult);
			return auditUploadMediaResult;
		} catch (WechatErrorException e) {
			logger.error("upload", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return
	 */
	@PostMapping("/create")
	@ApiIgnore
	public Result create(@Valid @RequestBody CreateForm createForm){

//		return this.success();

		try {
			Map<String,Object> obj = new HashMap<String,Object>();
			obj.put("name", createForm.getName());
			obj.put("code", createForm.getCode());
			obj.put("code_type", createForm.getCodeType());
			obj.put("legal_persona_wechat", createForm.getLegalPersonaWechat());
			obj.put("legal_persona_name", createForm.getLegalPersonaName());
			obj.put("component_phone", createForm.getComponentPhone());
			logger.info("应用创建入参数:{}", obj);
			ApiSetAuthorizerOption apiSetAuthorizerOption = wechatThirdAuthService.createApp(FastJsonUtils.map2json(obj));
			logger.info("应用创建结果:{}", apiSetAuthorizerOption);
			if(apiSetAuthorizerOption.getErrcode()==0){
				return this.success();
			}else{
				return this.failure();
			}
		} catch (WechatErrorException e) {
			logger.error("create", e);
			return this.failure(e.getError().getErrMsg());
		}
	}

	/**
	 * 授权完成后可提交审核
	 */
	@GetMapping("/commit")
	@ApiOperation("微信小程序提交审核")
//	@ApiIgnore
	public Result commit(@Valid @RequestParam("partyChannelId") String partyChannelId) throws Exception {


		String applicationParty = UserContextHolder.getInstance().getApplicationParty();
		PartyChannelQueryForm applicationChannelQueryForm = new PartyChannelQueryForm();
		applicationChannelQueryForm.setPartyChannelId(partyChannelId);
		PartyChannelVO applicationChannelVO = userClient.getPartyChannel(applicationChannelQueryForm).getData();

		if(applicationChannelVO==null
				|| applicationChannelVO.getAuthorizerAppId()==null){
			return this.failure("该渠道暂未成功发布");
		}

		/**
		 * 1、使用token调用微信接口modify_domain更换小程序的服务器域名，以便代替微信对小程序或公众号进行运营管理。
		 */
		ApiModifyDomainInfo apiModifyDomainInfo = wechatThirdMiniProgramService.modifyDomain(applicationParty, applicationChannelVO.getAuthorizerAppId(), "set", wechatThirdProperties.getWebViewDomain(), wechatThirdProperties.getSockceDomain(), wechatThirdProperties.getWebViewDomain(), wechatThirdProperties.getWebViewDomain());


		/**
		 * 2、在微信第三方平台设置页面里上传一个开发小程序的代码的，并添加为模板，取得模板ID
		 */
		List<TemplateListResult> templateListResultList = wechatThirdMiniProgramService.getTemplateList();
		/**
		 * 取最新的一个模板
		 * //TODO 模版数据库维护存储
		 */
		Collections.sort(templateListResultList, Comparator.comparing(TemplateListResult::getCreateTime).reversed());
		TemplateListResult template = templateListResultList.get(0);


		/**
		 * 3、在第三方平台开发程序中调用微信接口/wxa/commit传入参数：ext_json
		 */
		Map<String, Object> map = new HashMap<>();
		map.put("applicationParty", applicationParty);
		map.put("channelId", partyChannelId);
		ApiCodeCommitExtParameter apiCodeCommitExtParameter = ApiCodeCommitExtParameter
				.builder().extAppid(applicationChannelVO.getAuthorizerAppId())
				.extEnable(true)
				.directCommit(false)
				.ext(map)
				//.extPages(pageMap)
				//.pages(Lists.newArrayList("index","search/index"))
				//.networkTimeout(ApiCodeCommitExtParameter.NetworkTimeout.builder().build())
				//.tabBar(ApiCodeCommitExtParameter.TabBar.builder().build())
//				.window(ApiCodeCommitExtParameter.Window
//						.builder()
//						.navigationBarTitleText("自动创建的")
//						.build())
				.build();

		/**
		 * 4、在第三方平台开发程序中调用微信接口/wxa/commit传入参数：ext_json
		 */
		ApiSetWebviewDomainInfo apiSetWebviewDomainInfo = wechatThirdMiniProgramService.codeCommit(applicationParty, applicationChannelVO.getAuthorizerAppId(), template.getTemplateId(), template.getUserVersion(), template.getUserDesc(), apiCodeCommitExtParameter);


		/**
		 * 5 获取授权小程序帐号的可选类目
		 */
//		CategoryListResult categoryList = wechatThirdMiniProgramService.getCategoryList(applicationParty, applicationChannelVO.getAuthorizerAppId());

		/**
		 * 6 获取小程序的第三方提交代码的页面配置（仅供第三方开发者代小程序调用）
		 */
//		PageListResult pageList = wechatThirdMiniProgramService.getPageList(applicationParty, applicationChannelVO.getAuthorizerAppId());


		/**
		 * 7 将第三方提交的代码包提交审核
		 */
//		List<SubmitAuditParameter.Item> items = Lists.newArrayList();
//		for (int i = 0; i < categoryList.getCategoryList().size(); i++) {
//			CategoryListResult.CategoryList category = categoryList.getCategoryList().get(i);
//			SubmitAuditParameter.Item item = SubmitAuditParameter.Item.builder()
//					.address(pageList.getPageList().get(i))
//					.tag(getMiniPragramTag(category.getFirstClass(), category.getSecondClass(), category.getThirdClass()))
//					.first_class(category.getFirstClass())
//					.first_id(category.getFirstId())
//					.second_class(category.getSecondClass())
//					.third_class(category.getThirdClass())
//					.third_id(category.getThirdId())
//					.second_id(category.getSecondId())
//					.title("自动创建的")
//					.build();
//			items.add(item);
//		}
//		SubmitAuditParameter submitAuditParameter = SubmitAuditParameter
//				.builder()
//				.item_list(items).build();
//		SubmitAuditResult submitAuditResult = wechatThirdMiniProgramService.submitAudit(applicationParty, applicationChannelVO.getAuthorizerAppId(), submitAuditParameter);


		/**
		 * 查询某个指定版本的审核状态（仅供第三方代小程序调用）
		 * 审核状态，其中0为审核成功，1为审核失败，2为审核中，3已提交审核
		 */
//		AuditStatusResult auditStatus = wechatThirdMiniProgramService.getAuditStatus(applicationParty, applicationChannelVO.getAuthorizerAppId(), submitAuditResult.getAuditId());

		/**
		 * 更新数据库小程序的审核状态
		 */
//		return auditStatus.getStatus();
		return this.success(apiSetWebviewDomainInfo.getErrcode());
	}
	
	/**
	 * 获取体验二维码
	 * @param partyChannelId
	 * @return
	 */
	@GetMapping("/getTestQrcode")
	@ApiOperation("获取体验二维码")
	public Result getTestQrcode(@Valid @RequestParam("partyChannelId") String partyChannelId){

		try {
//			commit(applicationParty, channelId);//提交代码
//			logger.info("发布成功");
			String applicationParty = UserContextHolder.getInstance().getApplicationParty();
			PartyChannelQueryForm applicationChannelQueryForm = new PartyChannelQueryForm();
			applicationChannelQueryForm.setPartyChannelId(partyChannelId);
			PartyChannelVO applicationChannelVO = userClient.getPartyChannel(applicationChannelQueryForm).getData();

			if(applicationChannelVO==null
					|| applicationChannelVO.getAuthorizerAppId()==null){
				return this.failure("该渠道暂未成功发布");
			}
			String base64 = wechatThirdMiniProgramService.getTestQrcode(applicationParty, applicationChannelVO.getAuthorizerAppId(),null);
			logger.info("getTestQrcode", base64);
			return this.success("<img src=\"data:image/png;base64,"+base64+"\"\\/>");
		} catch (WechatErrorException e) {
			logger.error("getTestQrcode", e);
			return this.failure(String.valueOf(e.getError().getErrCode()), e.getError().getErrMsg(),null);
		} catch (UnsupportedEncodingException e){
			logger.error("getTestQrcode", e);
			throw new RuntimeException(e);
		} catch (Exception e){
			logger.error("getTestQrcode", e);
			throw new RuntimeException(e);
		}
	}

	
	@GetMapping("/bindTester")
	@ApiOperation("绑定体验者")
	public Result bindTester(@Valid @RequestParam("wechatid") String wechatid,
										@Valid @RequestParam("partyChannelId") String partyChannelId){
		try {
			String applicationParty = UserContextHolder.getInstance().getApplicationParty();
			PartyChannelQueryForm applicationChannelQueryForm = new PartyChannelQueryForm();
			applicationChannelQueryForm.setPartyChannelId(partyChannelId);
			PartyChannelVO applicationChannelVO = userClient.getPartyChannel(applicationChannelQueryForm).getData();

			if(applicationChannelVO==null
					|| applicationChannelVO.getAuthorizerAppId()==null){
				return this.failure("该渠道暂未成功发布");
			}
			ApiBindTesterInfo apiBindTesterInfo = wechatThirdMiniProgramService.bindTester(applicationParty, applicationChannelVO.getAuthorizerAppId(),wechatid);
			logger.info("bindTester", apiBindTesterInfo);
			return this.success(apiBindTesterInfo);
		} catch (WechatErrorException e) {
			logger.error("bindTester", e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 *手动发布已通过审核的小程序
	 */
	@GetMapping("/releaes")
	@ApiOperation("手动发布已通过审核的小程序")
	public Result releaesAudited(@Valid @RequestParam("partyChannelId") String partyChannelId) throws Exception {

		String applicationParty = UserContextHolder.getInstance().getApplicationParty();
		PartyChannelQueryForm applicationChannelQueryForm = new PartyChannelQueryForm();
		applicationChannelQueryForm.setPartyChannelId(partyChannelId);
		PartyChannelVO applicationChannelVO = userClient.getPartyChannel(applicationChannelQueryForm).getData();

		if(applicationChannelVO==null
				|| applicationChannelVO.getAuthorizerAppId()==null){
			return this.failure("该渠道暂未成功发布");
		}
		ApiSetWebviewDomainInfo apiSetWebviewDomainInfo=wechatThirdMiniProgramService.releaesAudited(applicationParty, applicationChannelVO.getAuthorizerAppId());
		return this.success(apiSetWebviewDomainInfo==null?false:true);
	}

	/**
	 *撤回发布已通过审核的小程序
	 *87013  撤回次数达到上限（每天一次，每个月10次）  慎用
	 */
	@GetMapping("/undo")
	@ApiOperation("撤回发布已通过审核的小程序 每天一次，每个月10次")
	public Result undoCodeAudit(@Valid @RequestParam("partyChannelId") String partyChannelId) throws Exception {

		String applicationParty = UserContextHolder.getInstance().getApplicationParty();
		PartyChannelQueryForm applicationChannelQueryForm = new PartyChannelQueryForm();
		applicationChannelQueryForm.setPartyChannelId(partyChannelId);
		PartyChannelVO applicationChannelVO = userClient.getPartyChannel(applicationChannelQueryForm).getData();

		if(applicationChannelVO==null
				|| applicationChannelVO.getAuthorizerAppId()==null){
			return this.failure("该渠道暂未成功发布");
		}
		ApiSetWebviewDomainInfo apiSetWebviewDomainInfo=wechatThirdMiniProgramService.undoCodeAudit(applicationParty, applicationChannelVO.getAuthorizerAppId());
		return this.success(apiSetWebviewDomainInfo==null?false:true);
	}


	private String getMiniPragramTag(String firstClass,String secondClass,String thirdClass){
		String tag=null;
		if(firstClass!=null && this.length(firstClass)<20){
			tag=firstClass;
		}
		if(secondClass!=null && this.length(secondClass)<20){
			tag=tag+" "+secondClass;
		}
		if(this.length(tag)<20 && thirdClass!=null && this.length(thirdClass)<20){
			tag=tag+" "+thirdClass;
		}
		return tag;
	}


	/**
	 * 获取字符串的长度，如果有中文，则每个中文字符计为2位     * @param value 指定的字符串     * @return 字符串的长度
	 */
	public  int length(String value) {
		int valueLength = 0;
		String chinese = "[\u0391-\uFFE5]";
		/* 获取字段值的长度，如果含中文字符，则每个中文字符长度为2，否则为1 */
		for (int i = 0; i < value.length(); i++) {
			/* 获取一个字符 */
			String temp = value.substring(i, i + 1);
			/* 判断是否为中文字符 */
			if (temp.matches(chinese)) {
				/* 中文字符长度为2 */
				valueLength += 2;
			} else {
				/* 其他字符长度为1 */
				valueLength += 1;
			}
		}
		return valueLength;
	}


}
