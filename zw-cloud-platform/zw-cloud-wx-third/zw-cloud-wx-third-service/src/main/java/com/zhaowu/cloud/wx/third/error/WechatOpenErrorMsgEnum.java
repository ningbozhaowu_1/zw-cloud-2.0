package com.zhaowu.cloud.wx.third.error;

import lombok.Getter;

/**
 * <pre>
 * 微信第三方平台全局错误码.
 * 参考文档：<a href="https://work.weixin.qq.com/api/doc#10649">企业微信全局错误码</a>
 * </pre>
 *
 */
@Getter
public enum WechatOpenErrorMsgEnum {
  /**
   * 系统繁忙；服务器暂不可用，建议稍候重试。建议重试次数不超过3次。
   */
  CODE_1(-1, "系统繁忙；服务器暂不可用，建议稍候重试。建议重试次数不超过3次。"),

  /**
   * component ticket is invalid hint
   */
  CODE_61006(61006, "component ticket失效"),

  //申请小程序错误start
  CODE_89249(89249, "该主体已有任务执行中，距上次任务 24h 后再试"),
  CODE_86004(86004, "无效微信号"),
  CODE_61070(61070, "法人姓名与微信号不一致"),
  CODE_89250(89250, "未找到该任务"),
  CODE_89251(89251, "待法人人脸核身校验"),
  CODE_89252(89252, "法人&企业信息一致性校验中"),
  CODE_89253(89253, "缺少参数"),
  CODE_89254(89254, "第三方权限集不全，请补充权限集后重试"),
  CODE_89255(89255, "code参数无效，请检查code长度以及内容是否正确"),
  //申请小程序错误end

  //提交代码错误start
  CODE_85013(85013,"无效的自定义配置"),
  CODE_85014(85014,"无效的模板编号"),
  CODE_85043(85043,"模板错误"),
  CODE_85044(85044,"代码包超过大小限制"),
  CODE_85045(85045,"ext_json 有不存在的路径"),
  CODE_85046(85046,"tabBar 中缺少 path"),
  CODE_85047(85047,"pages 字段为空"),
  CODE_85048(85048,"ext_json 解析失败"),
  CODE_80082(80082,"没有权限使用该插件"),
  CODE_80067(80067,"找不到使用的插件"),
  CODE_80066(80066,"非法的插件版本"),
  CODE_9402202(9402202,"请勿频繁提交，待上一次操作完成后再提交"),
  //提交代码错误end

  /**
   * 请求成功；接口调用成功
   */
  CODE_0(0, "请求成功；接口调用成功");

  private int code;
  private String msg;

  WechatOpenErrorMsgEnum(int code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  /**
   * 通过错误代码查找其中文含义.
   */
  public static String findMsgByCode(int code) {
    WechatOpenErrorMsgEnum[] values = WechatOpenErrorMsgEnum.values();
    for (WechatOpenErrorMsgEnum value : values) {
      if (value.code == code) {
        return value.msg;
      }
    }

    return null;
  }
}
