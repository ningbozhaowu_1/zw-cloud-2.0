package com.zhaowu.cloud.wx.third.service.impl;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.util.crypto.SHA1;
import com.zhaowu.cloud.wx.third.error.WechatErrorException;
import com.zhaowu.cloud.wx.third.service.WechatNotifyService;
import com.zhaowu.cloud.wx.third.constant.WechatConstant;
import com.zhaowu.cloud.wx.third.message.WechatThirdXmlMessage;
import com.zhaowu.cloud.wx.third.properties.WechatThirdProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author xxp
 * @date 2019/12/6
 */
@Service
public class WechatNotifyServiceImpl implements WechatNotifyService {
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WechatThirdProperties wechatThirdProperties;

	@CreateCache(name="wxThirdCache:", cacheType = CacheType.REMOTE, expire = 7200)
	private Cache<String, String> wxThirdCache;

	@Override
	public boolean checkSignature(String timestamp, String nonce, String signature) {
		try {
			return SHA1.gen(wechatThirdProperties.getComponentToken(), timestamp, nonce)
					.equals(signature);
		} catch (Exception e) {
			this.log.error("Checking signature failed, and the reason is :" + e.getMessage());
			return false;
		}
	}


	@Override
	public String route(WechatThirdXmlMessage wechatThirdXmlMessage) throws WechatErrorException {
		if (wechatThirdXmlMessage == null) {
			throw new ServiceException("message is empty");
		}
		if (StringUtils.equalsIgnoreCase(wechatThirdXmlMessage.getInfoType(), WechatConstant.COMPONENT_VERIFY_TICKET_KEY)) {
			wxThirdCache.put(WechatConstant.COMPONENT_VERIFY_TICKET_KEY,wechatThirdXmlMessage.getComponentVerifyTicket(),600, TimeUnit.SECONDS);
			return "success";
		}else if(StringUtils.equals(wechatThirdXmlMessage.getInfoType(), WechatConstant.NOTIFY_THIRD_FASTEREGISTER)){
			System.out.println("<>>>>>>>>>>>>>>>>>>>>>appId:"+wechatThirdXmlMessage.getAppId());
			System.out.println("<>>>>>>>>>>>>>>>>>>>>>auth_code:"+wechatThirdXmlMessage.getAuthorizationCode());
		}else if (StringUtils.equals(wechatThirdXmlMessage.getInfoType(), "unauthorized")) {
			wxThirdCache.remove(WechatConstant.AUTHORIZER_ACCESS_TOKEN_KEY+"_"+wechatThirdXmlMessage.getAuthorizerAppid());
			wxThirdCache.remove(WechatConstant.AUTHORIZER_REFRESH_TOKEN_KEY+"_"+wechatThirdXmlMessage.getAuthorizerAppid());
			return "success";
		}
		return "";
	}

}
