package com.zhaowu.cloud.wx.third.service.impl;

import com.github.binarywang.wxpay.bean.applyment.*;
import com.github.binarywang.wxpay.bean.media.ImageUploadResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.Applyment4SubService;
import com.github.binarywang.wxpay.service.MerchantMediaService;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.wx.third.service.WechatApplyment4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * 支付商户号申请
 * @author shen
 * @date 2021/10/18
 */
@Service
public class WechatApplyment4ServiceImpl implements WechatApplyment4Service {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Applyment4SubService applyment4SubService;
    @Resource
    private MerchantMediaService merchantMediaService;


    @Override
    public WxPayApplymentCreateResult createApply(WxPayApplyment4SubCreateRequest wxPayApplyment4SubCreateRequest) throws WxPayException {
        WxPayApplymentCreateResult wxPayApplymentCreateResult= applyment4SubService.createApply(wxPayApplyment4SubCreateRequest);
        return wxPayApplymentCreateResult;
    }

    @Override
    public ApplymentStateQueryResult queryApplyStatusByBusinessCode(String businessCode) throws WxPayException {
        return applyment4SubService.queryApplyStatusByBusinessCode(businessCode);
    }
    @Override
    public ApplymentStateQueryResult queryApplyStatusByApplymentId(String applymentId) throws WxPayException {
        return applyment4SubService.queryApplyStatusByApplymentId(applymentId);
    }
    @Override
    public SettlementInfoResult querySettlementBySubMchid(String subMchId) throws WxPayException {
        return applyment4SubService.querySettlementBySubMchid(subMchId);

    }

    @Override
    public void modifySettlement(String subMchid, ModifySettlementRequest request) throws WxPayException {
        applyment4SubService.modifySettlement(subMchid,request);
    }

    @Override
    public ImageUploadResult imageUploadV3(String path) throws Exception {
        log.info("文件上传：{}",path);
        URL url = new URL(path);
        URLConnection connection = url.openConnection();
        InputStream is = connection.getInputStream();
        return merchantMediaService.imageUploadV3(is,url.getFile());
    }

}
