package com.zhaowu.cloud.wx.third.entity.form;

import lombok.Data;

@Data
public class ModifyHeadImageForm {

    private String headImageMediaId;

    private String x1;

    private String y1;

    private String x2;

    private String y2;

    private String applicationParty;

    private String authorizerAppId;
}
