package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

/**
 * @author xxp
 * @date 2019/11/8
 * 查询某个指定版本的审核状态（仅供第三方代小程序调用）
 */
@Data
public class AuditStatusResult extends Result {
	/**
	 * 最新的审核ID
	 */
	private Long auditid;
	/**
	 * 审核状态，其中0为审核成功，1为审核失败，2为审核中
	 */
	private Integer status;

	/**
	 * 当status=1，审核被拒绝时，返回的拒绝原因
	 */
	private String reason;

}
