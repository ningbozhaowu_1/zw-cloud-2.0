package com.zhaowu.cloud.wx.third.entity.form;

import lombok.Data;

@Data
public class CreateForm {

    private String name;

    private String code;

    private String codeType;

    private String legalPersonaWechat;

    private String legalPersonaName;

    private String componentPhone;
}
