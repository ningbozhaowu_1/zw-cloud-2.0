package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

/**
 * @author xxp
 * @date 2019/09/11
 * 新增素材
 */
@Data
public class AuditUploadMediaResult extends Result {

	/**
	 * 媒体文件类型 分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
	 */
	private String type;
	/**
	 * 媒体文件上传后，获取标识
	 */
	private String media_id;

	/**
	 * 媒体文件上传时间戳
	 */
	private Long created_at;

}
