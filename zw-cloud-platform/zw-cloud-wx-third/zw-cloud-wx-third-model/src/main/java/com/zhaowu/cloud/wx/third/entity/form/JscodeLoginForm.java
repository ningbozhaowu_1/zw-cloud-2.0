package com.zhaowu.cloud.wx.third.entity.form;

import lombok.Data;

@Data
public class JscodeLoginForm {

    private String authorizerAppid;

    private String jsCode;
}
