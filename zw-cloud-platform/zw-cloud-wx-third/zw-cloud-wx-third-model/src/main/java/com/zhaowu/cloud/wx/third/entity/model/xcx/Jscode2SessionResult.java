package com.zhaowu.cloud.wx.third.entity.model.xcx;

import lombok.Data;

/**
 * @author xxp
 * @date 2019/11/26
 */
@Data
public class Jscode2SessionResult {

	/**
	 * 会话密钥
	 */
	private String sessionKey;
	/**
	 * 用户唯一标识的openid
	 */
	private String openId;
}
