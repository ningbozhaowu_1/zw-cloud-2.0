package com.zhaowu.cloud.wx.third.entity.model.xcx;

import java.util.List;

import lombok.Data;

/**
 * 
 * @author xxp
 *
 */
@Data
public class Category {

	private String first;
	
	private String second;
	
	private List<Certicate> certicates;
	
	@Data
	private static class Certicate{
		
		private String key;
		
		private String value;
	}
}
