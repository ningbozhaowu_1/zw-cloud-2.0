package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

import java.util.List;

/**
 * @author xxp
 * @date 2019/11/7
 * 设置小程序服务器域名
 */
@Data
public class ApiModifyDomainInfo extends Result {

	/**
	 *request合法域名，当action参数是get时不需要此字段
	 */
	List<String> requestdomain;
	/**
	 *socket合法域名，当action参数是get时不需要此字段
	 */
	List<String> wsrequestdomain;
	/**
	 *uploadFile合法域名，当action参数是get时不需要此字段
	 */
	List<String> uploaddomain;
	/**
	 *downloadFile合法域名，当action参数是get时不需要此字段
	 */
	List<String> downloaddomain;

}
