package com.zhaowu.cloud.wx.third.entity.model.xcx;

import java.util.List;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

/**
 * 
 * @author xxp
 * 账号可以设置的所有类目
 */
@Data
public class ApiAllCategoriesResult extends Result {
	
	private List<CategoryList> category_list;
	
	@Data
	public static class CategoryList{
		
		private List<Category> categories;
		
		@Data
		public static class Category{
			
			/**
			 * 类目父级ID
			 */
			private String father;
			
			/**
			 * 类目ID
			 */
			private String id;
			
			/**
			 * 类目层级
			 */
			private String level;
			
			/**
			 * 类目名称
			 */
			private String name;
			
			/**
			 * 资质
			 */
			private List<Qualify> qualify;
			
			/**
			 * 子级类目ID
			 */
			private List<String> children;
			
			/**
			 * 是否为敏感类目（1为敏感类目，需要提供相应资质审核；0为非敏感类目，无需审核）
			 */
			private String sensitive_type;
			
			@Data
			public static class Qualify{
				
				private List<InnerList>  exter_list;
				
				@Data
				public static class InnerList{
					
					/**
					 * Sensitive_type为1的类目需要提供的资质文件名称
					 */
					private String name;
					
					/**
					 * 资质文件示例
					 */
					private String url;
				}
			}
		}
	}
	
}
