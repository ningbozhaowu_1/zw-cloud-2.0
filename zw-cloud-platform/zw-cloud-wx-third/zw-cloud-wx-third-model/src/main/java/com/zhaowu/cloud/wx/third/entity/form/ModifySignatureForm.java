package com.zhaowu.cloud.wx.third.entity.form;

import lombok.Data;

@Data
public class ModifySignatureForm {

    private String signature;

    private String applicationParty;

    private String authorizerAppId;

}
