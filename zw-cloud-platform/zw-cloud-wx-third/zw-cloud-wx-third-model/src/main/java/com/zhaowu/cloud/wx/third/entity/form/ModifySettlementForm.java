package com.zhaowu.cloud.wx.third.entity.form;

import com.github.binarywang.wxpay.bean.applyment.ModifySettlementRequest;
import lombok.Data;

@Data
public class ModifySettlementForm {
    private String subMchid;
    private ModifySettlementRequest modifySettlementRequest;
}
