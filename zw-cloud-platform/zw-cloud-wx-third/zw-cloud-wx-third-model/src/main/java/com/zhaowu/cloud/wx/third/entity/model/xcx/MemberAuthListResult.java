package com.zhaowu.cloud.wx.third.entity.model.xcx;

import lombok.Data;

import java.util.List;

/**
 * @author xxp
 * @date 2019/11/7
 * 获取体验者列表
 */
@Data
public class MemberAuthListResult {
	/**
	 * 人员列表
	 */
	private List<Member> members;

	@Data
	public static  class Member{
		/**
		 * 人员对应的唯一字符串
		 */
		private String userstr;

	}

}
