package com.zhaowu.cloud.wx.third.entity.form;

import lombok.Data;

@Data
public class ModifyNickNameForm {

    private String nickName;

    private String license;

    private String applicationParty;

    private String authorizerAppId;
}
