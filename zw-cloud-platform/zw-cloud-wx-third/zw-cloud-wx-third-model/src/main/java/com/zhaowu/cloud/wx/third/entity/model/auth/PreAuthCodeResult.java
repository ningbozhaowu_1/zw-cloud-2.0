package com.zhaowu.cloud.wx.third.entity.model.auth;

import lombok.Data;

/**
 * @author xxp
 * @date 2019/12/14
 */
@Data
public class PreAuthCodeResult {

	private String preAuthCode;
	private Integer expiresIn;
}
