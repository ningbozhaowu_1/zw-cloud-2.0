package com.zhaowu.cloud.wx.third.entity.model.auth;

import lombok.Data;


@Data
public class ComponentAccessTokenResult {

	private String componentAccessToken;
	private Integer expiresIn;


}
