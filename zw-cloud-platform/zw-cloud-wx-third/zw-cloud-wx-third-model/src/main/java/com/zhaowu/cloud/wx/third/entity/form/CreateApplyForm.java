package com.zhaowu.cloud.wx.third.entity.form;


import com.github.binarywang.wxpay.bean.applyment.WxPayApplyment4SubCreateRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 创建申请单
 * @author shen
 * @date 2021/10/18
 */
@Data
public class CreateApplyForm {
    @NotNull(message = "申请提交表单内容不能为空")
    private WxPayApplyment4SubCreateRequest wxPayApplyment4SubCreateRequest;
    private Boolean isSubmit;
}
