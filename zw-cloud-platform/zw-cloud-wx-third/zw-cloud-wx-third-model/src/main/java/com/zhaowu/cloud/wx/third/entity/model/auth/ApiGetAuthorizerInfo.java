package com.zhaowu.cloud.wx.third.entity.model.auth;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xxp
 * @date 2019/11/6
 * 获取授权方的帐号基本信息
 */
@Data
@NoArgsConstructor
public class ApiGetAuthorizerInfo {

	private ApiAuthorizerInfo authorizerInfo;
	private ApiAuthorizationInfo authorizationInfo;

}
