package com.zhaowu.cloud.wx.third.entity.model.xcx;

import lombok.Data;

/**
 * @author xxp
 * @date 2019/11/7
 */
@Data
public class ApiSetWebviewDomainInfo {

	/**
	 *错误码
	 */
	private Integer errcode;
	/**
	 *错误信息
	 */
	private String errmsg;
}
