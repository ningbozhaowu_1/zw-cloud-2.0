package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

/**
 * @author xxp
 * @date 2019/11/7
 */
@Data
public class ApiBindTesterInfo extends Result {
	/**
	 * 人员对应的唯一字符串
	 */
	private String userstr;
}
