package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

/**
 * @author xxp
 * @date 2019/11/8
 * 微信开放平台小程序发布代码审核结果
 */
@Data
public class SubmitAuditResult extends Result {
	/**
	 * 审核编号
	 */
	private Long auditId;
}
