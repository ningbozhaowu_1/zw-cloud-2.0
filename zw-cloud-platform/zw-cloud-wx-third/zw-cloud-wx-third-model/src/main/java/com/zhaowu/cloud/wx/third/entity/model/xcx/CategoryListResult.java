package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

import java.util.List;

/**
 * @author xxp
 * @date 2019/11/8
 * 可填选的类目列表
 */
@Data
public class CategoryListResult extends Result {

	/**
	 * 可填选的类目列表
	 */
	private List<CategoryList> categoryList;

	@Data
	public static class CategoryList{
		/**
		 *一级类目名称
		 */
		private String firstClass;
		/**
		 *二级类目名称
		 */
		private String secondClass;
		/**
		 *三级类目名称
		 */
		private String thirdClass;
		/**
		 *一级类目的ID编号
		 */
		private Integer firstId;
		/**
		 *二级类目的ID编号
		 */
		private Integer secondId;
		/**
		 *三级类目的ID编号
		 */
		private Integer thirdId;
	}


}
