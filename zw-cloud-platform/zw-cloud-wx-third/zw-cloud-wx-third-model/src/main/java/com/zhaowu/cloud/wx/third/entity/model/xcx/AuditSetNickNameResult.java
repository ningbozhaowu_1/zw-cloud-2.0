package com.zhaowu.cloud.wx.third.entity.model.xcx;

import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.Data;

/**
 * @author xxp
 * @date 2019/09/11
 * 设置昵称
 */
@Data
public class AuditSetNickNameResult extends Result {

	/**
	 * 材料说明
	 */
	private String wording;
	/**
	 * 审核单id
	 */
	private String audit_id;

}
