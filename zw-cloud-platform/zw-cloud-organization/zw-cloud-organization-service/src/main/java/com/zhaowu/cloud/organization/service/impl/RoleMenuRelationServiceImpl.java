package com.zhaowu.cloud.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import com.zhaowu.cloud.organization.mapper.RoleMenuRelationMapper;
import com.zhaowu.cloud.organization.service.RoleMenuRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 角色和菜单关系表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-03-22
 */
@Service
public class RoleMenuRelationServiceImpl extends ServiceImpl<RoleMenuRelationMapper,RoleMenuRelation> implements RoleMenuRelationService {

    @Override
    public void batchAdd(List<RoleMenuRelation> roleMenuRelationList) {
        this.saveOrUpdateBatch(roleMenuRelationList);
    }

    @Override
    public void batchDel(Collection<String> menuIds, String roleId) {
        QueryWrapper<RoleMenuRelation> delete = new QueryWrapper<>();
        delete.eq("role_id", roleId);
        delete.in("menu_id", menuIds);
        this.remove(delete);
    }
}
