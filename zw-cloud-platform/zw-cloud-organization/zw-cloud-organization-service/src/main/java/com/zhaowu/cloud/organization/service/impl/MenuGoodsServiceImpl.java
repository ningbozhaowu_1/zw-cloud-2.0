package com.zhaowu.cloud.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.organization.entity.po.Menu;
import com.zhaowu.cloud.organization.entity.po.MenuGoods;
import com.zhaowu.cloud.organization.mapper.MenuGoodsMapper;
import com.zhaowu.cloud.organization.service.MenuGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 菜单商品关系表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@Service
public class MenuGoodsServiceImpl extends ServiceImpl<MenuGoodsMapper,MenuGoods> implements MenuGoodsService {

    @Override
    public void batchAdd(List<MenuGoods> menuGoodsList) {
         this.batchAdd(menuGoodsList);
    }

    @Override
    public void delete(String menuId) {
        QueryWrapper<MenuGoods> delete = new QueryWrapper<MenuGoods>();
        delete.eq("menu_id", menuId);
        this.remove(delete);
    }

    @Override
    public List<MenuGoods> getByGoodsIds(Collection<String> goodsIds) {
        QueryWrapper<MenuGoods> queryWrapper = new QueryWrapper<MenuGoods>();
        queryWrapper.in("goods_id", goodsIds);
        return  this.list(queryWrapper);
    }
}
