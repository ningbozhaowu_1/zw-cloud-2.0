package com.zhaowu.cloud.organization.service;

import com.zhaowu.cloud.organization.entity.po.MenuGoods;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 菜单商品关系表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
public interface MenuGoodsService{

    void batchAdd(List<MenuGoods> menuGoodsList);

    void delete(String menuId);

    List<MenuGoods> getByGoodsIds(Collection<String> goodsIds);
}
