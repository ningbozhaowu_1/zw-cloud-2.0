package com.zhaowu.cloud.organization.service.impl;

import com.zhaowu.cloud.organization.entity.po.UserPositionRelation;
import com.zhaowu.cloud.organization.mapper.UserPositionRelationMapper;
import com.zhaowu.cloud.organization.service.UserPositionRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和岗位关系表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-28
 */
@Service
public class UserPositionRelationServiceImpl extends ServiceImpl<UserPositionRelationMapper,UserPositionRelation> implements UserPositionRelationService {

}
