package com.zhaowu.cloud.organization.mapper;

import com.zhaowu.cloud.organization.entity.po.PartyMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参与者菜单关系表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
public interface PartyMenuMapper extends BaseMapper<PartyMenu> {

}
