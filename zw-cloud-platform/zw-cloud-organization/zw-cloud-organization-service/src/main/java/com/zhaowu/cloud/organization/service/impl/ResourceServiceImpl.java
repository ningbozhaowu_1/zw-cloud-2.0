package com.zhaowu.cloud.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.organization.config.BusConfig;
import com.zhaowu.cloud.organization.entity.param.ResourceQueryParam;
import com.zhaowu.cloud.organization.entity.po.Resource;
import com.zhaowu.cloud.organization.entity.po.RoleResource;
import com.zhaowu.cloud.organization.mapper.ResourceMapper;
import com.zhaowu.cloud.organization.service.ResourceService;
import com.zhaowu.cloud.organization.service.RoleResourceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements ResourceService {

    @Autowired
    private RoleResourceService roleResourceService;

    @Autowired
    private BusSender busSender;

    @Autowired
    private BusConfig busConfig;

    @Value("${rabbitmq.routing_key}")
    private String routing_key;

    @Override
    public boolean add(Resource resource) {
//        busSender.send(routing_key, resource);
        boolean checkResourceCode = this.checkResourceCode(resource);
        Assert.isTrue(checkResourceCode, "code已存在");
        return this.save(resource);
    }

    @Override
    public boolean delete(String id) {
        return this.removeById(id);
    }

    @Override
    public boolean update(Resource resource) {
        boolean checkResourceCode = this.checkResourceCode(resource);
        Assert.isTrue(checkResourceCode, "code已存在");
        return this.updateById(resource);
    }

    private boolean checkResourceCode(Resource resource) {
        QueryWrapper<Resource> wrapper = new QueryWrapper<Resource>();
        wrapper.eq("code", resource.getCode());
        if(!org.apache.commons.lang3.StringUtils.isEmpty(resource.getId())){
            wrapper.ne("id", resource.getId());
        }
        return this.getOne(wrapper) == null;
    }

    @Override
    public Resource get(String id) {
        return this.getById(id);
    }

    @Override
    public IPage<Resource> query(Page page, ResourceQueryParam resourceQueryParam) {
        QueryWrapper<Resource> queryWrapper = resourceQueryParam.build();
        queryWrapper.eq(StringUtils.isNotBlank(resourceQueryParam.getName()), "name", resourceQueryParam.getName());
        queryWrapper.eq(StringUtils.isNotBlank(resourceQueryParam.getType()), "type", resourceQueryParam.getType());
        queryWrapper.eq(StringUtils.isNotBlank(resourceQueryParam.getUrl()), "url", resourceQueryParam.getUrl());
        queryWrapper.eq(StringUtils.isNotBlank(resourceQueryParam.getMethod()), "method", resourceQueryParam.getMethod());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<Resource> getAll() {
        return this.list();
    }

    @Override
    public List<Resource> query(Collection<String> roleIds) {
        //根据角色列表查询到角色的资源的关联关系
        List<RoleResource> roleResources = roleResourceService.queryByRoleIds(roleIds);

        if(CollectionUtils.isEmpty(roleResources)){
            return new ArrayList<>();
        }

        //根据资源列表查询出所有资源对象
        Set<String> resourceIds = roleResources.stream().map(roleResource -> roleResource.getResourceId()).collect(Collectors.toSet());
        //根据resourceId列表查询出resource对象
        return (List<Resource>) this.listByIds(resourceIds);
    }
}
