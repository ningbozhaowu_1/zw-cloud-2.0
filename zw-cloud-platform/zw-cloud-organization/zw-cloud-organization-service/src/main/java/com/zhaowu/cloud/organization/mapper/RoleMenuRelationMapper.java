package com.zhaowu.cloud.organization.mapper;

import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和菜单关系表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-03-22
 */
public interface RoleMenuRelationMapper extends BaseMapper<RoleMenuRelation> {

}
