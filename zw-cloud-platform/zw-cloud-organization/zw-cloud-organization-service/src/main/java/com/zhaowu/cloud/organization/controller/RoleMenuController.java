package com.zhaowu.cloud.organization.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.organization.entity.form.RoleForm;
import com.zhaowu.cloud.organization.entity.form.RoleMenuForm;
import com.zhaowu.cloud.organization.entity.form.RoleQueryForm;
import com.zhaowu.cloud.organization.entity.form.RoleUpdateForm;
import com.zhaowu.cloud.organization.entity.param.RoleQueryParam;
import com.zhaowu.cloud.organization.entity.po.Role;
import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import com.zhaowu.cloud.organization.entity.vo.RoleVO;
import com.zhaowu.cloud.organization.service.RoleMenuRelationService;
import com.zhaowu.cloud.organization.service.RoleService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/roleMenu/admin/v1")
@Api("roleMenu")
@Slf4j
public class RoleMenuController extends BaseController {

    @Autowired
    private RoleMenuRelationService roleMenuRelationService;

    @ApiOperation(value = "批量绑定角色菜单", notes = "批量绑定角色菜单")
    @ApiImplicitParam(name = "roleMenuForm", value = "绑定角色菜单form表单", required = true, dataType = "roleMenuForm")
    @PostMapping(value = "/batchAdd")
    public Result batchAdd(@Valid @RequestBody RoleMenuForm roleMenuForm) {

        List<RoleMenuRelation> roleMenuRelationList = new ArrayList<>();
        for(String menuId : roleMenuForm.getMenuIds()){
            RoleMenuRelation roleMenuRelation = new RoleMenuRelation();
            roleMenuRelation.setRoleId(roleMenuForm.getRoleId());
            roleMenuRelation.setMenuId(menuId);
            roleMenuRelationList.add(roleMenuRelation);
        }

        roleMenuRelationService.batchAdd(roleMenuRelationList);
        return this.success();
    }

    @ApiOperation(value = "批量角色菜单解绑", notes = "批量角色菜单解绑")
    @PostMapping(value = "/batchDel")
    public Result batchDel(@Valid @RequestBody RoleMenuForm roleMenuForm) {

        roleMenuRelationService.batchDel(roleMenuForm.getMenuIds(), roleMenuForm.getRoleId());

        return this.success();
    }
}