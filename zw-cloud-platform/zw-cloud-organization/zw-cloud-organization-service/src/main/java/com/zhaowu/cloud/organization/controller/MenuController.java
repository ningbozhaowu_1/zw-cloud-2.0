package com.zhaowu.cloud.organization.controller;

import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.TreeUtil;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.framework.util.bean.Node;
import com.zhaowu.cloud.organization.entity.form.MenuForm;
import com.zhaowu.cloud.organization.entity.form.MenuQueryForm;
import com.zhaowu.cloud.organization.entity.param.MenuQueryParam;
import com.zhaowu.cloud.organization.entity.po.Menu;
import com.zhaowu.cloud.organization.entity.po.MenuGoods;
import com.zhaowu.cloud.organization.entity.po.Role;
import com.zhaowu.cloud.organization.service.MenuGoodsService;
import com.zhaowu.cloud.organization.service.MenuService;
import com.zhaowu.cloud.organization.service.RoleService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/menu/admin/v1")
@Api("menu")
@Slf4j
public class MenuController extends BaseController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuGoodsService menuGoodsService;

    @ApiOperation(value = "新增菜单", notes = "新增一个菜单")
    @ApiImplicitParam(name = "menuForm", value = "新增菜单form表单", required = true, dataType = "MenuForm")
    @PostMapping
    @Transactional
    public Result add(@Valid @RequestBody MenuForm menuForm) {
        log.debug("name:{}", menuForm);
        Menu menu = menuForm.toPo(Menu.class);
        menuService.add(menu);

        if(CommonConstant.MenuType.PM.equals(menu.getType())){
            if(CollectionUtils.isEmpty(menuForm.getGoodsIdList())){
                return this.failure("付费菜单 商品不能为空");
            }else{
                List<MenuGoods> menuGoodsList = new ArrayList<>();
                for(String goodsId : menuForm.getGoodsIdList()){
                    MenuGoods menuGoods = new MenuGoods();
                    menuGoods.setGoodsId(goodsId);
                    menuGoods.setMenuId(menu.getId());
                    menuGoods.setGoodsParty(CommonConstant.DefaultParty.PartyId);
                    menuGoodsList.add(menuGoods);
                }
                menuGoodsService.batchAdd(menuGoodsList);
            }
        }

        return this.success();
    }

    @ApiOperation(value = "删除菜单", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(paramType = "path", name = "id", value = "菜单ID", required = true, dataType = "string")
    @DeleteMapping(value = "/{id}")
    @Transactional
    public Result delete(@PathVariable String id) {

        menuService.delete(id);
        menuGoodsService.delete(id);

        return this.success();
    }

    @ApiOperation(value = "修改菜单", notes = "修改指定菜单信息")
    @PostMapping(value = "/update")
    public Result update(@Valid @RequestBody MenuForm menuForm) {
        Menu menu = menuForm.toPo(Menu.class);
        return this.success(menuService.update(menu));
    }

    @ApiOperation(value = "获取菜单", notes = "获取指定菜单信息")
    @ApiImplicitParam(paramType = "path", name = "id", value = "菜单ID", required = true, dataType = "string")
    @GetMapping(value = "/{id}")
    public Result get(@PathVariable String id) {
        log.debug("get with id:{}", id);
        return this.success(menuService.get(id));
    }

    @ApiOperation(value = "查询菜单", notes = "根据条件查询菜单信息，简单查询")
    @ApiImplicitParam(paramType = "query", name = "name", value = "菜单名称", required = true, dataType = "string")
    @ApiResponses(
            @ApiResponse(code = 200, message = "处理成功", response = Result.class)
    )
    @GetMapping
    public Result query(@RequestParam String name) {
        log.debug("query with name:{}", name);
        MenuQueryParam menuQueryParam = new MenuQueryParam(name);
        return this.success(menuService.query(menuQueryParam));
    }

    @ApiOperation(value = "菜单分页查询", notes = "分页查询菜单")
    @PostMapping(value = "/pageQuery")
    public Result pageQuery(@Valid @RequestBody MenuQueryForm menuQueryForm) {
        return this.success(menuService.pageList(menuQueryForm.getPage(), menuQueryForm.toParam(MenuQueryParam.class)));
    }

    @ApiOperation(value = "根据父id查询菜单", notes = "根据父id查询菜单列表")
    @ApiImplicitParam(paramType = "path", name = "id", value = "菜单父ID", required = true, dataType = "string")
    @GetMapping(value = "/parent/{id}")
    public Result search(@PathVariable String id) {
        log.debug("query with parent id:{}", id);
        return this.success(menuService.queryByParentId(id));
    }

    @ApiOperation(value = "查询角色菜单列表")
    @GetMapping(value = "/queryMenuListByRoleId")
    public Result<List<Node>> queryMenuListByRoleId(@Valid @RequestParam String roleId) {

        List<Menu> menuList = menuService.getRoleMenuList(roleId);

        List<Node> nodes = new LinkedList<Node>();
        for(Menu menu : menuList){
            if("-1".equals(menu.getId())){
                continue;
            }
            Node node = new Node();
            node.setIcon(menu.getIcon());
            node.setId(menu.getId());
            node.setPid(menu.getParentId());
            node.setName(menu.getName());
            node.setHerf(menu.getHref());
            nodes.add(node);
        }

        return this.success(TreeUtil.data("-1" ,nodes));
    }

    @ApiOperation(value = "查询当前用户菜单")
    @GetMapping(value = "/queryByApplicationParty")
    public Result<List<Node>> queryByApplicationParty() {

        Collection<String> roleIds = (UserContextHolder.getInstance().getAuthorities());

        String applicationParty = UserContextHolder.getInstance().getApplicationParty();
        List<Menu> menuList = null;
        if(roleIds.contains(roleService.selectByCode(CommonConstant.RoleCode.SUPER_ADMIN).getId())){//返回所有菜单
            menuList = menuService.getAllMenu();
        }else {
//            boolean basicMenu = false;
//            List<Role> roles = roleService.selectByIds(roleIds);
//            for(Role role : roles){
//                if("Y".equals(role.getBasicMenu())){
//                    basicMenu = true;
//                }
//            }
            menuList = menuService.getRoleMenuList(applicationParty, roleIds);
        }

        List<Node> nodes = new LinkedList<Node>();
        for(Menu menu : menuList){
            if("-1".equals(menu.getId())){
                continue;
            }
            Node node = new Node();
            node.setIcon(menu.getIcon());
            node.setId(menu.getId());
            node.setPid(menu.getParentId());
            node.setName(menu.getName());
            node.setHerf(menu.getHref());
            node.setOrderNum(menu.getOrderNum());
            nodes.add(node);
        }
        return this.success(TreeUtil.data("-1" ,nodes));
    }
}