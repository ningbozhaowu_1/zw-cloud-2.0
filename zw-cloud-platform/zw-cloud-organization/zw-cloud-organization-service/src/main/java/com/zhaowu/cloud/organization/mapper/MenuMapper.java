package com.zhaowu.cloud.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.organization.entity.po.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> getRoleMenuList(@Param("applicationParty") String applicationParty, @Param("roleIds") Collection<String> roleIds);

    List<Menu> getRoleMenuList1(@Param("roleId") String roleId);

    List<Menu> getMenuListByCode(@Param("applicationParty") String applicationParty, String roleCode);
}