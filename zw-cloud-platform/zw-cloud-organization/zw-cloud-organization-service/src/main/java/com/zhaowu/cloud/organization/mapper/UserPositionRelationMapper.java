package com.zhaowu.cloud.organization.mapper;

import com.zhaowu.cloud.organization.entity.po.UserPositionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和岗位关系表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-28
 */
public interface UserPositionRelationMapper extends BaseMapper<UserPositionRelation> {

}
