package com.zhaowu.cloud.organization.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.organization.entity.po.UserPositionRelation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.organization.service.UserPositionRelationService;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 用户和岗位关系表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-28
 */
@RestController
@RequestMapping("/organization/userPositionRelation")
@Api(tags = "用户和岗位关系表 接口")
public class UserPositionRelationController extends BaseController {

        @Autowired
        private UserPositionRelationService userPositionRelationService;


        /**
         * 新增用户和岗位关系表
         */
        @PostMapping(value = "/add")
        @ApiOperation("新增用户和岗位关系表")
        public Result<String> add(@Valid @RequestBody UserPositionRelation userPositionRelation) {


            return this.success();
        }

        /**
         * 删除用户和岗位关系表
         */
        @PostMapping(value = "/delete/{id}")
        @ApiOperation("删除用户和岗位关系表")
        public Result<String> delete(@PathVariable("id") Long id) {


            return this.success();
        }

}

