package com.zhaowu.cloud.organization.exception;

import com.zhaowu.cloud.framework.base.exception.BaseException;

public class RoleNotFoundException extends BaseException {
    public RoleNotFoundException() {
        super(OrganizationErrorStatus.ROLE_NOT_FOUND);
    }

    public RoleNotFoundException(String message) {
        super(OrganizationErrorStatus.ROLE_NOT_FOUND, message);
    }
}
