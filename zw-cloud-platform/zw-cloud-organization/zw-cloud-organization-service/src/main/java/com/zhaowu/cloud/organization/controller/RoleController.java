package com.zhaowu.cloud.organization.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.organization.entity.form.RoleForm;
import com.zhaowu.cloud.organization.entity.form.RoleQueryForm;
import com.zhaowu.cloud.organization.entity.form.RoleUpdateForm;
import com.zhaowu.cloud.organization.entity.param.RoleQueryParam;
import com.zhaowu.cloud.organization.entity.po.Role;
import com.zhaowu.cloud.organization.entity.vo.RoleVO;
import com.zhaowu.cloud.organization.service.RoleService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/role/admin/v1")
@Api("role")
@Slf4j
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "新增角色", notes = "新增一个角色")
    @ApiImplicitParam(name = "roleForm", value = "新增角色form表单", required = true, dataType = "RoleForm")
    @PostMapping
    public Result add(@Valid @RequestBody RoleForm roleForm) {
        log.debug("name:{}", roleForm);
        Role role = roleForm.toPo(Role.class);
        role.setPartyId(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
        return this.success(roleService.add(role));
    }

    @ApiOperation(value = "删除角色", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(paramType = "path", name = "id", value = "角色ID", required = true, dataType = "string")
    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable String id) {

        Role role = roleService.get(id);
        if(!role.getPartyId().equals(UserContextHolder.getInstance().getApplicationParty())){
            return this.failure("系统内置角色不允许删除");
        }
        return this.success(roleService.delete(id));
    }

    @ApiOperation(value = "修改角色", notes = "修改指定角色信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataType = "string"),
            @ApiImplicitParam(name = "roleForm", value = "角色实体", required = true, dataType = "RoleUpdateForm")
    })
    @PostMapping(value = "/{id}")
    public Result update(@PathVariable String id, @Valid @RequestBody RoleUpdateForm roleUpdateForm) {

        Role role = roleService.get(id);
        if(!role.getPartyId().equals(UserContextHolder.getInstance().getApplicationParty())){
            return this.failure("系统内置角色不允许修改");
        }

        role = roleUpdateForm.toPo(id, Role.class);
        return this.success(roleService.update(role));
    }

    @ApiOperation(value = "获取角色", notes = "获取指定角色信息")
    @ApiImplicitParam(paramType = "path", name = "id", value = "角色ID", required = true, dataType = "string")
    @GetMapping(value = "/{id}")
    public Result get(@PathVariable String id) {
        log.debug("get with id:{}", id);
        return this.success(roleService.get(id));
    }

    @ApiOperation(value = "获取所有角色", notes = "获取所有角色")
    @GetMapping(value = "/all")
    public Result get() {
        return this.success(roleService.getAll(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId()));
    }

    @ApiOperation(value = "搜索角色", notes = "根据条件搜索角色信息")
    @ApiImplicitParam(name = "roleQueryForm", value = "角色查询参数", required = true, dataType = "RoleQueryForm")
    @ApiResponses(
            @ApiResponse(code = 200, message = "处理成功", response = Result.class)
    )
    @PostMapping(value = "/conditions")
    public Result query(@Valid @RequestBody RoleQueryForm roleQueryForm) {
        log.debug("query with name:{}", roleQueryForm);
        RoleQueryParam roleQueryParam = roleQueryForm.toParam(RoleQueryParam.class);
        roleQueryParam.setPartyId(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
        return this.success(roleService.query(roleQueryForm.getPage(), roleQueryParam));
    }

    @ApiOperation(value = "根据角色编码获取编号", notes = "根据角色编码获取编号")
    @GetMapping(value = "/getRoleIdByCode")
    public Result<RoleVO> getRoleIdByCode(@Valid @RequestParam String code) {
        Role role = roleService.selectByCode(code);
        RoleVO roleVO = new RoleVO();
        BeanUtils.copyProperties(role, roleVO);
        return this.success(roleVO);
    }
}