package com.zhaowu.cloud.organization.mapper;

import com.zhaowu.cloud.organization.entity.po.MenuGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单商品关系表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
public interface MenuGoodsMapper extends BaseMapper<MenuGoods> {

}
