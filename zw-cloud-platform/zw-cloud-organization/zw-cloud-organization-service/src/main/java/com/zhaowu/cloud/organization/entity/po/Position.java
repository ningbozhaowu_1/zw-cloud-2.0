package com.zhaowu.cloud.organization.entity.po;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Position extends BasePo {

    private String applicationParty;
    private String partyId;
    private String name;
    private String description;
    @TableLogic(value = "N", delval = "Y" )
    private String deleted;
}

