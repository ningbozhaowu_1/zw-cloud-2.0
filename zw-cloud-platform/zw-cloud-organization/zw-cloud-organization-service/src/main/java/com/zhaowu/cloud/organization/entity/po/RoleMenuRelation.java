package com.zhaowu.cloud.organization.entity.po;

import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 角色和菜单关系表
 * </p>
 *
 * @author xxp
 * @since 2021-03-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class RoleMenuRelation extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单id
     */
    private String menuId;

    /**
     * 角色id
     */
    private String roleId;

}
