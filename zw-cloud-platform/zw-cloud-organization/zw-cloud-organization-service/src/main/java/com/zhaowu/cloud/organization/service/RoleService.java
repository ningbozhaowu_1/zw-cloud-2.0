package com.zhaowu.cloud.organization.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.organization.entity.param.RoleQueryParam;
import com.zhaowu.cloud.organization.entity.po.Role;

import java.util.Collection;
import java.util.List;

public interface RoleService {
    /**
     * 获取角色
     *
     * @param id
     * @return
     */
    @Cached(name = "roleCache::", key = "#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Role get(String id);

    List<Role> selectByIds(Collection<String> ids);

    /**
     * 获取所有角色
     *
     * @return
     */
    List<Role> getAll(String partyId);

    /**
     * 新增角色
     *
     * @param role
     * @return
     */
    boolean add(Role role);

    /**
     * 查询角色
     *
     * @return
     */
    IPage<Role> query(Page page, RoleQueryParam roleQueryParam);
    /**
     * 更新角色信息
     *
     * @param role
     */
    @CacheInvalidate(name = "roleCache::", key = "#role.id")
    boolean update(Role role);

    /**
     * 根据id删除角色
     *
     * @param id
     */
    @CacheInvalidate(name = "roleCache::", key = "#id")
    boolean delete(String id);

    Role selectByCode(String code);
}
