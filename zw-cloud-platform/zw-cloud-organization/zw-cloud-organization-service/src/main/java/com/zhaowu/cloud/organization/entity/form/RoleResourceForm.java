package com.zhaowu.cloud.organization.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import com.zhaowu.cloud.organization.entity.po.RoleResource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Set;

@ApiModel
@Data
public class RoleResourceForm extends BaseForm<RoleResource> {

    @ApiModelProperty(value = "资源编号,为空则删除角色所有资源")
    private Set<String> resourceIds;

    @NotBlank(message = "角色编号不能为空")
    @ApiModelProperty(value = "角色编号")
    private String roleId;

}
