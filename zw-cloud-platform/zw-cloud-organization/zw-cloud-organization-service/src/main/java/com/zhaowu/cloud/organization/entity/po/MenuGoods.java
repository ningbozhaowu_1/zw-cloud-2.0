package com.zhaowu.cloud.organization.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 菜单商品关系表
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@Data
@Accessors(chain = true)
public class MenuGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单编号
     */
    private String menuId;

    /**
     * 商品编号
     */
    private String goodsId;

    /**
     * 商品参与者
     */
    private String goodsParty;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

}
