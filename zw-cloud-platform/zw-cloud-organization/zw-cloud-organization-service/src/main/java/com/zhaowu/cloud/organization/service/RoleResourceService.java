package com.zhaowu.cloud.organization.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.organization.entity.po.RoleResource;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface RoleResourceService {

    /**
     * 批量给角色添加资源
     *
     * @param roleId      角色id
     * @param resourceIds 资源id列表
     * @return 是否操作成功
     */
    @CacheInvalidate(name = "resource4role::", key = "#roleId")
    boolean saveBatch(String roleId, Set<String> resourceIds);

    /**
     * 删除角色拥有的资源
     *
     * @param roleId 角色id
     * @return 是否操作成功
     */
    @CacheInvalidate(name = "resource4role::", key = "#roleId")
    boolean removeByRoleId(String roleId);

    /**
     * 删除角色拥有的资源
     *
     * @param roleId 角色id
     * @return 是否操作成功
     */
    @CacheInvalidate(name = "resource4role::", key = "#roleId")
    boolean removeBatch(String roleId, Set<String> resourceIds);

    /**
     * 查询角色拥有资源id
     *
     * @param roleId 角色id
     * @return 角色拥有的资源id集合
     */
    @Cached(name = "resource4role::", key = "#roleId", expire = 3600, cacheType = CacheType.BOTH)
    Set<String> queryByRoleId(String roleId);

    /**
     * 根据角色id列表查询资源关系
     *
     * @param roleIds 角色id集合
     * @return 角色资源关系集合
     */
    List<RoleResource> queryByRoleIds(Collection<String> roleIds);
}
