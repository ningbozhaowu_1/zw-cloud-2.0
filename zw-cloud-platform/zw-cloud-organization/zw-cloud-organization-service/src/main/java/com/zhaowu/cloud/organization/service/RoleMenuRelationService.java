package com.zhaowu.cloud.organization.service;

import com.baomidou.mybatisplus.extension.api.R;
import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 角色和菜单关系表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-03-22
 */
public interface RoleMenuRelationService{

    void batchAdd(List<RoleMenuRelation> roleMenuRelationList);

    void batchDel(Collection<String> menuIds, String roleId);
}
