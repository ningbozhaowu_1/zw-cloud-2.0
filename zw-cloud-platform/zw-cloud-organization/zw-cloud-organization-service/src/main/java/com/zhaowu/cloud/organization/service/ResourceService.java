package com.zhaowu.cloud.organization.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.organization.entity.param.ResourceQueryParam;
import com.zhaowu.cloud.organization.entity.po.Resource;

import java.util.Collection;
import java.util.List;

public interface ResourceService {
    /**
     * 获取资源
     *
     * @param id
     * @return
     */
    @Cached(name = "resource::", key = "#id", cacheType = CacheType.BOTH)
    Resource get(String id);

    /**
     * 新增资源
     *
     * @param resource
     * @return
     */
    boolean add(Resource resource);

    /**
     * 查询资源,分页
     *
     * @return
     */
    IPage<Resource> query(Page page, ResourceQueryParam resourceQueryParam);

    /**
     * 查询所有资源
     *
     * @return
     */
    List<Resource> getAll();

    /**
     * 根据rolecodes查询角色拥有的资源
     *
     * @return
     */
    List<Resource> query(Collection<String> roleIds);

    /**
     * 更新资源信息
     *
     * @param resource
     */
    @CacheInvalidate(name = "resource::", key = "#resource.id")
    boolean update(Resource resource);

    /**
     * 根据id删除资源
     *
     * @param id
     */
    @CacheInvalidate(name = "resource::", key = "#id")
    boolean delete(String id);
}
