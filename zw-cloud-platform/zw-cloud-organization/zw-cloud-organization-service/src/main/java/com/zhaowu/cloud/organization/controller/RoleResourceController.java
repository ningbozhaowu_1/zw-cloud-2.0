package com.zhaowu.cloud.organization.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.organization.entity.form.RoleMenuForm;
import com.zhaowu.cloud.organization.entity.form.RoleResourceForm;
import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import com.zhaowu.cloud.organization.entity.po.RoleResource;
import com.zhaowu.cloud.organization.service.RoleMenuRelationService;
import com.zhaowu.cloud.organization.service.RoleResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/roleResource/admin/v1")
@Api("roleResource")
@Slf4j
public class RoleResourceController extends BaseController {

    @Autowired
    private RoleResourceService roleResourceService;

    @ApiOperation(value = "批量绑定角色资源", notes = "批量绑定角色资源")
    @ApiImplicitParam(name = "roleResourceForm", value = "绑定角色资源form表单", required = true, dataType = "roleResourceForm")
    @PostMapping(value = "/batchAdd")
    public Result batchAdd(@Valid @RequestBody RoleResourceForm roleResourceForm) {

        roleResourceService.saveBatch(roleResourceForm.getRoleId(), roleResourceForm.getResourceIds());
        return this.success();
    }

    @ApiOperation(value = "批量角色资源解绑", notes = "批量角色资源解绑")
    @PostMapping(value = "/batchDel")
    public Result batchDel(@Valid @RequestBody RoleResourceForm roleResourceForm) {

        if(CollectionUtils.isEmpty(roleResourceForm.getResourceIds())){
            roleResourceService.removeByRoleId(roleResourceForm.getRoleId());
        }else{
            roleResourceService.removeBatch(roleResourceForm.getRoleId(), roleResourceForm.getResourceIds());
        }
        return this.success();
    }
}