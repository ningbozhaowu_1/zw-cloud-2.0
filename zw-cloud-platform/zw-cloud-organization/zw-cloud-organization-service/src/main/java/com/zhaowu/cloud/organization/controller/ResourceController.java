package com.zhaowu.cloud.organization.controller;

import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.organization.entity.form.ResourceForm;
import com.zhaowu.cloud.organization.entity.form.ResourceQueryForm;
import com.zhaowu.cloud.organization.entity.po.Resource;
import com.zhaowu.cloud.organization.entity.vo.ResourceVO;
import com.zhaowu.cloud.organization.service.ResourceService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/resource/admin/v1")
@Api("resource")
@Slf4j
public class ResourceController extends BaseController {

    @Autowired
    private ResourceService resourceService;

    @ApiOperation(value = "新增资源", notes = "新增一个资源")
    @ApiImplicitParam(name = "resourceForm", value = "新增资源form表单", required = true, dataType = "ResourceForm")
    @PostMapping
    public Result add(@Valid @RequestBody ResourceForm resourceForm) {
        log.debug("name:{}", resourceForm);
        Resource resource = resourceForm.toPo(Resource.class);
        return this.success(resourceService.add(resource));
    }

    @ApiOperation(value = "删除资源", notes = "根据url的id来指定删除对象")
    @ApiImplicitParam(paramType = "path", name = "id", value = "资源ID", required = true, dataType = "string")
    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable String id) {
        return this.success(resourceService.delete(id));
    }

    @ApiOperation(value = "修改资源", notes = "修改指定资源信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "资源ID", required = true, dataType = "string"),
            @ApiImplicitParam(name = "resourceForm", value = "资源实体", required = true, dataType = "ResourceForm")
    })
    @PutMapping(value = "/{id}")
    public Result update(@PathVariable String id, @Valid @RequestBody ResourceForm resourceForm) {
        Resource resource = resourceForm.toPo(id, Resource.class);
        return this.success(resourceService.update(resource));
    }

    @ApiOperation(value = "获取资源", notes = "获取指定资源信息")
    @ApiImplicitParam(paramType = "path", name = "id", value = "资源ID", required = true, dataType = "string")
    @GetMapping(value = "/{id}")
    public Result get(@PathVariable String id) {
        log.debug("get with id:{}", id);
        return this.success(resourceService.get(id));
    }

    @ApiOperation(value = "查询资源", notes = "根据roleIds查询角色所拥有的资源信息")
    @ApiResponses(
            @ApiResponse(code = 200, message = "处理成功", response = Result.class)
    )
    @PostMapping(value = "/queryByRoleIds")
    public Result<Collection<ResourceVO>> queryByRoleIds(@Valid @RequestBody ResourceQueryForm resourceQueryForm) {

        log.debug("query with roleIds:{}", resourceQueryForm.getRoleIds());
        List<Resource> resourceList = resourceService.query(resourceQueryForm.getRoleIds());
        if(CollectionUtils.isEmpty(resourceList)){
            return this.success();
        }
        List<ResourceVO> resourceVOList = JSON.parseArray(JSON.toJSONString(resourceList), ResourceVO.class);
        return this.success(resourceVOList);
    }

    @ApiOperation(value = "查询当前用户的资源")
    @GetMapping(value = "/get")
    public Result<Collection<ResourceVO>> get() {

        List<Resource> resourceList = resourceService.query(UserContextHolder.getInstance().getAuthorities());
        List<ResourceVO> resourceVOList = JSON.parseArray(JSON.toJSONString(resourceList), ResourceVO.class);
        return this.success(resourceVOList);
    }

    @ApiOperation(value = "查询所有资源", notes = "查询所有资源信息")
    @ApiResponses(
            @ApiResponse(code = 200, message = "处理成功", response = Result.class)
    )
    @GetMapping(value = "/all")
    public Result<Collection<ResourceVO>> queryAll() {

        List<Resource> resourceList = resourceService.getAll();
        List<ResourceVO> resourceVOList = JSON.parseArray(JSON.toJSONString(resourceList), ResourceVO.class);
        return this.success(resourceVOList);
    }

//    @ApiOperation(value = "搜索资源", notes = "根据条件搜索资源信息")
//    @ApiImplicitParam(name = "resourceQueryForm", value = "资源查询参数", required = true, dataType = "RoleQueryForm")
//    @ApiResponses(
//            @ApiResponse(code = 200, message = "处理成功", response = Result.class)
//    )
//    @PostMapping(value = "/conditions")
//    public Result query(@Valid @RequestBody ResourceQueryForm resourceQueryForm) {
//        log.debug("query with name:{}", resourceQueryForm);
//        return this.success(resourceService.query(resourceQueryForm.getPage(), resourceQueryForm.toParam(ResourceQueryParam.class)));
//    }
}