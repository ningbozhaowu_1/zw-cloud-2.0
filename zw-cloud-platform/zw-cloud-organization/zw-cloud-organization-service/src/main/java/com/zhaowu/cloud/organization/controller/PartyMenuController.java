package com.zhaowu.cloud.organization.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.organization.entity.po.PartyMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.organization.service.PartyMenuService;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 参与者菜单关系表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@RestController
@RequestMapping("/organization/partyMenu")
@Api(tags = "参与者菜单关系表 接口")
public class PartyMenuController extends BaseController {

        @Autowired
        private PartyMenuService partyMenuService;

        /**
         * 获取参与者菜单关系表列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取参与者菜单关系表列表")
        public Result<List<PartyMenu>> list() {
            return this.success();
        }

}

