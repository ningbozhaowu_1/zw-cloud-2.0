package com.zhaowu.cloud.organization.exception;

import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import lombok.Getter;

@Getter
public enum OrganizationErrorStatus implements ErrorStatus {

    ROLE_NOT_FOUND("030200", "角色未找到！");

    /**
     * 错误类型码
     */
    private String code;
    /**
     * 错误类型描述信息
     */
    private String message;

    OrganizationErrorStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
