package com.zhaowu.cloud.organization.service.impl;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.organization.entity.param.MenuQueryParam;
import com.zhaowu.cloud.organization.entity.po.Menu;
import com.zhaowu.cloud.organization.mapper.MenuMapper;
import com.zhaowu.cloud.organization.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@Slf4j
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public boolean add(Menu menu) {
        return this.save(menu);
    }

    @Override
    @CacheInvalidate(name = "menu::", key = "#id")
    public boolean delete(String id) {
        return this.removeById(id);
    }

    @Override
    public List<Menu> getRoleMenuList(String applicationParty, Collection<String> roleIds) {
        return menuMapper.getRoleMenuList(applicationParty, roleIds);
    }

    @Override
    public List<Menu> getRoleMenuList(String roleId) {
        return menuMapper.getRoleMenuList1(roleId);
    }

    @Override
    public List<Menu> getMenuListByCode(String applicationParty, String roleCode) {
        return menuMapper.getMenuListByCode(applicationParty, roleCode);
    }

    @Override
    public List<Menu> getAllMenu() {
        return this.list();
    }

    @Override
    public IPage<Menu> pageList(Page<Menu> page, MenuQueryParam menuQueryParam) {

        QueryWrapper<Menu> queryWrapper = menuQueryParam.build();
        queryWrapper.orderByDesc("order_num","create_time");
        return this.page(page, queryWrapper);
    }

    @Override
    @CacheInvalidate(name = "menu::", key = "#menu.id")
    public boolean update(Menu menu) {
        return this.updateById(menu);
    }

    @Override
    @Cached(name = "menu::", key = "#id", cacheType = CacheType.BOTH)
    public Menu get(String id) {
        return this.getById(id);
    }

    @Override
    public List<Menu> query(MenuQueryParam menuQueryParam) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(null != menuQueryParam.getName(), "name", menuQueryParam.getName());
        queryWrapper.orderByDesc("order_num","create_time");
        return this.list(queryWrapper);
    }

    @Override
    public List<Menu> queryByParentId(String id) {
        return this.list(new QueryWrapper<Menu>().eq("parent_id", id));
    }
}
