package com.zhaowu.cloud.organization.controller;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.organization.entity.form.RoleForm;
import com.zhaowu.cloud.organization.entity.po.MenuGoods;
import com.zhaowu.cloud.organization.entity.po.PartyMenu;
import com.zhaowu.cloud.organization.service.PartyMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.organization.service.MenuGoodsService;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 菜单商品关系表 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@RestController
@RequestMapping("/menuGoods/admin/v1")
@Api(tags = "菜单商品关系表 接口")
public class MenuGoodsController extends BaseController {

        @Autowired
        private MenuGoodsService menuGoodsService;

        @Autowired
        private PartyMenuService partyMenuService;

        /**
         * 根据商品编号授权应用菜单
         */
        @GetMapping(value = "/authorizerAppMenu")
        @ApiIgnore
        public Result authorizerAppMenu(@Valid @RequestParam Collection<String> goodsIds,
                                        @Valid @RequestParam String applicationParty) {
                List<MenuGoods> menuGoodsList = menuGoodsService.getByGoodsIds(goodsIds);

                List<PartyMenu> partyMenus = new ArrayList<PartyMenu>();
                for(MenuGoods menuGoods : menuGoodsList){
                        PartyMenu partyMenu = new PartyMenu();
                        partyMenu.setApplicationParty(applicationParty);
                        partyMenu.setMenuId(menuGoods.getMenuId());
                        partyMenus.add(partyMenu);
                }
                partyMenuService.batchAdd(partyMenus);
                return this.success();
        }


}

