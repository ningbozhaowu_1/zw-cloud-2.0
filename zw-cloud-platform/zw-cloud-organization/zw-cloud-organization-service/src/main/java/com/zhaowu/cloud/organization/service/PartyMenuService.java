package com.zhaowu.cloud.organization.service;

import com.zhaowu.cloud.organization.entity.po.PartyMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 参与者菜单关系表 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
public interface PartyMenuService{

    void batchAdd(List<PartyMenu> partyMenuList);
}
