package com.zhaowu.cloud.organization.service.impl;

import com.zhaowu.cloud.organization.entity.po.PartyMenu;
import com.zhaowu.cloud.organization.mapper.PartyMenuMapper;
import com.zhaowu.cloud.organization.service.PartyMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参与者菜单关系表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@Service
public class PartyMenuServiceImpl extends ServiceImpl<PartyMenuMapper,PartyMenu> implements PartyMenuService {

    @Override
    public void batchAdd(List<PartyMenu> partyMenuList) {
        this.saveBatch(partyMenuList);
    }
}
