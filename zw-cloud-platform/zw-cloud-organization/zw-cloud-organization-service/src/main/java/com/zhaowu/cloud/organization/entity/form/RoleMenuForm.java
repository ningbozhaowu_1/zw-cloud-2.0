package com.zhaowu.cloud.organization.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.organization.entity.po.RoleMenuRelation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@ApiModel
@Data
public class RoleMenuForm extends BaseForm<RoleMenuRelation> {

    @NotNull(message = "菜单编号不能为空")
    @ApiModelProperty(value = "菜单编号")
    private Collection<String> menuIds;

    @NotBlank(message = "角色编号不能为空")
    @ApiModelProperty(value = "角色编号")
    private String roleId;

}
