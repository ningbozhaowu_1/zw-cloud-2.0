package com.zhaowu.cloud.organization.client;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.organization.entity.form.ResourceQueryForm;
import com.zhaowu.cloud.organization.entity.vo.ResourceVO;
import com.zhaowu.cloud.organization.entity.vo.RoleVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@FeignClient(name= "organization-service")
public interface OrganizationClient {

    @PostMapping(value = "/resource/admin/v1/queryByRoleIds")
    Result<Collection<ResourceVO>> queryByRoleIds(@RequestBody ResourceQueryForm resourceQueryForm);

    @GetMapping(value = "/resource/admin/v1/all")
    Result<Collection<ResourceVO>> resources();

    @GetMapping(value = "/role/admin/v1/getRoleIdByCode")
    Result<RoleVO> getRoleIdByCode(@RequestParam(value = "code") String code);

    @GetMapping(value = "/role/admin/v1/{id}")
    Result<RoleVO> getRoleIdById(@PathVariable(value = "id") String id);

    @GetMapping(value = "/menuGoods/admin/v1/authorizerAppMenu")
    Result authorizerAppMenu(@RequestParam(value = "goodsIds")  Collection<String> goodsIds, @RequestParam(value = "applicationParty")  String applicationParty);
}
