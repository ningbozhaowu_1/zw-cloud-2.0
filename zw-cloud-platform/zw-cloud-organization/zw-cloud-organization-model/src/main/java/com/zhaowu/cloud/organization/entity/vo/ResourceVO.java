package com.zhaowu.cloud.organization.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResourceVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String code;
    private String type;
    private String url;
    private String method;
    private String name;
    private String description;
}
