package com.zhaowu.cloud.organization.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String code;
    private String name;
    private String partyId;
}
