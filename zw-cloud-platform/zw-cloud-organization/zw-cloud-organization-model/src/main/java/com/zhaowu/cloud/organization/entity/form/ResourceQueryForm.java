package com.zhaowu.cloud.organization.entity.form;

import com.zhaowu.cloud.organization.entity.vo.ResourceVO;
import lombok.Data;

import java.util.Collection;

@Data
public class ResourceQueryForm {

    private Collection<String> roleIds;

}
