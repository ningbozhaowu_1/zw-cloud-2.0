--商品中心
CREATE DATABASE IF NOT EXISTS `zw-goods` DEFAULT CHARACTER SET = utf8mb4;
CREATE USER `goods`@'%' IDENTIFIED BY 'goods123';
GRANT ALL PRIVILEGES ON `zw-goods`.* TO `goods`;
GRANT ALL PRIVILEGES ON `zw-goods`.* TO `goods`;

USE `zw-goods`;

DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `APPLICATION_ID` bigint(20) NOT NULL COMMENT '应用编号',
  `NAME` varchar(256) NOT NULL COMMENT '商品名称',
  `GOODS_CODE` varchar(50) DEFAULT NULL COMMENT '商品编号',
  `IMAGE` varchar(2048) DEFAULT NULL COMMENT '商品封面图',
  `VIDEO` varchar(2048) DEFAULT NULL COMMENT '商品封面视频',
  `STATUS` char(3) NOT NULL DEFAULT '1' COMMENT '商品状态 状态: 1-可用，0-下架，-1-上架',
  `STOCK` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `SALES` int(11) DEFAULT '0' COMMENT '销量',
  `SHARE_DESC` varchar(256) DEFAULT NULL COMMENT '分享描述',
  `INTRODUCTION` varchar(1024) DEFAULT NULL COMMENT '商品简介',
  `VIP_INDICATOR` char(3) NOT NULL DEFAULT '0' COMMENT '是否参与会员价 1是0否',
  `COST_TYPE` char(3) NOT NULL DEFAULT '1' COMMENT '库存扣减方式 1下单立减2付款后减',
  `GOODS_TYPE_ID` bigint(20) NOT NULL COMMENT '商品分类编号',
  `CATEGORY_ID` bigint(20) DEFAULT NULL COMMENT '类目编号',
  `REVISION` int(11) DEFAULT NULL COMMENT '乐观锁',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`),
  KEY `index_goods_type_id_goods` (`GOODS_TYPE_ID`,`APPLICATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347171171749085186 DEFAULT CHARSET=utf8mb4 COMMENT='商品信息表';

-- ----------------------------
-- Records of goods
-- ----------------------------
BEGIN;
INSERT INTO `goods` VALUES (1347170950231113730, -1, '测试商品1', 'F19C9Bk3X9BQIGTVdYORCd7NQ', 'https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg', NULL, '1', 115, 0, '这是个好商品', '这是个好商品这是个好商品', '0', '1', 1347169793043611650, NULL, NULL, 1, '2021-01-07 21:19:18', NULL, NULL);
INSERT INTO `goods` VALUES (1347171171749085185, -1, '测试商品2', 'fuOjLEJbSX2uD2IvBqBsEw', 'https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg', NULL, '1', 115, 0, '这是个好商品', '这是个好商品这是个好商品', '0', '1', 1347169845958950913, NULL, NULL, 1, '2021-01-07 21:20:11', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_category
-- ----------------------------
DROP TABLE IF EXISTS `goods_category`;
CREATE TABLE `goods_category` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `NAME` varchar(256) NOT NULL COMMENT '类目名称',
  `PARENT_ID` bigint(20) DEFAULT NULL COMMENT '父类目 父类目id,顶级类目填0',
  `IS_PARENT` char(3) DEFAULT NULL COMMENT '父类目标记 是否为父节点，0为否，1为是',
  `SORT` int(10) DEFAULT NULL COMMENT '排序编号 排序指数，越小越靠前',
  `REVISION` int(11) DEFAULT NULL COMMENT '乐观锁',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品类目';

-- ----------------------------
-- Records of goods_category
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for goods_category_param
-- ----------------------------
DROP TABLE IF EXISTS `goods_category_param`;
CREATE TABLE `goods_category_param` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `CATEGORY_ID` bigint(20) DEFAULT NULL COMMENT '类目编号',
  `APPLICATION_ID` bigint(20) DEFAULT NULL COMMENT '应用编号',
  `NAME` varchar(256) DEFAULT NULL COMMENT '属性名',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '属性单位',
  `VALUE` varchar(256) DEFAULT NULL COMMENT '属性值',
  `INIT` char(3) DEFAULT NULL COMMENT '是否是初始化参数 1是初始化0不是',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品类目参数表';

-- ----------------------------
-- Records of goods_category_param
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for goods_commission
-- ----------------------------
DROP TABLE IF EXISTS `goods_commission`;
CREATE TABLE `goods_commission` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `COMMISSION_TYPE` varchar(20) NOT NULL COMMENT '佣金类型',
  `PRICE` decimal(32,8) NOT NULL COMMENT '佣金金额',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品佣金';

-- ----------------------------
-- Records of goods_commission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for goods_contents
-- ----------------------------
DROP TABLE IF EXISTS `goods_contents`;
CREATE TABLE `goods_contents` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `CONTENTS` text COMMENT '描述',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GOODS_ID` (`GOODS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347171171937828866 DEFAULT CHARSET=utf8mb4 COMMENT='商品描述';

-- ----------------------------
-- Records of goods_contents
-- ----------------------------
BEGIN;
INSERT INTO `goods_contents` VALUES (1347170950709264385, 1347170950231113730, '<p>圣诞树1</p><p><img src=\"https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg\" alt=\"\"/></p>', 1, '2021-01-07 21:19:19', NULL, NULL);
INSERT INTO `goods_contents` VALUES (1347171171937828865, 1347171171749085185, '<p>圣诞树1</p><p><img src=\"https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg\" alt=\"\"/></p>', 1, '2021-01-07 21:20:11', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_group
-- ----------------------------
DROP TABLE IF EXISTS `goods_group`;
CREATE TABLE `goods_group` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `APPLICATION_ID` bigint(20) NOT NULL COMMENT '应用编号',
  `NAME` varchar(256) NOT NULL COMMENT '名称',
  `SHOW_TYPE` varchar(32) NOT NULL DEFAULT '1' COMMENT '显示样式 1大图2小图3一大两小4详细列表',
  `GOODS_NAME_INDICATOR` char(3) DEFAULT '1' COMMENT '商品名称显示标识 1是0否',
  `GOODS_PRICE_INDICATOR` char(3) DEFAULT '1' COMMENT '商品价格显示标识 1是0否',
  `ICON` varchar(2048) DEFAULT NULL COMMENT '分组图标',
  `STATUS` char(3) NOT NULL COMMENT '状态 1有效0失效',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1347372435271426050 DEFAULT CHARSET=utf8mb4 COMMENT='商品分组表';

-- ----------------------------
-- Records of goods_group
-- ----------------------------
BEGIN;
INSERT INTO `goods_group` VALUES (1347372435271426049, -1, '分组2', '2', '1', '1', NULL, '1', 1, '2021-01-08 10:39:56', 1, '2021-01-08 13:59:06');
COMMIT;

-- ----------------------------
-- Table structure for goods_group_goods
-- ----------------------------
DROP TABLE IF EXISTS `goods_group_goods`;
CREATE TABLE `goods_group_goods` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `GOODS_GROUP_ID` bigint(20) NOT NULL COMMENT '分组编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`),
  KEY `index_goods_group_id_goods_group_goods` (`GOODS_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品分组关系表';

-- ----------------------------
-- Records of goods_group_goods
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for goods_image
-- ----------------------------
DROP TABLE IF EXISTS `goods_image`;
CREATE TABLE `goods_image` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `IMAGE` varchar(2048) NOT NULL COMMENT '商品图片',
  `NAME` varchar(256) NOT NULL COMMENT '图片名称',
  `SIZE` bigint(20) DEFAULT NULL COMMENT '图片大小',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`),
  KEY `index_goods_id_goods_image` (`GOODS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347171171891691522 DEFAULT CHARSET=utf8mb4 COMMENT='商品图片表';

-- ----------------------------
-- Records of goods_image
-- ----------------------------
BEGIN;
INSERT INTO `goods_image` VALUES (1347170950600212482, 1347170950231113730, 'https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg', '图1', NULL, 1, '2021-01-07 21:19:19', NULL, NULL);
INSERT INTO `goods_image` VALUES (1347171171891691521, 1347171171749085185, 'https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg', '图1', NULL, 1, '2021-01-07 21:20:11', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_property
-- ----------------------------
DROP TABLE IF EXISTS `goods_property`;
CREATE TABLE `goods_property` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `NAME_ID` bigint(20) NOT NULL COMMENT '属性名称编号',
  `VALUE_ID` bigint(20) NOT NULL COMMENT '属性值编号',
  `TYPE` char(3) NOT NULL COMMENT '属性类型\n1-商品属性\n2-类目属性\n3-分类属性',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347171171971383299 DEFAULT CHARSET=utf8mb4 COMMENT='商品属性';

-- ----------------------------
-- Records of goods_property
-- ----------------------------
BEGIN;
INSERT INTO `goods_property` VALUES (1347170950776373250, 1347170950231113730, 1347169995062263809, 1347169995120984066, '3', 1, '2021-01-07 21:19:19', NULL, NULL);
INSERT INTO `goods_property` VALUES (1347171171971383298, 1347171171749085185, 1347170089190834178, 1347170089257943041, '3', 1, '2021-01-07 21:20:11', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_property_name
-- ----------------------------
DROP TABLE IF EXISTS `goods_property_name`;
CREATE TABLE `goods_property_name` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `APPLICATION_ID` bigint(20) NOT NULL COMMENT '应用编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `NAME` varchar(256) NOT NULL COMMENT '属性名称',
  `SALE_INDICATOR` char(3) NOT NULL COMMENT '销售属性 0否1是',
  `GOOD_TYPE_NAME_ID` bigint(20) DEFAULT NULL COMMENT '分类属性编号',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '属性单位',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品属性名表';

-- ----------------------------
-- Records of goods_property_name
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for goods_property_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_property_value`;
CREATE TABLE `goods_property_value` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `NAME_ID` bigint(20) NOT NULL COMMENT '属性名称编号',
  `APPLICATION_ID` bigint(20) NOT NULL COMMENT '应用编号',
  `VALUE` varchar(256) NOT NULL COMMENT '属性值',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品属性值';

-- ----------------------------
-- Records of goods_property_value
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE `goods_sku` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `GOODS_ID` bigint(20) NOT NULL COMMENT '商品编号',
  `PRICE` decimal(32,2) NOT NULL COMMENT '商品价格',
  `COST` decimal(32,2) DEFAULT NULL COMMENT '成本价',
  `STOCK` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `WEIGHT` decimal(32,2) DEFAULT '0.00' COMMENT '重量',
  `SALES` int(11) DEFAULT '0' COMMENT '销量',
  `PROPERTY_VALUE_IDS` varchar(256) DEFAULT NULL COMMENT '属性值 属性值ID组合',
  `REVISION` int(11) DEFAULT NULL COMMENT '乐观锁',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GOODS_ID` (`GOODS_ID`,`PROPERTY_VALUE_IDS`),
  KEY `index_goods_id_goods_sku` (`GOODS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347171171837165571 DEFAULT CHARSET=utf8mb4 COMMENT='商品库存';

-- ----------------------------
-- Records of goods_sku
-- ----------------------------
BEGIN;
INSERT INTO `goods_sku` VALUES (1347170950382108673, 1347170950231113730, 30.00, 20.00, 5, 0.00, 0, '1347170285354237954:1347170373367513090', NULL, 1, '2021-01-07 21:19:18', NULL, NULL);
INSERT INTO `goods_sku` VALUES (1347170950403080193, 1347170950231113730, 30.00, 20.00, 10, 0.00, 0, '1347170285354237954:1347170373388484610', NULL, 1, '2021-01-07 21:19:18', NULL, NULL);
INSERT INTO `goods_sku` VALUES (1347170950407274497, 1347170950231113730, 30.00, 20.00, 100, 0.00, 0, '1347170285421346818:1347170373447204866', NULL, 1, '2021-01-07 21:19:18', NULL, NULL);
INSERT INTO `goods_sku` VALUES (1347171171816194050, 1347171171749085185, 30.00, 20.00, 5, 0.00, 0, '1347170285354237954:1347170373367513090', NULL, 1, '2021-01-07 21:20:11', NULL, NULL);
INSERT INTO `goods_sku` VALUES (1347171171824582658, 1347171171749085185, 30.00, 20.00, 10, 0.00, 0, '1347170285354237954:1347170373388484610', NULL, 1, '2021-01-07 21:20:11', NULL, NULL);
INSERT INTO `goods_sku` VALUES (1347171171837165570, 1347171171749085185, 30.00, 20.00, 100, 0.00, 0, '1347170285421346818:1347170373447204866', NULL, 1, '2021-01-07 21:20:11', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_sku_property_name
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_property_name`;
CREATE TABLE `goods_sku_property_name` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `APPLICATION_ID` bigint(20) DEFAULT NULL COMMENT '应用编号',
  `NAME` varchar(256) DEFAULT NULL COMMENT '属性名称',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347170373287821314 DEFAULT CHARSET=utf8mb4 COMMENT='商品属性名表';

-- ----------------------------
-- Records of goods_sku_property_name
-- ----------------------------
BEGIN;
INSERT INTO `goods_sku_property_name` VALUES (1347170285320683522, -1, '颜色', 1, '2021-01-07 21:16:40', NULL, NULL);
INSERT INTO `goods_sku_property_name` VALUES (1347170373287821313, -1, '尺码', 1, '2021-01-07 21:17:01', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_sku_property_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_property_value`;
CREATE TABLE `goods_sku_property_value` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `NAME_ID` bigint(20) DEFAULT NULL COMMENT '属性名称编号',
  `VALUE` varchar(256) DEFAULT NULL COMMENT '属性值',
  `CREATE_BY` bigint(20) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` bigint(20) DEFAULT NULL COMMENT '更新人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1347170373447204867 DEFAULT CHARSET=utf8mb4 COMMENT='商品属性值';

-- ----------------------------
-- Records of goods_sku_property_value
-- ----------------------------
BEGIN;
INSERT INTO `goods_sku_property_value` VALUES (1347170285354237954, 1347170285320683522, '红色', 1, '2021-01-07 21:16:40', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170285391986689, 1347170285320683522, '绿色', 1, '2021-01-07 21:16:40', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170285421346818, 1347170285320683522, '白色', 1, '2021-01-07 21:16:40', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170285459095554, 1347170285320683522, '黑色', 1, '2021-01-07 21:16:40', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170373367513090, 1347170373287821313, 'M', 1, '2021-01-07 21:17:01', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170373388484610, 1347170373287821313, 'L', 1, '2021-01-07 21:17:01', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170373417844737, 1347170373287821313, 'XL', 1, '2021-01-07 21:17:01', NULL, NULL);
INSERT INTO `goods_sku_property_value` VALUES (1347170373447204866, 1347170373287821313, 'XXL', 1, '2021-01-07 21:17:01', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_type
-- ----------------------------
DROP TABLE IF EXISTS `goods_type`;
CREATE TABLE `goods_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `application_id` bigint(20) DEFAULT NULL COMMENT '应用编号',
  `name` varchar(256) NOT NULL COMMENT '分类名称',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父分类id 顶级分类填0',
  `is_parent` char(3) DEFAULT NULL COMMENT '是否为父节点 0为否，1为是',
  `sort` int(10) DEFAULT NULL COMMENT '排序编号 排序指数越小越靠前',
  `status` char(3) DEFAULT NULL COMMENT '状态 1有效0失效',
  `icon` varchar(2048) DEFAULT NULL COMMENT '图标 Icon地址',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1347458985015431170 DEFAULT CHARSET=utf8mb4 COMMENT='商品分类';

-- ----------------------------
-- Records of goods_type
-- ----------------------------
BEGIN;
INSERT INTO `goods_type` VALUES (1347169793043611650, -1, '新品', 0, '1', NULL, '0', 'https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg', 1, '2021-01-07 21:14:43', 1, '2021-01-08 10:39:17');
INSERT INTO `goods_type` VALUES (1347169845958950913, -1, '折扣', 0, '1', NULL, '1', 'https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com/1/1/IMAGE/HOTEL/d489b9bf-2ea0-4379-87ad-ede38fc527b1.jpg', 1, '2021-01-07 21:14:55', 1, '2021-01-08 16:23:51');
INSERT INTO `goods_type` VALUES (1347360767539802113, -1, '分类1', 0, '1', NULL, '1', NULL, 1, '2021-01-08 09:53:34', 1, '2021-01-08 10:22:43');
INSERT INTO `goods_type` VALUES (1347456349230911490, -1, 'test', 0, '0', NULL, '1', NULL, 1, '2021-01-08 16:13:23', NULL, NULL);
INSERT INTO `goods_type` VALUES (1347458985015431169, -1, 'test1', 1347169845958950913, '0', NULL, '1', NULL, 1, '2021-01-08 16:23:51', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_type_property_name
-- ----------------------------
DROP TABLE IF EXISTS `goods_type_property_name`;
CREATE TABLE `goods_type_property_name` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `application_id` bigint(20) NOT NULL COMMENT '应用编号',
  `goods_type_id` bigint(20) NOT NULL COMMENT '商品分类编号',
  `name` varchar(256) DEFAULT NULL COMMENT '属性名称',
  `search_indicator` char(3) DEFAULT NULL COMMENT '搜索标别 0否1是',
  `unit` varchar(100) DEFAULT NULL COMMENT '属性单位',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1347421711317913602 DEFAULT CHARSET=utf8mb4 COMMENT='商品分类属性名表';

-- ----------------------------
-- Records of goods_type_property_name
-- ----------------------------
BEGIN;
INSERT INTO `goods_type_property_name` VALUES (1347170114956443649, -1, 1347169845958950913, '材料', '1', NULL, 1, '2021-01-07 21:15:59', NULL, NULL);
INSERT INTO `goods_type_property_name` VALUES (1347420621771952129, -1, 1347169793043611650, 'test', '0', '分', 1, '2021-01-08 13:51:25', NULL, NULL);
INSERT INTO `goods_type_property_name` VALUES (1347421711317913601, -1, 1347169793043611650, 'test2', '1', '123', 1, '2021-01-08 13:55:45', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_type_property_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_type_property_value`;
CREATE TABLE `goods_type_property_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name_id` bigint(20) DEFAULT NULL COMMENT '属性名称编号',
  `value` varchar(256) DEFAULT NULL COMMENT '属性值',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1347421711393411074 DEFAULT CHARSET=utf8mb4 COMMENT='商品分类属性值';

-- ----------------------------
-- Records of goods_type_property_value
-- ----------------------------
BEGIN;
INSERT INTO `goods_type_property_value` VALUES (1347170114994192385, 1347170114956443649, '羊毛', 1, '2021-01-07 21:15:59', NULL, NULL);
INSERT INTO `goods_type_property_value` VALUES (1347170115023552514, 1347170114956443649, '鸭绒', 1, '2021-01-07 21:15:59', NULL, NULL);
INSERT INTO `goods_type_property_value` VALUES (1347420621830672386, 1347420621771952129, '1', 1, '2021-01-08 13:51:25', NULL, NULL);
INSERT INTO `goods_type_property_value` VALUES (1347420621931335682, 1347420621771952129, '3', 1, '2021-01-08 13:51:25', NULL, NULL);
INSERT INTO `goods_type_property_value` VALUES (1347420621969084417, 1347420621771952129, '5', 1, '2021-01-08 13:51:25', NULL, NULL);
INSERT INTO `goods_type_property_value` VALUES (1347421711393411073, 1347421711317913601, '1213', 1, '2021-01-08 13:55:45', NULL, NULL);
COMMIT;

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `type` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '日志类型:登录日志;系统日志;',
  `name` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '业务名称',
  `request_url` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求地址',
  `request_type` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求方法类型',
  `request_class` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '调用类',
  `request_method` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '调用方法',
  `request_ip` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '请求IP',
  `user_id` bigint(20) DEFAULT NULL COMMENT '创建用户Id',
  `user_name` varchar(225) COLLATE utf8_bin DEFAULT NULL COMMENT '创建用户名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `time` int(11) DEFAULT NULL COMMENT '消耗时间',
  `success` int(11) DEFAULT NULL COMMENT '是否成功',
  `exception` varchar(2250) COLLATE utf8_bin DEFAULT NULL COMMENT '异常信息',
  `request_parameter` varbinary(2250) DEFAULT NULL COMMENT '调用参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

DROP TABLE IF EXISTS goods_shopping_cart;
CREATE TABLE goods_shopping_cart(
    id BIGINT(19) NOT NULL AUTO_INCREMENT  COMMENT '编号' ,
    user_id BIGINT(19) NOT NULL   COMMENT '用户编号' ,
    goods_id BIGINT(19) NOT NULL   COMMENT '商品编号' ,
    goods_sku_id BIGINT(19) NOT NULL   COMMENT '商品sku编号' ,
    quantity INT NOT NULL   COMMENT '数量' ,
    selected CHAR(3) NOT NULL  DEFAULT 1 COMMENT '勾选表识 1是0否' ,
    create_by BIGINT(19) NOT NULL   COMMENT '创建人' ,
    create_time DATETIME NOT NULL   COMMENT '创建时间' ,
    update_by BIGINT(19)    COMMENT '更新人' ,
    update_time DATETIME    COMMENT '更新时间' ,
    PRIMARY KEY (id)
) COMMENT = '商品购物车 ';

ALTER TABLE goods_shopping_cart COMMENT '商品购物车';