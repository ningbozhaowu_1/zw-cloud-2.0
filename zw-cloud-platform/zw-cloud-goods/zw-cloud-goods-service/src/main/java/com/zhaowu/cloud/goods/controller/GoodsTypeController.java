package com.zhaowu.cloud.goods.controller;

import com.zhaowu.cloud.common.web.audit.EnableAudit;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.TreeUtil;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.framework.util.bean.Node;
import com.zhaowu.cloud.goods.entity.form.GoodsTypeForm;
import com.zhaowu.cloud.goods.entity.form.GoodsTypePropertyForm;
import com.zhaowu.cloud.goods.entity.form.ModifyGoodsTypeForm;
import com.zhaowu.cloud.goods.entity.po.GoodsType;
import com.zhaowu.cloud.goods.entity.po.GoodsTypePropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsTypePropertyValue;
import com.zhaowu.cloud.goods.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.goods.service.GoodsTypeService;

import java.util.LinkedList;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import javax.validation.Valid;
/**
 * <p>
 * 商品分类 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@RestController
@RequestMapping("/goodsType/admin/v1")
@Api(tags = "管理端商品分类 接口")
@EnableAudit
public class GoodsTypeController extends BaseController {

        @Autowired
        private GoodsTypeService goodsTypeService;
        /**
         * 新增商品分类
         */
        @PostMapping(value = "/add")
        @ApiOperation("新增商品分类")
        public Result<String> add(@Valid @RequestBody GoodsTypeForm goodsTypeForm) {
            GoodsType goodsType = new GoodsType();
            goodsType.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
            BeanUtils.copyProperties(goodsTypeForm, goodsType);
            this.goodsTypeService.add(goodsType);
            return this.success();
        }

        /**
         * 修改商品分类
         */
        @PostMapping(value = "/modify")
        @ApiOperation("修改商品分类")
        public Result<String> modify(@Valid @RequestBody ModifyGoodsTypeForm modifyGoodsTypeForm) {
            GoodsType goodsType = new GoodsType();
            BeanUtils.copyProperties(modifyGoodsTypeForm, goodsType);
            this.goodsTypeService.modify(goodsType);
            return this.success();
        }

        @DeleteMapping(value = "/delete")
        @ApiOperation("物理删除商品分类")
        public Result<String> delete(@Valid @RequestParam String id) {

            this.goodsTypeService.delete(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId(), id);
            return this.success();
        }

        @DeleteMapping(value = "/deleteGoodsTypeProperty")
        @ApiOperation("删除分类属性")
        public Result<String> deleteGoodsTypeProperty(@Valid @RequestParam String id) {
            goodsTypeService.removeGoodsTypeProperty(id);
            return this.success();
        }

        /**
         * 新增分类属性
         */
        @PostMapping(value = "/addGoodsTypeProperty")
        @ApiOperation("新增分类属性")
        public Result<String> addGoodsTypeProperty(@Valid @RequestBody GoodsTypePropertyForm goodsTypePropertyForm) {
            goodsTypeService.addGoodsTypeProperty(goodsTypePropertyForm);
            return this.success();
        }

        /**
         * 管理端分类属性列表
         */
        @GetMapping(value = "/goodsTypePropertyNameList")
        @ApiOperation("管理端分类属性列表")
        public Result<List<GoodsTypePropertyName>> goodsTypePropertyNameList(@Valid @RequestParam String groupTypeId) {

            return this.success(goodsTypeService.getGoodsTypePropertyNameList(groupTypeId));
        }

        /**
         * 管理端分类属性值列表
         */
        @GetMapping(value = "/goodsTypeValueNameList")
        @ApiOperation("管理端分类属性列表")
        public Result<List<GoodsTypePropertyValue>> goodsTypeValueNameList(@Valid @RequestParam String nameId) {

            return this.success(goodsTypeService.getGoodsTypePropertyValueList(nameId));
        }

        /**
         * 商品分类列表
         */
        @GetMapping(value = "/goodsTypeList")
        @ApiOperation("商品分类列表")
        public Result<List<GoodsType>> goodsTypeList() {

            List<GoodsType> goodsTypes = goodsTypeService.getGoodsTypeList(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());

            return success(goodsTypes);
        }

        /**
         * 商品分类一级列表
         */
        @GetMapping(value = "/goodsTypeRootList")
        @ApiOperation("商品分类一级列表")
        public Result<List<GoodsType>> goodsTypeRootList() {

            List<GoodsType> goodsTypes = goodsTypeService.getGoodsTypeRootList(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());

            return success(goodsTypes);
        }

        /**
         * 商品子分类列表
         */
        @GetMapping(value = "/goodsTypeListByParentId")
        @ApiOperation("商品子分类列表")
        public Result<List<GoodsType>> goodsTypeListByParentId(@Valid @RequestParam String parentId) {

            List<GoodsType> goodsTypes = goodsTypeService.getGoodsTypeListByParentId(parentId);
            return success(goodsTypes);
        }

        /**
         * 商品分类树
         */
        @GetMapping(value = "/getGoodsTypeTree")
        @ApiOperation("商品分类树")
        public Result<List<Node>> getGoodsTypeTree() {

            List<GoodsType> goodsTypes = goodsTypeService.getGoodsTypeList(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
            List<Node> nodes = new LinkedList<Node>();
            for(GoodsType goodsType : goodsTypes){
                Node node = new Node();
                node.setIcon(goodsType.getIcon());
                node.setId(goodsType.getId());
                node.setPid(goodsType.getParentId());
                node.setName(goodsType.getName());
                node.setStatus(goodsType.getStatus());
                nodes.add(node);
            }
            return this.success(TreeUtil.data("0" ,nodes));
        }

}

