package com.zhaowu.cloud.goods.entity.vo;

import com.zhaowu.cloud.goods.entity.po.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class GoodsDetailVO implements Serializable {

    private List<GoodsImage> goodsImageList;

    private GoodsContents goodsContents;

    private Goods goods;

    private Map<String, List<GoodsSkuPropertyValue>> goodsSkuPropertyValueListMap;

    private Map<String, GoodsSkuVO> goodsSkuListMap;

//    private Map<String, List> goodsPropertyList;

    private Map<String, List<GoodsPropertyVO>> goodsPropertyVOSMap;
}
