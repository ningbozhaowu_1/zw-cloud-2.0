package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.zhaowu.cloud.goods.entity.po.GoodsGroupGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分组关系表 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsGroupGoodsService{

    void batchAdd(String goodsGroupId, String[] goodsIds);

    /**
     * 物理删除分组商品
     * @param goodsId
     */
    void deleteByGoodsId(String goodsId);

    /**
     * 物理删除分组商品
     * @param goodsGroupId
     */
    void deleteByGoodsGroupId(String goodsGroupId);
}
