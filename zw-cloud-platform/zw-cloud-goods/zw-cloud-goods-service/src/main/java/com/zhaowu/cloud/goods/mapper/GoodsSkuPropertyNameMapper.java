package com.zhaowu.cloud.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyName;

public interface GoodsSkuPropertyNameMapper extends BaseMapper<GoodsSkuPropertyName> {
}
