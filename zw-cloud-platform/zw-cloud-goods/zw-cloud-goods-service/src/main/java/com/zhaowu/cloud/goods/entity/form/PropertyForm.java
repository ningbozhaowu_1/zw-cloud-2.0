package com.zhaowu.cloud.goods.entity.form;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class PropertyForm {

    @NotNull(message="属性名称不能为空")
    @Size(max= 256, message="属性名称长度必须在{max}个字符内")
    @ApiModelProperty("属性名称")
    private String name;

    @Size(max= 100, message="属性单位长度必须在{max}个字符内")
    @ApiModelProperty("属性单位")
    private String unit;

    @NotNull(message="属性值集合不能为空")
    @ApiModelProperty("属性值集合")
    private String[] values;

    @ApiModelProperty("筛选标示 1是 0否 默认否")
    private String searchIndicator;

    @ApiModelProperty("规格标示 1是 0否 默认否")
    private String saleIndicator;
}
