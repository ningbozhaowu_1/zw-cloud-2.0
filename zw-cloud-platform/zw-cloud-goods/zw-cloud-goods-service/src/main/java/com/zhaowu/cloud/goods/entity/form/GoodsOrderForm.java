package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.order.entity.form.OrderPayForm;
import com.zhaowu.cloud.order.entity.form.OrderShipForm;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class GoodsOrderForm {

    @NotNull(message = "商品信息不能为空")
    private List<GoodsShoppingCartForm> goodsShoppingCartFormList;

    @ApiModelProperty("支付信息")
    private List<OrderPayForm> orderPayFormList;

    @ApiModelProperty("收货地址编号")
    private String partyShipAddressId;

    @ApiModelProperty("管理员手机号")
    private String mobile;

    @ApiModelProperty("应用名称")
    private String appName;

    @ApiModelProperty("总金额")
    @NotNull(message = "总金额不能为空")
    private BigDecimal total;

}
