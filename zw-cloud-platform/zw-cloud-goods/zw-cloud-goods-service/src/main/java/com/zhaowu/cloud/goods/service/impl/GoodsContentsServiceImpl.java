package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.goods.entity.po.GoodsContents;
import com.zhaowu.cloud.goods.mapper.GoodsContentsMapper;
import com.zhaowu.cloud.goods.service.GoodsContentsService;
import org.springframework.stereotype.Service;

@Service
public class GoodsContentsServiceImpl extends ServiceImpl<GoodsContentsMapper, GoodsContents> implements GoodsContentsService {

    @Override
    public void add(GoodsContents goodsContents) {
        this.save(goodsContents);
    }

    @Override
    public GoodsContents modifyGoodsContents(String goodsId, GoodsContents goodsContents) {

        goodsContents.setGoodsId(goodsId);
        this.saveOrUpdate(goodsContents);
        return goodsContents;
    }

    @Override
    public GoodsContents getGoodsContents(String goodsId) {

        QueryWrapper<GoodsContents> queryWrapper = new QueryWrapper<GoodsContents>();
        queryWrapper.eq("goods_id", goodsId);
        return this.getOne(queryWrapper);
    }

    @Override
    public void delete(String goodsId) {
        QueryWrapper<GoodsContents> queryWrapper = new QueryWrapper<GoodsContents>();
        queryWrapper.eq("goods_id", goodsId);
        this.remove(queryWrapper);
    }
}
