package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.*;

import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品属性名表
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsSkuPropertyName extends BasePo {

    /**
     *  参与者编号
     */
    @TableField(value="goods_party")
    private String goodsParty;

    /**
     * 属性名称
     */
    @TableField("NAME")
    private String name;

}
