package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.goods.entity.po.GoodsPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsPropertyValue;
import com.zhaowu.cloud.goods.mapper.GoodsPropertyNameMapper;
import com.zhaowu.cloud.goods.service.GoodsPropertyNameService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class GoodsPropertyNameServiceImpl extends ServiceImpl<GoodsPropertyNameMapper, GoodsPropertyName> implements GoodsPropertyNameService {

    @Override
    public void add(GoodsPropertyName goodsPropertyName) {

        this.save(goodsPropertyName);
    }

    @Override
    public void modify(GoodsPropertyName goodsPropertyName) {
        this.updateById(goodsPropertyName);
    }

    @Override
    public void delete(String goodsId, String[] ids) {
        this.removeByIds(Arrays.asList(ids));
    }
}
