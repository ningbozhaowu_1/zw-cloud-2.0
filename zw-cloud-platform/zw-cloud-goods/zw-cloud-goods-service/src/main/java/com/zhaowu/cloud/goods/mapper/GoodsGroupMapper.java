package com.zhaowu.cloud.goods.mapper;

import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品分组表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsGroupMapper extends BaseMapper<GoodsGroup> {

}
