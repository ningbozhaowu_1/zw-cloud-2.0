package com.zhaowu.cloud.goods.mapper;

import com.zhaowu.cloud.goods.entity.po.GoodsPropertyValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性值 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsPropertyValueMapper extends BaseMapper<GoodsPropertyValue> {

}
