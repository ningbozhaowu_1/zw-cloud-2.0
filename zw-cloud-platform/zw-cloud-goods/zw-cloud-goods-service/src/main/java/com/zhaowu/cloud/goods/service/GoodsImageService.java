package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.*;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;

import java.util.List;

/**
 * <p>
 * 商品轮播图 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsImageService {

    void addBatch(List<GoodsImage> goodsImageList);

    /**
     * 修改商品轮播图
     * @param goodsId
     * @param goodsImageList
     */
    @CacheUpdate(name="goodsImageListCache:",key = "#goodsId", value="#result")
    List<GoodsImage> modifyGoodsImage(String goodsId, List<GoodsImage> goodsImageList);

    @Cached(name="goodsImageListCache:", key="#goodsId", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    List<GoodsImage> getGoodsImageList(String goodsId);

    /**
     * 物理删除轮播图
     * @param goodsId
     */
    @CacheInvalidate(name="goodsImageListCache:", key="#goodsId")
    void delete(String goodsId);

    @CacheInvalidate(name="goodsImageListCache:", key="#goodsId")
    void delete(String goodsId, String[] ids);
}
