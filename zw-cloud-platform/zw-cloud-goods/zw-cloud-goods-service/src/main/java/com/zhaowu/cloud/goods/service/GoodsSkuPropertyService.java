package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.goods.entity.form.GoodsSkuPropertyForm;
import com.zhaowu.cloud.goods.entity.form.PropertyForm;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyValue;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品属性 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
public interface GoodsSkuPropertyService {

    void addProperty(GoodsSkuPropertyForm goodsSkuPropertyForm);

    @Cached(name="goodsSkuPropertyValueCache::", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    GoodsSkuPropertyValue getValue(String id);

    @Cached(name="goodsSkuPropertyNameCache::", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    GoodsSkuPropertyName getName(String id);

    List<GoodsSkuPropertyName> skuPropertyNameList(String goodsParty);

    List<GoodsSkuPropertyValue> skuPropertyValueList(String nameId);
}
