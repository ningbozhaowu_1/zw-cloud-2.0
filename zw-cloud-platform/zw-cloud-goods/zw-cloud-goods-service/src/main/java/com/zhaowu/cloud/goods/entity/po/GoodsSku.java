package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 商品库存
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsSku extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 商品编号
     */
    @TableField("GOODS_ID")
    private String goodsId;

    /**
     * 商品价格
     */
    @TableField("PRICE")
    private BigDecimal price;

    /**
     * 成本价
     */
    @TableField("COST")
    private BigDecimal cost;

    /**
     * 库存
     */
    @TableField("STOCK")
    private Integer stock;

    /**
     * 重量
     */
    @TableField("WEIGHT")
    private BigDecimal weight;

    /**
     * 销量
     */
    @TableField("SALES")
    private Integer sales;

    /**
     * 属性值 属性值ID组合
     */
    @TableField("PROPERTY_VALUE_IDS")
    private String propertyValueIds;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Version
    private Integer revision;

}
