package com.zhaowu.cloud.goods.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品属性值
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsPropertyValue extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 属性名称编号
     */
    @TableField("NAME_ID")
    private String nameId;

    /**
     *  参与者编号
     */
    @TableField(value="APPLICATION_PARTY",  fill = FieldFill.INSERT)
    private String applicationParty;

    /**
     * 属性值
     */
    @TableField("VALUE")
    private String value;

}
