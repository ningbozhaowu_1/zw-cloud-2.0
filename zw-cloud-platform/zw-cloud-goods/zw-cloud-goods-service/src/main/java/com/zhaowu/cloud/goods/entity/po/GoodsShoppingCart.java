package com.zhaowu.cloud.goods.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品购物车
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsShoppingCart extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     *  参与者编号
     */
    @TableField(value="application_party",  fill = FieldFill.INSERT)
    private String applicationParty;

    @TableField(value = "user_id", fill = FieldFill.INSERT)
    private String userId;

    @TableField("goods_id")
    private String goodsId;

    @TableField("goods_sku_id")
    private String goodsSkuId;

    @TableField("quantity")
    private Integer quantity;

    @TableField("selected")
    private String selected;
}
