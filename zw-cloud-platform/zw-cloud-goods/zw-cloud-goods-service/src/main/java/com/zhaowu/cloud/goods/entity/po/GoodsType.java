package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsType extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     *  参与者编号
     */
    @TableField(value="goods_party")
    private String goodsParty;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 父分类id 顶级分类填0
     */
    @TableField(value="parent_id")
    private String parentId;

    /**
     * 是否为父节点 0为否，1为是
     */
    @TableField(value="is_parent")
    private String isParent;

    /**
     * 排序编号 排序指数越小越靠前
     */
    private Integer sort;

    /**
     * 状态 1正常 0 禁用
     */
    private String status;

    /**
     * 图标 Icon地址
     */
    private String icon;
}
