package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import com.zhaowu.cloud.goods.mapper.GoodsGroupMapper;
import com.zhaowu.cloud.goods.service.GoodsGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 商品分组表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Service
public class GoodsGroupServiceImpl extends ServiceImpl<GoodsGroupMapper,GoodsGroup> implements GoodsGroupService {

    @Autowired
    private Validator validator;

    @Override
    public void add(GoodsGroup goodsGroup) {
        checkGoodsGroup(goodsGroup);
        this.save(goodsGroup);
    }

    @Override
    public void modify(GoodsGroup goodsGroup) {
        checkGoodsGroup(goodsGroup);
        this.updateById(goodsGroup);
    }

    @Override
    public void delete(String goodsParty, String id) {
        QueryWrapper<GoodsGroup> wrapper = new QueryWrapper<GoodsGroup>();
        wrapper.eq("goods_party", goodsParty);
        wrapper.eq("id", id);
        this.remove(wrapper);
    }

    @Override
    public List<GoodsGroup> getGoodsGroupList(String goodsParty) {
        QueryWrapper<GoodsGroup> wrapper = new QueryWrapper<GoodsGroup>();
        wrapper.eq("goods_party", goodsParty);
        return this.list(wrapper);
    }

    public void checkGoodsGroup(GoodsGroup goodsGroup) {
        boolean checkGoodsName = this.checkGoodsGroupName(goodsGroup);
        Assert.isTrue(checkGoodsName, "商品分组名称已存在");
        //验证商品分类数据
        Set<ConstraintViolation<GoodsGroup>> constraintViolations = validator.validate(goodsGroup);
        if(constraintViolations.size() > 0){
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    /**
     * 检查商品分组名称
     *
     * @return
     */
    private boolean checkGoodsGroupName(GoodsGroup goodsGroup) {
        return this.checkProperty("name", goodsGroup.getName(), UserContextHolder.getInstance().getApplicationParty(),goodsGroup.getId());
    }

    public boolean checkProperty(String property, Object value, String goodsParty, String excludeId) {
        if (value == null || StringUtils.isEmpty(String.valueOf(value))) {
            return true;
        }
        List<GoodsGroup> goodsGroups = null;
        QueryWrapper<GoodsGroup> wrapper = new QueryWrapper<GoodsGroup>();
        wrapper.eq(property, value);
        if(goodsParty!=null){
            wrapper.eq("goods_party", goodsParty);
        }
        if (excludeId != null) {
            wrapper.ne("id", excludeId);
        }
        goodsGroups = this.list(wrapper);
        return (goodsGroups.size() == 0);
    }
}
