package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分类属性值
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsTypePropertyValue extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 属性名称编号
     */
    @TableField(value="name_id")
    private String nameId;

    /**
     * 属性值
     */
    private String value;
}
