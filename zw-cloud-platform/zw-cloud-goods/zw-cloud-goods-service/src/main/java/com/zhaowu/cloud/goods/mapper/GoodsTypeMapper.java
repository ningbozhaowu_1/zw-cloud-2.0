package com.zhaowu.cloud.goods.mapper;

import com.zhaowu.cloud.goods.entity.po.GoodsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品分类 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsTypeMapper extends BaseMapper<GoodsType> {

    List<GoodsType> getGoodsTypeTree(@Param("applicationParty") String applicationParty);
}
