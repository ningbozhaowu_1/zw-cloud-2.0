package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分类属性名表
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsTypePropertyName extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     *  参与者编号
     */
    @TableField(value="goods_party")
    private String goodsParty;

    @TableField(value="goods_type_id")
    private String goodsTypeId;

    /**
     * 属性名称
     */
    private String name;

    /**
     * 搜索标别 0否1是
     */
    @TableField(value="search_indicator")
    private String searchIndicator;

    /**
     * 属性单位
     */
    private String unit;

}
