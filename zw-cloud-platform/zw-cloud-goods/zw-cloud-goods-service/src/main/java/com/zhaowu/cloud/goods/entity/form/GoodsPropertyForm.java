package com.zhaowu.cloud.goods.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GoodsPropertyForm{


    @ApiModelProperty("商品属性名称编号")
    @NotNull(message = "商品属性名称编号不能为空")
    private String nameId;

    @ApiModelProperty("商品属性值")
    @NotNull(message = "商品属性值不能为空")
    private String[] valueIds;

    @NotNull(message = "商品属性类型不能为空")
    @Size(max= 3, message="商品属性类型必须在{max}个字符内")
    private String type;

//    @ApiModelProperty("分类属性名称编号")
//    private Long goodsTypeNameId;
}
