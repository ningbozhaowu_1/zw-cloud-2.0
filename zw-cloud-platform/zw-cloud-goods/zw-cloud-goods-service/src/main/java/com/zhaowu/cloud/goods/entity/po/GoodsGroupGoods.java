package com.zhaowu.cloud.goods.entity.po;


import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品分组关系表
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsGroupGoods extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 分组编号
     */
    @TableField("GOODS_GROUP_ID")
    private String goodsGroupId;

    /**
     * 商品编号
     */
    @TableField("GOODS_ID")
    private String goodsId;
}
