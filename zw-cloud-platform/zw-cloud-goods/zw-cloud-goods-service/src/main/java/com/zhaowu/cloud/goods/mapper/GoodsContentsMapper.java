package com.zhaowu.cloud.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.goods.entity.po.GoodsContents;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;

/**
 * <p>
 * 商品详情 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsContentsMapper extends BaseMapper<GoodsContents> {

}
