package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.goods.entity.po.Goods;
import com.zhaowu.cloud.goods.entity.po.GoodsSku;
import com.zhaowu.cloud.goods.mapper.GoodsSkuMapper;
import com.zhaowu.cloud.goods.service.GoodsSkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 商品库存 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@Service
public class GoodsSkuServiceImpl extends ServiceImpl<GoodsSkuMapper, GoodsSku> implements GoodsSkuService {

    @Override
    public List<GoodsSku> getGoodsSkuList(String goodsId) {
        QueryWrapper<GoodsSku> queryWrapper = new QueryWrapper<GoodsSku>();
        queryWrapper.eq("goods_id", goodsId);
        queryWrapper.orderByAsc("price");
        return this.list(queryWrapper);
    }

    @Override
    public List<GoodsSku> modifyGoodsSku(String goodsId, List<GoodsSku> goodsSkuList) {

        for(GoodsSku goodsSku : goodsSkuList){
            goodsSku.setGoodsId(goodsId);
        }

        this.saveOrUpdateBatch(goodsSkuList);
        return this.getGoodsSkuList(goodsId);
    }

    @Override
    public void modifyGoodsSku(GoodsSku goodsSku) {
        this.updateById(goodsSku);
    }

    @Override
    public GoodsSku selectGoodsSkuById(String goodsId, String valueIds) {
        QueryWrapper<GoodsSku> queryWrapper = new QueryWrapper<GoodsSku>();
        queryWrapper.eq("property_value_ids", valueIds);
        queryWrapper.eq("goods_id", goodsId);
        return this.getOne(queryWrapper);
    }

    @Override
    public GoodsSku selectGoodsSkuById(String goodsSKuId) {
        return this.getById(goodsSKuId);
    }

    @Override
    public void addBatch(List<GoodsSku> goodsSkuList) {

        this.saveBatch(goodsSkuList);
    }

    @Override
    public void delete(String goodsId) {
        QueryWrapper<GoodsSku> queryWrapper = new QueryWrapper<GoodsSku>();
        queryWrapper.eq("goods_id", goodsId);
        this.remove(queryWrapper);
    }

    @Override
    public void delete(String goodsId, String[] ids) {
        this.removeByIds(Arrays.asList(ids));
    }
}
