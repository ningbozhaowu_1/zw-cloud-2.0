package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.goods.entity.po.GoodsProperty;
import com.zhaowu.cloud.goods.entity.vo.GoodsPropertyVO;
import com.zhaowu.cloud.goods.mapper.GoodsPropertyMapper;
import com.zhaowu.cloud.goods.service.GoodsPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsPropertyServiceImpl extends ServiceImpl<GoodsPropertyMapper, GoodsProperty> implements GoodsPropertyService {

    @Autowired
    private GoodsPropertyMapper goodsPropertyMapper;

    @Override
    public void addOrModifyBatch(List<GoodsProperty> goodsPropertyList) {

        this.saveOrUpdateBatch(goodsPropertyList);
    }

    @Override
    public void delete(String goodsId) {

        QueryWrapper<GoodsProperty> removeWrapper = new QueryWrapper<GoodsProperty>();
        removeWrapper.eq("GOODS_ID", goodsId);
        this.remove(removeWrapper);
    }

    @Override
    public List<GoodsPropertyVO> getGoodsTypeGoodsPropertyList(String goodsId) {
        return goodsPropertyMapper.getGoodsTypeGoodsPropertyList(goodsId);
    }
}
