package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.zhaowu.cloud.goods.entity.po.GoodsPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsPropertyValue;

import java.util.List;

/**
 * <p>
 * 商品属性 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
public interface GoodsPropertyValueService {

    void addBatch(List<GoodsPropertyValue> goodsPropertyValueList);

    void modify(GoodsPropertyValue goodsPropertyValue);

    void delete(String goodsId, String[] nameIds);
}
