package com.zhaowu.cloud.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyValue;

public interface GoodsSkuPropertyValueMapper extends BaseMapper<GoodsSkuPropertyValue> {
}
