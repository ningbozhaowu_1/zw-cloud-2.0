package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品描述
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@Data
  @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsContents extends BasePo {

    private static final long serialVersionUID=1L;

      /**
     * 商品编号
     */
      @TableField("GOODS_ID")
    private String goodsId;

      /**
     * 描述
     */
      @TableField("CONTENTS")
    private String contents;


}
