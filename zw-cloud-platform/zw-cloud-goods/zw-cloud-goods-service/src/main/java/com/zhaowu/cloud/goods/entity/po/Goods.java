package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 商品信息表
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Goods extends BasePo {

    /**
     *  商品所属参与者编号
     */
    @TableField(value="goods_party")
    private String goodsParty;

    /**
     * 商品名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 商品编号
     */
    @TableField("GOODS_CODE")
    private String goodsCode;

    /**
     * 商品封面图
     */
    @TableField("IMAGE")
    private String image;

    /**
     * 商品封面视频
     */
    @TableField("VIDEO")
    private String video;

    /**
     * 商品状态 状态: 1-上架，0-下架
     */
    @TableField("STATUS")
    private String status;

    /**
     * 库存
     */
    @TableField("STOCK")
    private Integer stock;

    /**
     * 销量
     */
    @TableField("SALES")
    private Integer sales;

    /**
     * 分享描述
     */
    @TableField("SHARE_DESC")
    private String shareDesc;

    /**
     * 商品简介
     */
    @TableField("INTRODUCTION")
    private String introduction;

    /**
     * 是否参与会员价 1是0否
     */
    @TableField("IS_VIP")
    private String isVIP;

    /**
     * 库存扣减方式 1下单立减2付款后减
     */
    @TableField("COST_TYPE")
    private String costType;

    /**
     * 商品分类编号
     */
    @TableField("GOODS_TYPE_ID")
    private String goodsTypeId;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Version
    private Integer revision;

    @TableField("EXPRESS_FEE")
    private BigDecimal expressFee;

    @TableField("EXPRESS_TEMPLATE_ID")
    private String expressTempateId;

    @TableField("IS_TC")
    private String isTC;

    @TableField("IS_EXPRESS")
    private String isExpress;

    @TableField("IS_ZITI")
    private String isZiti;

}
