package com.zhaowu.cloud.goods.controller;


import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import com.zhaowu.cloud.goods.service.GoodsGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 商品分组表 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@RestController
@RequestMapping("/public/goodsGroup/v1")
@Api(tags = "应用端商品分组表 接口")
public class PublicGoodsGroupController extends BaseController {

        @Autowired
        private GoodsGroupService goodsGroupService;

        @GetMapping(value = "/list")
        @ApiOperation("获取商品分组表列表")
        public Result<List<GoodsGroup>> list(@Valid @RequestParam String goodsParty) {

            return success(goodsGroupService.getGoodsGroupList(goodsParty));
        }
}

