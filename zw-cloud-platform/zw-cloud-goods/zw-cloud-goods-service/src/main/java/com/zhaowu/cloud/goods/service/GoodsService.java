package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.goods.entity.form.GoodsForm;
import com.zhaowu.cloud.goods.entity.form.ModifyGoodsForm;
import com.zhaowu.cloud.goods.entity.param.GoodsQueryParam;
import com.zhaowu.cloud.goods.entity.po.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品信息表 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsService{

    /**
     * 创建商品
     * @param goods
     * @param goodsSkuList
     */
    Goods add(List<GoodsSku> goodsSkuList, Goods goods);

    /**
     * 修改商品
     * @param goods
     */
    @CacheUpdate(name="goodsCache:",key = "#goods.id", value="#result")
    Goods modifyGoods(List<GoodsSku> goodsSkuList, Goods goods);

    /**
     * 修改商品
     * @param goods
     */
    @CacheUpdate(name="goodsCache:",key = "#goods.id", value="#result")
    Goods modifyGoods(Goods goods);

    /**
     * 物理删除商品
     * @param id
     */
    @CacheInvalidate(name="goodsCache:", key="#id")
    boolean delete(String goodsParty, String id);

    @Cached(name="goodsCache:", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Goods selectGoodsById(String id);
    /**
     * 分页查询商品ID列表
     * @return
     */
    IPage<Goods> pageList(Page<Goods> page, GoodsQueryParam goodsQueryParam);

//    @Cached(name="goodsPropertyListCache:", key="#goodsId", expire = 3600, cacheType = CacheType.BOTH)
//    @CachePenetrationProtect
//    List<GoodsProperty> getGoodsPropertyList(String goodsId);
}
