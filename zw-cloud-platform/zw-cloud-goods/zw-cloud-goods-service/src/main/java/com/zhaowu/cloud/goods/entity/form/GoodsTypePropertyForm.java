package com.zhaowu.cloud.goods.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GoodsTypePropertyForm {

    @NotNull(message="分类属性名称不能为空")
    @Size(max= 256, message="分类属性名称长度必须在{max}个字符内")
    @ApiModelProperty("分类属性名称")
    private String name;

    @NotNull(message="商品分类编号不能为空")
    @ApiModelProperty("商品分类编号")
    private String goodsTypeId;

    @NotNull(message="搜索标别不能为空")
    @ApiModelProperty("搜索标别 0否1是")
    private String searchIndicator;

    @ApiModelProperty("属性单位 例如分")
    private String unit;

    @NotNull(message="商品分类属性值集合不能为空")
    @ApiModelProperty("商品分类属性值集合")
    private String[] values;

}
