package com.zhaowu.cloud.goods.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class GoodsVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private String goodsCode;

    private String image;

    private String video;

    private String status;

    private Integer stock;

    private Integer sales;

    private String shareDesc;

    private String introduction;

    private String isVIP;

    private String costType;

    private String goodsTypeId;

    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    private BigDecimal expressFee;

    private String expressTempateId;

    private String isTC;

    private String isExpress;

    private String isZiti;
}
