package com.zhaowu.cloud.goods.controller;

import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.constant.GoodsConstant;
import com.zhaowu.cloud.goods.entity.form.GoodsShoppingCartForm;
import com.zhaowu.cloud.goods.entity.form.ModifyGoodsShoppingCartForm;
import com.zhaowu.cloud.goods.entity.po.*;
import com.zhaowu.cloud.goods.entity.vo.GoodsShoppingCartVO;
import com.zhaowu.cloud.goods.service.GoodsService;
import com.zhaowu.cloud.goods.service.GoodsSkuPropertyService;
import com.zhaowu.cloud.goods.service.GoodsSkuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.goods.service.GoodsShoppingCartService;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 商品购物车 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/goodsShoppingCart")
@Api(tags = "商品购物车 接口")
public class GoodsShoppingCartController extends BaseController {

        @Autowired
        private GoodsShoppingCartService goodsShoppingCartService;

        @Autowired
        private GoodsService goodsService;

        @Autowired
        private GoodsSkuService goodsSkuService;

        @Autowired
        private GoodsSkuPropertyService goodsSkuPropertyService;

        /**
         * 获取商品购物车列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取商品购物车列表")
        public Result<Map<String, List<GoodsShoppingCartVO>>> list() {

            List<GoodsShoppingCart> goodsShoppingCartList = goodsShoppingCartService.getList(UserContextHolder.getInstance().getApplicationParty(), UserContextHolder.getInstance().getUserId());
            Goods goods = null;
            GoodsSku goodsSku = null;
            GoodsSkuPropertyValue goodsSkuPropertyValue = null;

            List<GoodsShoppingCartVO> goodsShoppingCartVOList = new LinkedList<GoodsShoppingCartVO>();
            List<GoodsShoppingCartVO> invalidGoodsShoppingCartVOList = new LinkedList<GoodsShoppingCartVO>();

            Map<String, List<GoodsShoppingCartVO>> map = new HashMap<String, List<GoodsShoppingCartVO>>();
            map.put(CommonConstant.GoodsStatus.A, goodsShoppingCartVOList);
            map.put(CommonConstant.GoodsStatus.P, invalidGoodsShoppingCartVOList);
            for(GoodsShoppingCart goodsShoppingCart : goodsShoppingCartList){
                goods = goodsService.selectGoodsById(goodsShoppingCart.getGoodsId());
                GoodsShoppingCartVO goodsShoppingCartVO = new GoodsShoppingCartVO();
                goodsShoppingCartVO.setId(goodsShoppingCart.getId());
                goodsShoppingCartVO.setGoodsId(goodsShoppingCart.getGoodsId());
                goodsShoppingCartVO.setQuantity(goodsShoppingCart.getQuantity());
                goodsShoppingCartVO.setSelected(goodsShoppingCart.getSelected());
                goodsShoppingCartVO.setImage(goods.getImage());
                goodsShoppingCartVO.setName(goods.getName());

                goodsSku = goodsSkuService.selectGoodsSkuById(goodsShoppingCart.getGoodsSkuId());
                goodsShoppingCartVO.setGoodsSkuId(goodsSku.getId());
                goodsShoppingCartVO.setPrice(goodsSku.getPrice());
                goodsShoppingCartVO.setStock(goodsSku.getStock());
                StringBuilder sb = new StringBuilder();
                for(String valueId : goodsSku.getPropertyValueIds().split(":")){
                    goodsSkuPropertyValue = goodsSkuPropertyService.getValue(valueId);
                    sb.append(goodsSkuPropertyValue.getValue());
                }
                goodsShoppingCartVO.setGoodsSkuNames(sb.toString());
                if(CommonConstant.GoodsStatus.A.equalsIgnoreCase(goods.getStatus())){
                    goodsShoppingCartVOList.add(goodsShoppingCartVO);
                }else{
                    invalidGoodsShoppingCartVOList.add(goodsShoppingCartVO);
                }

            }

            return this.success(map);
        }

        /**
         * 新增商品购物车
         */
        @PostMapping(value = "/add")
        @ApiOperation("新增商品购物车")
        public Result<String> add(@Valid @RequestBody GoodsShoppingCartForm goodsShoppingCartForm) {
            GoodsShoppingCart goodsShoppingCart = goodsShoppingCartForm.toPo(GoodsShoppingCart.class);
//            goodsShoppingCart.setUserId(SecurityUtils.getUserId());
//            goodsShoppingCart.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            goodsShoppingCartService.add(goodsShoppingCart);
            return this.success();
        }

        /**
         * 删除商品购物车
         */
        @DeleteMapping(value = "/delete/{id}")
        @ApiOperation("删除商品购物车")
        public Result<String> delete(@Valid @PathVariable("id") String id) {
            goodsShoppingCartService.delete(UserContextHolder.getInstance().getUserId(), id);
            return this.success();
        }

        /**
         * 修改商品购物车
         */
        @PostMapping(value = "/modify")
        @ApiOperation("修改商品购物车")
        public Result<String> modify(@Valid @RequestBody ModifyGoodsShoppingCartForm modifyGoodsShoppingCartForm) {

            GoodsShoppingCart goodsShoppingCartExsit = goodsShoppingCartService.selectById(UserContextHolder.getInstance().getUserId(), modifyGoodsShoppingCartForm.getId());
            if(goodsShoppingCartExsit == null){
                return this.success();
            }

            goodsShoppingCartExsit.setQuantity(modifyGoodsShoppingCartForm.getQuantity());
            goodsShoppingCartExsit.setSelected(modifyGoodsShoppingCartForm.getSelected());
            goodsShoppingCartService.modify(goodsShoppingCartExsit);
            return this.success();
        }
}

