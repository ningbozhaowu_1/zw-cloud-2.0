package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsGroupGoods;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GoodsGroupGoodsForm extends BaseForm<GoodsGroupGoods> {

    @NotNull(message="商品分组编号不能为空")
    @ApiModelProperty("商品分组编号")
    private String goodsGroupId;

    @NotNull(message="商品编号集合不能为空")
    @ApiModelProperty("商品编号集合")
    private String[] goodsIds;

}
