package com.zhaowu.cloud.goods.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.goods.entity.po.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.goods.entity.po.GoodsProperty;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品信息表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    IPage<Goods> pageList(Page<?> page, String goodsTypeId, String applicationParty, String search);

//    List<GoodsProperty> goodsPropertyList(String goodsId);

    IPage<Goods> pageList4Group(Page<?> page, String goodsGroupId, String applicationParty);
}
