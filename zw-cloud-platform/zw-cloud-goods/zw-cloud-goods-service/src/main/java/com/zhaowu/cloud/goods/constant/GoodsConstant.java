package com.zhaowu.cloud.goods.constant;

public class GoodsConstant {

    /**
     * 商品状态 1-上架，0-下架
     */
    public static final String GOODS_STATUS_ENABLED = "A";
    public static final String GOODS_STATUS_DISABLED = "P";

    /**
     * 商品库存扣减方式 1下单立减2付款后减
     */
    public static final String GOODS_COST_BEFORE_PAY = "1";
    public static final String GOODS_COST_AFTER_PAY = "2";

    /**
     * 商品属性类型
     * 1-商品属性
     * 2-类目属性
     * 3-分类属性
     */
    public static final String GOODS_PROPERTY_GOODS_TYPE = "3";
    public static final String GOODS_PROPERTY_GOODS_CATEGORY = "2";
    public static final String GOODS_PROPERTY_GOODS = "1";
}
