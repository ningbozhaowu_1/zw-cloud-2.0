package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;
import com.zhaowu.cloud.goods.entity.po.GoodsPropertyValue;
import com.zhaowu.cloud.goods.mapper.GoodsPropertyValueMapper;
import com.zhaowu.cloud.goods.service.GoodsPropertyValueService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class GoodsPropertyValueServiceImpl extends ServiceImpl<GoodsPropertyValueMapper, GoodsPropertyValue> implements GoodsPropertyValueService {
    @Override
    public void addBatch(List<GoodsPropertyValue> goodsPropertyValueList) {
        this.saveBatch(goodsPropertyValueList);
    }

    @Override
    public void modify(GoodsPropertyValue goodsPropertyValue) {

        QueryWrapper<GoodsPropertyValue> updateWrapper = new QueryWrapper<GoodsPropertyValue>();
        updateWrapper.eq("NAME_ID", goodsPropertyValue.getNameId());
        updateWrapper.eq("VALUE", goodsPropertyValue.getValue());
        this.saveOrUpdate(goodsPropertyValue, updateWrapper);
    }

    @Override
    public void delete(String goodsId, String[] nameIds) {
        for(String nameId : nameIds){
            QueryWrapper<GoodsPropertyValue> removeWrapper = new QueryWrapper<GoodsPropertyValue>();
            removeWrapper.eq("NAME_ID", nameId);
            this.remove(removeWrapper);
        }
    }
}
