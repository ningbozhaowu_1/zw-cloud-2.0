package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ModifyGoodsGroupForm extends BaseForm<GoodsGroup> {

    @NotNull(message="商品分组编号不能为空")
    @ApiModelProperty("商品分组编号")
    private String id;

    @NotNull(message="商品分组名称不能为空")
    @Size(max= 256, message="商品分组名称长度必须在{max}个字符内")
    @ApiModelProperty("商品分组名称")
    private String name;

    @NotNull(message="显示样式不能为空")
    @ApiModelProperty("显示样式 1:大图 2:小图 3:一大两小 4:详细列表")
    private String showType;

    @NotNull(message="商品名称显示标识不能为空")
    @ApiModelProperty("商品名称显示标识 1:是 0:否")
    private String goodsNameIndicator;

    @NotNull(message="商品价格显示标识不能为空")
    @ApiModelProperty("商品价格显示标识 1:是 0:否")
    private String goodsPriceIndicator;

    @Size(max= 2048, message="商品分组图标url长度必须在{max}个字符内")
    @ApiModelProperty("商品分组图标url")
    private String icon;

    @ApiModelProperty("商品分组状态 1：正常，0：禁用")
    private String status;
}
