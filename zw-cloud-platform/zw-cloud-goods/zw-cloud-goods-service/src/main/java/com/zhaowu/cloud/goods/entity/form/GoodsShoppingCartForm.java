package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsShoppingCart;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ApiModel("商品信息")
@Data
public class GoodsShoppingCartForm extends BaseForm<GoodsShoppingCart> {

    @ApiModelProperty("购物车编号 购物车提交时上送")
    private String id;

    @NotNull(message="商品编号不能为空")
    @ApiModelProperty("商品编号")
    private String goodsId;

    @NotNull(message="商品sku编号不能为空")
    @ApiModelProperty("商品sku编号")
    private String goodsSkuId;

    @NotNull(message="数量不能为空")
    @Min(0)
    @ApiModelProperty("数量")
    private Integer quantity;

    @ApiModelProperty("勾选表识 1是0否 默认是")
    private String selected;

    @ApiModelProperty("提货点编号")
    private String pickUpId;

    @ApiModelProperty("配送方式")
    private String deliveryType;
}
