package com.zhaowu.cloud.goods.entity.vo;

import com.zhaowu.cloud.uc.entity.vo.PartyPickUpVO;
import lombok.Data;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class GoodsOrderVO {

    private String shopId;

    private String shopName;

    private BigDecimal total;

    private BigDecimal totalFreight;

    /**
     * express
     * tc
     */
//    private String deliveryType;

    private Map<String, List<GoodsOrderItemVO>> goodsOrderItemVOListMap;

    private Map<String, BigDecimal> freightMap;

    private List<GoodsOrderItemVO> goodsOrderItemVOList;

    private List<PartyPickUpVO> partyPickUpVOList;
}
