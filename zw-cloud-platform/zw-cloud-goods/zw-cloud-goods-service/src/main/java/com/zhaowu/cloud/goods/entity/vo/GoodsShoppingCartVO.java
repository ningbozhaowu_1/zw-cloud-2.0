package com.zhaowu.cloud.goods.entity.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class GoodsShoppingCartVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String goodsId;

    private String selected;

    private Integer quantity;

    private String goodsSkuNames;

    private String image;

    private String name;

    private Integer stock;

    private BigDecimal price;

    private String goodsSkuId;
}
