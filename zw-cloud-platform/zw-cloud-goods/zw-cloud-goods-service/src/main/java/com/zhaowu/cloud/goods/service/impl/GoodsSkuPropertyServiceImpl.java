package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.entity.form.GoodsSkuPropertyForm;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyValue;
import com.zhaowu.cloud.goods.mapper.GoodsSkuMapper;
import com.zhaowu.cloud.goods.mapper.GoodsSkuPropertyNameMapper;
import com.zhaowu.cloud.goods.mapper.GoodsSkuPropertyValueMapper;
import com.zhaowu.cloud.goods.service.GoodsSkuPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品属性 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
@Service
public class GoodsSkuPropertyServiceImpl implements GoodsSkuPropertyService {

    @Autowired
    private GoodsSkuPropertyNameMapper goodsSkuPropertyNameMapper;

    @Autowired
    private GoodsSkuPropertyValueMapper goodsSkuPropertyValueMapper;

    @Override
    @Transactional
    public void addProperty(GoodsSkuPropertyForm goodsSkuPropertyForm) {

        GoodsSkuPropertyName goodsSkuPropertyName = new GoodsSkuPropertyName();
        goodsSkuPropertyName.setName(goodsSkuPropertyForm.getName());
        goodsSkuPropertyName.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
        goodsSkuPropertyNameMapper.insert(goodsSkuPropertyName);

        for(String value : goodsSkuPropertyForm.getValues()){
            GoodsSkuPropertyValue goodsPropertyValue = new GoodsSkuPropertyValue();
            goodsPropertyValue.setNameId(goodsSkuPropertyName.getId());
            goodsPropertyValue.setValue(value);
            goodsSkuPropertyValueMapper.insert(goodsPropertyValue);
        }
    }

    @Override
    public GoodsSkuPropertyValue getValue(String id) {
        return goodsSkuPropertyValueMapper.selectById(id);
    }

    @Override
    public GoodsSkuPropertyName getName(String id) {
        return goodsSkuPropertyNameMapper.selectById(id);
    }

    @Override
    public List<GoodsSkuPropertyName> skuPropertyNameList(String goodsParty) {
        QueryWrapper<GoodsSkuPropertyName> wrapper = new QueryWrapper<GoodsSkuPropertyName>();
        wrapper.eq("goods_party", goodsParty);
        return goodsSkuPropertyNameMapper.selectList(wrapper);
    }

    @Override
    public List<GoodsSkuPropertyValue> skuPropertyValueList(String nameId) {
        QueryWrapper<GoodsSkuPropertyValue> wrapper = new QueryWrapper<GoodsSkuPropertyValue>();
        wrapper.eq("name_id", nameId);
        return goodsSkuPropertyValueMapper.selectList(wrapper);
    }
}
