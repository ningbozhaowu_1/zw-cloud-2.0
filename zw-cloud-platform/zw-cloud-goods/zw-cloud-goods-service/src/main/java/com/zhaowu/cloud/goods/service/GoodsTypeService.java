package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.zhaowu.cloud.goods.entity.form.GoodsForm;
import com.zhaowu.cloud.goods.entity.form.GoodsTypePropertyForm;
import com.zhaowu.cloud.goods.entity.po.GoodsType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhaowu.cloud.goods.entity.po.GoodsTypePropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsTypePropertyValue;

import java.util.List;

/**
 * <p>
 * 商品分类 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsTypeService{

    /**
     * 创建商品分类
     * @param goodsType
     */
    void add(GoodsType goodsType);

    void modify(GoodsType goodsType);

    /**
     * 删除商品分类
     * @param id
     */
    void delete(String goodsParty, String id);

    List<GoodsType> getGoodsTypeListByParentId(String parentId);

    List<GoodsType> getGoodsTypeRootList(String goodsParty);

    List<GoodsType> getAllGoodsType(String goodsParty);

    void addGoodsTypeProperty(GoodsTypePropertyForm goodsTypePropertyForm);

    void removeGoodsTypeProperty(String nameId);

    List<GoodsTypePropertyName> getGoodsTypePropertyNameList(String groupTypeId);

    GoodsTypePropertyName getGoodsTypePropertyNameById(String nameId);

    List<GoodsTypePropertyValue> getGoodsTypePropertyValueList(String nameId);

    List<GoodsTypePropertyName> getGoodsTypeSearchNameList(String groupTypeId);

    List<GoodsType> getGoodsTypeList(String goodsParty);
}
