package com.zhaowu.cloud.goods.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.goods.entity.po.Goods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsQueryParam extends BaseParam<Goods> {

    private String goodsTypeId;

    private String goodsGroupId;

    private String search;

    private String scene;

    private String goodsParty;
}
