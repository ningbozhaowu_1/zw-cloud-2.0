package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsShoppingCart;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ModifyGoodsShoppingCartForm extends BaseForm<GoodsShoppingCart> {

    @NotNull(message="商品购物车编号不能为空")
    @ApiModelProperty("商品购物车编号")
    private String id;

    @NotNull(message="数量不能为空")
    @Min(0)
    @ApiModelProperty("数量")
    private Integer quantity;

    @ApiModelProperty("勾选表识 1是0否 默认是")
    private String selected;
}
