package com.zhaowu.cloud.goods.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GoodsSkuPropertyForm {

    @NotNull(message="sku属性名称不能为空")
    @Size(max= 256, message="sku属性名称长度必须在{max}个字符内")
    @ApiModelProperty("sku属性名称")
    private String name;

    @NotNull(message="sku属性值集合不能为空")
    @ApiModelProperty("sku属性值集合")
    private String[] values;
}
