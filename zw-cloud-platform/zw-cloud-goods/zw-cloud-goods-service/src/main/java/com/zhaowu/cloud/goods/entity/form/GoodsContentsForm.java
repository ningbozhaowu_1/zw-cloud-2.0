package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsContents;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GoodsContentsForm extends BaseForm<GoodsContents> {


    @ApiModelProperty("商品内容")
    private String contents;
}
