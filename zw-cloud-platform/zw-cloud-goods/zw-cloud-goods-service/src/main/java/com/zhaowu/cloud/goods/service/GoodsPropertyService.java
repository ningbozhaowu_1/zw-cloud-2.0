package com.zhaowu.cloud.goods.service;

import com.zhaowu.cloud.goods.entity.po.GoodsProperty;
import com.zhaowu.cloud.goods.entity.vo.GoodsPropertyVO;

import java.util.List;

public interface GoodsPropertyService {

    void addOrModifyBatch(List<GoodsProperty> goodsPropertyList);

    void delete(String goodsId);

    List<GoodsPropertyVO> getGoodsTypeGoodsPropertyList(String goodsId);
}
