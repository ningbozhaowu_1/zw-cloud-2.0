package com.zhaowu.cloud.goods.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.goods.entity.po.GoodsSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品库存 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsSkuMapper extends BaseMapper<GoodsSku> {

    List<Map> skuPropertyList(String applicationParty);
}
