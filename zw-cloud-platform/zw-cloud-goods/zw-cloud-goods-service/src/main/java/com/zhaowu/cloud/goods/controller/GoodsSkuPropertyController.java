package com.zhaowu.cloud.goods.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.entity.form.GoodsSkuPropertyForm;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyValue;
import com.zhaowu.cloud.goods.service.GoodsSkuPropertyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 商品属性 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/goodsSkuProperty/admin/v1")
@Api(tags = "管理端商品sku属性 接口")
public class GoodsSkuPropertyController extends BaseController {

        @Autowired
        private GoodsSkuPropertyService goodsSkuPropertyService;

        /**
         * 新增规格属性项
         */
        @PostMapping(value = "/addProperty")
        @ApiOperation("新增sku规格")
        public Result<String> addProperty(@Valid @RequestBody GoodsSkuPropertyForm goodsSkuPropertyForm) {
            goodsSkuPropertyService.addProperty(goodsSkuPropertyForm);
            return this.success();
        }

        @GetMapping(value = "/skuPropertyNameList")
        @ApiOperation("sku规格名列表")
        public Result<List<GoodsSkuPropertyName>> skuPropertyNameList() {
            return this.success(goodsSkuPropertyService.skuPropertyNameList(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId()));
        }

        @GetMapping(value = "/skuPropertyValueList")
        @ApiOperation("sku规格值列表")
        public Result<List<GoodsSkuPropertyValue>> skuPropertyValueList(@Valid @RequestParam String nameId) {
            return this.success(goodsSkuPropertyService.skuPropertyValueList(nameId));
        }

}

