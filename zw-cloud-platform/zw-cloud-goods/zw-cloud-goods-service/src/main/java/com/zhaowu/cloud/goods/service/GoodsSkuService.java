package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.*;
import com.zhaowu.cloud.goods.entity.po.GoodsProperty;
import com.zhaowu.cloud.goods.entity.po.GoodsSku;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品库存 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsSkuService{

    @Cached(name="goodsSkuListCache:", key="#goodsId", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    List<GoodsSku> getGoodsSkuList(String goodsId);
    /**
     * 修改商品库存
     * @param goodsId
     * @param goodsSkuList
     */
    @CacheUpdate(name="goodsSkuListCache:",key = "#goodsId", value="#result")
    List<GoodsSku> modifyGoodsSku(String goodsId, List<GoodsSku> goodsSkuList);

    /**
     * 修改商品库存
     * @param goodsSku
     */
    @CacheInvalidate(name="goodsSkuListCache:",key = "#goodsSku.goodsId")
    void modifyGoodsSku(GoodsSku goodsSku);

    @Cached(name="goodsSkuCache:", key="#goodsId"+"-"+"valueIds", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    GoodsSku selectGoodsSkuById(String goodsId, String valueIds);

    GoodsSku selectGoodsSkuById(String goodsSKuId);

    void addBatch(List<GoodsSku> goodsSkuList);

    /**
     * 物理删除sku
     * @param goodsId
     */
    @CacheInvalidate(name="goodsSkuCache:", key="#goodsId")
    void delete(String goodsId);

    @CacheInvalidate(name="goodsSkuCache:", key="#goodsId")
    void delete(String goodsId, String[] ids);
}
