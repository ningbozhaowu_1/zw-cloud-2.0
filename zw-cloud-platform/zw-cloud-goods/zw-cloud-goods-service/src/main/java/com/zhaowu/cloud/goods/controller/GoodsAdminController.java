package com.zhaowu.cloud.goods.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.zhaowu.cloud.common.web.audit.EnableAudit;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.map.MapService;
import com.zhaowu.cloud.framework.map.entity.Location;
import com.zhaowu.cloud.framework.util.FastJsonUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.constant.GoodsConstant;
import com.zhaowu.cloud.goods.entity.form.*;
import com.zhaowu.cloud.goods.entity.param.GoodsQueryParam;
import com.zhaowu.cloud.goods.entity.po.*;
import com.zhaowu.cloud.goods.entity.vo.*;
import com.zhaowu.cloud.goods.service.*;
import com.zhaowu.cloud.order.client.OrderClient;
import com.zhaowu.cloud.order.entity.form.*;
import com.zhaowu.cloud.order.entity.vo.OrderItemVO;
import com.zhaowu.cloud.order.entity.vo.OrderVOExt;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.GoodsFeeForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressFeeQueryForm;
import com.zhaowu.cloud.uc.entity.form.UserAddressForm;
import com.zhaowu.cloud.uc.entity.vo.PartyDeliveryVO;
import com.zhaowu.cloud.uc.entity.vo.PartyPickUpVO;
import com.zhaowu.cloud.uc.entity.vo.PartyShipAddressVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 商品信息表 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@RestController
@RequestMapping("/goods/admin/v1")
@Api(tags = "商品信息表 管理接口")
@EnableAudit
public class GoodsAdminController extends BaseController {

        @Autowired
        private GoodsService goodsService;

        @Autowired
        private GoodsSkuService goodsSkuService;

        @Autowired
        private GoodsImageService goodsImageService;

        @Autowired
        private GoodsContentsService goodsContentsService;

        @Autowired
        private GoodsGroupGoodsService goodsGroupGoodsService;

        @Autowired
        private GoodsSkuPropertyService goodsSkuPropertyService;

        @Autowired
        private GoodsPropertyService goodsPropertyService;

        @PostMapping(value = "/add")
        @ApiOperation("新增商品")
        @Transactional
        public Result<String> add(@Valid @RequestBody GoodsForm goodsForm) {
            Goods goods = new Goods();
            BeanUtils.copyProperties(goodsForm, goods);


            List<GoodsSkuForm> goodsSkuFormList = goodsForm.getGoodsSkuForms();
            List<GoodsSku> goodsSkuList = JSON.parseArray(JSON.toJSONString(goodsSkuFormList), GoodsSku.class);
            //spu add
            goods.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
            goods = goodsService.add(goodsSkuList, goods);

            //sku add
            for(GoodsSku goodsSku : goodsSkuList){
                goodsSku.setGoodsId(goods.getId());
            }
            goodsSkuService.addBatch(goodsSkuList);

            //轮播图add
            if(!CollectionUtils.isEmpty(goodsForm.getGoodsImageForms())){
                List<GoodsImageForm> goodsImageFormList = goodsForm.getGoodsImageForms();
                List<GoodsImage> goodsImageList = JSON.parseArray(JSON.toJSONString(goodsImageFormList), GoodsImage.class);
                for(GoodsImage goodsImage : goodsImageList){
                    goodsImage.setGoodsId(goods.getId());
                }
                goodsImageService.addBatch(goodsImageList);
            }

            //商品内容
            if(goodsForm.getGoodsContentsForm()!=null && !StringUtils.isEmpty(goodsForm.getGoodsContentsForm().getContents())){
                GoodsContents goodsContents = new GoodsContents();
                BeanUtils.copyProperties(goodsForm.getGoodsContentsForm(), goodsContents);
                goodsContents.setGoodsId(goods.getId());
                goodsContentsService.add(goodsContents);
            }

            // 商品属性
            if(!CollectionUtils.isEmpty(goodsForm.getGoodsPropertyForms())){
                List<GoodsProperty> goodsPropertyList = new ArrayList<>();
                for(GoodsPropertyForm goodsPropertyForm : goodsForm.getGoodsPropertyForms()){
                    for(String valueId : goodsPropertyForm.getValueIds()){
                        GoodsProperty goodsProperty = new GoodsProperty();
                        goodsProperty.setNameId(goodsPropertyForm.getNameId());
                        goodsProperty.setValueId(valueId);
                        goodsProperty.setType(goodsPropertyForm.getType());
                        goodsProperty.setGoodsId(goods.getId());
                        goodsPropertyList.add(goodsProperty);
                    }
                }
                goodsPropertyService.addOrModifyBatch(goodsPropertyList);
            }

            return this.success();
        }

        @PostMapping(value = "/modify")
        @ApiOperation("修改商品信息")
        @Transactional
        public Result<String> modify(@Valid @RequestBody ModifyGoodsForm modifyGoodsForm) {

            Goods goods = new Goods();
            BeanUtils.copyProperties(modifyGoodsForm, goods);
            goods.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());

            //修改库存信息
            List<GoodsSku> goodsSkuList = new ArrayList<>();
            if(!CollectionUtils.isEmpty(modifyGoodsForm.getModifyGoodsSkuForms())){

                List<ModifyGoodsSkuForm> modifyGoodsSkuFormList = modifyGoodsForm.getModifyGoodsSkuForms();
                goodsSkuList = JSON.parseArray(JSON.toJSONString(modifyGoodsSkuFormList), GoodsSku.class);
                goodsSkuService.modifyGoodsSku(goods.getId(), goodsSkuList);
            }

            if(modifyGoodsForm.getDeletedSkuIds()!=null){
                goodsSkuService.delete(goods.getId() ,modifyGoodsForm.getDeletedSkuIds());
            }


            //修改商品内容
            if(modifyGoodsForm.getModifyGoodsContentsForm()!=null){
                GoodsContents goodsContents = new GoodsContents();
                BeanUtils.copyProperties(modifyGoodsForm.getModifyGoodsContentsForm(), goodsContents);
                goodsContentsService.modifyGoodsContents(goods.getId(), goodsContents);
            }

            //修改商品轮播图
            if(!CollectionUtils.isEmpty(modifyGoodsForm.getModifyGoodsImageForms())){
                List<ModifyGoodsImageForm> modifyGoodsImageFormList = modifyGoodsForm.getModifyGoodsImageForms();
                List<GoodsImage> goodsImageList = JSON.parseArray(JSON.toJSONString(modifyGoodsImageFormList), GoodsImage.class);
                goodsImageService.modifyGoodsImage(goods.getId(), goodsImageList);
                if(modifyGoodsForm.getDeletedImageIds()!=null){
                    goodsImageService.delete(goods.getId() ,modifyGoodsForm.getDeletedImageIds());
                }
            }

            //修改spu信息
            goodsService.modifyGoods(goodsSkuList, goods);

            if(!CollectionUtils.isEmpty(modifyGoodsForm.getModifyGoodsPropertyForms())){
                List<GoodsProperty> goodsPropertyList = new ArrayList<>();
                for(ModifyGoodsPropertyForm modifyGoodsPropertyForm : modifyGoodsForm.getModifyGoodsPropertyForms()){
                    for(String valueId : modifyGoodsPropertyForm.getValueIds()){
                        GoodsProperty goodsProperty = new GoodsProperty();
                        goodsProperty.setNameId(modifyGoodsPropertyForm.getNameId());
                        goodsProperty.setValueId(valueId);
                        goodsProperty.setGoodsId(goods.getId());
                        goodsProperty.setType(modifyGoodsPropertyForm.getType());
                        goodsProperty.setId(modifyGoodsPropertyForm.getId());
                        goodsPropertyList.add(goodsProperty);
                    }
                }
                goodsPropertyService.addOrModifyBatch(goodsPropertyList);
            }

            return this.success();
        }

        @DeleteMapping(value = "/delete")
        @ApiOperation("物理删除商品")
        @Transactional
        public Result<String> delete(@Valid @RequestParam String id) {

            boolean delete = this.goodsService.delete(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId(), id);
            if(delete){
                this.goodsSkuService.delete(id);
                this.goodsContentsService.delete(id);
                this.goodsImageService.delete(id);
                this.goodsGroupGoodsService.deleteByGoodsId(id);
                this.goodsPropertyService.delete(id);
            }
            return success();
        }

        /**
         * 管理端商品列表
         */
        @PostMapping(value = "/goodsPageList")
        @ApiOperation("管理端商品列表")
        public Result<Page<Goods>> goodsPageList(@Valid @RequestBody GoodsQueryForm goodsQueryForm) {

            goodsQueryForm.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());

            IPage<Goods> page = goodsService.pageList(goodsQueryForm.getPage(), goodsQueryForm.toParam(GoodsQueryParam.class));
            for(Goods goods : page.getRecords()){
                BeanUtils.copyProperties(this.goodsService.selectGoodsById(goods.getId()), goods);
            }
            return this.success(page);
        }

        /**
         * 商品详情
         */
        @GetMapping(value = "/goodsDetail")
        @ApiOperation("商品详情")
        public Result<GoodsDetailVO> goodsDetail(@Valid @RequestParam String id) {

            GoodsDetailVO goodsDetailVO = new GoodsDetailVO();
            goodsDetailVO.setGoodsContents(goodsContentsService.getGoodsContents(id));
            goodsDetailVO.setGoodsImageList(goodsImageService.getGoodsImageList(id));
            goodsDetailVO.setGoods(goodsService.selectGoodsById(id));
            List<GoodsSku> goodsSkuList = goodsSkuService.getGoodsSkuList(id);

            Map<String, List<GoodsSkuPropertyValue>> goodsSkuPropertyValueListMap = new LinkedHashMap<String, List<GoodsSkuPropertyValue>>();
            GoodsSkuPropertyValue goodsSkuPropertyValue = null;
            GoodsSkuPropertyName goodsSkuPropertyName = null;

            Map<String, GoodsSkuVO> goodsSkuListMap = new LinkedHashMap<String, GoodsSkuVO>();
            List values = new ArrayList();
            for(GoodsSku goodsSku : goodsSkuList){
                String[] valueIds = goodsSku.getPropertyValueIds().split(":");
                for(String valueId : valueIds){
                    goodsSkuPropertyValue = goodsSkuPropertyService.getValue(valueId);
                    goodsSkuPropertyName =goodsSkuPropertyService.getName(goodsSkuPropertyValue.getNameId());
                    if(!values.contains(valueId)){
                        values.add(valueId);
                        if(goodsSkuPropertyValueListMap.containsKey(goodsSkuPropertyName.getName())){
                            goodsSkuPropertyValueListMap.get(goodsSkuPropertyName.getName()).add(goodsSkuPropertyValue);
                        }else{
                            List<GoodsSkuPropertyValue> goodsSkuPropertyValueList = new LinkedList<GoodsSkuPropertyValue>();
                            goodsSkuPropertyValueList.add(goodsSkuPropertyValue);
                            goodsSkuPropertyValueListMap.put(goodsSkuPropertyName.getName(), goodsSkuPropertyValueList);
                        }
                    }
                }
                GoodsSkuVO goodsSkuVO = new GoodsSkuVO();
                BeanUtils.copyProperties(goodsSku, goodsSkuVO);
                goodsSkuListMap.put(goodsSku.getPropertyValueIds(), goodsSkuVO);
            }

            goodsDetailVO.setGoodsSkuPropertyValueListMap(goodsSkuPropertyValueListMap);
            goodsDetailVO.setGoodsSkuListMap(goodsSkuListMap);

            Map<String, List<GoodsPropertyVO>> goodsPropertyVOSMap = new LinkedHashMap<>();
            goodsPropertyVOSMap.put(GoodsConstant.GOODS_PROPERTY_GOODS_TYPE,this.goodsPropertyService.getGoodsTypeGoodsPropertyList(id));
            goodsDetailVO.setGoodsPropertyVOSMap(goodsPropertyVOSMap);

            return this.success(goodsDetailVO);
        }

}

