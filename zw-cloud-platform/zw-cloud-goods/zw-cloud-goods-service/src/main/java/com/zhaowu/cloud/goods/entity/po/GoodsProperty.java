package com.zhaowu.cloud.goods.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品属性
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsProperty extends BasePo {

    @TableField("GOODS_ID")
    private String goodsId;

    @TableField("NAME_ID")
    private String nameId;

    @TableField("VALUE_ID")
    private String valueId;

    private String type;
}
