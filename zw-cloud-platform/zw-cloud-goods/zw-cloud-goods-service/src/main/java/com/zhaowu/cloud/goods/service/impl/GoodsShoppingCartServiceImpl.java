package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.entity.po.GoodsShoppingCart;
import com.zhaowu.cloud.goods.mapper.GoodsShoppingCartMapper;
import com.zhaowu.cloud.goods.service.GoodsShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品购物车 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
@Service
public class GoodsShoppingCartServiceImpl extends ServiceImpl<GoodsShoppingCartMapper,GoodsShoppingCart> implements GoodsShoppingCartService {

    @Override
    public List<GoodsShoppingCart> getList(String applicationParty,String userId) {

        QueryWrapper<GoodsShoppingCart> wrapper = new QueryWrapper<GoodsShoppingCart>();
        wrapper.eq("user_id", userId);
        wrapper.eq("application_party", applicationParty);
        return this.list(wrapper);
    }

    @Override
    public void modify(GoodsShoppingCart goodsShoppingCart) {
        this.updateById(goodsShoppingCart);
    }

    @Override
    public void delete(String userId, String id) {
        QueryWrapper<GoodsShoppingCart> wrapper = new QueryWrapper<GoodsShoppingCart>();
        wrapper.eq("user_id", userId);
        wrapper.eq("id", id);
        this.remove(wrapper);
    }

    @Override
    public void add(GoodsShoppingCart goodsShoppingCart) {
        QueryWrapper<GoodsShoppingCart> wrapper = new QueryWrapper<GoodsShoppingCart>();
        wrapper.eq("goods_id", goodsShoppingCart.getGoodsId());
        wrapper.eq("goods_sku_id", goodsShoppingCart.getGoodsSkuId());
        wrapper.eq("user_id", UserContextHolder.getInstance().getUserId());
        wrapper.eq("application_party", UserContextHolder.getInstance().getApplicationParty());
        GoodsShoppingCart goodsShoppingCartExsit = this.getOne(wrapper);
        if(goodsShoppingCartExsit!=null){
            goodsShoppingCartExsit.setQuantity(goodsShoppingCartExsit.getQuantity()+goodsShoppingCart.getQuantity());
//            goodsShoppingCartExsit.setSelected(goodsShoppingCart.getSelected());
            this.modify(goodsShoppingCartExsit);
        }else{
            this.save(goodsShoppingCart);
        }
    }

    @Override
    public GoodsShoppingCart selectById(String userId, String id) {

        QueryWrapper<GoodsShoppingCart> wrapper = new QueryWrapper<GoodsShoppingCart>();
        wrapper.eq("user_id", userId);
        wrapper.eq("id", id);
        return this.getOne(wrapper);
    }
}
