package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.*;
import com.zhaowu.cloud.goods.entity.po.GoodsContents;
/**
 * <p>
 * 商品轮播图 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsContentsService {

    void add(GoodsContents goodsContents);

    /**
     * 修改商品描述
     * @param goodsId
     * @param goodsContents
     */
    @CacheUpdate(name="goodsContentsCache:",key = "#goodsId", value="#result")
    GoodsContents modifyGoodsContents(String goodsId, GoodsContents goodsContents);

    @Cached(name="goodsContentsCache:", key="#goodsId", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    GoodsContents getGoodsContents(String goodsId);

    /**
     * 物理删除商品内容
     * @param goodsId
     */
    @CacheInvalidate(name="goodsContentsCache:", key="#goodsId")
    void delete(String goodsId);
}
