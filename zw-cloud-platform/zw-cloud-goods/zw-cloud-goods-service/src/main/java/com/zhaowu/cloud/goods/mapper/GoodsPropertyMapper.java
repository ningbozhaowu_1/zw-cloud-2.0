package com.zhaowu.cloud.goods.mapper;

import com.zhaowu.cloud.goods.entity.po.GoodsProperty;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.goods.entity.vo.GoodsPropertyVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品属性 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
public interface GoodsPropertyMapper extends BaseMapper<GoodsProperty> {

    List<GoodsPropertyVO> getGoodsTypeGoodsPropertyList(@Param("goodsId") String goodsId);
}
