package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GoodsTypeForm extends BaseForm<GoodsType> {

    @NotNull(message="商品分类名称不能为空")
    @Size(max= 256, message="商品分类名称长度必须在{max}个字符内")
    @ApiModelProperty("商品分类名称")
    private String name;

    @ApiModelProperty("上级分类编号 可为空")
    private String parentId;

    private Integer sort;

    @ApiModelProperty("商品分类状态 1：正常，0：禁用")
    private String status;

    @Size(max= 2048, message="商品分类图标url长度必须在{max}个字符内")
    @ApiModelProperty("商品分类图标url")
    private String icon;
}
