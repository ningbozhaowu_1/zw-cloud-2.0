package com.zhaowu.cloud.goods.mapper;

import com.zhaowu.cloud.goods.entity.po.GoodsGroupGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品分组关系表 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsGroupGoodsMapper extends BaseMapper<GoodsGroupGoods> {

}
