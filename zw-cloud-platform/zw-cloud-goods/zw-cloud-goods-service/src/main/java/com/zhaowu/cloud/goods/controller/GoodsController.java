package com.zhaowu.cloud.goods.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.zhaowu.cloud.common.web.audit.EnableAudit;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.FastJsonUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.constant.GoodsConstant;
import com.zhaowu.cloud.goods.entity.form.*;
import com.zhaowu.cloud.goods.entity.po.*;
import com.zhaowu.cloud.goods.entity.vo.*;
import com.zhaowu.cloud.goods.entity.param.GoodsQueryParam;
import com.zhaowu.cloud.goods.service.*;
import com.zhaowu.cloud.order.client.OrderClient;
import com.zhaowu.cloud.order.entity.form.*;
import com.zhaowu.cloud.order.entity.vo.OrderItemVO;
import com.zhaowu.cloud.order.entity.vo.OrderVOExt;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.GoodsFeeForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressFeeQueryForm;
import com.zhaowu.cloud.uc.entity.vo.PartyDeliveryVO;
import com.zhaowu.cloud.uc.entity.vo.PartyPickUpVO;
import com.zhaowu.cloud.framework.map.MapService;
import com.zhaowu.cloud.framework.map.entity.Location;
import com.zhaowu.cloud.uc.entity.form.UserAddressForm;
import com.zhaowu.cloud.uc.entity.vo.PartyShipAddressVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import sun.applet.Main;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品信息表 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@RestController
@RequestMapping("/goods/v1")
@Api(tags = "商品信息表 接口")
@EnableAudit
@Slf4j
public class GoodsController extends BaseController {

        @Autowired
        private GoodsService goodsService;

        @Autowired
        private GoodsSkuService goodsSkuService;

        @Autowired
        private GoodsSkuPropertyService goodsSkuPropertyService;

        @Autowired
        private OrderClient orderClient;

        @Autowired
        private UserClient userClient;

        @Autowired
        private MapService mapService;

        @Autowired
        private GoodsShoppingCartService goodsShoppingCartService;

        @CreateCache(name = "goodsLockCache:", expire = 3600, cacheType = CacheType.REMOTE)
        private com.alicp.jetcache.Cache<String, Goods> goodsLockCache;

        @PostMapping(value = "/purchasePrepare")
        @ApiOperation("订单预创建")
        @Transactional
        public Result purchasePrepare(@Valid @RequestBody GoodsOrderPrepareForm goodsOrderPrepareForm) {

            //遍历出shopIds
            Map<String, List<GoodsShoppingCartForm>> goodsIdGoodsShoppingCartFormMap = new HashMap<>();
            Goods goods = null;
            GoodsSku goodsSku = null;
            Map<String, Goods> goodsMap = new HashMap<>();
            List<GoodsShoppingCartForm> goodsShoppingCartFormList = goodsOrderPrepareForm.getGoodsShoppingCartFormList();
            for(GoodsShoppingCartForm goodsShoppingCartForm : goodsShoppingCartFormList){
                goods = goodsService.selectGoodsById(goodsShoppingCartForm.getGoodsId());
                goodsMap.put(goods.getId(), goods);
                if(goodsIdGoodsShoppingCartFormMap.containsKey(goods.getGoodsParty())){
                    goodsIdGoodsShoppingCartFormMap.get(goods.getGoodsParty()).add(goodsShoppingCartForm);
                }else{
                    List<GoodsShoppingCartForm> list = new ArrayList<>();
                    list.add(goodsShoppingCartForm);
                    goodsIdGoodsShoppingCartFormMap.put(goods.getGoodsParty(), list);
                }
            }

            //一个shopId一个Order
            List<GoodsOrderVO> goodsOrderVOList = new LinkedList<>();
            PartyShipAddressVO partyShipAddressVO = userClient.getPartyShipAddressbyId(goodsOrderPrepareForm.getPartyShipAddressId()).getData();

            if(partyShipAddressVO == null){
                return this.failure("收货地址错误");
            }
            PartyDeliveryVO partyDeliveryVO = null;
            for(String  shopId : goodsIdGoodsShoppingCartFormMap.keySet()){
                //计算订单总邮费
                BigDecimal totalFreight = BigDecimal.ZERO;
                GoodsOrderVO goodsOrderVO = new GoodsOrderVO();
                Map<String, BigDecimal> freightMap = Maps.newHashMap();
                Map<String, List<GoodsOrderItemVO>> goodsOrderItemVOListMap = Maps.newHashMap();
                goodsOrderVO.setGoodsOrderItemVOListMap(goodsOrderItemVOListMap);
                goodsOrderVO.setFreightMap(freightMap);

                BigDecimal singleOrderTotal = BigDecimal.ZERO;
                for(GoodsShoppingCartForm goodsShoppingCartForm : goodsIdGoodsShoppingCartFormMap.get(shopId)) {
                    goods = goodsMap.get(goodsShoppingCartForm.getGoodsId());
                    if (GoodsConstant.GOODS_STATUS_DISABLED.equalsIgnoreCase(goods.getStatus())) {
                        return this.failure("商品" + goods.getName() + "已失效");
                    }

                    goodsSku = goodsSkuService.selectGoodsSkuById(goodsShoppingCartForm.getGoodsSkuId());
                    //判断库存
                    if (GoodsConstant.GOODS_COST_BEFORE_PAY.equalsIgnoreCase(goods.getCostType())) {
                        if (Integer.compare(goods.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                            return this.failure("商品" + goods.getName() + "库存不足");
                        }

                        if (Integer.compare(goodsSku.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                            return this.failure("商品" + goods.getName() + "库存不足");
                        }
                    }

                    //判断库存扣减规则
                    if (GoodsConstant.GOODS_COST_BEFORE_PAY.equalsIgnoreCase(goods.getCostType())) {
                        boolean hasRun = false;
                        while(!hasRun){
                            hasRun = goodsLockCache.tryLockAndRun(goodsShoppingCartForm.getGoodsId(),300, TimeUnit.SECONDS, () -> {

                                //重新读取商品信息 避免脏读
                                Goods goodsSafe = goodsService.selectGoodsById(goodsShoppingCartForm.getGoodsId());

                                if (GoodsConstant.GOODS_STATUS_DISABLED.equalsIgnoreCase(goodsSafe.getStatus())) {
                                    new RuntimeException("商品" + goodsSafe.getName() + "已失效");
                                }

                                if (Integer.compare(goodsSafe.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                                    throw new RuntimeException("商品" + goodsSafe.getName() + "库存不足");
                                }

                                GoodsSku goodsSkuSave = goodsSkuService.selectGoodsSkuById(goodsShoppingCartForm.getGoodsSkuId());

                                if (Integer.compare(goodsSkuSave.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                                    throw new RuntimeException("商品" + goodsSafe.getName() + "库存不足");
                                }
                            });
                        }
                    }

                    GoodsOrderItemVO goodsOrderItemVO = new GoodsOrderItemVO();
                    goodsOrderItemVO.setGoodsId(goods.getId());
                    goodsOrderItemVO.setGoodsSkuId(goodsSku.getId());
                    goodsOrderItemVO.setGoodsName(goods.getName());
                    goodsOrderItemVO.setActualPrice(goodsSku.getPrice());
                    goodsOrderItemVO.setQuantity(goodsShoppingCartForm.getQuantity());
                    goodsOrderItemVO.setGoodsImage(goods.getImage());
                    goodsOrderItemVO.setPrice(goodsSku.getPrice());

                    StringBuilder sb = new StringBuilder();
                    for(String valueId : goodsSku.getPropertyValueIds().split(":")){
                        GoodsSkuPropertyValue goodsSkuPropertyValue = goodsSkuPropertyService.getValue(valueId);
                        sb.append(goodsSkuPropertyValue.getValue());
                    }

                    goodsOrderItemVO.setGoodsSkus(sb.toString());

                    singleOrderTotal = singleOrderTotal.add(goodsSku.getPrice().multiply(new BigDecimal(goodsShoppingCartForm.getQuantity())));
                    //运费计算
                    if("Y".equals(goods.getIsTC())){
                        partyDeliveryVO = userClient.getDelivery(goods.getGoodsParty()).getData();

                        Location location = new Location();
                        location.setProvince(partyShipAddressVO.getProvinceName());
                        location.setCity(partyShipAddressVO.getCityName());
                        location.setDistrict(partyShipAddressVO.getDistrictName());
                        location.setAddress(partyShipAddressVO.getAddress());

                        String json = "{\"longitude\":\"121.50589\",\"latitude\":\"29.929079\"}";
                        Location shop = (Location) FastJsonUtils.json2object(json, Location.class);
                        Double distance = mapService.getDistance(location, shop.getLatitude(), shop.getLongitude(), CommonConstant.DistanceUnit.K);
                        if(partyDeliveryVO!=null &&
                                singleOrderTotal.compareTo(partyDeliveryVO.getDeliveryBeginAmount()) >= 0
                                    && new BigDecimal(distance).compareTo(partyDeliveryVO.getMaxDistance())<=0){
                            if(goodsOrderItemVOListMap.containsKey(CommonConstant.DeliveryType.TC)){
                                goodsOrderItemVOListMap.get(CommonConstant.DeliveryType.TC).add(goodsOrderItemVO);
                            }else{
                                List<GoodsOrderItemVO> tcList = new LinkedList<>();
                                tcList.add(goodsOrderItemVO);
                                goodsOrderItemVOListMap.put(CommonConstant.DeliveryType.TC, tcList);
                            }

                            BigDecimal freight = calculateDeliveryFee(goodsSku.getPrice().multiply(new BigDecimal(goodsShoppingCartForm.getQuantity())),partyDeliveryVO,distance);
                            totalFreight = totalFreight.add(freight);
                            if(freightMap.containsKey(CommonConstant.DeliveryType.TC)){
                                freightMap.get(CommonConstant.DeliveryType.TC).add(freight);
                            }else{
                                freightMap.put(CommonConstant.DeliveryType.TC, freight);
                            }
                            continue;
                        }
                    }

                    if("Y".equals(goods.getIsZiti())){
                        List<PartyPickUpVO> partyPickUpVOList = userClient.getPickUplistByCity(partyShipAddressVO.getCityName(), goods.getGoodsParty()).getData();
                        if(!CollectionUtils.isEmpty(partyPickUpVOList)){
                            Double distance = 0.00;
                            Location location = new Location();
                            location.setProvince(partyShipAddressVO.getProvinceName());
                            location.setCity(partyShipAddressVO.getCityName());
                            location.setDistrict(partyShipAddressVO.getDistrictName());
                            location.setAddress(partyShipAddressVO.getAddress());
                            for(PartyPickUpVO partyPickUpVO : partyPickUpVOList){
                                distance = mapService.getDistance(location, partyPickUpVO.getLatitude(), partyPickUpVO.getLongitude(), CommonConstant.DistanceUnit.K);
                                partyPickUpVO.setDistance(new BigDecimal(distance).setScale(2,RoundingMode.HALF_UP));
                            }
                            Collections.sort(partyPickUpVOList, new DistanceComparator());

                            if(goodsOrderItemVOListMap.containsKey(CommonConstant.DeliveryType.ZITI)){
                                goodsOrderItemVOListMap.get(CommonConstant.DeliveryType.ZITI).add(goodsOrderItemVO);
                            }else{
                                List<GoodsOrderItemVO> zitiList = new LinkedList<>();
                                zitiList.add(goodsOrderItemVO);
                                goodsOrderItemVOListMap.put(CommonConstant.DeliveryType.ZITI, zitiList);
                            }
                            goodsOrderVO.setPartyPickUpVOList(partyPickUpVOList);
                            freightMap.put(CommonConstant.DeliveryType.ZITI, BigDecimal.ZERO);
                            continue;
                        }
                    }

                    if("Y".equals(goods.getIsExpress()) && !StringUtils.isEmpty(goods.getExpressTempateId())){
                        PartyExpressFeeQueryForm partyExpressFeeQueryForm = new PartyExpressFeeQueryForm();
                        UserAddressForm userAddressVo = new UserAddressForm();
                        userAddressVo.setAddress(partyShipAddressVO.getAddress());
                        userAddressVo.setProvince(partyShipAddressVO.getProvince());
                        userAddressVo.setCity(partyShipAddressVO.getCity());
                        userAddressVo.setStreet(partyShipAddressVO.getDistrict());
                        partyExpressFeeQueryForm.setAddress(userAddressVo);
                        partyExpressFeeQueryForm.setPartyId(goods.getGoodsParty());
                        GoodsFeeForm goodsFeeForm = new GoodsFeeForm();
                        BeanUtils.copyProperties(goods, goodsFeeForm);
                        goodsFeeForm.setTemplateId(goods.getExpressTempateId());
                        goodsFeeForm.setCount(goodsOrderItemVO.getQuantity().toString());
                        goodsFeeForm.setWeight((goodsSku.getWeight().multiply(new BigDecimal(goodsOrderItemVO.getQuantity()))).toString());
                        if(goods.getExpressFee()!=null){
                            goodsFeeForm.setIsUnionFee("Y");
                            goodsFeeForm.setUnionFee(goods.getExpressFee().toString());
                        }
                        partyExpressFeeQueryForm.setGoodList(Arrays.asList(goodsFeeForm));
                        BigDecimal freight = BigDecimal.ZERO;
                        Result<BigDecimal> result = userClient.fee(partyExpressFeeQueryForm);
                        if(result != null && result.getSuccess()){
                            freight = result.getData();
                            totalFreight = totalFreight.add(freight);
                            if(freightMap.containsKey(CommonConstant.DeliveryType.EXP)){
                                freightMap.get(CommonConstant.DeliveryType.EXP).add(freight);
                            }else{
                                freightMap.put(CommonConstant.DeliveryType.EXP, freight);
                            }

                            if(goodsOrderItemVOListMap.containsKey(CommonConstant.DeliveryType.EXP)){
                                goodsOrderItemVOListMap.get(CommonConstant.DeliveryType.EXP).add(goodsOrderItemVO);
                            }else{
                                List<GoodsOrderItemVO> ExpList = new LinkedList<>();
                                ExpList.add(goodsOrderItemVO);
                                goodsOrderItemVOListMap.put(CommonConstant.DeliveryType.EXP, ExpList);
                            }
                            continue;
                        }
                    }

                    singleOrderTotal = singleOrderTotal.subtract(goodsSku.getPrice().multiply(new BigDecimal(goodsShoppingCartForm.getQuantity())));

                    if(goodsOrderItemVOListMap.containsKey(CommonConstant.DeliveryType.INVALID)){
                        goodsOrderItemVOListMap.get(CommonConstant.DeliveryType.INVALID).add(goodsOrderItemVO);
                    }else{
                        List<GoodsOrderItemVO> invalidList = new LinkedList<>();
                        invalidList.add(goodsOrderItemVO);
                        goodsOrderItemVOListMap.put(CommonConstant.DeliveryType.INVALID, invalidList);
                    }

                }
                goodsOrderVO.setTotal(singleOrderTotal.add(totalFreight));
                goodsOrderVO.setShopId(shopId);
                goodsOrderVO.setShopName(userClient.getPartyById(shopId).getData().getPartyName());
                goodsOrderVOList.add(goodsOrderVO);
            }

            return this.success(goodsOrderVOList);
        }

        BigDecimal calculateDeliveryFee(BigDecimal total, PartyDeliveryVO partyDeliveryVO, Double distance){

//            userClient.gePartyAttr("","");
            if(total.compareTo(partyDeliveryVO.getDeliveryFreeAmount())>=0){
                return BigDecimal.ZERO;
            } else if(new BigDecimal(distance).compareTo(partyDeliveryVO.getDeliveryFreeDistance())<=0){
                return BigDecimal.ZERO;
            }

            String additionalDistance = partyDeliveryVO.getAdditionalDistance();
            BigDecimal additionalFee = partyDeliveryVO.getAdditionalFee();

            return partyDeliveryVO.getFee().add((new BigDecimal(distance.toString()).subtract(partyDeliveryVO.getDeliveryFreeDistance())).divide(new BigDecimal(additionalDistance), 0, RoundingMode.UP).multiply(additionalFee));
        }

        static class DistanceComparator implements Comparator<Object> {
            public int compare(Object object1, Object object2) {// 实现接口中的方法
                PartyPickUpVO p1 = (PartyPickUpVO) object1; // 强制转换
                PartyPickUpVO p2 = (PartyPickUpVO) object2;
                return (p1.getDistance()).compareTo((p2.getDistance()));
            }
        }

        @PostMapping(value = "/purchase")
        @ApiOperation("购买商品")
        @Transactional
        public Result purchase(@Valid @RequestBody GoodsOrderForm goodsOrderForm) {

            BigDecimal payTotal = BigDecimal.ZERO;
            Date expireTime = new Date();

            if(goodsOrderForm.getOrderPayFormList()!=null){
                for(OrderPayForm orderPayForm : goodsOrderForm.getOrderPayFormList()){
                    payTotal = payTotal.add(orderPayForm.getPay());
                }
            }

            if(payTotal.compareTo(goodsOrderForm.getTotal())!=0){
                return this.failure("购物车价格已过期");
            }

            PartyShipAddressVO partyShipAddressVO = null;

            if(!StringUtils.isEmpty(goodsOrderForm.getPartyShipAddressId())){

                partyShipAddressVO = userClient.getPartyShipAddressbyId(goodsOrderForm.getPartyShipAddressId()).getData();
            }

            Map<String, List<GoodsShoppingCartForm>> goodsIdGoodsShoppingCartFormMap = new HashMap<>();

            Goods goods = null;
            GoodsSku goodsSku = null;
            Map<String, Goods> goodsMap = new HashMap<>();
            //遍历出shopIds
            List<GoodsShoppingCartForm> goodsShoppingCartFormList = goodsOrderForm.getGoodsShoppingCartFormList();

            String tempKey = "";
            Collection<String> goodsIds = new ArrayList<>();
            String goodsName = "";
            for(GoodsShoppingCartForm goodsShoppingCartForm : goodsShoppingCartFormList){
                if(StringUtils.isEmpty(goodsShoppingCartForm.getDeliveryType())){
                    goodsShoppingCartForm.setDeliveryType(CommonConstant.DeliveryType.UNNESSASERY);
                }
                goods = goodsService.selectGoodsById(goodsShoppingCartForm.getGoodsId());
                goodsName = goods.getName();
                goodsIds.add(goodsShoppingCartForm.getGoodsId());
                tempKey = goods.getGoodsParty().concat(":").concat(goodsShoppingCartForm.getDeliveryType());
                goodsMap.put(goods.getId(), goods);
                if(goodsIdGoodsShoppingCartFormMap.containsKey(tempKey)){

                    goodsIdGoodsShoppingCartFormMap.get(tempKey).add(goodsShoppingCartForm);

                }else{
                    List<GoodsShoppingCartForm> list = new ArrayList<>();
                    list.add(goodsShoppingCartForm);
                    goodsIdGoodsShoppingCartFormMap.put(tempKey, list);
                }
            }

            //一个shopId一个Order
            List<OrderForm> orderFormList = new LinkedList<>();

            //重新计算订单总金额 判断价格是否已过期
            BigDecimal totalNow = BigDecimal.ZERO;
            CreateOrderForm createOrderForm = new CreateOrderForm();
            createOrderForm.setOrderFormList(orderFormList);
            PartyDeliveryVO partyDeliveryVO = null;

            for(String  key : goodsIdGoodsShoppingCartFormMap.keySet()){

                OrderForm orderForm = new OrderForm();
                if(partyShipAddressVO !=null ){

                    OrderShipForm orderShipForm = new OrderShipForm();
                    orderShipForm.setShipProvince(partyShipAddressVO.getProvinceName());
                    orderShipForm.setShipCity(partyShipAddressVO.getCityName());
                    orderShipForm.setShipDistrict(partyShipAddressVO.getDistrictName());
                    orderShipForm.setShipAddr(partyShipAddressVO.getAddress());
                    orderShipForm.setShipName(partyShipAddressVO.getName());
                    orderShipForm.setShipPhone(partyShipAddressVO.getPhone());
                    orderForm.setOrderShipForm(orderShipForm);
                }else{
                    orderForm.setDeliveryType(CommonConstant.DeliveryType.UNNESSASERY);
                }

                List<OrderItemForm> orderItemFormList = new LinkedList<OrderItemForm>();
                orderForm.setOrderItemFormList(orderItemFormList);

                BigDecimal singleOrderTotal = BigDecimal.ZERO;
                BigDecimal totalFreight = BigDecimal.ZERO;
                for(GoodsShoppingCartForm goodsShoppingCartForm : goodsIdGoodsShoppingCartFormMap.get(key)) {
                        goods = goodsMap.get(goodsShoppingCartForm.getGoodsId());
                        if (GoodsConstant.GOODS_STATUS_DISABLED.equalsIgnoreCase(goods.getStatus())) {
                            return this.failure("商品" + goods.getName() + "已失效");
                        }

                        goodsSku = goodsSkuService.selectGoodsSkuById(goodsShoppingCartForm.getGoodsSkuId());
                        //判断库存
                        if (GoodsConstant.GOODS_COST_BEFORE_PAY.equalsIgnoreCase(goods.getCostType())) {
                            if (Integer.compare(goods.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                                return this.failure("商品" + goods.getName() + "库存不足");
                            }

                            if (Integer.compare(goodsSku.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                                return this.failure("商品" + goods.getName() + "库存不足");
                            }
                        }

                        //判断库存扣减规则
                        if (GoodsConstant.GOODS_COST_BEFORE_PAY.equalsIgnoreCase(goods.getCostType())) {
                            boolean hasRun = false;
                            while (!hasRun) {
                                hasRun = goodsLockCache.tryLockAndRun(goodsShoppingCartForm.getGoodsId(), 300, TimeUnit.SECONDS, () -> {

                                    //重新读取商品信息 避免脏读
                                    Goods goodsSafe = goodsService.selectGoodsById(goodsShoppingCartForm.getGoodsId());

                                    if (GoodsConstant.GOODS_STATUS_DISABLED.equalsIgnoreCase(goodsSafe.getStatus())) {
                                        new RuntimeException("商品" + goodsSafe.getName() + "已失效");
                                    }

                                    if (Integer.compare(goodsSafe.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                                        throw new RuntimeException("商品" + goodsSafe.getName() + "库存不足");
                                    }

                                    GoodsSku goodsSkuSave = goodsSkuService.selectGoodsSkuById(goodsShoppingCartForm.getGoodsSkuId());

                                    if (Integer.compare(goodsSkuSave.getStock(), goodsShoppingCartForm.getQuantity()) == -1) {
                                        throw new RuntimeException("商品" + goodsSafe.getName() + "库存不足");
                                    }

                                    goodsSafe.setStock(goodsSafe.getStock() - goodsShoppingCartForm.getQuantity());
                                    goodsSkuSave.setStock(goodsSkuSave.getStock() - goodsShoppingCartForm.getQuantity());
                                    //下单立即扣减库存
                                    goodsService.modifyGoods(goodsSafe);
                                    goodsSkuService.modifyGoodsSku(goodsSkuSave);
                                    if (goodsShoppingCartForm.getId() != null) {
                                        goodsShoppingCartService.delete(UserContextHolder.getInstance().getUserId(), goodsShoppingCartForm.getId());
                                    }
                                });
                            }
                        }
                    OrderItemForm orderItemForm = new OrderItemForm();
                    orderItemForm.setGoodsId(goods.getId());
                    orderItemForm.setGoodsSkuId(goodsSku.getId());
                    orderItemForm.setGoodsName(goods.getName());
                    orderItemForm.setActualPrice(goodsSku.getPrice());
                    orderItemForm.setQuantity(goodsShoppingCartForm.getQuantity());
                    orderItemForm.setGoodsImage(goods.getImage());
                    orderItemForm.setPrice(goodsSku.getPrice());
                    orderItemForm.setTotal(goodsSku.getPrice().multiply(new BigDecimal(goodsShoppingCartForm.getQuantity())));

                    StringBuilder sb = new StringBuilder();
                    for(String valueId : goodsSku.getPropertyValueIds().split(":")){
                        GoodsSkuPropertyValue goodsSkuPropertyValue = goodsSkuPropertyService.getValue(valueId);
                        sb.append(goodsSkuPropertyValue.getValue());
                        log.info("GoodsSkuPropertyValue Message:<{}>", goodsSkuPropertyValue);
                        if("一年".equals(goodsSkuPropertyValue.getValue())){
                            expireTime = DateUtils.addYears(new Date(),1);
                        }else if("半年".equals(goodsSkuPropertyValue.getValue())){
                            expireTime = DateUtils.addMonths(new Date(),6);
                        }else if("一个月".equals(goodsSkuPropertyValue.getValue())){
                            expireTime = DateUtils.addMonths(new Date(),1);
                        }else{
                            expireTime = DateUtils.addDays(new Date(),15);
                        }
                    }

                    orderItemForm.setGoodsSkus(sb.toString());
                    orderItemFormList.add(orderItemForm);
                    singleOrderTotal = singleOrderTotal.add(orderItemForm.getTotal());

                    //运费计算
                    if(partyShipAddressVO !=null ){
                        if(CommonConstant.DeliveryType.TC.equals(goodsShoppingCartForm.getDeliveryType())){

                            Location location = new Location();
                            location.setProvince(partyShipAddressVO.getProvinceName());
                            location.setCity(partyShipAddressVO.getCityName());
                            location.setDistrict(partyShipAddressVO.getDistrictName());
                            location.setAddress(partyShipAddressVO.getAddress());

                            String json = "{\"longitude\":\"121.50589\",\"latitude\":\"29.929079\"}";
                            Location shop = (Location) FastJsonUtils.json2object(json, Location.class);
                            Double distance = mapService.getDistance(location, shop.getLatitude(), shop.getLongitude(), CommonConstant.DistanceUnit.K);

                            partyDeliveryVO = userClient.getDelivery(goods.getGoodsParty()).getData();
                            if(partyDeliveryVO!=null &&
                                    singleOrderTotal.compareTo(partyDeliveryVO.getDeliveryBeginAmount()) >= 0
                                    && new BigDecimal(distance).compareTo(partyDeliveryVO.getMaxDistance())<=0){
                                BigDecimal freight = calculateDeliveryFee(goodsSku.getPrice().multiply(new BigDecimal(goodsShoppingCartForm.getQuantity())),partyDeliveryVO,distance);
                                totalFreight = totalFreight.add(freight);
                                singleOrderTotal = singleOrderTotal.add(totalFreight);
                                orderForm.setDeliveryType(CommonConstant.DeliveryType.TC);
                                continue;
                            }
                        }

                        if(CommonConstant.DeliveryType.ZITI.equals(goodsShoppingCartForm.getDeliveryType())){

                            orderForm.getOrderShipForm().setPickUpId(goodsShoppingCartForm.getPickUpId());
                            orderForm.setDeliveryType(CommonConstant.DeliveryType.ZITI);
                            continue;
                        }

                        if(CommonConstant.DeliveryType.EXP.equals(goodsShoppingCartForm.getDeliveryType())){
                            PartyExpressFeeQueryForm partyExpressFeeQueryForm = new PartyExpressFeeQueryForm();
                            UserAddressForm userAddressVo = new UserAddressForm();
                            userAddressVo.setAddress(partyShipAddressVO.getAddress());
                            userAddressVo.setProvince(partyShipAddressVO.getProvince());
                            userAddressVo.setCity(partyShipAddressVO.getCity());
                            userAddressVo.setStreet(partyShipAddressVO.getDistrict());
                            partyExpressFeeQueryForm.setAddress(userAddressVo);
                            partyExpressFeeQueryForm.setPartyId(goods.getGoodsParty());
                            GoodsFeeForm goodsFeeForm = new GoodsFeeForm();
                            BeanUtils.copyProperties(goods, goodsFeeForm);
                            goodsFeeForm.setTemplateId(goods.getExpressTempateId());
                            goodsFeeForm.setCount(goodsShoppingCartForm.getQuantity().toString());
                            goodsFeeForm.setWeight((goodsSku.getWeight().multiply(new BigDecimal(goodsShoppingCartForm.getQuantity()))).toString());
                            if(goods.getExpressFee()!=null){
                                goodsFeeForm.setIsUnionFee("Y");
                                goodsFeeForm.setUnionFee(goods.getExpressFee().toString());
                            }
                            partyExpressFeeQueryForm.setGoodList(Arrays.asList(goodsFeeForm));
                            BigDecimal freight = BigDecimal.ZERO;
                            Result<BigDecimal> result = userClient.fee(partyExpressFeeQueryForm);
                            if(result != null && result.getSuccess()) {
                                freight = result.getData();
                                totalFreight = totalFreight.add(freight);
                                singleOrderTotal = singleOrderTotal.add(totalFreight);
                                orderForm.setDeliveryType(CommonConstant.DeliveryType.EXP);
                                continue;
                            }
                        }
                    }
                }
                orderForm.setFreight(totalFreight);
                orderForm.setShopId(key.split(":")[0]);
                orderForm.setShopName(userClient.getPartyById(orderForm.getShopId()).getData().getPartyName());
                if(orderForm.getAttach()==null){
                    orderForm.setAttach("GOODS");
                }
                orderForm.setTotal(singleOrderTotal);
                orderFormList.add(orderForm);

                totalNow = totalNow.add(singleOrderTotal);
            }

            if(totalNow.compareTo(goodsOrderForm.getTotal())!=0){//购物车价格与商品现价不一致
                return this.failure("购物车价格已过期");
            }

            createOrderForm.setOrderPayFormList(goodsOrderForm.getOrderPayFormList());
            createOrderForm.setTotal(totalNow);

            if(UserContextHolder.getInstance().getApplicationParty().equals(CommonConstant.DefaultParty.PartyId)){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mobile", goodsOrderForm.getMobile());
                jsonObject.put("partyId", UserContextHolder.getInstance().getPartyId());
                jsonObject.put("appName", goodsOrderForm.getAppName());
                jsonObject.put("expireTime", expireTime);
                jsonObject.put("goodsIds", goodsIds);
                jsonObject.put("goodsName", goodsName);
                createOrderForm.setAttach(jsonObject.toJSONString());
                createOrderForm.setRoutingKey(CommonConstant.RouteKey.UC);
            }

            return orderClient.createOrder(createOrderForm);
        }

    @PostMapping(value = "/goodsPay")
    @ApiOperation("商品支付")
    @Transactional
    public Result goodsPay(@Valid @RequestBody PayOrderForm payOrderForm) {

        //重新计算订单总金额 判断价格是否已过期
        OrderQueryForm orderQueryForm = new OrderQueryForm();
        BeanUtils.copyProperties(payOrderForm, orderQueryForm);
        OrderVOExt orderVOExt = orderClient.getOrder(orderQueryForm).getData();

        if(orderVOExt == null){
            return  this.failure("订单不存在");
        }
        //重新计算订单总金额 判断价格是否已过期
        BigDecimal totalNow = BigDecimal.ZERO;
        Goods goods = null;
        GoodsSku goodSku = null;

        for(OrderItemVO orderItemVO : orderVOExt.getOrderItemVOList()){
            goodSku = goodsSkuService.selectGoodsSkuById(orderItemVO.getGoodsSkuId());
            goods = goodsService.selectGoodsById(goodSku.getGoodsId());
            //判断付款扣减库存的商品库存是否还充足
            if (GoodsConstant.GOODS_COST_AFTER_PAY.equalsIgnoreCase(goods.getCostType())) {
                if(Integer.compare(goodSku.getStock(), orderItemVO.getQuantity()) == -1){
                    return this.failure("商品" + goods.getName() + "库存不足");
                }
            }
            totalNow = totalNow.add(goodSku.getPrice().multiply(new BigDecimal(orderItemVO.getQuantity())));
        }

        if(totalNow.compareTo(orderVOExt.getTotal().subtract(orderVOExt.getFreight()))!=0){//购物车价格与商品现价不一致
            return this.failure("商品价格已过期");
        }

        return orderClient.payOrder(payOrderForm);
    }
}

