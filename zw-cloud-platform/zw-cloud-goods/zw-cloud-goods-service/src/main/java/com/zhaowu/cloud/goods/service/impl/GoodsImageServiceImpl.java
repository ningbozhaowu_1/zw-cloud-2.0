package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;
import com.zhaowu.cloud.goods.entity.po.GoodsSku;
import com.zhaowu.cloud.goods.mapper.GoodsImageMapper;
import com.zhaowu.cloud.goods.service.GoodsImageService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class GoodsImageServiceImpl extends ServiceImpl<GoodsImageMapper, GoodsImage> implements GoodsImageService {

    @Override
    public void addBatch(List<GoodsImage> goodsImageList) {
        this.saveBatch(goodsImageList);
    }

    @Override
    public List<GoodsImage> modifyGoodsImage(String goodsId, List<GoodsImage> goodsImageList) {

        for(GoodsImage goodsImage : goodsImageList){
            goodsImage.setGoodsId(goodsId);
        }

        this.saveOrUpdateBatch(goodsImageList);
        return this.getGoodsImageList(goodsId);
    }

    @Override
    public List<GoodsImage> getGoodsImageList(String goodsId) {
        QueryWrapper<GoodsImage> queryWrapper = new QueryWrapper<GoodsImage>();
        queryWrapper.eq("goods_id", goodsId);
        return this.list(queryWrapper);
    }

    @Override
    public void delete(String goodsId) {
        QueryWrapper<GoodsImage> queryWrapper = new QueryWrapper<GoodsImage>();
        queryWrapper.eq("goods_id", goodsId);
        this.remove(queryWrapper);
    }

    @Override
    public void delete(String goodsId, String[] ids) {
        this.removeByIds(Arrays.asList(ids));
    }
}
