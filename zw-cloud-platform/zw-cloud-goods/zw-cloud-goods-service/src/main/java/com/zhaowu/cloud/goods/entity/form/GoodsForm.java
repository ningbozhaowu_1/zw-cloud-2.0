package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.Goods;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class GoodsForm extends BaseForm<Goods> {

    @NotNull(message="商品sku不能为空")
    @ApiModelProperty("商品sku集合")
    private List<GoodsSkuForm> goodsSkuForms;

    @ApiModelProperty("商品轮播图集合")
    private List<GoodsImageForm> goodsImageForms;

    @ApiModelProperty("商品内容")
    private GoodsContentsForm goodsContentsForm;

    @ApiModelProperty("商品属性集合")
    private List<GoodsPropertyForm> goodsPropertyForms;

    @ApiModelProperty("分组编号")
    private String goodsGroupId;

    @ApiModelProperty("类目编号")
    private String categoryId;

    @NotNull(message="商品名称不能为空")
    @Size(max= 256, message="商品名称长度必须在{max}个字符内")
    @ApiModelProperty("商品名称")
    private String name;

    @ApiModelProperty("商品编码")
    @Size(max= 50, message="商品编码长度必须在{max}个字符内")
    private String goodsCode;

    @ApiModelProperty("商品封面图")
    @Size(max= 2048, message="商品封面图长度必须在{max}个字符内")
    private String image;

    @ApiModelProperty("商品封面视频")
    @Size(max= 2048, message="商品封面视频长度必须在{max}个字符内")
    private String video;

    @ApiModelProperty("分享描述")
    @Size(max= 256, message="分享描述长度必须在{max}个字符内")
    private String shareDesc;

    @ApiModelProperty("商品简介")
    @Size(max= 1024, message="商品简介长度必须在{max}个字符内")
    private String introduction;

    @ApiModelProperty("商品是否参与会员价")
    @NotNull(message="商品是否参与会员价不能为空")
    private String isVIP;

    @ApiModelProperty("库存扣减方式 1-下单立减 2-付款后减")
    @NotNull(message="库存扣减方式不能为空")
    private String costType;

    @ApiModelProperty("商品分类")
    @NotNull(message="商品分类不能为空")
    private String goodsTypeId;
}
