package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.constant.GoodsConstant;
import com.zhaowu.cloud.goods.entity.form.GoodsTypePropertyForm;
import com.zhaowu.cloud.goods.entity.po.*;
import com.zhaowu.cloud.goods.mapper.GoodsMapper;
import com.zhaowu.cloud.goods.mapper.GoodsTypeMapper;
import com.zhaowu.cloud.goods.mapper.GoodsTypePropertyNameMapper;
import com.zhaowu.cloud.goods.mapper.GoodsTypePropertyValueMapper;
import com.zhaowu.cloud.goods.service.GoodsTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 商品分类 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Service
public class GoodsTypeServiceImpl extends ServiceImpl<GoodsTypeMapper,GoodsType> implements GoodsTypeService {

    @Autowired
    private Validator validator;

    @Autowired
    private GoodsTypePropertyNameMapper goodsTypePropertyNameMapper;

    @Autowired
    private GoodsTypePropertyValueMapper goodsTypePropertyValueMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    @Transactional
    public void add(GoodsType goodsType) {

        checkGoodsType(goodsType);

        //更新父类节点状态
        if(goodsType.getParentId()!=null){
            GoodsType parent = this.getById(goodsType.getParentId());
            parent.setIsParent(CommonConstant.INDICATOR_YES);
            this.updateById(parent);
        }

        goodsType.setIsParent(CommonConstant.INDICATOR_NO);
        if(StringUtils.isEmpty(goodsType.getStatus())){
            goodsType.setStatus(CommonConstant.GoodsStatus.A);
        }

        //一级分类 parentid 0
        if(goodsType.getParentId()==null){
            goodsType.setParentId("0");
        }

        this.save(goodsType);
    }

    @Override
    public void modify(GoodsType goodsType) {

        checkGoodsType(goodsType);

        GoodsType goodsTypeExsit = this.getById(goodsType.getId());

        //更新新父类节点状态
        if(!"0".equals(goodsType.getParentId())){
            GoodsType parent = this.getById(goodsType.getParentId());
            if(CommonConstant.INDICATOR_NO.equals(parent.getIsParent())){
                parent.setIsParent(CommonConstant.INDICATOR_YES);
                this.updateById(parent);
            }
        }

        //一级分类 parentid 0
        if(goodsType.getParentId()==null){
            goodsType.setParentId("0");
        }

        //更新原父节点状态
        if(!goodsTypeExsit.getParentId().equals(goodsType.getParentId())){
            List<GoodsType> children = this.getGoodsTypeListByParentId(goodsTypeExsit.getParentId());
            if(children!=null && children.size()==1
                    && children.get(0).getId()==goodsTypeExsit.getId()){
                GoodsType parent = this.getById(goodsTypeExsit.getParentId());
                parent.setIsParent(CommonConstant.INDICATOR_NO);
                this.updateById(parent);
            }else if(children == null){
                GoodsType parent = this.getById(goodsTypeExsit.getParentId());
                parent.setIsParent(CommonConstant.INDICATOR_NO);
                this.updateById(parent);
            }
        }

        this.updateById(goodsType);
    }

    @Override
    public void delete(String goodsParty, String id) {

        QueryWrapper<GoodsType> typeQueryWrapper = new QueryWrapper<GoodsType>();
        typeQueryWrapper.eq("parent_id", id);
        if(this.count(typeQueryWrapper)>0){
            Assert.isTrue(false,"当前分类下存在子分类不允许删除");
        }
        QueryWrapper<Goods> wrapper = new QueryWrapper<Goods>();
        wrapper.eq("goods_type_id", id);

        if(goodsMapper.selectCount(wrapper)>0){
            Assert.isTrue(false,"当前分类下存在商品不允许删除");
        }

        QueryWrapper<GoodsType> delete = new QueryWrapper<GoodsType>();
        delete.eq("id", id);
        delete.eq("goods_party", goodsParty);
        this.remove(delete);
    }

    @Override
    @Transactional
    public void addGoodsTypeProperty(GoodsTypePropertyForm goodsTypePropertyForm) {

        GoodsTypePropertyName goodsTypePropertyName = new GoodsTypePropertyName();
        BeanUtils.copyProperties(goodsTypePropertyForm, goodsTypePropertyName);
        goodsTypePropertyName.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
        checkGoodsTypeProperty(goodsTypePropertyName);
        this.goodsTypePropertyNameMapper.insert(goodsTypePropertyName);

        for(String value : goodsTypePropertyForm.getValues()){
            GoodsTypePropertyValue goodsSkuPropertyValue = new GoodsTypePropertyValue();
            goodsSkuPropertyValue.setNameId(goodsTypePropertyName.getId());
            goodsSkuPropertyValue.setValue(value);
            goodsTypePropertyValueMapper.insert(goodsSkuPropertyValue);
        }
    }

    @Override
    @Transactional
    public void removeGoodsTypeProperty(String nameId) {
        this.goodsTypePropertyNameMapper.deleteById(nameId);

        QueryWrapper<GoodsTypePropertyValue> wrapper = new QueryWrapper<GoodsTypePropertyValue>();
        wrapper.eq("name_id", nameId);
        this.goodsTypePropertyValueMapper.delete(wrapper);
    }

    @Override
    public List<GoodsTypePropertyName> getGoodsTypePropertyNameList(String groupTypeId) {
        QueryWrapper<GoodsTypePropertyName> wrapper = new QueryWrapper<GoodsTypePropertyName>();
        wrapper.eq("goods_type_id", groupTypeId);
        return this.goodsTypePropertyNameMapper.selectList(wrapper);
    }

    @Override
    public GoodsTypePropertyName getGoodsTypePropertyNameById(String nameId) {
        return this.goodsTypePropertyNameMapper.selectById(nameId);
    }

    @Override
    public List<GoodsTypePropertyValue> getGoodsTypePropertyValueList(String nameId) {
        QueryWrapper<GoodsTypePropertyValue> wrapper = new QueryWrapper<GoodsTypePropertyValue>();
        wrapper.eq("name_id", nameId);
        return this.goodsTypePropertyValueMapper.selectList(wrapper);
    }

    @Override
    public List<GoodsTypePropertyName> getGoodsTypeSearchNameList(String groupTypeId) {
        QueryWrapper<GoodsTypePropertyName> wrapper = new QueryWrapper<GoodsTypePropertyName>();
        wrapper.eq("goods_type_id", groupTypeId);
        wrapper.eq("search_indicator", "1");
        return this.goodsTypePropertyNameMapper.selectList(wrapper);
    }

    @Override
    public List<GoodsType> getGoodsTypeRootList(String goodsParty) {

        QueryWrapper<GoodsType> wrapper = new QueryWrapper<GoodsType>();
        wrapper.eq("goods_party", goodsParty);
        wrapper.eq("parent_id", "0");
        return this.list(wrapper);
    }

    @Override
    public List<GoodsType> getGoodsTypeList(String goodsParty) {
        QueryWrapper<GoodsType> wrapper = new QueryWrapper<GoodsType>();
        wrapper.eq("goods_party", goodsParty);
        wrapper.eq("status", GoodsConstant.GOODS_STATUS_ENABLED);
        return this.list(wrapper);
    }

    @Override
    public List<GoodsType> getGoodsTypeListByParentId(String parentId) {
        QueryWrapper<GoodsType> wrapper = new QueryWrapper<GoodsType>();
        wrapper.eq("parent_id", parentId);
        return this.list(wrapper);
    }

    @Override
    public List<GoodsType> getAllGoodsType(String goodsParty) {
        QueryWrapper<GoodsType> wrapper = new QueryWrapper<GoodsType>();
        wrapper.eq("goods_party", goodsParty);
        return this.list(wrapper);
    }

    public void checkGoodsTypeProperty(GoodsTypePropertyName goodsTypePropertyName) {

        QueryWrapper<GoodsTypePropertyName> wrapper = new QueryWrapper<GoodsTypePropertyName>();
        wrapper.eq("goods_party", goodsTypePropertyName.getGoodsParty());
        wrapper.eq("name", goodsTypePropertyName.getName());
        wrapper.eq("goods_type_id", goodsTypePropertyName.getGoodsTypeId());
        if(this.goodsTypePropertyNameMapper.selectOne(wrapper)!=null){
            Assert.isTrue(false, "分类属性名已存在");
        }
        //验证数据
        Set<ConstraintViolation<GoodsTypePropertyName>> constraintViolations = validator.validate(goodsTypePropertyName);
        if(constraintViolations.size() > 0){
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    public void checkGoodsType(GoodsType goodsType) {
        boolean checkGoodsName = this.checkGoodsTypeName(goodsType);
        Assert.isTrue(checkGoodsName, "商品分类名称已存在");
        if(goodsType.getParentId()!=null){
            boolean checkGoodsTypeParent = checkGoodsTypeParent(goodsType);
            Assert.isTrue(checkGoodsTypeParent, "商品分类父类不存在或状态异常");
        }
        //验证商品分类数据
        Set<ConstraintViolation<GoodsType>> constraintViolations = validator.validate(goodsType);
        if(constraintViolations.size() > 0){
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    /**
     * 检查商品分类名称
     *
     * @return
     */
    private boolean checkGoodsTypeName(GoodsType goodsType) {
        return this.checkProperty("name", goodsType.getName(), goodsType.getGoodsParty(),goodsType.getId());
    }

    /**
     * 检查商品父节点
     *
     * @return
     */
    private boolean checkGoodsTypeParent(GoodsType goodsType) {
        GoodsType parent = this.getById(goodsType.getParentId());
        return parent == null || (parent!=null && CommonConstant.GoodsTypeStatus.A.equals(parent.getStatus()));
    }

    public boolean checkProperty(String property, Object value, String goodsParty, String excludeId) {
        if (value == null || StringUtils.isEmpty(String.valueOf(value))) {
            return true;
        }
        List<GoodsType> goodsTypes = null;
        QueryWrapper<GoodsType> wrapper = new QueryWrapper<GoodsType>();
        wrapper.eq(property, value);
        if(goodsParty!=null){
            wrapper.eq("goods_party", goodsParty);
        }
        if (excludeId != null) {
            wrapper.ne("id", excludeId);
        }
        goodsTypes = this.list(wrapper);
        return (goodsTypes.size() == 0);
    }
}
