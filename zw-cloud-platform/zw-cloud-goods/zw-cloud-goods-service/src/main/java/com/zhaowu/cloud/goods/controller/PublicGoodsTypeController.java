package com.zhaowu.cloud.goods.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.TreeUtil;
import com.zhaowu.cloud.framework.util.bean.Node;
import com.zhaowu.cloud.goods.entity.po.GoodsType;
import com.zhaowu.cloud.goods.entity.po.GoodsTypePropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsTypePropertyValue;
import com.zhaowu.cloud.goods.service.GoodsTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 商品分类 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@RestController
@RequestMapping("/public/goodsType/v1")
@Api(tags = "应用端商品分类 接口")
public class PublicGoodsTypeController extends BaseController {

        @Autowired
        private GoodsTypeService goodsTypeService;

        /**
         * 分类筛选属性列表
         */
        @GetMapping(value = "/goodsTypeSearchNameList")
        @ApiOperation("分类筛选属性列表")
        public Result<List<GoodsTypePropertyName>> goodsTypeSearchNameList(@Valid @RequestParam String groupTypeId) {
            return this.success(goodsTypeService.getGoodsTypeSearchNameList(groupTypeId));
        }

        /**
         * 分类属性值列表
         */
        @GetMapping(value = "/goodsTypeValueNameList")
        @ApiOperation("应用端分类属性值列表")
        public Result<List<GoodsTypePropertyValue>> goodsTypeValueNameList(@Valid @RequestParam String nameId) {

            return this.success(goodsTypeService.getGoodsTypePropertyValueList(nameId));
        }

        /**
         * 商品分类一级列表
         */
        @GetMapping(value = "/goodsTypeRootList")
        @ApiOperation("商品分类一级列表")
        public Result<List<GoodsType>> goodsTypeRootList(@Valid @RequestParam String goodsParty) {

            List<GoodsType> goodsTypes = goodsTypeService.getGoodsTypeRootList(goodsParty);

            return success(goodsTypes);
        }

        /**
         * 商品子分类列表
         */
        @GetMapping(value = "/goodsTypeListByParentId")
        @ApiOperation("商品子分类列表")
        public Result<List<GoodsType>> goodsTypeListByParentId(@Valid @RequestParam String parentId) {

            List<GoodsType> goodsTypes = goodsTypeService.getGoodsTypeListByParentId(parentId);
            return success(goodsTypes);
        }

        /**
         * 商品分类树
         */
        @GetMapping(value = "/getGoodsTypeTree")
        @ApiOperation("商品分类树")
        public Result<List<Node>> getGoodsTypeTree(@Valid @RequestParam String goodsParty) {

            List<GoodsType> goodsTypes = goodsTypeService.getAllGoodsType(goodsParty);
            List<Node> nodes = new LinkedList<Node>();
            for(GoodsType goodsType : goodsTypes){
                Node node = new Node();
                node.setIcon(goodsType.getIcon());
                node.setId(goodsType.getId());
                node.setPid(goodsType.getParentId());
                node.setName(goodsType.getName());
                nodes.add(node);
            }
            return this.success(TreeUtil.data("0" ,nodes));
        }

}

