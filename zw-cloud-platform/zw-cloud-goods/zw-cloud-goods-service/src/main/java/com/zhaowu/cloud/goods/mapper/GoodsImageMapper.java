package com.zhaowu.cloud.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;
import com.zhaowu.cloud.goods.entity.po.GoodsSku;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品图片 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
public interface GoodsImageMapper extends BaseMapper<GoodsImage> {

}
