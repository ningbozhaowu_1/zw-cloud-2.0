package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhaowu.cloud.goods.entity.po.GoodsType;

import java.util.List;

/**
 * <p>
 * 商品分组表 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
public interface GoodsGroupService{

    /**
     * 创建商品分组
     * @param goodsGroup
     */
    @CacheInvalidate(name="goodsGroupListCache::", key="#goodsGroup.goodsParty")
    void add(GoodsGroup goodsGroup);

    /**
     * 修改商品分组
     * @param goodsGroup
     */
    @CacheInvalidate(name="goodsGroupListCache::", key="#goodsGroup.goodsParty")
    void modify(GoodsGroup goodsGroup);

    /**
     * 删除商品分组
     * @param id
     * @param goodsParty
     */
    @CacheInvalidate(name="goodsGroupListCache::", key="#goodsParty")
    void delete(String goodsParty, String id);

    @Cached(name="goodsGroupListCache::", key="#goodsParty", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    List<GoodsGroup> getGoodsGroupList(String goodsParty);
}
