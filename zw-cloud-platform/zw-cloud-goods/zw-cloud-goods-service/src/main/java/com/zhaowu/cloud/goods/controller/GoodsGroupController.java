package com.zhaowu.cloud.goods.controller;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.entity.form.GoodsGroupForm;
import com.zhaowu.cloud.goods.entity.form.GoodsGroupGoodsForm;
import com.zhaowu.cloud.goods.entity.form.ModifyGoodsGroupForm;
import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import com.zhaowu.cloud.goods.service.GoodsGroupGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.goods.service.GoodsGroupService;

import java.util.List;


import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 商品分组表 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@RestController
@RequestMapping("/goodsGroup/admin/v1")
@Api(tags = "管理端商品分组表 接口")
public class GoodsGroupController extends BaseController {

        @Autowired
        private GoodsGroupService goodsGroupService;

        @Autowired
        private GoodsGroupGoodsService goodsGroupGoodsService;

        /**
         * 获取商品分组表列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取商品分组表列表")
        public Result<List<GoodsGroup>> list() {
            return success(goodsGroupService.getGoodsGroupList(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId()));
        }

        @PostMapping(value = "/add")
        @ApiOperation("新增商品分组")
        public Result<String> add(@Valid @RequestBody GoodsGroupForm goodsGroupForm) {

            GoodsGroup goodsGroup = new GoodsGroup();
            goodsGroup.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
            BeanUtils.copyProperties(goodsGroupForm, goodsGroup);
            goodsGroupService.add(goodsGroup);
            return this.success();
        }

        @DeleteMapping(value = "/delete")
        @ApiOperation("物理删除商品分组")
        @Transactional
        public Result<String> delete(@Valid @RequestParam String id) {

            goodsGroupService.delete(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId(), id);
            goodsGroupGoodsService.deleteByGoodsGroupId(id);
            return this.success();
        }

        @PostMapping(value = "/modify")
        @ApiOperation("修改商品分组")
        public Result<String> modify(@Valid @RequestBody ModifyGoodsGroupForm modifyGoodsGroupForm) {

            GoodsGroup goodsGroup = new GoodsGroup();
            BeanUtils.copyProperties(modifyGoodsGroupForm, goodsGroup);
            goodsGroup.setGoodsParty(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
            goodsGroupService.modify(goodsGroup);
            return this.success();
        }

        @PostMapping(value = "/addGroupGoods")
        @ApiOperation("新增分组商品")
        public Result<String> addGroupGoods(@Valid @RequestBody GoodsGroupGoodsForm goodsGroupForm) {

            goodsGroupGoodsService.batchAdd(goodsGroupForm.getGoodsGroupId(), goodsGroupForm.getGoodsIds());
            return this.success();
        }
}

