package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.goods.entity.form.GoodsPropertyForm;
import com.zhaowu.cloud.goods.entity.form.GoodsSkuPropertyForm;
import com.zhaowu.cloud.goods.entity.po.GoodsPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyName;
import com.zhaowu.cloud.goods.entity.po.GoodsSkuPropertyValue;

import java.util.List;

/**
 * <p>
 * 商品属性 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-12-29
 */
public interface GoodsPropertyNameService {

    @CacheInvalidate(name="goodsPropertyListCache:", key="#goodsPropertyName.goodsId")
    void add(GoodsPropertyName goodsPropertyName);

    @CacheInvalidate(name="goodsPropertyListCache:", key="#goodsPropertyName.goodsId")
    void modify(GoodsPropertyName goodsPropertyName);

    @CacheInvalidate(name="goodsPropertyListCache:", key="#goodsId")
    void delete(String goodsId, String[] ids);
}
