package com.zhaowu.cloud.goods.mapper;

import com.zhaowu.cloud.goods.entity.po.GoodsShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品购物车 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface GoodsShoppingCartMapper extends BaseMapper<GoodsShoppingCart> {

}
