package com.zhaowu.cloud.goods.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品属性名表
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsPropertyName extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     *  参与者编号
     */
    @TableField(value="goods_party")
    private String goodsParty;

    /**
     * 商品编号
     */
    @TableField("GOODS_ID")
    private String goodsId;

    /**
     * 属性名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 销售属性 0否1是
     */
    @TableField("SALE_INDICATOR")
    private String saleIndicator;

    /**
     * 分类属性编号
     */
    @TableField("GOOD_TYPE_NAME_ID")
    private String goodTypeNameId;

    /**
     * 属性单位
     */
    @TableField("UNIT")
    private String unit;

}
