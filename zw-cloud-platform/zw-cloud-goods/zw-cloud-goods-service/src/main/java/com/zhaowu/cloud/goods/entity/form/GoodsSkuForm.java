package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsSku;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class GoodsSkuForm extends BaseForm<GoodsSku> {

    @NotNull(message = "价格不能为空")
    @Min(0)
    @ApiModelProperty("价格(元)")
    private BigDecimal price;

    @Min(0)
    @ApiModelProperty("成本价(元)")
    private BigDecimal cost;

    @NotNull(message = "库存不能为空")
    @Min(0)
    @ApiModelProperty("库存")
    private Integer stock;

    @ApiModelProperty("重量(克)")
    private BigDecimal weight;

    @ApiModelProperty("属性值ID组合 id1:id2")
    private String propertyValueIds;
}
