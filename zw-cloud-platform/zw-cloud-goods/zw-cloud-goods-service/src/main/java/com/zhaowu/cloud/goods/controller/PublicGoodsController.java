package com.zhaowu.cloud.goods.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.goods.constant.GoodsConstant;
import com.zhaowu.cloud.goods.entity.form.GoodsQueryForm;
import com.zhaowu.cloud.goods.entity.param.GoodsQueryParam;
import com.zhaowu.cloud.goods.entity.po.*;
import com.zhaowu.cloud.goods.entity.vo.GoodsDetailVO;
import com.zhaowu.cloud.goods.entity.vo.GoodsPropertyVO;
import com.zhaowu.cloud.goods.entity.vo.GoodsSkuVO;
import com.zhaowu.cloud.goods.entity.vo.GoodsVO;
import com.zhaowu.cloud.goods.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

/**
 * <p>
 * 商品信息表 控制器
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@RestController
@RequestMapping("/public/goods/v1")
@Api(tags = "应用端商品信息表 接口")
public class PublicGoodsController extends BaseController {

        @Autowired
        private GoodsService goodsService;

        @Autowired
        private GoodsSkuService goodsSkuService;

        @Autowired
        private GoodsImageService goodsImageService;

        @Autowired
        private GoodsContentsService goodsContentsService;

        @Autowired
        private GoodsSkuPropertyService goodsSkuPropertyService;

        @Autowired
        private GoodsPropertyService goodsPropertyService;

        /**
         * 应用端商品列表
         */
        @PostMapping(value = "/goodsPageList")
        @ApiOperation("应用端商品列表 含筛选")
        public Result<Page<Goods>> publicGoodsPageList(@Valid @RequestBody GoodsQueryForm goodsQueryForm) {

            if(goodsQueryForm.getGoodsParty()==null){
                Assert.isTrue(false,"应用编号不能为空");
            }
            IPage<Goods> page = goodsService.pageList(goodsQueryForm.getPage(), goodsQueryForm.toParam(GoodsQueryParam.class));

            List<Goods> goodsList = page.getRecords();

            page.setRecords(null);

            IPage<GoodsVO> goodsVOPage = new Page<GoodsVO>();

            BeanUtils.copyProperties(page, goodsVOPage);

            List<GoodsVO> goodsVOList = JSON.parseArray(JSON.toJSONString(goodsList), GoodsVO.class);

            goodsVOPage.setRecords(goodsVOList);

            List<GoodsSku> goodsSkuList = null;

            for(GoodsVO goodsVO : goodsVOPage.getRecords()){
                BeanUtils.copyProperties(this.goodsService.selectGoodsById(goodsVO.getId()), goodsVO);
                goodsSkuList = this.goodsSkuService.getGoodsSkuList(goodsVO.getId());
                if(!CollectionUtils.isEmpty(goodsSkuList)){
                    goodsVO.setMinPrice(goodsSkuList.get(0).getPrice());
                    goodsVO.setMaxPrice(goodsSkuList.get(goodsSkuList.size()-1).getPrice());
                }
            }
            return this.success(goodsVOPage);
        }

        /**
         * 商品详情
         */
        @GetMapping(value = "/goodsDetail")
        @ApiOperation("应用端商品详情")
        public Result<GoodsDetailVO> goodsDetail(@Valid @RequestParam String id) {

            GoodsDetailVO goodsDetailVO = new GoodsDetailVO();
            Goods goods = goodsService.selectGoodsById(id);
            if(goods==null || GoodsConstant.GOODS_STATUS_DISABLED.equals(goods.getStatus())){
                return this.failure("商品已失效");
            }
            goodsDetailVO.setGoods(goods);

            goodsDetailVO.setGoodsContents(goodsContentsService.getGoodsContents(id));
            goodsDetailVO.setGoodsImageList(goodsImageService.getGoodsImageList(id));
            List<GoodsSku> goodsSkuList = goodsSkuService.getGoodsSkuList(id);

            Map<String, List<GoodsSkuPropertyValue>> goodsSkuPropertyValueListMap = new LinkedHashMap<String, List<GoodsSkuPropertyValue>>();
            GoodsSkuPropertyValue goodsSkuPropertyValue = null;
            GoodsSkuPropertyName goodsSkuPropertyName = null;

            Map<String, GoodsSkuVO> goodsSkuListMap = new LinkedHashMap<String, GoodsSkuVO>();
            List values = new ArrayList();
            for(GoodsSku goodsSku : goodsSkuList){
                String[] valueIds = goodsSku.getPropertyValueIds().split(":");
                for(String valueId : valueIds){
                    goodsSkuPropertyValue = goodsSkuPropertyService.getValue(valueId);
                    goodsSkuPropertyName =goodsSkuPropertyService.getName(goodsSkuPropertyValue.getNameId());
                    if(!values.contains(valueId)){
                        values.add(valueId);
                        if(goodsSkuPropertyValueListMap.containsKey(goodsSkuPropertyName.getName())){
                            goodsSkuPropertyValueListMap.get(goodsSkuPropertyName.getName()).add(goodsSkuPropertyValue);
                        }else{
                            List<GoodsSkuPropertyValue> goodsSkuPropertyValueList = new LinkedList<GoodsSkuPropertyValue>();
                            goodsSkuPropertyValueList.add(goodsSkuPropertyValue);
                            goodsSkuPropertyValueListMap.put(goodsSkuPropertyName.getName(), goodsSkuPropertyValueList);
                        }
                    }
                }
                GoodsSkuVO goodsSkuVO = new GoodsSkuVO();
                BeanUtils.copyProperties(goodsSku, goodsSkuVO);
                goodsSkuListMap.put(goodsSku.getPropertyValueIds(), goodsSkuVO);
            }

            goodsDetailVO.setGoodsSkuPropertyValueListMap(goodsSkuPropertyValueListMap);
            goodsDetailVO.setGoodsSkuListMap(goodsSkuListMap);

//            List<GoodsProperty> goodsPropertyList = goodsService.getGoodsPropertyList(id);
//
//            Map<String, List> goodsPropertyMap = new LinkedHashMap();
//            for(GoodsProperty goodsProperty : goodsPropertyList){
//                if(goodsPropertyMap.containsKey(goodsProperty.getName())){
//                    goodsPropertyMap.get(goodsProperty.getName()).add(goodsProperty.getValue());
//                }else{
//                    List list = new LinkedList();
//                    list.add(goodsProperty.getValue());
//                    goodsPropertyMap.put(goodsProperty.getName(), list);
//                }
//            }

//            goodsDetailVO.setGoodsPropertyList(goodsPropertyMap);
            Map<String, List<GoodsPropertyVO>> goodsPropertyVOSMap = new LinkedHashMap<>();
            goodsPropertyVOSMap.put(GoodsConstant.GOODS_PROPERTY_GOODS_TYPE,this.goodsPropertyService.getGoodsTypeGoodsPropertyList(id));
            goodsDetailVO.setGoodsPropertyVOSMap(goodsPropertyVOSMap);

            return this.success(goodsDetailVO);
        }

}

