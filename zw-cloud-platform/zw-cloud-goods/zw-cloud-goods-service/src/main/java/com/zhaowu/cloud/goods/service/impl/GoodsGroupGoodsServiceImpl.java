package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.goods.entity.po.GoodsGroup;
import com.zhaowu.cloud.goods.entity.po.GoodsGroupGoods;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;
import com.zhaowu.cloud.goods.mapper.GoodsGroupGoodsMapper;
import com.zhaowu.cloud.goods.mapper.GoodsMapper;
import com.zhaowu.cloud.goods.service.GoodsGroupGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 商品分组关系表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Service
public class GoodsGroupGoodsServiceImpl extends ServiceImpl<GoodsGroupGoodsMapper,GoodsGroupGoods> implements GoodsGroupGoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    @Transactional
    public void batchAdd(String goodsGroupId, String[] goodsIds) {

        List<GoodsGroupGoods> goodsGroupGoodsList = new ArrayList<GoodsGroupGoods>();
        for(String goodsId : goodsIds){
            GoodsGroupGoods goodsGroupGoods = new GoodsGroupGoods();
            goodsGroupGoods.setGoodsId(goodsId);
            goodsGroupGoods.setGoodsGroupId(goodsGroupId);
            if(checkExsit(goodsGroupGoods)
                && checkGoods(goodsGroupGoods)){
                goodsGroupGoodsList.add(goodsGroupGoods);
            }
        }
        this.saveBatch(goodsGroupGoodsList, goodsGroupGoodsList.size());
    }

    @Override
    public void deleteByGoodsId(String goodsId) {
        QueryWrapper<GoodsGroupGoods> queryWrapper = new QueryWrapper<GoodsGroupGoods>();
        queryWrapper.eq("goods_id", goodsId);
        this.remove(queryWrapper);
    }

    @Override
    public void deleteByGoodsGroupId(String goodsGroupId) {
        QueryWrapper<GoodsGroupGoods> queryWrapper = new QueryWrapper<GoodsGroupGoods>();
        queryWrapper.eq("goods_group_id", goodsGroupId);
        this.remove(queryWrapper);
    }

    public boolean checkExsit(GoodsGroupGoods goodsGroupGoods) {
        QueryWrapper<GoodsGroupGoods> wrapper = new QueryWrapper<GoodsGroupGoods>();
        wrapper.eq("GOODS_GROUP_ID", goodsGroupGoods.getGoodsGroupId());
        wrapper.eq("GOODS_ID", goodsGroupGoods.getGoodsId());
        return this.list(wrapper).size()==0;
    }

    public boolean checkGoods(GoodsGroupGoods goodsGroupGoods) {
        return goodsMapper.selectById(goodsGroupGoods.getGoodsId()) != null;
    }
}
