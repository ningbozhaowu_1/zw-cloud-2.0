package com.zhaowu.cloud.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.framework.util.IdUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.goods.entity.param.GoodsQueryParam;
import com.zhaowu.cloud.goods.entity.po.*;
import com.zhaowu.cloud.goods.mapper.*;
import com.zhaowu.cloud.goods.service.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 商品信息表 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-12-28
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private GoodsTypeMapper goodsTypeMapper;

    @Autowired
    private GoodsImageMapper goodsImageMapper;

    @Autowired
    private GoodsContentsMapper goodsContentsMapper;

    @Autowired
    private Validator validator;

    @Override
    public Goods add(List<GoodsSku> goodsSkuList, Goods goods) {

        goods.setStock(0);
        if(goods.getGoodsCode()==null){
            goods.setGoodsCode(IdUtils.generate());
        }

//        checkGoods(goods);

        if(!CollectionUtils.isEmpty(goodsSkuList)){
            for(GoodsSku goodsSku : goodsSkuList){
                goods.setStock(goods.getStock()+goodsSku.getStock());
            }
        }

        this.save(goods);
        return goods;
    }

    @Override
    public Goods modifyGoods(List<GoodsSku> goodsSkuList, Goods goods) {

        checkGoods(goods);

        if(goods.getStock()==null){
            goods.setStock(0);
        }

        if(!CollectionUtils.isEmpty(goodsSkuList)){
            for(GoodsSku goodsSku : goodsSkuList){
                goods.setStock(goods.getStock()+goodsSku.getStock());
            }
        }

        this.updateById(goods);
        return goods;
    }

    @Override
    public Goods modifyGoods(Goods goods) {
        this.updateById(goods);
        return goods;
    }

    @Override
    public boolean delete(String goodsParty, String id) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<Goods>();
        wrapper.eq("goods_party", goodsParty);
        wrapper.eq("id", id);
        return this.remove(wrapper);
    }

    @Override
    public Goods selectGoodsById(String id) {
        return this.getById(id);
    }


    @Override
    public IPage<Goods> pageList(Page<Goods> page, GoodsQueryParam goodsQueryParam) {

        String scene = goodsQueryParam.getScene();
        switch(scene){//1:单分类编号查询 2:分类编号+search筛选值查询 3:单分组编号查询
            case "1":
                QueryWrapper<Goods> queryWrapper = goodsQueryParam.build();
                queryWrapper.select("id");
                queryWrapper.eq("goods_party", goodsQueryParam.getGoodsParty());
                queryWrapper.eq("status", CommonConstant.GoodsStatus.A);
                if(goodsQueryParam.getGoodsTypeId()!=null){
                    queryWrapper.eq("goods_type_id", goodsQueryParam.getGoodsTypeId());
                }
                return this.page(page, queryWrapper);
            case "2":
                return goodsMapper.pageList(page, goodsQueryParam.getGoodsTypeId(), goodsQueryParam.getApplicationParty(), goodsQueryParam.getSearch());
            case "3":
                return goodsMapper.pageList4Group(page, goodsQueryParam.getGoodsGroupId(), goodsQueryParam.getApplicationParty());
            default:
                return page;
        }
    }

//    @Override
//    public List<GoodsProperty> getGoodsPropertyList(String goodsId) {
//        return goodsMapper.goodsPropertyList(goodsId);
//    }

    public void checkGoods(Goods goods) {
        boolean checkGoodsName = this.checkGoodsName(goods);
        boolean checkGoodsType = this.checkGoodsType(goods);
        Assert.isTrue(checkGoodsName, "商品名称已存在");
        Assert.isTrue(checkGoodsType, "商品分类不存在");
        //验证商品数据
        Set<ConstraintViolation<Goods>> constraintViolations = validator.validate(goods);
        if(constraintViolations.size() > 0){
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    /**
     * 检查商品名称
     *
     * @return
     */
    private boolean checkGoodsName(Goods goods) {
        return this.checkProperty("name", goods.getName(), goods.getGoodsParty(), goods.getId());
    }

    /**
     * 检查商品分类
     *
     * @return
     */
    private boolean checkGoodsType(Goods goods) {
        QueryWrapper<GoodsType> wrapper = new QueryWrapper<GoodsType>();
        wrapper.eq("goods_party", goods.getGoodsParty());
        wrapper.eq("id", goods.getGoodsTypeId());
        return goodsTypeMapper.selectOne(wrapper)!=null;
    }

    public boolean checkProperty(String property, Object value, String goodsParty, String excludeId) {
        if (value == null || StringUtils.isEmpty(String.valueOf(value))) {
            return true;
        }
        List<Goods> goods = null;
        QueryWrapper<Goods> wrapper = new QueryWrapper<Goods>();
        wrapper.eq(property, value);
        if(goodsParty!=null){
            wrapper.eq("goods_party", goodsParty);
        }
        if (excludeId != null) {
            wrapper.ne("id", excludeId);
        }
        goods = this.list(wrapper);
        return (goods.size() == 0);
    }
}
