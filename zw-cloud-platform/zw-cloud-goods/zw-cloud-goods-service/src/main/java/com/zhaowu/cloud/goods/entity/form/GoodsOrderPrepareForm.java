package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.order.entity.form.OrderPayForm;
import com.zhaowu.cloud.order.entity.form.OrderShipForm;
import com.zhaowu.cloud.uc.entity.vo.PartyShipAddressVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class GoodsOrderPrepareForm {

    @NotNull(message = "商品信息不能为空")
    private List<GoodsShoppingCartForm> goodsShoppingCartFormList;

    @NotNull(message = "收货地址编号不能为空")
    private String partyShipAddressId;

}
