package com.zhaowu.cloud.goods.entity.vo;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class GoodsOrderItemVO {

    private String goodsId;

    private String goodsSkuId;

//    private BigDecimal total;

    private BigDecimal price;

    private Integer quantity;

    private BigDecimal actualPrice;

    private String goodsImage;

    private String goodsName;

    private String goodsSkus;
}