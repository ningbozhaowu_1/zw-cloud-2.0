package com.zhaowu.cloud.goods.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 商品分组表
 * </p>
 *
 * @author xxp
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GoodsGroup extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     *  参与者编号
     */
    @TableField(value="goods_party")
    private String goodsParty;

    /**
     * 名称
     */
    private String name;

    /**
     * 显示样式 1大图2小图3一大两小4详细列表
     */
    @TableField("SHOW_TYPE")
    private String showType;

    /**
     * 商品名称显示标识 1是0否
     */
    @TableField("GOODS_NAME_INDICATOR")
    private String goodsNameIndicator;

    /**
     * 商品价格显示标识 1是0否
     */
    @TableField("GOODS_PRICE_INDICATOR")
    private String goodsPriceIndicator;

    /**
     * 分组图标
     */
    private String icon;

    /**
     * 状态 1正常 0 禁用
     */
    private String status;
}
