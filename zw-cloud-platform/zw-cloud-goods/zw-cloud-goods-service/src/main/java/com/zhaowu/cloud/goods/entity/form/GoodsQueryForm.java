package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.goods.entity.param.GoodsQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
public class GoodsQueryForm extends BaseQueryForm<GoodsQueryParam> {

    @ApiModelProperty("商品所属参与者编号 应用端查询必输 管理端非必输")
    private String goodsParty;

    @ApiModelProperty("分类编号筛选")
    private String goodsTypeId;

    @ApiModelProperty("自定义值筛选")
    private String search;

    @ApiModelProperty("分组编号筛选")
    private String goodsGroupId;

    @NotNull(message = "查询场景不能为空")
    @ApiModelProperty("查询场景 1:单分类编号查询 2:分类编号+search筛选值 3:分组编号")
    private String scene;
}
