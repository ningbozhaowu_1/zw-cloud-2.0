package com.zhaowu.cloud.goods.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.goods.entity.po.GoodsShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品购物车 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-13
 */
public interface GoodsShoppingCartService{

    List<GoodsShoppingCart> getList(String applicationParty, String userId);

    void modify(GoodsShoppingCart goodsShoppingCart);

    void delete(String userId, String id);

    void add(GoodsShoppingCart goodsShoppingCart);

    GoodsShoppingCart selectById(String userId, String id);
}
