package com.zhaowu.cloud.goods.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class GoodsSkuVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String goodsId;

    private String id;

    private BigDecimal price;

    private BigDecimal cost;

    private Integer stock;

    private BigDecimal weight;

    private Integer sales;

    private String propertyValueIds;
}
