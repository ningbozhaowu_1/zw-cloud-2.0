package com.zhaowu.cloud.goods.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.goods.entity.po.GoodsImage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ModifyGoodsImageForm extends BaseForm<GoodsImage> {

    @ApiModelProperty("商品图片编号")
    private String id;

    @ApiModelProperty("商品图片")
    @NotNull(message = "商品图片链接不能为空")
    @Size(max= 2048, message="商品封面图链接长度必须在{max}个字符内")
    private String image;

    @ApiModelProperty("图片名称")
    private String name;

    @ApiModelProperty("图片大小")
    private Long size;

}
