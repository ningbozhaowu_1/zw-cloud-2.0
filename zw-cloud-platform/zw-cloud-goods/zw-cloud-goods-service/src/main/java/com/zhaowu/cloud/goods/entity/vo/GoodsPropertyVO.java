package com.zhaowu.cloud.goods.entity.vo;

import lombok.Data;

@Data
public class GoodsPropertyVO {

    private String nameId;

    private String valueId;

    private String name;

    private String value;
}
