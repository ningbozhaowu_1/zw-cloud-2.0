package com.zhaowu.cloud.uc.client;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.form.*;
import com.zhaowu.cloud.uc.entity.vo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * @author xxp
 */
@FeignClient(name = "uc-service")
public interface UserClient {
    /**
     * 通过用户名查询用户认证信息
     * @return UserAuthentication
     */
    @PostMapping("/user/v1/findUser4Login")
    Result<UserAuthentication> findUser4Login(@RequestBody UserQueryForm userQueryForm);

    /**
     * 通过编号查询用户认证信息
     * @return UserAuthentication
     */
    @PostMapping("/user/v1/findUserById")
    Result<UserAuthentication> findUserById(@RequestBody UserQueryForm userQueryForm);

    @PostMapping("/user/admin/v1/findUserBatch")
    Result<List<UserVO>> findUserBatch(@RequestBody BatchUserQueryForm batchUserQueryForm);

    @PostMapping(value = "/app/admin/v1/getPartyChannel")
    Result<PartyChannelVO> getPartyChannel(@RequestBody PartyChannelQueryForm applicationChannelQueryForm);

    @PostMapping(value = "/app/admin/v1/getPartyPay")
    Result<List<PartyPayVO>> getPartyPay(@RequestBody PartyPayQueryForm applicationPayQueryForm);

    @GetMapping(value = "/app/admin/v1/getByApplicationParty")
    Result<ApplicationVO> getByApplicationParty(@RequestParam("applicationParty") String applicationParty);

    @PostMapping(value = "/app/admin/v1/getPartyChannelByAuthorizerAppId")
    Result<PartyChannelVO> getPartyChannelByAuthorizerAppId(@RequestBody PartyChannelQueryForm applicationChannelQueryForm);

    @PostMapping(value = "/app/admin/v1/updateIntroductionByAuthorizerAppId")
    Result<PartyChannelVO> updateIntroductionByAuthorizerAppId(@RequestBody PartyChannelUpdateForm partyChannelUpdateForm);

    @PostMapping(value = "/app/admin/v1/partyChannelAuthUpdate")
    Result authorizeUpdate(@RequestBody PartyChannelAuthorizeForm applicationChannelAuthorizeForm);

    @PostMapping(value = "/app/admin/v1/partyChannelAdd")
    Result authorizeAdd(@RequestBody PartyChannelAuthorizeForm applicationChannelAuthorizeForm);

    @GetMapping(value = "/party/v1/getDelivery")
    Result<PartyDeliveryVO> getDelivery(@RequestParam("partyId") String partyId);

    @GetMapping(value = "/party/v1/gePartyAttr")
    Result<PartyAttrVO> gePartyAttr(@RequestParam("partyAttrType") String partyAttrType, @RequestParam("partyAttrCode") String partyAttrCode);

    @PostMapping(value = "/partyExpress/v1/fee")
    Result<BigDecimal> fee(@RequestBody PartyExpressFeeQueryForm partyExpressFeeQueryRequest);

    @GetMapping(value = "/partyPickUp/v1/listByCity")
    Result<List<PartyPickUpVO>> getPickUplistByCity(@RequestParam("cityName") String cityName, @RequestParam("partyId") String partyId);

    @GetMapping(value = "/partyPickUp/v1/getById")
    Result<PartyPickUpVO> getPartyPickUpById(@RequestParam("id") String id);

    @GetMapping(value = "/partyShipAddress/v1/getById")
    Result<PartyShipAddressVO> getPartyShipAddressbyId(@RequestParam("id") String id);

    /**
     * 根据关联类型查询关联的参与者列表
     * @param assoType
     * @return
     */
    @GetMapping("/user/v1/partyListByAssoType")
    Result<List<PartyVO>> partyListByAssoType(@RequestParam("assoType") String assoType);

    /**
     * 根据关联类型和用户参与者编号查询关联的参与者列表
     * @param assoType
     * @return
     */
    @GetMapping("/user/admin/v1/partyListByAssoType")
    Result<List<PartyVO>> partyListByAssoType(@RequestParam("assoType") String assoType, @RequestParam("partyId") String partyId);

    @GetMapping(value = "/party/v1/getPartyById",consumes = MediaType.APPLICATION_JSON_VALUE)
    Result<PartyVO> getPartyById(@RequestParam("id") String id);

    /**
     * 支付进件草稿保存
     */
    @PostMapping(value = "/party/payApplyment/v1/saveOrUpdate")
    Result saveOrUpdate(@Valid @RequestBody PayApplymentForm payApplymentForm);

    /**
     * 支付进件草稿查询
     * @param businessCode
     * @return
     */
    @GetMapping(value = "/party/payApplyment/v1/query")
    Result<PayApplymentVO> queryByBusinessCode(@RequestParam("businessCode")  String businessCode);

    /**
     * 支付进件列表查询
     * @return
     */
    @GetMapping(value = "/party/payApplyment/v1/list")
    Result<List<PayApplymentVO>> list();
    /**
     * 微信mediaID文件关系保存
     */
    @PostMapping(value = "/party/payApplyment/v1/media/save")
    Result mediaSave(@RequestParam("mediaId")  String mediaId,@RequestParam("fullUrlPath")  String fullUrlPath);

    /**
     * 根据微信mediaID获取文件路径
     */
    @GetMapping(value = "/party/payApplyment/v1/media/id")
    @ApiOperation("根据mediaID获取文件地址")
     Result findMediaUrl(@Valid @RequestParam("mediaId") String mediaId);
}
