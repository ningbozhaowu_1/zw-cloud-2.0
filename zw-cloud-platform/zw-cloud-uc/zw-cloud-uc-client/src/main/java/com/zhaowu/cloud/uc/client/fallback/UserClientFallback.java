//package com.zhaowu.cloud.uc.client.fallback;
//
//import com.zhaowu.cloud.framework.base.protocol.Result;
//import com.zhaowu.cloud.uc.entity.form.*;
//import com.zhaowu.cloud.uc.entity.vo.*;
//import com.zhaowu.cloud.uc.client.UserClient;
//import org.springframework.stereotype.Component;
//
//import java.math.BigDecimal;
//import java.util.List;
//
//@Component
//public class UserClientFallback implements UserClient {
//
//    @Override
//    public Result<UserAuthentication> findUser4Login(UserQueryForm userQueryForm) {
//        return Result.buildFailure();
//    }
//
//    @Override
//    public Result<UserAuthentication> findUserById(UserQueryForm userQueryForm) {
//        return Result.buildFailure();
//    }
//
//    @Override
//    public Result<PartyChannelVO> getPartyChannel(PartyChannelQueryForm applicationChannelQueryForm) {
//        return null;
//    }
//
//    @Override
//    public Result<List<PartyPayVO>> getPartyPay(PartyPayQueryForm applicationPayQueryForm) {
//        return null;
//    }
//
//    @Override
//    public Result<PartyChannelVO> getPartyChannelByAuthorizerAppId(PartyChannelQueryForm applicationChannelQueryForm) {
//        return null;
//    }
//
//    @Override
//    public Result authorizeUpdate(PartyChannelAuthorizeForm applicationChannelAuthorizeForm) {
//        return null;
//    }
//
//    @Override
//    public Result authorizeAdd(PartyChannelAuthorizeForm applicationChannelAuthorizeForm) {
//        return null;
//    }
//
//    @Override
//    public Result<PartyDeliveryVO> getDelivery(String partyId) {
//        return null;
//    }
//
//    @Override
//    public Result<PartyAttrVO> gePartyAttr(String partyAttrType, String partyAttrCode) {
//        return null;
//    }
//
//    @Override
//    public Result<BigDecimal> fee(PartyExpressFeeQueryForm partyExpressFeeQueryRequest) {
//        return null;
//    }
//
//    @Override
//    public Result<List<PartyPickUpVO>> getPickUplistByCity(String cityName, String partyId) {
//        return null;
//    }
//
//    @Override
//    public Result<PartyPickUpVO> getPartyPickUpById(String id) {
//        return Result.buildSuccess(new PartyPickUpVO());
//    }
//
//    @Override
//    public Result<PartyShipAddressVO> getPartyShipAddressbyId(String id) {
//        return Result.buildSuccess(new PartyShipAddressVO());
//    }
//
//    @Override
//    public Result<List<PartyVO>> partyListByAssoType(String assoType) {
//        return null;
//    }
//
//
//}
