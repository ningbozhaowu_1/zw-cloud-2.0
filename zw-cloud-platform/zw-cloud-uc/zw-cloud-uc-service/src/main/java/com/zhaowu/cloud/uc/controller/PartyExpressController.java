package com.zhaowu.cloud.uc.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.form.PartyExpressFeeQueryForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressSettingForm;
import com.zhaowu.cloud.uc.service.PartyExpressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * <p>
 * 参与者快递 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@RestController
@RequestMapping("/partyExpress/v1")
@Api(tags = "参与者快递 接口")
public class PartyExpressController extends BaseController {

    @Autowired
    private PartyExpressService partyExpressService;

    @PostMapping("/setting")
    @ApiOperation("参与者快递设置")
    public void setPartyAttr(@Valid @RequestBody PartyExpressSettingForm request) {
        partyExpressService.setPartyAttr(request);
    }

    @GetMapping("/setting")
    @ApiOperation("参与者快递查询")
    public PartyExpressSettingForm getPartyAttr() {
        return partyExpressService.getPartyAttr();
    }

    @PostMapping("/fee")
    @ApiOperation("参与者快递费查询 返回费用为元单位")
    public Result<BigDecimal> fee(@Valid @RequestBody PartyExpressFeeQueryForm request) {
        return this.success(partyExpressService.queryPartyExpressFee(request).divide(BigDecimal.TEN).divide(BigDecimal.TEN).setScale(2, RoundingMode.HALF_UP));
    }
}
