package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CacheUpdate;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.param.UserQueryParam;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.entity.vo.UserVO;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface UserService {

    /**
     * 通过用户名获取用户
     * @param username
     * @return
     */
    User findUserByUsername(String username, String applicationParty);


    /**
     * 通过手机号获取用户
     * @param mobileNo
     * @return
     */
    User findUserByMobileNo(String mobileNo, String applicationParty);
    /**
     * 通过手机号获取用户,需要将owner也验证通过
     * @param mobileNo
     * @return
     */
    User findUsersByMobileNo(String mobileNo, String applicationParty);

    /**
     * 通过用户编号获取用户,需要将owner也验证通过
     * @param userId
     * @return
     */
    User findUserById(String userId, String applicationParty);



    List<User> findUserByPartyId(String partyId, String applicationParty);

    /**
     * 新增用户
     * @param user
     * @return
     */
    void add(User user);

    /**
     * 用户修改
     * @param user
     * @return
     */
    void modify(User user);

    /**
     * 分页查询用户列表
     * @param page
     * @return
     */
    IPage<User> pageList(Page<User> page, UserQueryParam userQueryParam);

    List<UserVO> batchQuery(Collection<String> userIds, Collection<String> mobileNos, String applicationParty, String partyChannelId);

}
