package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.PartyAttr;
import com.zhaowu.cloud.uc.entity.po.PartyExpressTemplate;

/**
 * <p>
 * 应用属性 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyAttrMapper extends BaseMapper<PartyAttr> {

}
