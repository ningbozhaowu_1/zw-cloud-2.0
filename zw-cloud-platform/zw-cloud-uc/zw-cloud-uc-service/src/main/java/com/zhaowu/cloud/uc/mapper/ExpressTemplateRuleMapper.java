package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.ExpressTemplateRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参与者快递运费规则 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface ExpressTemplateRuleMapper extends BaseMapper<ExpressTemplateRule> {

}
