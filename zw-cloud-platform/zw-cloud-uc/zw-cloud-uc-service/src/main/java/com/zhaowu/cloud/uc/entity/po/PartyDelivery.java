package com.zhaowu.cloud.uc.entity.po;

import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * <p>
 * 配送
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class PartyDelivery extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者编号
     */
    private String partyId;

    /**
     * 配送方式
     */
    private String deliveryType;

    /**
     * 配送范围名称
     */
    private String name;

    /**
     * 配送范围介绍
     */
    private String description;

    /**
     * 免配送费订单金额
     */
    private BigDecimal deliveryFreeAmount;

    /**
     * 免配送费配送范围（km）
     */
    private BigDecimal deliveryFreeDistance;

    /**
     * 起送金额
     */
    private BigDecimal deliveryBeginAmount;

    /**
     * 超出距离
     */
    private String additionalDistance;

    /**
     * 超出距离价格
     */
    private BigDecimal additionalFee;

    /**
     * 价格
     */
    private BigDecimal fee;

    private BigDecimal maxDistance;
}
