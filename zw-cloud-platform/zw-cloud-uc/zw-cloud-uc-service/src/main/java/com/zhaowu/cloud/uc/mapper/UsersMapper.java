package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.entity.vo.UserVO;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface UsersMapper extends BaseMapper<User> {

    IPage<User> pageUserList(Page<?> page, @Param("applicationParty") String applicationParty, @Param("userType") String userType, @Param("name") String name);

    List<UserVO> batchQuery(@Param("userIds") Collection<String> userIds, @Param("mobileNos") Collection<String> mobileNos, @Param("applicationParty") String applicationParty, @Param("partyChannelId") String partyChannelId);
     List<User> findUsersByMobileNo(@Param("mobileNo") String mobileNo, @Param("applicationParty") String applicationParty);
    List<User> findUserById(@Param("userId") String userId, @Param("applicationParty") String applicationParty);
}
