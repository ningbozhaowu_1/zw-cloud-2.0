package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.PartyDelivery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 配送 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyDeliveryMapper extends BaseMapper<PartyDelivery> {

}
