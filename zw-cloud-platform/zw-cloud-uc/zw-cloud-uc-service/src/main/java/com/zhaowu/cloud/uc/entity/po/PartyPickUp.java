package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 * 提货点
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class PartyPickUp extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者编号
     */
    @TableField(value = "party_id")
    private String partyId;

    /**
     * 自提点名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String componentPhone;

    /**
     * 自提点照片
     */
    private String picture;

    /**
     * 是否门店
     */
    private String isShop;

    /**
     * 开门时间
     */
    private String openTime;

    /**
     * 关门时间
     */
    private String closeTime;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 街道
     */
    private String district;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

}
