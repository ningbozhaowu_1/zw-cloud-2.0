package com.zhaowu.cloud.uc.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressTemplateSetForm;
import com.zhaowu.cloud.uc.entity.param.PartyExpressTemplateQueryParam;
import com.zhaowu.cloud.uc.entity.vo.PartyExpressTemplateVo;
import com.zhaowu.cloud.uc.service.PartyExpressTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 参与者快递模版 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@RestController
@RequestMapping("/partyExpressTemplate")
@Api(tags = "参与者快递模版 接口")
public class PartyExpressTemplateController extends BaseController {

    @Autowired
    private PartyExpressTemplateService partyExpressTemplateService;

    @PostMapping("/setting")
    @ApiOperation("参与者快递模版设置")
    public void setPartyExpressTemplate(@RequestBody PartyExpressTemplateSetForm request) {
        partyExpressTemplateService.setPartyExpressTemplate(request);
    }

    @DeleteMapping("/setting")
    @ApiOperation("参与者快递模版删除")
    public void deletePartyExpressTemplate(@RequestParam String templateId) {
        partyExpressTemplateService.deletePartyExpressTemplate(templateId);
    }

    @GetMapping("/setting")
    @ApiOperation("参与者快递模版查询")
    public IPage<PartyExpressTemplateVo> queryPartyExpressTemplate(@Valid BaseQueryForm<PartyExpressTemplateQueryParam> queryForm) {
        return partyExpressTemplateService.queryPartyExpressTemplate(queryForm);
    }

}

