package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.uc.entity.po.Organization;
import com.zhaowu.cloud.uc.mapper.OrganizationMapper;
import com.zhaowu.cloud.uc.service.OrganizationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组织者 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@Service
public class OrganizationServiceImpl extends ServiceImpl<OrganizationMapper,Organization> implements OrganizationService {

    @Override
    public void add(Organization organization) {
        this.save(organization);
    }

    @Override
    public void modify(Organization organization) {
        QueryWrapper<Organization> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id",organization.getPartyId());
        this.saveOrUpdate(organization, queryWrapper);
    }

    @Override
    public Organization selectById(String id) {
        QueryWrapper<Organization> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id",id);
        return this.getOne(queryWrapper);
    }

    @Override
    public Organization selectByHost(String host) {
        QueryWrapper<Organization> queryWrapper = new QueryWrapper();
        queryWrapper.eq("web_page",host);
        return this.getOne(queryWrapper);
    }
}
