package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.UserPin;

/**
 * <p>
 * 用户密码 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface UserPinMapper extends BaseMapper<UserPin> {

}
