package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyDelivery;
import com.zhaowu.cloud.uc.entity.po.PartyPickUp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class PartyDeliveryForm extends BaseForm<PartyDelivery> {

    @ApiModelProperty("配送编号")
    private String id;

    @NotNull(message="配送范围名称不能为空")
    @ApiModelProperty("配送范围名称")
    private String name;

    @NotNull(message="配送方式")
    @ApiModelProperty(value = "配送方式", allowableValues = "SELF,THIRD")
    private String deliveryType;

    @NotNull(message="免费方式不能为空")
    @ApiModelProperty(value = "配送免费方式", allowableValues = "AMOUNT,DISTANCE")
    private String freeType;

    @NotNull(message="配送范围介绍不能为空")
    @ApiModelProperty("配送范围介绍")
    private String description;

    @NotNull(message="免配送费最低金额不能为空")
    @ApiModelProperty("免配送费最低金额")
    private BigDecimal deliveryFreeAmount;

    @NotNull(message="免配送费配送距离不能为空")
    @ApiModelProperty("免配送费配送距离 km")
    private Integer deliveryFreeDistance;

    @NotNull(message="起送金额不能为空")
    @ApiModelProperty("起送金额")
    private BigDecimal deliveryBeginAmount;

    @NotNull(message="超出距离不能为空")
    @ApiModelProperty("超出距离")
    private String additionalDistance;

    @NotNull(message="超出距离价格不能为空")
    @ApiModelProperty("超出距离价格")
    private BigDecimal additionalFee;
}