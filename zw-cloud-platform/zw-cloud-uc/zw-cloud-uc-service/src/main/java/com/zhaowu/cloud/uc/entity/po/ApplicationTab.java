package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 应用tab
 * </p>
 *
 * @author xxp
 * @since 2021-04-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ApplicationTab extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 页面编号
     */
    private String applicationHtmlId;

    /**
     * 参与者编号
     */
    private String applicationParty;

    private String path;

    /**
     * 顺序
     */
    private Integer ordNum;

    /**
     * 是否显示
     */
    private String isShow;

    /**
     * tab名称
     */
    private String name;

    /**
     * tab名称
     */
    private String nameChoosed;

    private String icon;

    private String iconChoosed;
}
