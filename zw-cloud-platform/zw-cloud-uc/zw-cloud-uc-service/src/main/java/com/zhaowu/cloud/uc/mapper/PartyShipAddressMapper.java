package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.PartyShipAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参与者收货地址 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface PartyShipAddressMapper extends BaseMapper<PartyShipAddress> {

}
