package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.mapper.ApplicationHtmlMapper;
import com.zhaowu.cloud.uc.service.ApplicationHtmlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 应用页面DIY 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-03-18
 */
@Service
public class ApplicationHtmlServiceImpl extends ServiceImpl<ApplicationHtmlMapper,ApplicationHtml> implements ApplicationHtmlService {

    @Override
    public void add(ApplicationHtml applicationHtml) {

        boolean checkPath = this.checkPath(applicationHtml);
        Assert.isTrue(checkPath, "path已存在");

        boolean checkName = this.checkName(applicationHtml);
        Assert.isTrue(checkName, "页面名称已存在");
        this.save(applicationHtml);
    }

    @Override
    public ApplicationHtml findById(String applicationParty, String id) {
        QueryWrapper<ApplicationHtml> queryWrapper = new QueryWrapper();
        queryWrapper.eq("application_party", applicationParty);
        queryWrapper.eq("id", id);
        return this.getOne(queryWrapper);
    }

    @Override
    public void delete(ApplicationHtml applicationHtml) {
        this.removeById(applicationHtml.getId());
    }

    @Override
    public void modify(ApplicationHtml applicationHtml) {

        boolean checkPath = this.checkPath(applicationHtml);
        Assert.isTrue(checkPath, "path已存在");

        boolean checkName = this.checkName(applicationHtml);
        Assert.isTrue(checkName, "页面名称已存在");

        this.updateById(applicationHtml);
    }

    @Override
    public List<ApplicationHtml> getList(String applicationParty) {

        QueryWrapper<ApplicationHtml> queryWrapper = new QueryWrapper();
        queryWrapper.eq("application_party", applicationParty);
//        queryWrapper.eq(!StringUtils.isEmpty(isTab), "is_tab", isTab);
        return this.list(queryWrapper);
    }

    @Override
    public ApplicationHtml getByPath(String path, String applicationParty) {

        QueryWrapper<ApplicationHtml> queryWrapper = new QueryWrapper();
        queryWrapper.eq("application_party", applicationParty);
        queryWrapper.eq( "path", path);
        return this.getOne(queryWrapper);
    }

    private boolean checkPath(ApplicationHtml applicationHtml) {
        QueryWrapper<ApplicationHtml> wrapper = new QueryWrapper<ApplicationHtml>();
        wrapper.eq("path", applicationHtml.getPath());
        wrapper.eq("application_party", applicationHtml.getApplicationParty());
        if(!StringUtils.isEmpty(applicationHtml.getId())){
            wrapper.ne("id", applicationHtml.getId());
        }
        return this.getOne(wrapper) == null;
    }

    private boolean checkName(ApplicationHtml applicationHtml) {
        QueryWrapper<ApplicationHtml> wrapper = new QueryWrapper<ApplicationHtml>();
        wrapper.eq("name", applicationHtml.getName());
        wrapper.eq("application_party", applicationHtml.getApplicationParty());
        if(!StringUtils.isEmpty(applicationHtml.getId())){
            wrapper.ne("id", applicationHtml.getId());
        }
        return this.getOne(wrapper) == null;
    }
}
