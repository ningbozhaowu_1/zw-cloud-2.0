package com.zhaowu.cloud.uc.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.po.Account;
import com.zhaowu.cloud.uc.entity.vo.UserAssets;
import com.zhaowu.cloud.uc.entity.vo.UserDetail;
import com.zhaowu.cloud.uc.service.AccountService;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 账户 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/account/v1")
@Api(tags = "账户 接口")
public class AccountController extends BaseController {

        @Autowired
        private AccountService accountService;

        @RequestMapping(value = "/getUserAssets", method = {RequestMethod.POST, RequestMethod.GET})
        @ApiOperation("用户资产 后续增加优惠券")
        public Result<UserAssets> getUserAssets() {

                UserAssets userAssets = new UserAssets();
                List<Account> accountList = this.accountService.getAccountList();
                userAssets.setAccountList(accountList);
                return this.success(userAssets);
        }
}

