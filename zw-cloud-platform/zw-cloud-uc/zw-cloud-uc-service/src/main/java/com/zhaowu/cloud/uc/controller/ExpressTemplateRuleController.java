package com.zhaowu.cloud.uc.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

/**
 * <p>
 * 参与者快递运费规则 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@RestController
@RequestMapping("/expressTemplateRule")
@Api(tags = "参与者快递运费规则 接口")
public class ExpressTemplateRuleController extends BaseController {


}

