package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.uc.entity.param.PartyApplyQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyStaffQueryForm extends BaseQueryForm<PartyApplyQueryParam> {

    @NotNull(message="关联关系不能为空")
    @ApiModelProperty(value = "关联关系 STAFF-普通员工 HR-人事", allowableValues = "STAFF,HR")
    private String assoType;
}