package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CacheUpdate;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.entity.po.UserChannel;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface UserChannelService {

    /**
     * 新增用户渠道
     * @param userChannel
     */
    void add(UserChannel userChannel);

    /**
     * 通过openId获取用户渠道
     * @param openId
     * @return
     */
    UserChannel findUserChannelByOpenId(String openId, String applicationParty);

}
