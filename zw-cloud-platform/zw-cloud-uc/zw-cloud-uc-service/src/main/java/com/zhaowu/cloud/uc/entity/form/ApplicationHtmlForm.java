package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.Application;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ApplicationHtmlForm extends BaseForm<ApplicationHtml> {

    @NotNull(message="页面名称不能为空")
    @Size(max= 256, message="页面名称必须在{max}个字符内")
    @ApiModelProperty("页面名称")
    private String name;

    @NotNull(message="页面标题不能为空")
    @Size(max= 256, message="页面标题必须在{max}个字符内")
    @ApiModelProperty("页面标题")
    private String title;

    @NotNull(message="页面路径不能为空")
    @Size(max= 256, message="页面路径必须在{max}个字符内")
    @ApiModelProperty("页面路径")
    private String path;

    @NotNull(message="页面主题色不能为空")
    @Size(max= 8, message="页面主题色必须在{max}个字符内")
    @ApiModelProperty("页面主题色")
    private String color;

    @NotNull(message="背景色不能为空")
    @Size(max= 8, message="背景色必须在{max}个字符内")
    @ApiModelProperty("背景色")
    private String backColor;

    @NotNull(message="头部文字颜色不能为空")
    @Size(max= 8, message="头部文字颜色必须在{max}个字符内")
    @ApiModelProperty("头部文字颜色")
    private String headTextColor;

//    @NotNull(message="是否tab不能为空")
//    @Size(max= 3, message="是否tab必须在{max}个字符内")
//    @ApiModelProperty(value = "是否tab",allowableValues = "Y,N")
//    private String isTab;

    @NotNull(message="页面数据不能为空")
    @ApiModelProperty(value = "页面json数据")
    private String data;
}