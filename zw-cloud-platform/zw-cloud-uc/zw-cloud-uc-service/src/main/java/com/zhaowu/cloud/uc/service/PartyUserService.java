package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.po.PartyUser;

import java.util.List;

/**
 * <p>
 * 参与者用户 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PartyUserService {

    void add(PartyUser partyUser);

    void del(String userId, String userParty, String roleParty, String roleId);

    PartyUser getPartyUser(String userId, String userParty, String roleParty, String roleId);

    List<PartyUser> getListByUserId(String userId, String userParty, String roleParty);
}
