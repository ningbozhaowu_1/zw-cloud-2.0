package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.PartyMediaAttachement;

public interface PartyMediaAttachementMapper extends BaseMapper<PartyMediaAttachement> {
}
