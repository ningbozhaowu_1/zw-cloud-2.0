//package com.zhaowu.cloud.uc.entity.po;
//
//import com.baomidou.mybatisplus.annotation.FieldFill;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.zhaowu.cloud.common.web.entity.po.BasePo;
//import lombok.Data;
//import lombok.experimental.Accessors;
//
///**
// * <p>
// * 域应用
// * </p>
// *
// * @author xxp
// * @since 2020-10-26
// */
//@Data
//@Accessors(chain = true)
//@TableName("application_apply")
//public class ApplicationApply extends BasePo {
//
//    @TableField(value="party_id",  fill = FieldFill.INSERT)
//    private String partyId;
//
//    @TableField(value = "apply_user", fill = FieldFill.INSERT)
//    private Long applyUser;
//
//    @TableField("application_channel_id")
//    private String applicationChannelId;
//
//    @TableField("name")
//    private String name;
//
//    @TableField("code")
//    private String code;
//
//    @TableField("code_type")
//    private String codeType;
//
//    @TableField("legal_persona_wechat")
//    private String legalPersonaWechat;
//
//    @TableField("legal_persona_name")
//    private String legalPersonaName;
//
//    @TableField("component_phone")
//    private String componentPhone;
//
//    /**
//     * 状态
//     */
//    @TableField("status")
//    private String status;
//
//}
