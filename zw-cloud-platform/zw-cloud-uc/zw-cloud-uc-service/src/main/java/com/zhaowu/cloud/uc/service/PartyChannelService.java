package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheUpdate;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.PartyChannel;

import java.util.List;

/**
 * <p>
 * 参与者渠道 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PartyChannelService {

    @Cached(name="applicationChannelCache::", key = "#partyChannelId", expire = 3600)
    @CachePenetrationProtect
    PartyChannel selectById(String partyChannelId);

    PartyChannel add(PartyChannel applicationChannel);

    List<PartyChannel> getPartyChannelList(String partyId, String type);

    PartyChannel selectByAppId(String partyId, String authorizerAppId);

    @CacheUpdate(name="applicationChannelCache::",key = "#applicationChannel.id", value="#result")
    PartyChannel modify(PartyChannel applicationChannel);
}
