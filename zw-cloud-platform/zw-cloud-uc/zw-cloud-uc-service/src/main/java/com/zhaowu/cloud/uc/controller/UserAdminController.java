package com.zhaowu.cloud.uc.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.organization.client.OrganizationClient;
import com.zhaowu.cloud.organization.entity.vo.RoleVO;
import com.zhaowu.cloud.uc.entity.form.BatchUserQueryForm;
import com.zhaowu.cloud.uc.entity.form.UserForm;
import com.zhaowu.cloud.uc.entity.form.UserPageQueryForm;
import com.zhaowu.cloud.uc.entity.form.UserQueryForm;
import com.zhaowu.cloud.uc.entity.param.UserQueryParam;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.entity.vo.OrganizationVO;
import com.zhaowu.cloud.uc.entity.vo.PartyVO;
import com.zhaowu.cloud.uc.entity.vo.UserVO;
import com.zhaowu.cloud.uc.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * <p> 用户 控制器 </p>
 * @author xxp @since 2021-01-20
 */
@RestController
@RequestMapping("/user/admin/v1")
@Api(tags = "用户管理 接口")
public class UserAdminController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private PersonService personService;

    @Autowired
    private PartyUserService partyUserService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private OrganizationClient organizationClient;

    @Autowired
    private PartyAssoService partyAssoService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private OrganizationService organizationService;

    @RequestMapping(value = "/modifyUser", method = {RequestMethod.POST})
    @ApiOperation("用户信息修改  基础信息以及资产")
    @Transactional
    public Result modifyUser(@Valid @RequestBody UserForm userForm) {

        //用户基础信息修改
        User user = userService.findUserById(userForm.getUserId(), UserContextHolder.getInstance().getApplicationParty());

        if(user==null){
            return this.failure("用户不存在");
        }

        if(!StringUtils.isEmpty(userForm.getMobile())){
            user.setMobile(userForm.getMobile());
        }

        if(!StringUtils.isEmpty(userForm.getNickname())){
            user.setNickname(userForm.getNickname());
        }

        if(!StringUtils.isEmpty(userForm.getStatus())){
            user.setStatus(userForm.getStatus());
        }

        userService.modify(user);

        //用户资产修改
        if(userForm.getIntegration()!=null){
            Account account = accountService.getByAcType(CommonConstant.AcType.INTE, UserContextHolder.getInstance().getPartyId());
            account.setBalance(userForm.getIntegration());
            accountService.modify(account);
        }

        if(userForm.getBalance()!=null){
            Account account = accountService.getByAcType(CommonConstant.AcType.BAL, UserContextHolder.getInstance().getPartyId());
            account.setBalance(userForm.getBalance());
            accountService.modify(account);
        }

        if(userForm.getCash()!=null){
            Account account = accountService.getByAcType(CommonConstant.AcType.CASH, UserContextHolder.getInstance().getPartyId());
            account.setBalance(userForm.getCash());
            accountService.modify(account);
        }

        return this.success();
    }

    @RequestMapping(value = "/getRoleListByUserId", method = {RequestMethod.GET})
    @ApiOperation("用户角色列表查询")
    public Result<Collection<String>> getRoleListByUserId(@Valid @RequestParam String userId) {

        User userExsit = userService.findUserById(userId, UserContextHolder.getInstance().getApplicationParty());

        if(userExsit==null){
            return this.failure("用户不存在");
        }

        List<PartyUser> partyUserList = partyUserService.getListByUserId(userExsit.getId(), userExsit.getPartyId(), UserContextHolder.getInstance().getApplicationParty());
        Collection<String> roleIds  = CollectionUtils.collect(partyUserList.iterator(), input -> input.getRoleId());
        return this.success(roleIds);
    }

    @RequestMapping(value = "/addRole", method = {RequestMethod.GET})
    @ApiOperation("添加用户角色")
    public Result addRole(@Valid @RequestParam String userId,
                                 @Valid @RequestParam String roleId) {

        User user = userService.findUserById(userId, UserContextHolder.getInstance().getApplicationParty());

        if(user==null){
            return this.failure("用户不存在");
        }

        RoleVO roleVO = organizationClient.getRoleIdById(roleId).getData();

        if(roleVO==null ||
                (!"system".equals(roleVO.getPartyId()) && !roleVO.getPartyId().equals(UserContextHolder.getInstance().getApplicationParty()))){
            return this.failure("角色不存在");
        }

        if(CommonConstant.RoleCode.SUPER_ADMIN.equals(roleVO.getCode())){
            return this.failure("权限不足");
        }

        PartyUser partyUser = partyUserService.getPartyUser(userId, user.getPartyId(), UserContextHolder.getInstance().getApplicationParty(), roleVO.getId());

        if(partyUser != null){
            return this.failure("用户角色已存在");
        }

        partyUser = new PartyUser();
        partyUser.setRoleId(roleId);
        partyUser.setRoleParty(UserContextHolder.getInstance().getApplicationParty());
        partyUser.setUserId(userId);
        partyUser.setUserParty(user.getPartyId());
        partyUserService.add(partyUser);
        return this.success();
    }

    @RequestMapping(value = "/delRole", method = {RequestMethod.GET})
    @ApiOperation("取消用户角色")
    public Result delRole(@Valid @RequestParam String userId,
                          @Valid @RequestParam String roleId) {

        User user = userService.findUserById(userId, UserContextHolder.getInstance().getApplicationParty());

        if(user==null){
            return this.failure("用户不存在");
        }

        RoleVO roleVO = organizationClient.getRoleIdById(roleId).getData();

        if(roleVO==null ||
                (!"system".equals(roleVO.getPartyId()) && !roleVO.getPartyId().equals(UserContextHolder.getInstance().getApplicationParty()))){
            return this.failure("角色不存在");
        }

        if(CommonConstant.RoleCode.SUPER_ADMIN.equals(roleVO.getCode())){
            return this.failure("权限不足");
        }

        PartyUser partyUser = partyUserService.getPartyUser(userId, user.getPartyId(), UserContextHolder.getInstance().getApplicationParty(), roleVO.getId());

        if(partyUser==null){
            return this.failure("用户角色不存在");
        }

        partyUserService.del(userId, user.getPartyId(), UserContextHolder.getInstance().getApplicationParty(), roleVO.getId());

        return this.success();
    }

    /**
     * 分页查询应用下所有用户
     *
     * @return List<UseVo> 对象
     */
    @PostMapping("/list")
    @ApiOperation("分页查询应用下所有用户")
    public Result<Page<UserVO>> list(@Valid @RequestBody UserPageQueryForm userQueryForm) {
        IPage<User> page = this.userService.pageList(userQueryForm.getPage(), userQueryForm.toParam(UserQueryParam.class));
        List<User> userList = page.getRecords();
        IPage<UserVO> result = new Page<>();
        page.setRecords(null);
        BeanUtils.copyProperties(page, result);
        List<UserVO> userVOList = JSON.parseArray(JSON.toJSONString(userList), UserVO.class);
        result.setRecords(userVOList);
        for(UserVO userVO : result.getRecords()){
            BeanUtils.copyProperties(this.userService.findUserById(userVO.getId(), UserContextHolder.getInstance().getApplicationParty()), userVO);
            if(CommonConstant.UserType.ENT.equals(userVO.getUserType())){
                OrganizationVO organizationVO = new OrganizationVO();
                BeanUtils.copyProperties(this.organizationService.selectById(userVO.getPartyId()) ,organizationVO);
                userVO.setOrganizationVO(organizationVO);
                userVO.setPartyName(this.partyService.selectById(userVO.getPartyId()).getPartyName());
            }else if(CommonConstant.UserType.PER.equals(userVO.getUserType())){
                userVO.setPerson(this.personService.selectByPartyId(userVO.getPartyId()));
            }
        }
        return this.success(result);
    }

    @PostMapping("/findUserBatch")
    @ApiIgnore
    Result<List<UserVO>> findUserBatch(@Valid @RequestBody BatchUserQueryForm batchUserQueryForm){

        return this.success(this.userService.batchQuery(batchUserQueryForm.getUserIds(), batchUserQueryForm.getMobileNos(), UserContextHolder.getInstance().getApplicationParty(), batchUserQueryForm.getPartyChannelId()));
    }

    @RequestMapping(value = "/partyListByAssoType", method = {RequestMethod.GET})
    @ApiOperation("参与者关联列表")
    public Result partyListByAssoType(@Valid @RequestParam String assoType,
                                      @Valid @RequestParam String partyId) {

        List<User> userList = userService.findUserByPartyId(partyId, UserContextHolder.getInstance().getApplicationParty());

        if(!CollectionUtils.isEmpty(userList)){
            return this.failure("用户不存在");
        }

        List<PartyAsso> partyAssoList = this.partyAssoService.getListByAssoType(assoType, partyId);

        if(CollectionUtils.isEmpty(partyAssoList)){
            return this.success();
        }

        List<PartyVO> partyVOList = new LinkedList<>();
        for(PartyAsso partyAsso : partyAssoList){
            Party party =  partyService.selectById(partyAsso.getAssoParty());
            Organization organization = organizationService.selectById(partyAsso.getAssoParty());
            OrganizationVO organizationVO = new OrganizationVO();
            BeanUtils.copyProperties(organization, organizationVO);
            PartyVO partyVO = new PartyVO();
            BeanUtils.copyProperties(party, partyVO);
            partyVO.setOrganizationVO(organizationVO);
            partyVO.setPartyId(party.getId());
            partyVOList.add(partyVO);
        }

        return this.success(partyVOList);
    }

    /**
     * 手机号、用户名查询用户
     */
    @PostMapping("/search")
    @ApiOperation("用户搜索")
    public Result<UserVO> search(@Valid @RequestBody UserQueryForm userQueryForm) {

        if(!StringUtils.isEmpty(userQueryForm.getMobileNo())){
            UserVO userVO = new UserVO();
            User user = this.userService.findUsersByMobileNo(userQueryForm.getMobileNo(), UserContextHolder.getInstance().getApplicationParty());
            if(user == null){
                return this.failure();
            }
            BeanUtils.copyProperties(user, userVO);
            return this.success(userVO);
        }

        if(!StringUtils.isEmpty(userQueryForm.getUsername())){

            UserVO userVO = new UserVO();

            User user = this.userService.findUserByUsername(userQueryForm.getUsername(), UserContextHolder.getInstance().getApplicationParty());

            if(user == null){
                return this.failure();
            }
            BeanUtils.copyProperties(user, userVO);
            return this.success(userVO);
        }

        return this.failure();
    }
}
