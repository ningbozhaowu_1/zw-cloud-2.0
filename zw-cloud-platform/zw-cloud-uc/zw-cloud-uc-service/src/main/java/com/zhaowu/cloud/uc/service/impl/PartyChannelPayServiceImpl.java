package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.uc.entity.po.PartyChannelPay;
import com.zhaowu.cloud.uc.mapper.PartyChannelPayMapper;
import com.zhaowu.cloud.uc.service.PartyChannelPayService;
import org.springframework.stereotype.Service;

@Service
public class PartyChannelPayServiceImpl extends ServiceImpl<PartyChannelPayMapper, PartyChannelPay> implements PartyChannelPayService {


    @Override
    public void add(PartyChannelPay partyChannelPay) {

        QueryWrapper<PartyChannelPay> queryWrapper = new QueryWrapper<PartyChannelPay>();
        queryWrapper.eq("party_channel_id",partyChannelPay.getPartyChannelId());
        queryWrapper.eq("party_pay_id",partyChannelPay.getPartyPayId());
        if(this.getOne(queryWrapper)!=null){
            throw new ServiceException("该渠道已绑定该支付方式");
        }

        this.save(partyChannelPay);
    }

    @Override
    public void delete(String partyChannelId, String partyPayId) {
        QueryWrapper<PartyChannelPay> deteteWrapper = new QueryWrapper<PartyChannelPay>();
        deteteWrapper.eq("party_channel_id",partyChannelId);
        deteteWrapper.eq("party_pay_id",partyPayId);
        this.remove(deteteWrapper);
    }

    @Override
    public void modify(PartyChannelPay partyChannelPay) {

        QueryWrapper<PartyChannelPay> queryWrapper = new QueryWrapper<PartyChannelPay>();
        queryWrapper.eq("party_channel_id",partyChannelPay.getPartyChannelId());
        queryWrapper.eq("party_pay_id",partyChannelPay.getPartyPayId());
        if(this.getOne(queryWrapper)!=null){
            throw new ServiceException("该渠道已绑定该支付方式");
        }

        this.updateById(partyChannelPay);
    }
}
