package com.zhaowu.cloud.uc.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.PayApplymentForm;
import com.zhaowu.cloud.uc.entity.po.PartyMediaAttachement;
import com.zhaowu.cloud.uc.entity.po.PartyPayApplyment;
import com.zhaowu.cloud.uc.entity.vo.PayApplymentVO;
import com.zhaowu.cloud.uc.service.PartyPayApplymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 微信支付进件
 * </p>
 *
 * @author shen
 * @since 2021-10-26
 */
@RestController
@RequestMapping("/party/payApplyment/v1")
@Api(tags = "支付进件")
public class PartyPayApplymentController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private PartyPayApplymentService partyPayApplymentService;
    /**
     * 支付进件草稿
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation("支付进件草稿")
    @Transactional
    public Result saveOrUpdate(@Valid @RequestBody PayApplymentForm payApplymentForm) {
        this.logger.info("支付进件草稿payApplymentForm:{} ", payApplymentForm);

        PartyPayApplyment partyPayApplyment = new PartyPayApplyment();
        BeanUtils.copyProperties(payApplymentForm, partyPayApplyment);
        partyPayApplyment.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
        partyPayApplymentService.saveOrUpdate(partyPayApplyment);
        return this.success();
    }
    /**
     * 支付进件草稿查询
     */
    @GetMapping(value = "/query")
    @ApiOperation("支付进件草稿")
    @Transactional
    public Result<PayApplymentVO> queryByBusinessCode(@Valid @RequestParam String businessCode) {
        this.logger.info("支付进件草稿查询:{} ", businessCode);
        PartyPayApplyment partyPayApplyment=partyPayApplymentService.select(businessCode,UserContextHolder.getInstance().getApplicationParty());
        PayApplymentVO payApplymentVO = new PayApplymentVO();
        BeanUtils.copyProperties(partyPayApplyment, payApplymentVO);
        this.logger.info("支付进件草稿查询结果:{} ", payApplymentVO);
        return this.success(payApplymentVO);
    }
    /**
     * 支付进件草稿查询
     */
    @GetMapping(value = "/list")
    @ApiOperation("支付进件草稿")
    @Transactional
    public Result<List<PayApplymentVO>> list() {
        this.logger.info("支付进件草稿列表查询:{} ", UserContextHolder.getInstance().getApplicationParty());
        List<PayApplymentVO> payApplymentVO=partyPayApplymentService.list(UserContextHolder.getInstance().getApplicationParty());
        this.logger.info("支付进件草稿列表查询结果:{} ", payApplymentVO);
        return this.success(payApplymentVO);
    }

    /**
     * 微信mediaID文件关系保存
     */
    @PostMapping(value = "/media/save")
    @ApiOperation("微信mediaID文件保存")
    @Transactional
    public Result mediaSave(@Valid @RequestParam String mediaId,@RequestParam String fullUrlPath) {
        this.logger.info("微信mediaID文件关系保存:{} ", UserContextHolder.getInstance().getApplicationParty());
        partyPayApplymentService.mediaSave(mediaId,fullUrlPath);
        Map result =new HashMap();
        result.put("mediaId",mediaId);
        result.put("fullUrlPath",fullUrlPath);
        return this.success(result);
    }
    /**
     * 根据微信mediaID获取文件路径
     */
    @GetMapping(value = "/media/id")
    @ApiOperation("根据mediaID获取文件地址")
    @Transactional
    public Result findMediaUrl(@Valid @RequestParam String mediaId) {
        this.logger.info("根据微信mediaID获取文件地址:{}，{} ", mediaId,UserContextHolder.getInstance().getApplicationParty());
        PartyMediaAttachement partyMediaAttachement=  partyPayApplymentService.findMediaUrl(mediaId);
        Map result =new HashMap();
        result.put("mediaId",mediaId);
        result.put("fullUrlPath",partyMediaAttachement.getFullUrlPath());
        return this.success(result);
    }

}
