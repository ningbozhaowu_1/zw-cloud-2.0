package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.uc.entity.po.UserPin;

/**
 * <p>
 * 用户密码 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface UserPinService {

    UserPin getByUserId(String userId);

    void add(UserPin userPin);

    void modifyPassword(UserPin userPin);

    void modify(UserPin userPin);

    void updatePwdErrorCount(BusMessage message);
}
