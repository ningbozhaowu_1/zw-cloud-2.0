package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.param.PartyApplyQueryParam;
import com.zhaowu.cloud.uc.entity.po.PartyApplyInfo;
import com.zhaowu.cloud.uc.mapper.PartyChannelApplyInfoMapper;
import com.zhaowu.cloud.uc.service.PartyApplyInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class PartyApplyInfoServiceImpl extends ServiceImpl<PartyChannelApplyInfoMapper, PartyApplyInfo> implements PartyApplyInfoService {

    @Override
    public void add(PartyApplyInfo partyChannelApplyInfo) {
        this.save(partyChannelApplyInfo);
    }

    @Override
    public PartyApplyInfo selectByCode(String codeType, String code, String applyType) {

        QueryWrapper<PartyApplyInfo> wrapper = new QueryWrapper<PartyApplyInfo>();
        wrapper.eq("code", code);
        wrapper.eq("code_type", codeType);
        wrapper.eq("apply_type", applyType);
        wrapper.eq("status", CommonConstant.ApplyStatus.A);
        return this.getOne(wrapper);
    }

    @Override
    public PartyApplyInfo selectByChannelId(String channelId) {
        return null;
    }

    @Override
    public void modify(PartyApplyInfo partyChannelApplyInfo) {
        this.updateById(partyChannelApplyInfo);
    }

    @Override
    public void delete(String id) {
        this.removeById(id);
    }

    @Override
    public PartyApplyInfo selectById(String id) {
        return this.getById(id);
    }

    @Override
    public IPage<PartyApplyInfo> pageList(Page<PartyApplyInfo> page, PartyApplyQueryParam partyApplyQueryParam) {
        QueryWrapper<PartyApplyInfo> queryWrapper = partyApplyQueryParam.build();
        queryWrapper.eq("apply_type", partyApplyQueryParam.getApplyType());

        if(!StringUtils.isEmpty(partyApplyQueryParam.getPartyId())){
            queryWrapper.eq("user_party", partyApplyQueryParam.getPartyId());
        }

        if(!StringUtils.isEmpty(partyApplyQueryParam.getStatus())){
            queryWrapper.eq("status", partyApplyQueryParam.getStatus());
        }

        queryWrapper.eq("application_party", UserContextHolder.getInstance().getApplicationParty());
        return this.page(page, queryWrapper);
    }
}
