package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.uc.entity.po.PartyChannel;
import com.zhaowu.cloud.uc.mapper.PartyChannelMapper;
import com.zhaowu.cloud.uc.service.PartyChannelService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PartyChannelServiceImpl extends ServiceImpl<PartyChannelMapper, PartyChannel> implements PartyChannelService {

    @Override
    public PartyChannel add(PartyChannel applicationChannel) {

        this.save(applicationChannel);

        return applicationChannel;
    }

    @Override
    public PartyChannel selectById(String partyChannelId) {
        return  this.getById(partyChannelId);
    }

    @Override
    public List<PartyChannel> getPartyChannelList(String partyId, String type) {
        QueryWrapper<PartyChannel> wrapper = new QueryWrapper<PartyChannel>();
        wrapper.eq("party_id", partyId);
        wrapper.eq("type", type);
        return this.list(wrapper);
    }

    @Override
    public PartyChannel selectByAppId(String partyId, String authorizerAppId) {

        QueryWrapper<PartyChannel> wrapper = new QueryWrapper<PartyChannel>();
        wrapper.eq("authorizer_app_id", authorizerAppId);
        wrapper.eq("party_id", partyId);
        return this.getOne(wrapper);
    }

    @Override
    public PartyChannel modify(PartyChannel applicationChannel) {
        this.updateById(applicationChannel);
        return applicationChannel;
    }
}
