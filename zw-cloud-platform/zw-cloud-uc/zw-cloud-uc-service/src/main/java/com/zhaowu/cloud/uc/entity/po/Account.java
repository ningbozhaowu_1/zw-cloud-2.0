package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * <p>
 * 账户表
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Account extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者编号
     */
    @TableField("party_id")
    private String partyId;

    /**
     * 账号类型
     */
    @TableField("ac_type")
    private String acType;

    /**
     * 余额
     */
    private BigDecimal balance;
}
