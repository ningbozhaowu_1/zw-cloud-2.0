package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.PartyAsso;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参与者关联 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
public interface PartyAssoMapper extends BaseMapper<PartyAsso> {

}
