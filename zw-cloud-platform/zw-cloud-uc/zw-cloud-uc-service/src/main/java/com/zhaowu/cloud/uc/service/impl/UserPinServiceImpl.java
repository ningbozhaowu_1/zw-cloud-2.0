package com.zhaowu.cloud.uc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.util.DateUtils;
import com.zhaowu.cloud.uc.entity.po.UserPin;
import com.zhaowu.cloud.uc.password.PasswordStrategy;
import com.zhaowu.cloud.uc.password.PasswordValidateResult;
import com.zhaowu.cloud.uc.service.UserPinService;
import com.zhaowu.cloud.uc.mapper.UserPinMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import javax.validation.Validator;

@Service
public class UserPinServiceImpl extends ServiceImpl<UserPinMapper, UserPin> implements UserPinService {

    @Autowired
    private PasswordStrategy passwordStrategy;

    @Autowired
    private Validator validator;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public UserPin getByUserId(String userId) {
        QueryWrapper<UserPin> wrapper = new QueryWrapper<UserPin>();
        wrapper.eq("user_id", userId);
        return this.getOne(wrapper);
    }

    @Override
    public void add(UserPin userPin) {

        if (StringUtils.isEmpty(userPin.getPassword())) {
            userPin.setPassword(passwordStrategy.getPassword());
        }
        userPin.setPassword(encoder.encode(userPin.getPassword()));
        this.save(userPin);
    }

    @Override
    public void modifyPassword(UserPin userPin) {
//        PasswordValidateResult result = passwordStrategy.validate(userPin.getPassword());
//        if(!result.getPassed()){
//            throw new ServiceException(result.getMessage());
//        }
        userPin.setPassword(encoder.encode(userPin.getPassword()));
        this.updateById(userPin);
    }

    @Override
    public void modify(UserPin userPin) {
        this.updateById(userPin);
    }

    @Override
    public void updatePwdErrorCount(BusMessage message) {

        UserPin userPin = JSON.parseObject(JSON.toJSONString(message.getJsonObject()), UserPin.class);
        userPin = this.getByUserId(userPin.getUserId());

        if(CommonConstant.InfoType.LOGIN_FAIL.equals(message.getInfoType())){

            Integer errorBalance = message.getJsonObject().getInteger("errorCountBalance");
            if(errorBalance<=0){
                userPin.setStatus(CommonConstant.PinStatus.L);
            }

            userPin.setPwdErrCount(userPin.getPwdErrCount()+1);
            userPin.setPwdErrTime(DateUtils.currentDate());
        }else if(CommonConstant.InfoType.LOGIN_SUCCESS.equals(message.getInfoType())){
            userPin = this.getByUserId(userPin.getUserId());
            userPin.setStatus(CommonConstant.PinStatus.A);
            userPin.setPwdErrCount(0);
        }

        this.updateById(userPin);
    }
}
