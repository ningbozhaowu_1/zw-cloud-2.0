package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.form.PartyPickUpPageQueryForm;
import com.zhaowu.cloud.uc.entity.param.PartyPickUpQueryParam;
import com.zhaowu.cloud.uc.entity.po.PartyPickUp;
import com.zhaowu.cloud.uc.mapper.PartyPickUpMapper;
import com.zhaowu.cloud.uc.service.PartyPickUpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 提货点 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Service
public class PartyPickUpServiceImpl extends ServiceImpl<PartyPickUpMapper,PartyPickUp> implements PartyPickUpService {

    @Override
    public void add(PartyPickUp partyPickUp) {

        this.save(partyPickUp);
    }

    @Override
    public IPage<PartyPickUp> pageList(Page<PartyPickUp> page, PartyPickUpQueryParam partyPickUpQueryParam) {
        QueryWrapper<PartyPickUp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("party_id", partyPickUpQueryParam.getPartyId());
        return this.page(page, queryWrapper);
    }

    @Override
    public void delete(String partyId, String id) {
        QueryWrapper<PartyPickUp> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("party_id", partyId);
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public void modify(PartyPickUp partyPickUp) {

        this.updateById(partyPickUp);
    }

    @Override
    public PartyPickUp selectById(String id) {
        return this.getById(id);
    }

    @Override
    public List<PartyPickUp> selectByCity(String cityName, String partyId) {

        QueryWrapper<PartyPickUp> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("party_id", partyId);
        queryWrapper.eq("city", cityName);
        return this.list(queryWrapper);
    }
}
