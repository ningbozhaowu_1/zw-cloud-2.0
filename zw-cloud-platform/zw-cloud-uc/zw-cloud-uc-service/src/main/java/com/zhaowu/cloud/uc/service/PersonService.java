package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.Party;
import com.zhaowu.cloud.uc.entity.po.Person;

/**
 * <p>
 * 个人 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PersonService {

    void add(Person person);

    @CacheInvalidate(name="personCache::",key = "#person.partyId")
    void modify(Person person);

    @Cached(name="personCache::", key="#partyId", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Person selectByPartyId(String partyId);
}
