package com.zhaowu.cloud.uc.controller;


import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.ApplicationHtmlForm;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.zhaowu.cloud.uc.entity.vo.ApplicationHtmlVO;
import com.zhaowu.cloud.uc.service.ApplicationHtmlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 应用页面DIY 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-03-18
 */
@RestController
@RequestMapping("/public/applicationHtml/v1")
@Api(tags = "public应用页面DIY 接口")
public class PublicApplicationHtmlController extends BaseController {

        @Autowired
        private ApplicationHtmlService applicationHtmlService;

        @GetMapping(value = "/getByPath")
        @ApiOperation("根据path获取页面DIY数据")
        public Result<ApplicationHtmlVO> getByPath(@Valid @RequestParam("path") String path,
                                                       @Valid @RequestParam("applicationParty") String applicationParty) {
                ApplicationHtml applicationHtml = applicationHtmlService.getByPath(path, applicationParty);
                if(applicationHtml==null){
                        return this.failure("页面无效");
                }
                ApplicationHtmlVO applicationHtmlVO = new ApplicationHtmlVO();
                BeanUtils.copyProperties(applicationHtml, applicationHtmlVO);
                return this.success(applicationHtmlVO);
        }
}

