package com.zhaowu.cloud.uc.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import com.zhaowu.cloud.framework.util.DateUtils;
import com.zhaowu.cloud.framework.util.ShortNumUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.organization.client.OrganizationClient;
import com.zhaowu.cloud.uc.entity.form.*;
import com.zhaowu.cloud.uc.entity.param.PartyApplyQueryParam;
import com.zhaowu.cloud.uc.entity.param.UserQueryParam;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.entity.vo.*;
import com.zhaowu.cloud.uc.password.PasswordStrategy;
import com.zhaowu.cloud.uc.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 参与者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/party")
@Api(tags = "参与者管理 接口")
public class PartyAdminController extends BaseController {

        @Autowired
        private PartyApplyInfoService partyApplyInfoService;

        @Autowired
        private UserService userService;

        @Autowired
        private PartyService partyService;

        @Autowired
        private PartyUserService partyUserService;

        @Autowired
        private PartyAssoService partyAssoService;

        @Autowired
        private UserPinService userPinService;

        @Autowired
        private OrganizationService organizationService;

        @Autowired
        private AccountService accountService;

        @Autowired
        private UserChannelService userChannelService;

        @Autowired
        private BusSender busSender;

        @Autowired
        private PasswordStrategy passwordStrategy;

        @Autowired
        private OrganizationClient organizationClient;

        @RequestMapping(value = "/admin/v1/partyApplyList", method = {RequestMethod.POST})
        @ApiOperation("企业注册申请列表查询")
        public Result partyApply(@Valid @RequestBody PartyApplyQueryForm partyApplyQueryForm) {

                return this.success(partyApplyInfoService.pageList(partyApplyQueryForm.getPage(), partyApplyQueryForm.toParam(PartyApplyQueryParam.class)));
        }

        @PostMapping("/admin/v1/partyApplyAudit")
        @ApiOperation("企业注册审核")
        @Transactional
        public Result partyApplyAudit(@Valid @RequestBody PartyApplyAuditForm partyApplyAuditForm) {

                PartyApplyInfo partyApplyInfo = partyApplyInfoService.selectById(partyApplyAuditForm.getId());

                if(!CommonConstant.ApplyStatus.A.equals(partyApplyInfo.getStatus())){
                    return this.failure("该笔任务已审核");
                }
                partyApplyInfo.setRemark(partyApplyAuditForm.getRemark());
                partyApplyInfo.setStatus(partyApplyAuditForm.getStatus());

                if(CommonConstant.ApplyStatus.P.equals(partyApplyAuditForm.getStatus())){
                        //生成party数据
                        User user = register(UserContextHolder.getInstance().getApplicationParty(), UserContextHolder.getInstance().getChannelId(),partyApplyInfo);
                        String format = "申请已通过,企业账号为%s 登陆初始密码为%s";
                        partyApplyInfo.setRemark(String.format(format,user.getUsername(),passwordStrategy.getPassword()));

                        //通过通知中心发送通知
                        BusMessage message = new BusMessage();
                        message.setStandalone("Y");
                        message.setInfoType(CommonConstant.InfoType.SMS);
                        JSONObject jsonObject = new JSONObject();
                        message.setJsonObject(jsonObject);
                        jsonObject.put("phoneNumber", partyApplyInfo.getComponentPhone());
                        jsonObject.put("signName", "微世信息");
                        jsonObject.put("templateCode", "856663");
                        jsonObject.put("contents", partyApplyInfo.getName() +"," + user.getUsername() +"," +passwordStrategy.getPassword());
                        busSender.send(CommonConstant.RouteKey.notification,message);
                }

                partyApplyInfoService.modify(partyApplyInfo);
                return this.success();
        }

        User register(String applicationParty, String partyChannelId, PartyApplyInfo partyApplyInfo){

                Party party = new Party();
                party.setPartyType(CommonConstant.PartyType.E);
                party.setPartyName(partyApplyInfo.getName());
                partyService.add(party);

                PartyAsso partyAsso = new PartyAsso();
                partyAsso.setPartyId(party.getId());
                partyAsso.setAssoParty(applicationParty);
                if(CommonConstant.PartyApplyType.ENT.equals(partyApplyInfo.getApplyType())){
                        partyAsso.setAssoType(CommonConstant.AssoType.CON);
                        partyAsso.setAssoCode(CommonConstant.AssoType.CON);
                }else if(CommonConstant.PartyApplyType.SHOP.equals(partyApplyInfo.getApplyType())){
                        partyAsso.setAssoType(CommonConstant.AssoType.SIGN);
                        partyAsso.setAssoCode(CommonConstant.AssoType.SIGN);
                }
                partyAssoService.add(partyAsso);

                if(CommonConstant.PartyApplyType.ENT.equals(partyApplyInfo.getApplyType())){
                        partyAsso = new PartyAsso();//申请人为员工
                        partyAsso.setPartyId(partyApplyInfo.getUserParty());
                        partyAsso.setAssoParty(party.getId());
                        partyAsso.setAssoType(CommonConstant.AssoType.STAFF);
                        partyAsso.setAssoCode(CommonConstant.AssoType.STAFF);
                        partyAssoService.add(partyAsso);

                        partyAsso = new PartyAsso();//申请人为hr
                        partyAsso.setPartyId(partyApplyInfo.getUserParty());
                        partyAsso.setAssoParty(party.getId());
                        partyAsso.setAssoType(CommonConstant.AssoType.HR);
                        partyAsso.setAssoCode(CommonConstant.AssoType.HR);
                        partyAssoService.add(partyAsso);
                }

                User user = new User();
                user.setUsername(ShortNumUtils.getOutTradeNo(10));
//                user.setMobile(partyApplyInfo.getComponentPhone());
//                user.setNickname(partyApplyInfo.getName());
                user.setPartyId(party.getId());
                user.setApplicationParty(applicationParty);

                if(CommonConstant.PartyApplyType.ENT.equals(partyApplyInfo.getApplyType())){
                        user.setUserType(CommonConstant.UserType.ENT);
                }else if (CommonConstant.PartyApplyType.SHOP.equals(partyApplyInfo.getApplyType())){
                        user.setUserType(CommonConstant.UserType.MCH);
                }

                user.setStatus(CommonConstant.Status.A);
                userService.add(user);

                PartyUser partyUser = new PartyUser();
                partyUser.setUserParty(party.getId());
                partyUser.setRoleParty(applicationParty);
                partyUser.setUserId(user.getId());

                if(CommonConstant.PartyApplyType.ENT.equals(partyApplyInfo.getApplyType())){
                        partyUser.setRoleId(organizationClient.getRoleIdByCode(CommonConstant.RoleCode.ENT).getData().getId());
                }else if (CommonConstant.PartyApplyType.SHOP.equals(partyApplyInfo.getApplyType())){
                        partyUser.setRoleId(organizationClient.getRoleIdByCode(CommonConstant.RoleCode.MERCHANTS).getData().getId());
                }

                partyUserService.add(partyUser);

                UserPin userPin = new UserPin();
                userPin.setEffectTime(DateUtils.currentDate());
                userPin.setLoginCount(0);
                userPin.setPartyId(party.getId());
                userPin.setUserId(user.getId());
                userPin.setPwdErrCount(0);
                userPin.setStatus(CommonConstant.Status.A);
                userPinService.add(userPin);

                Organization organization = new Organization();
                organization.setPartyId(party.getId());
                organization.setCode(partyApplyInfo.getCode());
                organization.setCodeType(partyApplyInfo.getCodeType());
                organization.setLegalName(partyApplyInfo.getLegalPersonaName());
                organization.setContactContact(partyApplyInfo.getComponentPhone());
                organizationService.add(organization);

                Account account = new Account();
                account.setPartyId(party.getId());
                account.setAcType(CommonConstant.AcType.BAL);
                account.setBalance(BigDecimal.ZERO);
                this.accountService.add(account);

                account = new Account();
                account.setPartyId(party.getId());
                account.setBalance(BigDecimal.ZERO);
                account.setAcType(CommonConstant.AcType.INTE);
                this.accountService.add(account);

                account = new Account();
                account.setPartyId(party.getId());
                account.setBalance(BigDecimal.ZERO);
                account.setAcType(CommonConstant.AcType.CASH);
                this.accountService.add(account);


                if(partyChannelId != null){
                        UserChannel userChannel = new UserChannel();
                        userChannel.setPartyChannelId(partyChannelId);
                        userChannel.setStatus(CommonConstant.Status.A);
                        userChannel.setUserId(user.getId());
                        userChannel.setApplicationParty(applicationParty);
                        userChannelService.add(userChannel);
                }

                return user;
        }

        @RequestMapping(value = "/ent/v1/partyStaffList", method = {RequestMethod.POST})
        @ApiOperation("企业员工列表查询")
        public Result<List<UserVO>> partyStaffList(@Valid @RequestBody PartyStaffQueryForm partyStaffQueryForm) {

                List<PartyAsso> partyAssoList = this.partyAssoService.getListByAssoParty(partyStaffQueryForm.getAssoType(), UserContextHolder.getInstance().getPartyId());
                List<UserVO> userVOList = new LinkedList<>();
                for(PartyAsso partyAsso : partyAssoList){
                        User user = userService.findUserByPartyId(partyAsso.getPartyId(), UserContextHolder.getInstance().getApplicationParty()).get(0);
                        UserVO userVO = new UserVO();
                        BeanUtils.copyProperties(user, userVO);
                        userVO.setAssoType(partyStaffQueryForm.getAssoType());
                        userVOList.add(userVO);
                }

                return this.success(userVOList);
        }

        @RequestMapping(value = "/ent/v1/addPartyStaff", method = {RequestMethod.POST})
        @ApiOperation("绑定企业员工关系")
        public Result addPartyStaff(@Valid @RequestBody PartyStaffForm partyStaffForm) {

                List<User> users = userService.findUserByPartyId(partyStaffForm.getPartyId(), UserContextHolder.getInstance().getApplicationParty());

                if(CollectionUtils.isEmpty(users)){
                        return this.failure("该用户不存在");
                }

                PartyAsso partyAsso = this.partyAssoService.getByAssoType(partyStaffForm.getAssoType(), partyStaffForm.getPartyId(),UserContextHolder.getInstance().getPartyId());

                if(partyAsso!=null){
                        return this.failure("该用户已经是您的员工");
                }

                partyAsso = new PartyAsso();
                partyAsso.setPartyId(partyStaffForm.getPartyId());
                partyAsso.setAssoParty(UserContextHolder.getInstance().getPartyId());
                partyAsso.setAssoType(partyStaffForm.getAssoType());
                partyAsso.setAssoCode(partyStaffForm.getAssoType());
                partyAssoService.add(partyAsso);

                return this.success();
        }

        @RequestMapping(value = "/ent/v1/delPartyStaff", method = {RequestMethod.POST})
        @ApiOperation("解绑企业员工关系")
        public Result delPartyStaff(@Valid @RequestBody PartyStaffForm partyStaffForm) {

                PartyAsso partyAsso = this.partyAssoService.getByAssoType(partyStaffForm.getAssoType(), partyStaffForm.getPartyId(),UserContextHolder.getInstance().getPartyId());

                if(partyAsso==null){
                        return this.success();
                }

                partyAssoService.delByAssoType(partyStaffForm.getAssoType(), partyStaffForm.getPartyId(),UserContextHolder.getInstance().getPartyId());
                return this.success();
        }

        @RequestMapping(value = "/admin/v1/partyModify", method = {RequestMethod.POST})
        @ApiOperation("参与者信息修改")
        public Result partyModify(@Valid @RequestBody OrganizationForm organizationForm) {

                Organization organization = organizationService.selectById(UserContextHolder.getInstance().getApplicationParty());
                if(organization==null){
                        organization = new Organization();
                        organization.setPartyId(UserContextHolder.getInstance().getApplicationParty());
                }
                BeanUtils.copyProperties(organizationForm, organization);
                organizationService.modify(organization);
                return this.success();
        }

        @RequestMapping(value = "/admin/v1/getEntPartyList", method = {RequestMethod.POST})
        @ApiOperation("获取企业参与者列表")
        public Result <List<PartyDetailVO>> getEntPartyList(@Valid @RequestBody EntPartyQueryForm entPartyQueryForm) {

                UserQueryParam userQueryParam = new UserQueryParam();
                userQueryParam.setUserType(entPartyQueryForm.getUserType());
                userQueryParam.setName(entPartyQueryForm.getName());
                IPage<User> userPage = userService.pageList(entPartyQueryForm.getPage(), userQueryParam);
                if(userPage.getRecords()==null){
                        return this.success();
                }
                List<PartyDetailVO> list = new ArrayList<>();

                for(User user : userPage.getRecords()){
                        Party party = partyService.selectById(user.getPartyId());
                        Organization organization = organizationService.selectById(user.getPartyId());
                        PartyDetailVO partyDetail = new PartyDetailVO();
                        BeanUtils.copyProperties(party, partyDetail);
                        BeanUtils.copyProperties(organization, partyDetail);
                        list.add(partyDetail);
                }
                return this.success(list);
        }

        @RequestMapping(value = "/admin/v1/getShopList", method = {RequestMethod.POST})
        @ApiOperation("分页查询商户列表")
        public Result<Shop> getShopList(@Valid @RequestBody ShopQueryForm shopQueryForm) {

                Page<PartyAsso> partyAssoList = this.partyAssoService.getPageListByAssoParty(shopQueryForm.getPage(), CommonConstant.AssoType.SIGN, UserContextHolder.getInstance().getApplicationParty());

                if(CollectionUtils.isEmpty(partyAssoList.getRecords())){
                        return this.success();
                }

                List<Shop> shopList = new LinkedList<>();
                for(PartyAsso partyAsso : partyAssoList.getRecords()){
                        Organization organization = organizationService.selectById(partyAsso.getPartyId());
                        Shop shop = new Shop();
                        BeanUtils.copyProperties(organization, shop);
                        shopList.add(shop);
                }

                return this.success(shopList);
        }
}

