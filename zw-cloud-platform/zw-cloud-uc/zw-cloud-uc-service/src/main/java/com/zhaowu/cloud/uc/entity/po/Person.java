package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 个人
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@Accessors(chain = true)
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 参与者编号
     */
    @TableField("party_id")
    @TableId
    private String partyId;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 性别
     */
    private String gender;

    /**
     * 证件号码
     */
    @TableField("id_type")
    private String idType;

    /**
     * 证件号
     */
    @TableField("id_no")
    private String idNo;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 国籍
     */
    private String country;

    /**
     * 地区
     */
    private String city;

    /**
     * 地址
     */
    private String address;

    /**
     * 邮编
     */
    private String zipcode;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 名族
     */
    private String nation;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;
}
