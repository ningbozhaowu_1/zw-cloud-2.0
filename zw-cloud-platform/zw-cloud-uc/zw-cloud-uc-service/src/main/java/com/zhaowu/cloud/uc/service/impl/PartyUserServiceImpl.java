package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.uc.entity.po.PartyUser;
import com.zhaowu.cloud.uc.entity.po.UserChannel;
import com.zhaowu.cloud.uc.mapper.PartyUserMapper;
import com.zhaowu.cloud.uc.service.PartyUserService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

@Service
public class PartyUserServiceImpl extends ServiceImpl<PartyUserMapper, PartyUser> implements PartyUserService {

    @Override
    public void add(PartyUser partyUser) {
        this.save(partyUser);
    }

    @Override
    public void del(String userId, String userParty, String roleParty, String roleId) {
        QueryWrapper<PartyUser> wrapper = new QueryWrapper<PartyUser>();
        wrapper.eq("user_party", userParty);
        wrapper.eq("role_party", roleParty);
        wrapper.eq("user_id", userId);
        wrapper.eq("role_id", roleId);
        this.remove(wrapper);
    }

    @Override
    public PartyUser getPartyUser(String userId, String userParty, String roleParty, String roleId) {
        QueryWrapper<PartyUser> wrapper = new QueryWrapper<PartyUser>();
        wrapper.eq("user_party", userParty);
        wrapper.eq("role_party", roleParty);
        wrapper.eq("user_id", userId);
        wrapper.eq("role_id", roleId);
        return this.getOne(wrapper);
    }

    @Override
    public List<PartyUser> getListByUserId(String userId, String userParty, String roleParty) {

        QueryWrapper<PartyUser> wrapper = new QueryWrapper<PartyUser>();
        wrapper.eq("user_party", userParty);
        //默认将1号应用的角色查出来
        wrapper.in("role_party", Arrays.asList(roleParty,1));
        wrapper.eq("user_id", userId);
        List<PartyUser> partyUsers  = this.list(wrapper);
        if(!CollectionUtils.isEmpty(partyUsers)){
            for (PartyUser partyUser:partyUsers){
                //默认将购买者当成当前的管理员角色
                if (!"1".equals(roleParty) && "1".equals(partyUser.getRoleParty())){
                    partyUser.setRoleId("101");
                }
            }
        }
        return this.list(wrapper);
    }
}
