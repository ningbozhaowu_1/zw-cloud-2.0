package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.PartyPay;
import com.zhaowu.cloud.uc.entity.vo.PartyPayVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartyPayMapper extends BaseMapper<PartyPay> {

    List<PartyPayVO> selectByChannelId(@Param("partyChannelId") String partyChannelId, @Param("partyId") String partyId, @Param("merchantType") String merchantType);
}
