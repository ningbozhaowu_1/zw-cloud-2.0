package com.zhaowu.cloud.uc.entity.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import java.util.Date;

@Data
public class OrganizationForm{

    @ApiModelProperty(value = "成立时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date openday;

    @ApiModelProperty(value = "联系人姓名")
    private String contactName;

    @ApiModelProperty(value = "联系人电话")
    private String contactContact;

    @ApiModelProperty(value = "联系人qq")
    private String contactContact1;

    @ApiModelProperty(value = "联系人备用联系方式")
    private String contactContact2;

    @ApiModelProperty(value = "联系人联系地址")
    private String contactAddr;

//    @ApiModelProperty(value = "企业性质")
//    private String characters;
//
//    @ApiModelProperty(value = "企业类型")
//    private String industry;

    @ApiModelProperty(value = "企业logo")
    private String logo;

//    @ApiModelProperty(value = "平台子域名")
//    private String webPage;

    @ApiModelProperty(value = "企业主页")
    private String homePage;

    @ApiModelProperty(value = "企业地址")
    private String orgAddr;

    @ApiModelProperty(value = "企业联系邮箱")
    @Email
    private String mail;

    @ApiModelProperty(value = "联系人微信二维码")
    private String contactWechat;
}
