package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.po.Party;
import com.zhaowu.cloud.uc.entity.po.PartyPay;
import com.zhaowu.cloud.uc.entity.vo.PartyPayVO;
import com.zhaowu.cloud.uc.mapper.PartyPayMapper;
import com.zhaowu.cloud.uc.service.PartyPayService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartyPayServiceImpl extends ServiceImpl<PartyPayMapper, PartyPay> implements PartyPayService {

    @Autowired
    private PartyPayMapper partyPayMapper;

    @Override
    public List<PartyPay> selectByPartyId(String merchantType, String partyId) {

        QueryWrapper<PartyPay> wrapper = new QueryWrapper<PartyPay>();
        wrapper.eq("party_id", partyId);
        if(!StringUtils.isEmpty(merchantType)){
            wrapper.eq("merchant_type", merchantType);
        }
        return this.list(wrapper);
    }

    @Override
    public PartyPay selectByMerchantId(String merchantId, String partyId) {
        QueryWrapper<PartyPay> wrapper = new QueryWrapper<PartyPay>();
        wrapper.eq("merchant_id", merchantId);
        wrapper.eq("party_id", partyId);
        return this.getOne(wrapper);
    }

    @Override
    public List<PartyPayVO> selectByChannelId(String partyChannelId, String partyId, String merchantType) {
        return partyPayMapper.selectByChannelId(partyChannelId, partyId, merchantType);
    }

//    @Override
//    public List<PartyPay> getApplicationPayList(String partyChannelId, String partyId) {
//
//        QueryWrapper<PartyPay> wrapper = new QueryWrapper<PartyPay>();
//        wrapper.eq("party_id", partyId);
//        wrapper.eq("party_channel_id", partyChannelId);
//        return this.list(wrapper);
//    }

    @Override
    public void add(PartyPay partyPay) {

        QueryWrapper<PartyPay> wrapper = new QueryWrapper<PartyPay>();
        wrapper.eq("party_id", UserContextHolder.getInstance().getPartyId());
        wrapper.eq("merchant_type", partyPay.getMerchantType());
        wrapper.eq("merchant_id", partyPay.getMerchantId());
        if(this.count(wrapper)>0){
            throw new ServiceException("商户已存在");
        }
        this.save(partyPay);
    }

    @Override
    public void modify(PartyPay partyPay) {
        this.updateById(partyPay);
    }

    @Override
    public void delete(String id) {

        this.removeById(id);
    }
}
