package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.uc.entity.po.Person;
import com.zhaowu.cloud.uc.mapper.PersonMapper;
import com.zhaowu.cloud.uc.service.PersonService;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl extends ServiceImpl<PersonMapper, Person> implements PersonService {
    @Override
    public void add(Person person) {
        this.save(person);
    }

    @Override
    public void modify(Person person) {
        this.updateById(person);
    }

    @Override
    public Person selectByPartyId(String partyId) {

        QueryWrapper<Person> wrapper = new QueryWrapper<Person>();
        wrapper.eq("party_id", partyId);
        return this.getOne(wrapper);
    }
}
