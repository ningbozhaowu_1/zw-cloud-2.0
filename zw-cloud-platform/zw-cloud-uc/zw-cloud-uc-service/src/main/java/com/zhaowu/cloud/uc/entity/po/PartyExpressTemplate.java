package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 参与者快递模版
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_express_template")
public class PartyExpressTemplate extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者编号
     */
    private String partyId;

    /**
     * 复制标示
     */
    private String isCopyof;

    /**
     * 免费标示
     */
    private String isFree;

    /**
     * 模版名称
     */
    private String name;

    /**
     * 区域规则
     */
    private String regionRules;

    /**
     * 使用次数
     */
    private Integer useCount;

    /**
     * 计费方式
     */
    private String valuationType;
    /**
     * 计费规则
     */
    private String valuationRules;

}
