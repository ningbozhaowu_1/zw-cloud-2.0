package com.zhaowu.cloud.uc.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 密码策略配置项
 * @author xxp
 **/
@ConfigurationProperties(prefix = "zw.password")
public class PasswordStragtegyProperties {

    private Integer minLength;

    private Integer maxLength;

    private Integer strength;

    private String defaultPassword;

    private Integer unlockHour;//单位小时

    private Integer errorCount;

    public Integer getUnlockHour() {
        return unlockHour;
    }

    public void setUnlockHour(Integer unlockHour) {
        this.unlockHour = unlockHour;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }
}
