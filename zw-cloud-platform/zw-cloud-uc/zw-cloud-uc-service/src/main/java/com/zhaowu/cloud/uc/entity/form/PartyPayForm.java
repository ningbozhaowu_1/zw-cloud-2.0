package com.zhaowu.cloud.uc.entity.form;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyPay;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyPayForm extends BaseForm<PartyPay> {

    @NotNull(message="商户编号不能为空")
    @ApiModelProperty("商户编号")
    private String merchantId;

//    @NotNull(message="参与者渠道编号不能为空")
//    @ApiModelProperty("参与者渠道编号")
//    private String partyChannelId;

    @NotNull(message="商户类型不能为空")
    @ApiModelProperty("商户类型 wechatBusiness:微信直连商户 wechatService:微信特约商户 alipayBusiness:支付宝直连商户 alipayService:支付宝特约商户")
    private String merchantType;

    @NotNull(message="商户密钥不能为空")
    @ApiModelProperty("商户密钥")
    private String merchantKey;

    @ApiModelProperty("商户证书路径")
    private String merchantCertKey;

}