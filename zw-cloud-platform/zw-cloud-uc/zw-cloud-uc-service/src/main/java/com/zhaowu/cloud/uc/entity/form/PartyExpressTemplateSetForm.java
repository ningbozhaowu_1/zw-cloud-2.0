package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.uc.entity.vo.ValuationRuleVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 参与者快递模版设置
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@ApiModel
public class PartyExpressTemplateSetForm {
    /**模板名称*/
    @NotNull(message = "模板名称不能为空")
    @ApiModelProperty("模板名称")
    private String name;
    /**计费方式 1按件数 2按重量*/
    @NotNull(message = "计费方式不能为空")
    @ApiModelProperty("计费方式 1按件数 2按重量")
    private String valuationType;
    /**????*/
    private String payType;
    /**是否免费用*/
    @ApiModelProperty("是否免费用")
    private String isFreeDelivery;
    /**可配送区域规则*/
    @NotNull(message = "可配送区域规则不能为空")
    @ApiModelProperty("可配送区域规则")
    private List<ValuationRuleVo> valuationRules;
}
