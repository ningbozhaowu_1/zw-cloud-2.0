package com.zhaowu.cloud.uc.controller;

import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.ModifyPartyAttrForm;
import com.zhaowu.cloud.uc.entity.form.PartyAttrForm;
import com.zhaowu.cloud.uc.entity.po.PartyAttr;
import com.zhaowu.cloud.uc.entity.vo.ApplicationTabVO;
import com.zhaowu.cloud.uc.entity.vo.PartyAttrVO;
import com.zhaowu.cloud.uc.service.PartyAttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 参与者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/public/party/v1")
@Api(tags = "应用端参与者属性 接口")
public class PublicPartyAttrController extends BaseController {

        @Autowired
        private PartyAttrService partyAttrService;

        @RequestMapping(value = "/getList", method = {RequestMethod.GET})
        @ApiOperation("参与者属性列表查询")
        public Result getList(@Valid @RequestParam("partyId") String partyId,
                              @Valid @RequestParam("attrType") String attrType) {

                List<PartyAttr> partyAttrList = partyAttrService.getListByAttrType(partyId, attrType);
                List<PartyAttrVO> partyAttrVOList = JSON.parseArray(JSON.toJSONString(partyAttrList), PartyAttrVO.class);
                return this.success(partyAttrVOList);
        }

}

