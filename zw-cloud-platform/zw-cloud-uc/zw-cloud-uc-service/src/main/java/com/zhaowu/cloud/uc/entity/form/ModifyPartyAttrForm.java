package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyAttr;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ModifyPartyAttrForm extends BaseForm<PartyAttr> {

    @NotNull(message="编号不能为空")
    @ApiModelProperty(value = "编号")
    private String id;

    @NotNull(message="属性类型不能为空")
    @Size(max= 16, message="属性类型必须在{max}个字符内")
    @ApiModelProperty(value = "属性类型 功能开关等",allowableValues = "IND,PAY,THEME,EXPRESS")
    private String attrType;

    @NotNull(message="属性code不能为空")
    @Size(max= 32, message="属性code必须在{max}个字符内")
    @ApiModelProperty(value = "属性code")
    private String attrCode;

    @NotNull(message="属性值不能为空")
    @Size(max= 32, message="属性值必须在{max}个字符内")
    @ApiModelProperty(value = "属性值")
    private String attrValue;

    @NotNull(message="属性描述不能为空")
    @Size(max= 512, message="属性描述必须在{max}个字符内")
    @ApiModelProperty(value = "属性描述")
    private String attrDesc;
}
