package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 参与者渠道
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_channel_pay")
public class PartyChannelPay extends BasePo {

    private static final long serialVersionUID = 1L;

    @TableField(value = "party_channel_id")
    private String partyChannelId;

    @TableField("application_party")
    private String applicationParty;

    @TableField("party_pay_id")
    private String partyPayId;

    @TableField("app_id")
    private String appId;
}
