package com.zhaowu.cloud.uc.password.validator;


import com.zhaowu.cloud.uc.password.PasswordValidateResult;

/**
 * 密码校验器
 * @author xxp
 **/
public interface PasswordValidator {

    /**
     * 校验密码
     * @param password
     * @return
     */
    PasswordValidateResult validate(String password);
}
