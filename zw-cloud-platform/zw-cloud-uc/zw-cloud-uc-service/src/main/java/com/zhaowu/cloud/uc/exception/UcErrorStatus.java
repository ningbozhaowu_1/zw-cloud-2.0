package com.zhaowu.cloud.uc.exception;

import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import lombok.Getter;

@Getter
public enum UcErrorStatus implements ErrorStatus {
    /**至少需要开启快递发货、同城配送、上门自提中的一种配送方式，店铺才能正常经营。*/
    INVALID_PARTY_EXPRESS_ERROR("050001", "至少需要开启快递发货、同城配送、上门自提中的一种配送方式，店铺才能正常经营。"),
    /**无操作权限*/
    INVALID_PARTY_EXPRESS_PARTY_ERROR("050002", "无操作权限"),
    /**该快递模板被占用，不可删除*/
    INVALID_PARTY_EXPRESS_TEMPLATE_USED_ERROR("050003", "该快递模板被占用，不可删除"),
    /**快递费计算数据有误*/
    INVALID_PARTY_EXPRESS_CALCULATE_ERROR("050004", "快递费计算数据有误"),
    INVALID_TOKEN("050005", "无效token"),
    TOKEN_INVALID("050081", "验证码错误或已过期"),;

    /**
     * 错误类型码
     */
    private String code;
    /**
     * 错误类型描述信息
     */
    private String message;

    UcErrorStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
