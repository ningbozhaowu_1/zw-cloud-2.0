package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.Application;

import java.util.List;

/**
 * <p>
 * 域应用 服务类
 * </p>
 *
 * @author xxp
 * @since 2020-10-26
 */
public interface ApplicationService {

//    @Cached(name="applicationCache::", expire = 3600)
//    @CachePenetrationProtect
    Application selectById(String id);

//    @Cached(name="applicationCache::", expire = 3600)
//    @CachePenetrationProtect
    Application selectByPartyId(String partyId);

//    @CacheInvalidate(name="applicationCache::", key="#application.partyId")
    void add(Application application);

//    @CacheInvalidate(name="applicationCache::", key="#application.partyId")
    void modify(Application application);
}
