package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.Party;
import com.zhaowu.cloud.uc.entity.po.Person;

/**
 * <p>
 * 个人 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PersonMapper extends BaseMapper<Person> {

}
