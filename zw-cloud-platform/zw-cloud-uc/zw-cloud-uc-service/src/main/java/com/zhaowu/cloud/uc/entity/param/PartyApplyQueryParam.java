package com.zhaowu.cloud.uc.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.uc.entity.po.PartyApplyInfo;
import com.zhaowu.cloud.uc.entity.po.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartyApplyQueryParam extends BaseParam<PartyApplyInfo> {

    private String applyType;

    private String status;
}
