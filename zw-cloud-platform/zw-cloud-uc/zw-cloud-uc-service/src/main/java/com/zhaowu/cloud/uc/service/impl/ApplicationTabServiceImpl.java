package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.po.Application;
import com.zhaowu.cloud.uc.entity.po.ApplicationTab;
import com.zhaowu.cloud.uc.mapper.ApplicationTabMapper;
import com.zhaowu.cloud.uc.service.ApplicationTabService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 应用tab 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-04-01
 */
@Service
public class ApplicationTabServiceImpl extends ServiceImpl<ApplicationTabMapper,ApplicationTab> implements ApplicationTabService {

    @Override
    public List<ApplicationTab> getList() {
        QueryWrapper<ApplicationTab> wrapper = new QueryWrapper<ApplicationTab>();
        wrapper.eq("application_party", UserContextHolder.getInstance().getApplicationParty());
        return this.list(wrapper);
    }

    @Override
    public List<ApplicationTab> getList(String applicationParty) {
        QueryWrapper<ApplicationTab> wrapper = new QueryWrapper<ApplicationTab>();
        wrapper.eq("application_party", applicationParty);
        wrapper.eq("is_show", "Y");
        wrapper.orderByDesc("ord_num");
        wrapper.last("limit 0 , 6");
        return this.list(wrapper);
    }


    @Override
    public void add(ApplicationTab applicationTab) {
        this.save(applicationTab);
    }

    @Override
    public void delete(String applicationParty, String id) {

        QueryWrapper<ApplicationTab> deleteWrapper = new QueryWrapper<ApplicationTab>();
        deleteWrapper.eq("id",id);
        deleteWrapper.eq("application_party", applicationParty);
        this.remove(deleteWrapper);
    }

    @Override
    public void modify(ApplicationTab applicationTab) {
        this.updateById(applicationTab);
    }
}
