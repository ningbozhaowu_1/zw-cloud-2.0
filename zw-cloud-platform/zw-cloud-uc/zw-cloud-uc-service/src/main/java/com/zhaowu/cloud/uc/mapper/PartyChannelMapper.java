package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.PartyChannel;

public interface PartyChannelMapper extends BaseMapper<PartyChannel> {
}
