package com.zhaowu.cloud.uc.controller;

import com.alibaba.fastjson.JSONObject;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.DateUtils;
import com.zhaowu.cloud.framework.util.IdUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.organization.client.OrganizationClient;
import com.zhaowu.cloud.uc.entity.form.*;
import com.zhaowu.cloud.uc.entity.param.PartyApplyQueryParam;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.entity.vo.*;
import com.zhaowu.cloud.uc.exception.UcErrorStatus;
import com.zhaowu.cloud.uc.service.*;
import com.zhaowu.cloud.vcc.client.VerificationCodeClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * <p> 用户 控制器 </p>
 * @author xxp @since 2021-01-20
 */
@RestController
@RequestMapping("/user/v1")
@Api(tags = "用户 接口")
@Slf4j
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private PartyUserService partyUserService;

    @Autowired
    private PartyAssoService partyAssoService;

    @Autowired
    private UserPinService userPinService;

    @Autowired
    private PersonService personService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserChannelService userChannelService;

    @Autowired
    private PartyApplyInfoService partyApplyInfoService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private OrganizationClient organizationClient;

    @Autowired
    private VerificationCodeClient verificationCodeClient;

    @ApiIgnore
    @RequestMapping(value = "/findUserById", method = {RequestMethod.POST, RequestMethod.GET})
    @Transactional
    public Result<UserAuthentication> findUserById(@Valid @RequestBody UserQueryForm userQueryForm) {

        String applicationParty = userQueryForm.getApplicationParty();

        if(StringUtils.isEmpty(userQueryForm.getUserId()) || applicationParty==null){
            throw new ServiceException("参数错误");
        }
        User userExsit = this.userService.findUserById(userQueryForm.getUserId(), applicationParty);
        if(userExsit!=null){
            return this.success(getUserAuthentication(userExsit, applicationParty, true));
        }else{
            throw new ServiceException("用户不存在");
        }
    }

    @ApiIgnore
    @RequestMapping(value = "/findUser4Login", method = {RequestMethod.POST, RequestMethod.GET})
    @Transactional
    public Result<UserAuthentication> findUser4Login(@Valid @RequestBody UserQueryForm userQueryForm) {
        log.info("UserQueryForm Message:<{}>", userQueryForm);
        switch (userQueryForm.getScene()) {

            case "1"://用户名查询
                String username = userQueryForm.getUsername();
                String applicationParty = userQueryForm.getApplicationParty();

                if(StringUtils.isEmpty(username) || applicationParty==null){
                    throw new ServiceException("参数错误");
                }
                User userExsit = this.userService.findUserByUsername(username, applicationParty);
                if(userExsit!=null){
                    return this.success(getUserAuthentication(userExsit, applicationParty, true));
                }else{
                    userExsit = this.userService.findUsersByMobileNo(username, applicationParty);
                    if(userExsit==null){
                        throw new ServiceException("用户未注册");
                    } else{
                        return this.success(getUserAuthentication(userExsit, applicationParty, true));
                    }
                }
            case "2"://手机号查询
                String mobileNo = userQueryForm.getMobileNo();
                applicationParty = userQueryForm.getApplicationParty();
                if(StringUtils.isEmpty(mobileNo) || applicationParty==null){
                    throw new ServiceException("参数错误");
                }
                userExsit = this.userService.findUsersByMobileNo(mobileNo, applicationParty);
                if(userExsit!=null){
                    return this.success(getUserAuthentication(userExsit, applicationParty, false));
                }else{
                    return this.success(register(applicationParty, userQueryForm.getPartyChannelId(), mobileNo, userQueryForm.getOpenId()));
                }
            case "3"://openId查询
                String openId = userQueryForm.getOpenId();
                applicationParty = userQueryForm.getApplicationParty();
                if(StringUtils.isEmpty(openId) || applicationParty==null){
                    throw new ServiceException("参数错误");
                }
                UserChannel userChannel = this.userChannelService.findUserChannelByOpenId(openId, applicationParty);
                mobileNo = userQueryForm.getMobileNo();
                if(userChannel!=null){
                    userExsit = this.userService.findUserById(userChannel.getUserId(), applicationParty);
                    if(StringUtils.isNotEmpty(mobileNo)
                        && !mobileNo.equals(userExsit.getMobile())){
                        this.userService.modify(userExsit);
                    }
                    return this.success(getUserAuthentication(userExsit, applicationParty,false));
                }else{
                    if(StringUtils.isNotEmpty(mobileNo)){
                        userExsit = this.userService.findUsersByMobileNo(mobileNo, applicationParty);
                        if(userExsit!=null){
                            return this.success(bindChannel(userExsit, applicationParty, userQueryForm.getPartyChannelId(), userQueryForm.getOpenId()));
                        }else{
                            return this.success(register(applicationParty, userQueryForm.getPartyChannelId(), mobileNo, userQueryForm.getOpenId()));
                        }
                    }
                }
                break;
        }
        throw new ServiceException("用户未注册");
    }

    UserAuthentication getUserAuthentication(User userExsit, String applicationParty, Boolean pin){

        UserAuthentication userAuthentication = new UserAuthentication();
        BeanUtils.copyProperties(userExsit, userAuthentication);
        if(pin){
            UserPin userPin = userPinService.getByUserId(userExsit.getId());
            if(userPin==null || userPin.getPassword()==null){
                throw new ServiceException("用户未设置密码");
            }
            if(CommonConstant.PinStatus.L.equals(userPin.getStatus())){
                if(DateUtils.currentDate().getTime()-userPin.getPwdErrTime().getTime()<2*60*60*1000L){//密码锁定2小时解锁
                    throw new ServiceException("用户密码锁定,请2小时后重试");
                }else{
                    userPin.setPwdErrCount(0);
                    userPin.setStatus(CommonConstant.PinStatus.A);
                    userPinService.modify(userPin);
                }
            }
            userAuthentication.setPassword(userPin.getPassword());
            userAuthentication.setPwdErrorCount(userPin.getPwdErrCount());
        }

        setRoleCodes(userExsit, userAuthentication ,applicationParty);
        return userAuthentication;
    }

    void setRoleCodes(User userExsit, UserAuthentication userAuthentication, String applicationParty){
        log.info("setRoleCodes begin userExsit:{} ,userAuthentication:{}, applicationParty:{}", userExsit,userAuthentication,applicationParty);
        List<PartyUser> partyUserList = partyUserService.getListByUserId(userExsit.getId(), userExsit.getPartyId(), applicationParty);
        log.info("partyUserList :{}",partyUserList);
        Collection<String> roleIds  = CollectionUtils.collect(partyUserList.iterator(), input -> input.getRoleId());
        if(roleIds.contains(organizationClient.getRoleIdByCode(CommonConstant.RoleCode.MERCHANTS).getData().getId())){
            userAuthentication.setMerchantPartyId(userExsit.getPartyId());
        }
        log.info("roleIds :{}",roleIds);
        userAuthentication.setRoleIds(roleIds);
    }

    UserAuthentication bindChannel(User userExsit, String applicationParty, String partyChannelId, String openId){
        UserChannel userChannel = new UserChannel();
        userChannel.setPartyChannelId(partyChannelId);
        userChannel.setOpenId(openId);
        userChannel.setStatus(CommonConstant.Status.A);
        userChannel.setUserId(userExsit.getId());
        userChannel.setApplicationParty(applicationParty);
        userChannelService.add(userChannel);

        UserAuthentication userAuthentication = new UserAuthentication();
        BeanUtils.copyProperties(userExsit, userAuthentication);
        userAuthentication.setStatus(userExsit.getStatus());
        userAuthentication.setApplicationParty(applicationParty);
        userAuthentication.setOpenId(openId);

        setRoleCodes(userExsit, userAuthentication ,applicationParty);
        return userAuthentication;
    }

    UserAuthentication register(String applicationParty, String partyChannelId, String mobile, String openId){

        Party party = new Party();
        party.setPartyType(CommonConstant.PartyType.P);
        partyService.add(party);

        PartyAsso partyAsso = new PartyAsso();
        partyAsso.setPartyId(party.getId());
        partyAsso.setAssoParty(applicationParty);
        partyAsso.setAssoType(CommonConstant.AssoType.CON);
        partyAsso.setAssoCode(CommonConstant.AssoType.CON);
        partyAssoService.add(partyAsso);

        User user = new User();
        user.setUsername("用户" + IdUtils.generate());
        user.setMobile(mobile);
        user.setPartyId(party.getId());
        user.setApplicationParty(applicationParty);
        user.setUserType(CommonConstant.UserType.PER);
        user.setStatus(CommonConstant.Status.A);
        userService.add(user);

        PartyUser partyUser = new PartyUser();
        partyUser.setUserParty(party.getId());
        partyUser.setRoleParty(applicationParty);
        partyUser.setUserId(user.getId());
        partyUser.setRoleId(organizationClient.getRoleIdByCode(CommonConstant.RoleCode.CUSTOMER).getData().getId());
        partyUserService.add(partyUser);

        UserPin userPin = new UserPin();
        userPin.setEffectTime(DateUtils.currentDate());
        userPin.setLoginCount(0);
        userPin.setPartyId(party.getId());
        userPin.setUserId(user.getId());
        userPin.setPwdErrCount(0);
        userPin.setStatus(CommonConstant.Status.A);
        userPinService.add(userPin);

        Person person = new Person();
        person.setPartyId(party.getId());
        person.setMobile(mobile);
        personService.add(person);

        Account account = new Account();
        account.setPartyId(party.getId());
        account.setAcType(CommonConstant.AcType.BAL);
        account.setBalance(BigDecimal.ZERO);
        this.accountService.add(account);

        account = new Account();
        account.setPartyId(party.getId());
        account.setBalance(BigDecimal.ZERO);
        account.setAcType(CommonConstant.AcType.INTE);
        this.accountService.add(account);

        account = new Account();
        account.setPartyId(party.getId());
        account.setBalance(BigDecimal.ZERO);
        account.setAcType(CommonConstant.AcType.CASH);
        this.accountService.add(account);


        if(partyChannelId != null){
            UserChannel userChannel = new UserChannel();
            userChannel.setPartyChannelId(partyChannelId);
            userChannel.setOpenId(openId);
            userChannel.setStatus(CommonConstant.Status.A);
            userChannel.setUserId(user.getId());
            userChannel.setApplicationParty(applicationParty);
            userChannelService.add(userChannel);
        }

        UserAuthentication userAuthentication = new UserAuthentication();
        BeanUtils.copyProperties(user, userAuthentication);
        userAuthentication.setStatus(user.getStatus());
        userAuthentication.setApplicationParty(applicationParty);
        userAuthentication.setOpenId(openId);
        setRoleCodes(user, userAuthentication ,applicationParty);
        return userAuthentication;
    }

    @RequestMapping(value = "/getUserDetail", method = {RequestMethod.POST, RequestMethod.GET})
    @ApiOperation("用户详情")
    public Result<UserDetail> getUserDetail() {

        User userExsit = this.userService.findUserById(UserContextHolder.getInstance().getUserId(), UserContextHolder.getInstance().getApplicationParty());
        UserDetail userDetail = new UserDetail();
        BeanUtils.copyProperties(userExsit, userDetail);
        Person person = this.personService.selectByPartyId(userExsit.getPartyId());
        userDetail.setPerson(person);

        List<Account> accountList = this.accountService.getAccountList();
        userDetail.setAccountList(accountList);

        return this.success(userDetail);
    }

    @RequestMapping(value = "/partyApply", method = {RequestMethod.POST})
    @ApiOperation("企业注册申请")
    public Result partyApply(@Valid @RequestBody PartyApplyForm partyApplyForm) {

        Result<Boolean> sms = verificationCodeClient.validate(CommonConstant.ParamsType.ENT_REG_SMS,partyApplyForm.getToken(),partyApplyForm.getMobileToken(),partyApplyForm.getComponentPhone());

        if(!sms.getData()){
            throw  new ServiceException(UcErrorStatus.TOKEN_INVALID.getCode(), UcErrorStatus.TOKEN_INVALID.getMessage());
        }

        List<PartyAsso> partyAssoList = partyAssoService.getListByAssoType(CommonConstant.AssoType.HR, UserContextHolder.getInstance().getPartyId());
        if(!CollectionUtils.isEmpty(partyAssoList)){
            Party partyEnt = partyService.selectById(partyAssoList.get(0).getAssoParty());
            return this.failure("您已经是 "+partyEnt.getPartyName()+" 的hr,请联系管理员解绑后申请");
        }

        PartyApplyInfo partyApplyInfoExsit = partyApplyInfoService.selectByCode(partyApplyForm.getCodeType(), partyApplyForm.getCode(), partyApplyForm.getApplyType());

        if(partyApplyInfoExsit!=null){
            return this.failure("已存在一笔相同主体的待审核任务");
        }

        PartyApplyInfo partyChannelApplyInfo = new PartyApplyInfo();
        BeanUtils.copyProperties(partyApplyForm, partyChannelApplyInfo);
        partyChannelApplyInfo.setUserParty(UserContextHolder.getInstance().getPartyId());
        partyChannelApplyInfo.setStatus(CommonConstant.ApplyStatus.A);
        partyApplyInfoService.add(partyChannelApplyInfo);
        return this.success();
    }

    @RequestMapping(value = "/partyApplyList", method = {RequestMethod.POST})
    @ApiOperation("企业注册申请列表查询")
    public Result partyApply(@Valid @RequestBody PartyApplyQueryForm partyApplyQueryForm) {

        PartyApplyQueryParam partyApplyQueryParam = partyApplyQueryForm.toParam(PartyApplyQueryParam.class);
        partyApplyQueryParam.setPartyId(UserContextHolder.getInstance().getPartyId());
        return this.success(partyApplyInfoService.pageList(partyApplyQueryForm.getPage(), partyApplyQueryParam));
    }

    @RequestMapping(value = "/partyApplyCancel", method = {RequestMethod.GET})
    @ApiOperation("企业注册申请取消")
    public Result partyApplyCancel(@Valid @RequestParam String id) {

        PartyApplyInfo partyApplyInfoExsit = partyApplyInfoService.selectById(id);

        if(!UserContextHolder.getInstance().getPartyId().equals(partyApplyInfoExsit.getUserParty())){
            return this.failure("任务不存在");
        }

        if(!CommonConstant.ApplyStatus.A.equals(partyApplyInfoExsit.getStatus())){
            return this.failure("任务已审核, 请查看审核结果");
        }

        this.partyApplyInfoService.delete(id);
        return this.success();
    }

    @RequestMapping(value = "/partyListByAssoType", method = {RequestMethod.GET})
    @ApiOperation("参与者关联列表")
    public Result partyListByAssoType(@Valid @RequestParam String assoType) {

        List<PartyAsso> partyAssoList = this.partyAssoService.getListByAssoType(assoType, UserContextHolder.getInstance().getPartyId());

        List<PartyVO> partyVOList = new LinkedList<>();
        for(PartyAsso partyAsso : partyAssoList){
            Party party =  partyService.selectById(partyAsso.getAssoParty());
            Organization organization = organizationService.selectById(partyAsso.getAssoParty());
            OrganizationVO organizationVO = new OrganizationVO();
            BeanUtils.copyProperties(organization, organizationVO);
            PartyVO partyVO = new PartyVO();
            BeanUtils.copyProperties(party, partyVO);
            partyVO.setOrganizationVO(organizationVO);
            partyVO.setPartyId(party.getId());
            partyVOList.add(partyVO);
        }

        return this.success(partyVOList);
    }

    @RequestMapping(value = "/modify", method = {RequestMethod.POST})
    @ApiOperation("用户信息修改")
    @Transactional
    public Result modify(@Valid @RequestBody UserModifyForm userModifyForm) {

        User user = userService.findUserById(UserContextHolder.getInstance().getUserId(), UserContextHolder.getInstance().getApplicationParty());

        if(user==null){
            return this.failure("用户不存在");
        }

        if(!StringUtils.isEmpty(userModifyForm.getPassword())){

            if(StringUtils.isEmpty(userModifyForm.getToken())
                    || StringUtils.isEmpty(userModifyForm.getMobileToken()) ){
                throw  new ServiceException("短信验证失败");
            }

            Result<Boolean> sms = verificationCodeClient.validate(CommonConstant.ParamsType.PWD_MOD_SMS,userModifyForm.getToken(),userModifyForm.getMobileToken(),user.getMobile());

            if(!sms.getData()){
                throw  new ServiceException(UcErrorStatus.TOKEN_INVALID.getCode(), UcErrorStatus.TOKEN_INVALID.getMessage());
            }

            UserPin userPin = userPinService.getByUserId(user.getId());
            userPin.setPassword(userModifyForm.getPassword());
            userPinService.modifyPassword(userPin);
        }

        if(!StringUtils.isEmpty(userModifyForm.getNickname())){
            user.setNickname(userModifyForm.getNickname());
        }

        if(!StringUtils.isEmpty(userModifyForm.getUsername())){
            user.setUsername(userModifyForm.getUsername());
        }

        if(!StringUtils.isEmpty(userModifyForm.getAvatar())){
            user.setAvatar(userModifyForm.getAvatar());
        }

        if(!StringUtils.isEmpty(userModifyForm.getEmail())){
            Person person = personService.selectByPartyId(user.getPartyId());
            person.setEmail(userModifyForm.getEmail());
            personService.modify(person);
        }

        userService.modify(user);
        return this.success();
    }

}
