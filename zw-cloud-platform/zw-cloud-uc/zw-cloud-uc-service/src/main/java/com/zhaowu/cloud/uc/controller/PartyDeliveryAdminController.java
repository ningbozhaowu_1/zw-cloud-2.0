package com.zhaowu.cloud.uc.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.PartyDeliveryForm;
import com.zhaowu.cloud.uc.entity.form.PartyPickUpForm;
import com.zhaowu.cloud.uc.entity.po.PartyDelivery;
import com.zhaowu.cloud.uc.entity.vo.PartyDeliveryVO;
import com.zhaowu.cloud.uc.service.PartyDeliveryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 配送 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@RestController
@RequestMapping("/partyDelivery/admin/v1")
@Api(tags = "配送 接口")
public class PartyDeliveryAdminController extends BaseController {

    @Autowired
    private PartyDeliveryService partyDeliveryService;

    @RequestMapping(value = "/setDelivery", method = {RequestMethod.POST})
    @ApiOperation("同城配送设置")
    public Result setDelivery(@Valid @RequestBody PartyDeliveryForm partyDeliveryForm) {

        PartyDelivery partyDelivery = partyDeliveryForm.toPo(PartyDelivery.class);
        partyDelivery.setPartyId(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
        partyDeliveryService.add(partyDelivery);
        return this.success();
    }

    @RequestMapping(value = "/getDelivery", method = {RequestMethod.GET})
    @ApiOperation("获取配送设置")
    public Result<PartyDeliveryVO> getDelivery() {

        PartyDelivery partyDelivery = partyDeliveryService.get(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
        PartyDeliveryVO partyDeliveryVO = new PartyDeliveryVO();
        BeanUtils.copyProperties(partyDelivery,partyDeliveryVO);
        return this.success(partyDeliveryVO);
    }
}

