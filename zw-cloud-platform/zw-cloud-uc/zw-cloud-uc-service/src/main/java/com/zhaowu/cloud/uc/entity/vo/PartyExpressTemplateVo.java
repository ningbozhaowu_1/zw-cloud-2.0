package com.zhaowu.cloud.uc.entity.vo;

import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class PartyExpressTemplateVo extends BasePo {
    /**
     * 参与者编号
     */
    private String partyId;

    /**
     * 复制标示
     */
    private String isCopyof;

    /**
     * 免费标示
     */
    private String isFree;

    /**
     * 模版名称
     */
    private String name;

    /**
     * 区域规则
     */
    private Map<String, Map<String, Integer>> regionRules;

    /**
     * 使用次数
     */
    private Integer useCount;

    /**
     * 计费方式
     */
    private String valuationType;
    /**
     * 计费规则
     */
    private List<ValuationRuleVo> valuationRules;

}
