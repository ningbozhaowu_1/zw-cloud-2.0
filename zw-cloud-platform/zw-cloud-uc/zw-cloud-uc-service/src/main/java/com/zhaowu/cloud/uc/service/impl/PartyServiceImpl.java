package com.zhaowu.cloud.uc.service.impl;

import com.zhaowu.cloud.uc.entity.po.Party;
import com.zhaowu.cloud.uc.mapper.PartyMapper;
import com.zhaowu.cloud.uc.service.PartyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参与者 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@Service
public class PartyServiceImpl extends ServiceImpl<PartyMapper, Party> implements PartyService {

    @Override
    public void add(Party party) {
         this.save(party);
    }

    @Override
    public Party selectById(String id) {
        return this.getById(id);
    }
}
