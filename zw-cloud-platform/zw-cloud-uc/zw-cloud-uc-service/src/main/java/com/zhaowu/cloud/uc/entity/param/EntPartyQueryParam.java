package com.zhaowu.cloud.uc.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import lombok.Data;

@Data
public class EntPartyQueryParam extends BaseParam {

    private String partyId;

}
