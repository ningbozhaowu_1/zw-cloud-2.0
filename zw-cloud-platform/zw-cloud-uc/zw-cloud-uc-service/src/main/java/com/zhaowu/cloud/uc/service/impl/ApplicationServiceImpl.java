package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.uc.entity.po.Application;
import com.zhaowu.cloud.uc.mapper.ApplicationMapper;
import com.zhaowu.cloud.uc.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.validation.Validator;

/**
 * <p>
 * 域应用 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2020-10-26
 */
@Service
public class ApplicationServiceImpl extends ServiceImpl<ApplicationMapper, Application> implements ApplicationService {

    @Autowired
    public Validator validator;

    @Override
    public Application selectById(String id) {

        return this.getById(id);
    }

    @Override
    public Application selectByPartyId(String partyId) {

        QueryWrapper<Application> wrapper = new QueryWrapper<Application>();
        wrapper.eq("party_id", partyId);
        return this.getOne(wrapper);
    }

    @Override
    public void add(Application application) {
        this.save(application);
    }

    @Override
    public void modify(Application application) {
        this.updateById(application);
    }
}
