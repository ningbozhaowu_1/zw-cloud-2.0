package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 参与者用户
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@Accessors(chain = true)
public class PartyUser implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色编号
     */
    @TableField("role_id")
    private String roleId;

    /**
     * 角色所属参与者
     */
    @TableField("role_party")
    private String roleParty;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 用户参与者
     */
    @TableField("user_party")
    private String userParty;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

}
