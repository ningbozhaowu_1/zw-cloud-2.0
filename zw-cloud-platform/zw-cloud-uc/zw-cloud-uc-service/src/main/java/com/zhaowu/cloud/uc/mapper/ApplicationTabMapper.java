package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.ApplicationTab;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用tab Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-04-01
 */
public interface ApplicationTabMapper extends BaseMapper<ApplicationTab> {

}
