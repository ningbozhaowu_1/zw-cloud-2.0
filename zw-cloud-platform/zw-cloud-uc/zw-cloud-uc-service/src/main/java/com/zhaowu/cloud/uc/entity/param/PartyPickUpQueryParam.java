package com.zhaowu.cloud.uc.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.uc.entity.po.PartyPickUp;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PartyPickUpQueryParam extends BaseParam<PartyPickUp> {
}
