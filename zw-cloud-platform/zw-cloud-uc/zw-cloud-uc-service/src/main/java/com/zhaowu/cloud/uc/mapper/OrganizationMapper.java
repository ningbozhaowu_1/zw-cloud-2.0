package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.Organization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 组织者 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
public interface OrganizationMapper extends BaseMapper<Organization> {

}
