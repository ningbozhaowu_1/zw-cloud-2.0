package com.zhaowu.cloud.uc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.param.PartyApplyQueryParam;
import com.zhaowu.cloud.uc.entity.po.PartyApplyInfo;

public interface PartyApplyInfoService {

    void add(PartyApplyInfo partyChannelApplyInfo);

    PartyApplyInfo selectByCode(String codeType, String code, String applyType);

    PartyApplyInfo selectByChannelId(String channelId);

    void modify(PartyApplyInfo partyChannelApplyInfo);

    void delete(String id);

    PartyApplyInfo selectById(String id);

    /**
     * 分页查询申请列表
     * @return
     */
    IPage<PartyApplyInfo> pageList(Page<PartyApplyInfo> page, PartyApplyQueryParam partyApplyQueryParam);
}
