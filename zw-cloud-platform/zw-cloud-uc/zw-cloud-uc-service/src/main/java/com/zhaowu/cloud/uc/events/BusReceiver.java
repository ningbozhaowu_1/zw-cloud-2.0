package com.zhaowu.cloud.uc.events;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.cache.entity.CacheMessageWithName;
import com.zhaowu.cloud.framework.cache.events.BusCacheReceiver;
import com.zhaowu.cloud.framework.util.DateUtils;
import com.zhaowu.cloud.framework.util.IdUtils;
import com.zhaowu.cloud.framework.util.ShortNumUtils;
import com.zhaowu.cloud.organization.client.OrganizationClient;
import com.zhaowu.cloud.uc.config.BusConfig;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

@Component
@Slf4j
public class BusReceiver {

    @Autowired
    private UserPinService userPinService;

    @Autowired
    private UserService userService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private PartyUserService partyUserService;

    @Autowired
    private PersonService personService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PartyAssoService partyAssoService;

    @Autowired
    private OrganizationClient organizationClient;

    @Autowired
    private BusCacheReceiver busCacheReceiver;

    @Autowired
    private BusConfig busConfig;

    @Autowired
    private OrganizationService organizationService;

    public void handleMessage(BusMessage message) {
        log.info("Received Message:<{}>", message);

        if(CommonConstant.InfoType.LOGIN_FAIL.equals(message.getInfoType())){
            userPinService.updatePwdErrorCount(message);
        }else if(CommonConstant.InfoType.LOGIN_SUCCESS.equals(message.getInfoType())){
            userPinService.updatePwdErrorCount(message);
        }else if(CommonConstant.InfoType.ORDER.equals(message.getInfoType())){
            if((Boolean) (message.getJsonObject().get("success"))){
                JSONObject attach = message.getJsonObject().getJSONObject("attach");
                register(attach.getString("partyId"), attach.getString("mobile"), attach.getString("appName"),
                        new Date((Long) attach.get("expireTime")), (Collection<String>) attach.get("goodsIds"), attach.getString("goodsName"));
            }
        }else if(CommonConstant.InfoType.CACHE_UPDATE.equals(message.getInfoType())){
            CacheMessageWithName cacheMessageWithName = JSON.parseObject(JSON.toJSONString(message.getJsonObject()),CacheMessageWithName.class);
            //添加到线程池进行处理
            busCacheReceiver.handleMessage(cacheMessageWithName);
        }
    }

    @Transactional
    void register(String partyId, String mobile, String appName, Date expireTime, Collection<String> goodsIds, String goodsName){

        //生成应用
        Party applicationParty = new Party();
        applicationParty.setPartyType(CommonConstant.PartyType.A);
        applicationParty.setPartyName(appName==null?goodsName:appName);
        partyService.add(applicationParty);

        Application application = new Application();
        application.setPartyId(applicationParty.getId());
        application.setEffectiveTime(DateUtils.currentDate());
        application.setExpireTime(expireTime);
        application.setName(appName==null?goodsName:appName);
        application.setStatus(CommonConstant.Status.A);
        application.setVersionName(goodsName);
        applicationService.add(application);

        Organization organization = new Organization();
        organization.setPartyId(applicationParty.getId());
        organization.setWebPage(ShortNumUtils.getOutTradeNo(10)+".zhaowu.cc");
        organizationService.add(organization);

        //生成管理员
        Party party = new Party();
        party.setPartyType(CommonConstant.PartyType.P);
        partyService.add(party);

        PartyAsso partyAsso = new PartyAsso();
        partyAsso.setPartyId(party.getId());
        partyAsso.setAssoParty(application.getPartyId());
        partyAsso.setAssoType(CommonConstant.AssoType.CON);
        partyAsso.setAssoCode(CommonConstant.AssoType.CON);
        partyAssoService.add(partyAsso);

        User userXX = userService.findUserByPartyId(partyId, CommonConstant.DefaultParty.PartyId).get(0);
        User user = new User();
        if(mobile==null){
            mobile = userXX.getMobile();
        }
        user.setUsername(userXX.getUsername());
        user.setMobile(mobile);
        user.setPartyId(party.getId());
        user.setApplicationParty(applicationParty.getId());
        user.setUserType(CommonConstant.UserType.PER);
        user.setStatus(CommonConstant.Status.A);
        userService.add(user);

        partyAsso = new PartyAsso();
        partyAsso.setPartyId(partyId);
        partyAsso.setAssoParty(application.getPartyId());
        partyAsso.setAssoType(CommonConstant.AssoType.OWNER);
        partyAsso.setAssoCode(CommonConstant.AssoType.OWNER);
        partyAssoService.add(partyAsso);

        PartyUser partyUser = new PartyUser();
        partyUser.setUserParty(party.getId());
        partyUser.setRoleParty(application.getPartyId());
        partyUser.setUserId(user.getId());
        partyUser.setRoleId(organizationClient.getRoleIdByCode(CommonConstant.RoleCode.CUSTOMER).getData().getId());
        partyUserService.add(partyUser);

        partyUser = new PartyUser();
        partyUser.setUserParty(party.getId());
        partyUser.setRoleParty(application.getPartyId());
        partyUser.setUserId(user.getId());
        partyUser.setRoleId(organizationClient.getRoleIdByCode(CommonConstant.RoleCode.ADMIN).getData().getId());
        partyUserService.add(partyUser);

        UserPin userPin = new UserPin();
        userPin.setEffectTime(DateUtils.currentDate());
        userPin.setLoginCount(0);
        userPin.setPartyId(party.getId());
        userPin.setUserId(user.getId());
        userPin.setPwdErrCount(0);
        userPin.setStatus(CommonConstant.Status.A);
        userPinService.add(userPin);

        Person person = new Person();
        person.setPartyId(party.getId());
        person.setMobile(mobile);
        personService.add(person);

        Account account = new Account();
        account.setPartyId(party.getId());
        account.setAcType(CommonConstant.AcType.BAL);
        account.setBalance(BigDecimal.ZERO);
        this.accountService.add(account);

        account = new Account();
        account.setPartyId(party.getId());
        account.setBalance(BigDecimal.ZERO);
        account.setAcType(CommonConstant.AcType.INTE);
        this.accountService.add(account);

        account = new Account();
        account.setPartyId(party.getId());
        account.setBalance(BigDecimal.ZERO);
        account.setAcType(CommonConstant.AcType.CASH);
        this.accountService.add(account);

        organizationClient.authorizerAppMenu(goodsIds, applicationParty.getId());

    }
}