package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 应用支付
 * </p>
 *
 * @author xxp
 * @since 2020-10-26
 */
@Data
@Accessors(chain = true)
@TableName("party_pay")
public class PartyPay extends BasePo {

    @TableField("merchant_id")
    private String merchantId;

    @TableField("merchant_alias")
    private String merchantAlias;

    @TableField(value = "party_id", fill = FieldFill.INSERT)
    private String partyId;

    @TableField("merchant_key")
    private String merchantKey;

    @TableField("merchant_cert_key")
    private String merchantCertKey;

    @TableField("merchant_type")
    private String merchantType;


}
