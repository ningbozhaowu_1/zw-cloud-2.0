package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.param.UserQueryParam;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.entity.vo.UserVO;
import com.zhaowu.cloud.uc.mapper.UsersMapper;
import com.zhaowu.cloud.uc.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UsersMapper, User> implements UserService {

    @Autowired
    private Validator validator;

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public User findUserByUsername(String username, String applicationParty) {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("username", username);
        wrapper.eq("application_party", applicationParty);
        return this.getOne(wrapper);
    }


    @Override
    public User findUserByMobileNo(String mobileNo, String applicationParty) {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("mobile", mobileNo);
        wrapper.eq("application_party", applicationParty);
        return this.getOne(wrapper);
    }
    @Override
    public User findUsersByMobileNo(String mobileNo, String applicationParty) {

        List<User> users = this.usersMapper.findUsersByMobileNo (mobileNo,applicationParty);
        //一个应用ID存在两个用户，一个是owner、一个是con
        if(!CollectionUtils.isEmpty(users)){
            //存在只有查到一个信息的情况下，有两种情况，一个是当前查询到的就是此应用管理员，另一个是默认的购买者，此两者都认为具备此应用管理能力
            if (users.size()==1){
                users.get(0).setApplicationParty(applicationParty);
                return users.get(0);
            }
            for (User user :users){
                if(applicationParty.equals(user.getApplicationParty())){
                    return user;
                }
            }
        }
        return null;
    }

//    @Override
//    public User findUserById(String userId, String applicationParty) {
//        QueryWrapper<User> wrapper = new QueryWrapper<User>();
//        wrapper.eq("id", userId);
//        wrapper.eq("application_party", applicationParty);
//        return this.getOne(wrapper);
//    }

    @Override
    public User findUserById(String userId, String applicationParty) {
        List<User> users = this.usersMapper.findUserById (userId,applicationParty);
        if(!CollectionUtils.isEmpty(users)){
            return users.get(0);
        }
        return null;
    }

    @Override
    public List<User> findUserByPartyId(String partyId, String applicationParty) {

        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("party_id", partyId);
        wrapper.eq("application_party", applicationParty);
        return this.list(wrapper);
    }

    @Override
    public void add(User user) {

        this.checkUser(user);
        this.save(user);

    }

    @Override
    public void modify(User user) {
        this.checkUser(user);
        this.updateById(user);
    }

    @Override
    public IPage<User> pageList(Page<User> page, UserQueryParam userQueryParam) {
        return this.usersMapper.pageUserList(page, UserContextHolder.getInstance().getApplicationParty(), userQueryParam.getUserType(), userQueryParam.getName());
    }

    @Override
    public List<UserVO> batchQuery(Collection<String> userIds, Collection<String> mobileNos, String applicationParty, String partyChannelId) {
        return this.usersMapper.batchQuery(userIds, mobileNos, applicationParty, partyChannelId);
    }

    /**
     * 检查用户数据重复项
     *
     * @param user
     * @return
     */
    public void checkUser(User user) {

        if(!StringUtils.isEmpty(user.getMobile())){
            boolean checkPhoneNumber = this.checkMobile(user);
            Assert.isTrue(checkPhoneNumber, "手机号已存在");
        }

        boolean checkUsername = this.checkUsername(user);
        Assert.isTrue(checkUsername, "用户名已存在");
        //验证用户数据
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);
        if(constraintViolations.size() > 0){
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    private boolean checkMobile(User user) {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("mobile", user.getMobile());
        wrapper.eq("application_party", user.getApplicationParty());
        if(!StringUtils.isEmpty(user.getId())){
            wrapper.ne("id", user.getId());
        }
        return this.getOne(wrapper) == null;
    }

    private boolean checkUsername(User user) {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("username", user.getMobile());
        wrapper.eq("application_party", user.getApplicationParty());
        if(!StringUtils.isEmpty(user.getId())){
            wrapper.ne("id", user.getId());
        }
        return this.getOne(wrapper) == null;
    }
}
