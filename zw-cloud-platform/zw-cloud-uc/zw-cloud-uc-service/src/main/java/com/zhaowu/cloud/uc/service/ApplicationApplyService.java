//package com.zhaowu.cloud.uc.service;
//
//import com.zhaowu.cloud.uc.entity.po.ApplicationApply;
//
///**
// * <p>
// * 应用申请记录 服务类
// * </p>
// *
// * @author xxp
// * @since 2020-10-26
// */
//public interface ApplicationApplyService {
//
//    void add(ApplicationApply applicationApply);
//
//    ApplicationApply selectByCode(String codeType, String code);
//
//    ApplicationApply selectByChannelId(Long channelId);
//
//    void modify(ApplicationApply applicationApply);
//}
