package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 参与者渠道
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_pay_applyment")
public class PartyPayApplyment extends BasePo {
    private static final long serialVersionUID = 1L;
    /**
     * 当前应用ID
     */
    @TableField("application_party")
    private String applicationParty;
    /**
     * 业务申请编号
     */
    @TableField("business_code")
    private String businessCode;

    /**
     * 商户名称
     */
    @TableField("merchant_name")
    private String merchantName;

    /**
     * 微信支付分配的申请单号
     */
    @TableField("applyment_id")
    private String applymentId;
    /**
     * 申请状态
     */
    @TableField("status")
    private Integer status;

    /**
     * 超级管理员信息
     */
    @TableField("contact_info")
    private String contactInfo;

    /**
     * 主体资料
     */
    @TableField("subject_info")
    private String subjectInfo;

    /**
     * 经营资料
     */
    @TableField("business_info")
    private String businessInfo;

    /**
     * 结算规则
     */
    @TableField("settlement_info")
    private String settlementInfo;

    /**
     * 结算银行账户
     */
    @TableField("bank_account_info")
    private String bankAccountInfo;

    /**
     * 补充材料
     */
    @TableField("addition_info")
    private String additionInfo;

}
