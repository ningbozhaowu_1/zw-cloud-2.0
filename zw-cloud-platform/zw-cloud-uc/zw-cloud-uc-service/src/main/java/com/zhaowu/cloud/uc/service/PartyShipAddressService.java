package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.po.PartyShipAddress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 参与者收货地址 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
public interface PartyShipAddressService{

    PartyShipAddress selectById(String id);

    List<PartyShipAddress> selectList();

    void add(PartyShipAddress partyShipAddress);

    void delete(String id);

    void modify(PartyShipAddress partyShipAddress);
}
