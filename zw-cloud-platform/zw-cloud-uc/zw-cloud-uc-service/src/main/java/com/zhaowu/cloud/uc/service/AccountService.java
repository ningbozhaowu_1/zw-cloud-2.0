package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.po.Account;

import java.util.List;

/**
 * <p>
 * 账户 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface AccountService{

    void add(Account account);

    void modify(Account account);

    Account getByAcType(String acType, String partyId);

    List<Account> getAccountList();
}
