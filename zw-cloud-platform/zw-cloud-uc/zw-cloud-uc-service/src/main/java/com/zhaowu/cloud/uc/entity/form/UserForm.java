package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.Application;
import com.zhaowu.cloud.uc.entity.po.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class UserForm extends BaseForm<User> {

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("用户头像")
    private String avatar;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("用户状态")
    private String status;

    @ApiModelProperty("积分")
    private BigDecimal integration;

    @ApiModelProperty("余额")
    private BigDecimal balance;

    @ApiModelProperty("代金卡余额")
    private BigDecimal cash;

}
