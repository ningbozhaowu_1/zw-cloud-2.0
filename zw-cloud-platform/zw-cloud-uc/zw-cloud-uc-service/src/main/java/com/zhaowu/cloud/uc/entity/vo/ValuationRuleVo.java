package com.zhaowu.cloud.uc.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 可配送区域规则
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@ApiModel
@ToString
public class ValuationRuleVo {
    //首重(Kg)	运费(元)	续重(Kg)	续费(元)
    //首件(个)	运费(元)	续件(个)	续费(元)
    //"firstAmount":1000,"firstFee":600,"additionalAmount":7000,"additionalFee":800
    /**首重(Kg) */
    @NotNull(message = "首重不能为空")
    @ApiModelProperty("首重 1000=1kg =1个")
    private String firstAmount;
    /**运费(元)*/
    @NotNull(message = "运费不能为空")
    @ApiModelProperty("运费 100=1元")
    private String firstFee;
    /**续重(Kg)*/
    @NotNull(message = "续重不能为空")
    @ApiModelProperty("续重 1000=1kg =1个")
    private String additionalAmount;
    /**续费(元)*/
    @NotNull(message = "续费不能为空")
    @ApiModelProperty("续费 100=1元")
    private String additionalFee;
    /**区域*/
    @NotNull(message = "区域不能为空")
    @ApiModelProperty("区域")
    private List<String> regions;

}
