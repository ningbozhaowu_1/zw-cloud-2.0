package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.uc.entity.param.UserQueryParam;
import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel
@Data
public class UserPageQueryForm extends BaseQueryForm<UserQueryParam> {
}
