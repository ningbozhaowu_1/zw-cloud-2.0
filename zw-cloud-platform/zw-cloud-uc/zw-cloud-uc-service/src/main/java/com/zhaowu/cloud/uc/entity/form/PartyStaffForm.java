package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyStaffForm {

    @NotNull(message="关联类型不能为空")
    @ApiModelProperty("关联类型")
    private String assoType;

    @NotNull(message="员工参与者编号不能为空")
    @ApiModelProperty("员工参与者编号")
    private String partyId;

}
