package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyApplyInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyApplyAuditForm extends BaseForm<PartyApplyInfo> {

    @ApiModelProperty(value = "备注")
    private String remark;

    @NotNull(message="审核结果不能为空")
    @ApiModelProperty(value = "审核结果", notes = "P:同意 R:拒绝")
    private String status;

    @NotNull(message="申请编号不能为空")
    @ApiModelProperty(value = "申请编号")
    private String id;
}