package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.PartyPickUp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 提货点 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyPickUpMapper extends BaseMapper<PartyPickUp> {

}
