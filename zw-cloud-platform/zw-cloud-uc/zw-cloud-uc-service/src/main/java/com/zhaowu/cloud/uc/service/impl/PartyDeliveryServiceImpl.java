package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.uc.entity.po.PartyDelivery;
import com.zhaowu.cloud.uc.mapper.PartyDeliveryMapper;
import com.zhaowu.cloud.uc.service.PartyDeliveryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配送 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Service
public class PartyDeliveryServiceImpl extends ServiceImpl<PartyDeliveryMapper,PartyDelivery> implements PartyDeliveryService {

    @Override
    public void add(PartyDelivery partyDelivery) {

        this.saveOrUpdate(partyDelivery);
    }

    @Override
    public PartyDelivery get(String partyId) {
        QueryWrapper<PartyDelivery> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id", partyId);
        return this.getOne(queryWrapper);
    }
}
