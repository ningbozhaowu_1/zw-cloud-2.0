package com.zhaowu.cloud.uc.controller;


import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.ApplicationHtmlForm;
import com.zhaowu.cloud.uc.entity.form.ModifyApplicationHtmlForm;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.zhaowu.cloud.uc.entity.vo.ApplicationHtmlVO;
import com.zhaowu.cloud.uc.entity.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.uc.service.ApplicationHtmlService;

import java.util.ArrayList;
import java.util.List;


import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 应用页面DIY 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-03-18
 */
@RestController
@RequestMapping("/applicationHtml/admin/v1")
@Api(tags = "应用页面DIY 管理接口")
public class ApplicationHtmlAdminController extends BaseController {

        @Autowired
        private ApplicationHtmlService applicationHtmlService;

        /**
         * 获取应用页面DIY列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取所有页面DIY列表")
        public Result<List<ApplicationHtmlVO>> list() {

            List<ApplicationHtml> applicationHtml = applicationHtmlService.getList(UserContextHolder.getInstance().getApplicationParty());
            List<ApplicationHtmlVO> applicationHtmlVOList = JSON.parseArray(JSON.toJSONString(applicationHtml), ApplicationHtmlVO.class);
            return this.success(applicationHtmlVOList);
        }

        /**
         * 新增应用页面DIY
         */
        @PostMapping(value = "/add")
        @ApiOperation("新增应用页面DIY")
        public Result add(@Valid @RequestBody ApplicationHtmlForm applicationHtmlForm) {

            ApplicationHtml applicationHtml = applicationHtmlForm.toPo(ApplicationHtml.class);
            applicationHtml.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            applicationHtmlService.add(applicationHtml);
            return this.success();
        }

        /**
         * 删除应用页面DIY
         */
        @DeleteMapping(value = "/delete/{id}")
        @ApiOperation("删除应用页面DIY")
        public Result delete(@PathVariable("id") String id) {

            ApplicationHtml applicationHtml = applicationHtmlService.findById(UserContextHolder.getInstance().getApplicationParty(), id);
            if(applicationHtml==null){
                return this.failure("页面不存在");
            }

            applicationHtmlService.delete(applicationHtml);
            return this.success();
        }

        /**
         * 修改应用页面DIY
         */
        @PostMapping(value = "/update")
        @ApiOperation("修改应用页面DIY")
        public Result update(@Valid @RequestBody ModifyApplicationHtmlForm modifyApplicationHtmlForm) {

            ApplicationHtml applicationHtml = modifyApplicationHtmlForm.toPo(ApplicationHtml.class);
            applicationHtml.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            applicationHtmlService.modify(applicationHtml);
            return this.success();
        }
}

