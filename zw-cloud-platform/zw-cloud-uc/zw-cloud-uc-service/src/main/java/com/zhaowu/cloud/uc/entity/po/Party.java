package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 参与者
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Party extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 参与者姓名
     */
    @TableField("party_name")
    private String partyName;

    /**
     * 参与者类型
     */
    @TableField("party_type")
    private String partyType;


}
