package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.po.PartyAsso;
import com.zhaowu.cloud.uc.mapper.PartyAssoMapper;
import com.zhaowu.cloud.uc.service.PartyAssoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参与者关联 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Service
public class PartyAssoServiceImpl extends ServiceImpl<PartyAssoMapper,PartyAsso> implements PartyAssoService {

    @Override
    public void add(PartyAsso partyAsso) {
        this.save(partyAsso);
    }

    @Override
    public List<PartyAsso> getListByAssoType(String assoType, String partyId) {

        QueryWrapper<PartyAsso> wrapper = new QueryWrapper<PartyAsso>();
        wrapper.eq("asso_type", assoType);
        wrapper.eq("party_id", partyId);
        return this.list(wrapper);
    }

    @Override
    public List<PartyAsso> getListByAssoParty(String assoType, String assoParty) {
        QueryWrapper<PartyAsso> wrapper = new QueryWrapper<PartyAsso>();
        wrapper.eq("asso_type", assoType);
        wrapper.eq("asso_party", assoParty);
        return this.list(wrapper);
    }

    @Override
    public Page<PartyAsso> getPageListByAssoParty(Page<PartyAsso> page, String assoType, String assoParty) {

        QueryWrapper<PartyAsso> wrapper = new QueryWrapper<PartyAsso>();
        wrapper.eq("asso_type", assoType);
        wrapper.eq("asso_party", assoParty);
        return this.page(page, wrapper);
    }

    @Override
    public PartyAsso getByAssoType(String assoType, String partyId, String assoParty) {
        QueryWrapper<PartyAsso> wrapper = new QueryWrapper<PartyAsso>();
        wrapper.eq("asso_type", assoType);
        wrapper.eq("party_id", partyId);
        wrapper.eq("asso_party", assoParty);
        return this.getOne(wrapper);
    }

    @Override
    public void delByAssoType(String assoType, String partyId, String assoParty) {
        QueryWrapper<PartyAsso> wrapper = new QueryWrapper<PartyAsso>();
        wrapper.eq("asso_type", assoType);
        wrapper.eq("party_id", partyId);
        wrapper.eq("asso_party", assoParty);
        this.remove(wrapper);
    }
}
