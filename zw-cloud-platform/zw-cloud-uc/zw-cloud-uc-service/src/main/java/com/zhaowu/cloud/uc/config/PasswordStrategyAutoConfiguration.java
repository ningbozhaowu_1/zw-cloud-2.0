package com.zhaowu.cloud.uc.config;

import com.zhaowu.cloud.uc.password.DefaultPasswordStrategy;
import com.zhaowu.cloud.uc.password.PasswordStrategy;
import com.zhaowu.cloud.uc.password.validator.LengthValidator;
import com.zhaowu.cloud.uc.password.validator.StrengthValidator;
import com.zhaowu.cloud.uc.properties.PasswordStragtegyProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xxp
 **/
@Configuration
@ConditionalOnMissingBean(PasswordStrategy.class)
@EnableConfigurationProperties(PasswordStragtegyProperties.class)
@Slf4j
public class PasswordStrategyAutoConfiguration {

    private static final String DEFAULT_PASSWORD = "111111";

    @Autowired
    PasswordStragtegyProperties platformPasswordStragtegyProperties;

    @Bean
    public PasswordStrategy passwordStrategy(){
        DefaultPasswordStrategy passwordStrategy = new DefaultPasswordStrategy();
        //长度验证器
        if(platformPasswordStragtegyProperties.getMinLength() != null || platformPasswordStragtegyProperties.getMaxLength() != null){
            LengthValidator lengthValidator = new LengthValidator();
            lengthValidator.setMinLength(platformPasswordStragtegyProperties.getMinLength());
            lengthValidator.setMinLength(platformPasswordStragtegyProperties.getMinLength());
            passwordStrategy.addValidator(lengthValidator);
            log.info("Password Strategy Register Validator :" + lengthValidator);
        }
        //强度验证器
        if(platformPasswordStragtegyProperties.getStrength() != null){
            StrengthValidator strengthValidator = new StrengthValidator();
            strengthValidator.setStrength(platformPasswordStragtegyProperties.getStrength());
            passwordStrategy.addValidator(strengthValidator);
            log.info("Password Strategy Register Validator :" + strengthValidator);
        }

        //默认密码配置
        String defaultPassword = platformPasswordStragtegyProperties.getDefaultPassword();
        defaultPassword = StringUtils.isNotEmpty(defaultPassword) ? defaultPassword : DEFAULT_PASSWORD;
        passwordStrategy.setDefaultPassword(defaultPassword);
        log.info("Password Strategy configured :" + passwordStrategy);

        return passwordStrategy;
    }
}