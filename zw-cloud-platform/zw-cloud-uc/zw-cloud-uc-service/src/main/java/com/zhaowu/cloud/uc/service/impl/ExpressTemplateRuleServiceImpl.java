package com.zhaowu.cloud.uc.service.impl;

import com.zhaowu.cloud.uc.entity.po.ExpressTemplateRule;
import com.zhaowu.cloud.uc.mapper.ExpressTemplateRuleMapper;
import com.zhaowu.cloud.uc.service.ExpressTemplateRuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参与者快递运费规则 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Service
public class ExpressTemplateRuleServiceImpl extends ServiceImpl<ExpressTemplateRuleMapper,ExpressTemplateRule> implements ExpressTemplateRuleService {

}
