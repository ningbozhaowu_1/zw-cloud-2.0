package com.zhaowu.cloud.uc.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.ShopQueryForm;
import com.zhaowu.cloud.uc.entity.po.Organization;
import com.zhaowu.cloud.uc.entity.po.Party;
import com.zhaowu.cloud.uc.entity.po.PartyAsso;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.entity.vo.PartyDetail;
import com.zhaowu.cloud.uc.entity.vo.Shop;
import com.zhaowu.cloud.uc.entity.vo.UserVO;
import com.zhaowu.cloud.uc.service.OrganizationService;
import com.zhaowu.cloud.uc.service.PartyAssoService;
import com.zhaowu.cloud.uc.service.PartyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 参与者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/public/party/v1")
@Api(tags = "public参与者 接口")
public class PublicPartyController extends BaseController {

        @Autowired
        private PartyService partyService;

        @Autowired
        private OrganizationService organizationService;

        @Autowired
        private PartyAssoService partyAssoService;

        private static final String X_CLIENT_HOST = "zw-client-host";

        @RequestMapping(value = "/getPartyDetail", method = {RequestMethod.GET})
        @ApiOperation("获取参与者基础信息")
        public Result<PartyDetail> getPartyDetail(@Valid @RequestParam String applicationParty, HttpServletRequest request) {

                PartyDetail partyDetail = new PartyDetail();
                Party party = partyService.selectById(applicationParty);
                Organization organization = organizationService.selectById(applicationParty);
                organization.setCodeType(null);
                organization.setCode(null);
                organization.setLegalIdno(null);
                organization.setLegalName(null);
                organization.setLegalContacts(null);
                organization.setLegalIdno(null);
                organization.setLegalIdtype(null);
                BeanUtils.copyProperties(party, partyDetail);
                BeanUtils.copyProperties(organization, partyDetail);
                return this.success(partyDetail);
        }

        @RequestMapping(value = "/getPartyDetailByHost", method = {RequestMethod.GET})
        @ApiOperation("根据域名获取参与者基础信息")
        public Result<PartyDetail> getPartyDetailByHost(HttpServletRequest request) {

                PartyDetail partyDetail = new PartyDetail();
                String host = request.getHeader(X_CLIENT_HOST).split(":")[0];
                Organization organization = organizationService.selectByHost(host);
                Party party  = partyService.selectById(organization.getPartyId());
                BeanUtils.copyProperties(organization, partyDetail);
                BeanUtils.copyProperties(party, partyDetail);
                return this.success(partyDetail);
        }

        @RequestMapping(value = "/getShopList", method = {RequestMethod.POST})
        @ApiOperation("分页查询商户列表")
        public Result<Shop> getShopList(@Valid @RequestBody ShopQueryForm shopQueryForm) {

                Page<PartyAsso> partyAssoList = this.partyAssoService.getPageListByAssoParty(shopQueryForm.getPage(), CommonConstant.AssoType.SIGN, shopQueryForm.getApplicationParty());

                if(CollectionUtils.isEmpty(partyAssoList.getRecords())){
                        return this.success();
                }

                List<Shop> shopList = new LinkedList<>();
                for(PartyAsso partyAsso : partyAssoList.getRecords()){
                        Organization organization = organizationService.selectById(partyAsso.getPartyId());
                        Shop shop = new Shop();
                        BeanUtils.copyProperties(organization, shop);
                        shopList.add(shop);
                }

                return this.success(shopList);
        }

}

