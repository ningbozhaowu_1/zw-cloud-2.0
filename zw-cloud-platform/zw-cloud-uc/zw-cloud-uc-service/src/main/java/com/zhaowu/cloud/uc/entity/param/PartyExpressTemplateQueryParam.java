package com.zhaowu.cloud.uc.entity.param;

import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import com.zhaowu.cloud.uc.entity.po.PartyExpressTemplate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PartyExpressTemplateQueryParam extends BaseParam<PartyExpressTemplate> {
}
