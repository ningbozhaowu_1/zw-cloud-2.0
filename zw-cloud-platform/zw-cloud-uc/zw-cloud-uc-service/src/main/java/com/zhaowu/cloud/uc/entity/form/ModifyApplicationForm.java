package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.Application;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ModifyApplicationForm extends BaseForm<Application> {

    @Size(max= 256, message="应用名称长度必须在{max}个字符内")
    @ApiModelProperty("应用名称")
    private String name;

    @Size(max= 1024, message="应用logo长度必须在{max}个字符内")
    @ApiModelProperty("应用logo")
    private String logo;

    @NotNull(message="编号不能为空")
    @ApiModelProperty(value = "编号")
    private String id;
}
