package com.zhaowu.cloud.uc.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.OrganizationForm;
import com.zhaowu.cloud.uc.entity.po.Organization;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.uc.service.OrganizationService;

import java.util.List;


import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 组织者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@RestController
@RequestMapping("/organization/v1")
@Api(tags = "组织者 接口")
public class OrganizationController extends BaseController {

        @Autowired
        private OrganizationService organizationService;

        @RequestMapping(value = "/organizationModify", method = {RequestMethod.POST})
        @ApiOperation("企业用户信息修改")
        public Result organizationModify(@Valid @RequestBody OrganizationForm organizationForm) {

                Organization organization = organizationService.selectById(UserContextHolder.getInstance().getPartyId());
                if(organization==null){
                        organization = new Organization();
                        organization.setPartyId(UserContextHolder.getInstance().getPartyId());
                }
                BeanUtils.copyProperties(organizationForm, organization);
                organizationService.modify(organization);
                return this.success();
        }
}

