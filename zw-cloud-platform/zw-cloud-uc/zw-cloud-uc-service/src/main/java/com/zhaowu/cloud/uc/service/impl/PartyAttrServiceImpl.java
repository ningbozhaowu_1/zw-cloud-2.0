package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.util.Assert;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.po.PartyAsso;
import com.zhaowu.cloud.uc.entity.po.PartyAttr;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.mapper.PartyAssoMapper;
import com.zhaowu.cloud.uc.mapper.PartyAttrMapper;
import com.zhaowu.cloud.uc.service.PartyAttrService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartyAttrServiceImpl extends ServiceImpl<PartyAttrMapper, PartyAttr> implements PartyAttrService {

    @Override
    public void add(PartyAttr partyAttr) {

        boolean checkAttrCode = checkAttrCode(partyAttr);
        Assert.isTrue(checkAttrCode, "属性code已存在");
        this.save(partyAttr);
    }

    @Override
    public PartyAttr findById(String partyId, String id) {
        QueryWrapper<PartyAttr> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id",partyId);
        queryWrapper.eq("id",id);
        return this.getOne(queryWrapper);
    }

    @Override
    public void modify(PartyAttr partyAttr) {
        boolean checkAttrCode = checkAttrCode(partyAttr);

        Assert.isTrue(checkAttrCode, "属性code已存在");
        this.updateById(partyAttr);
    }

    @Override
    public void delete(PartyAttr partyAttr) {

        this.removeById(partyAttr.getId());
    }

    @Override
    public List<PartyAttr> getListByAttrType(String partyId, String attrType) {

        QueryWrapper<PartyAttr> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id",partyId);
        queryWrapper.eq("attr_type",attrType);
        return this.list(queryWrapper);
    }

    private boolean checkAttrCode(PartyAttr partyAttr) {
        QueryWrapper<PartyAttr> wrapper = new QueryWrapper<PartyAttr>();
        wrapper.eq("attr_code", partyAttr.getAttrCode());
        wrapper.eq("attr_type", partyAttr.getAttrType());
        wrapper.eq("party_id", partyAttr.getPartyId());
        if(!StringUtils.isEmpty(partyAttr.getId())){
            wrapper.ne("id", partyAttr.getId());
        }
        return this.getOne(wrapper) == null;
    }
}
