package com.zhaowu.cloud.uc.entity.vo;

import com.zhaowu.cloud.uc.entity.po.Account;
import lombok.Data;

import java.util.List;

@Data
public class UserAssets {

    private List<Account> accountList;

}
