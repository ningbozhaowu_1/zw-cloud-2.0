package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyPay;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ModifyPartyPayForm extends BaseForm<PartyPay> {

    @ApiModelProperty("商户编号")
    private String merchantId;

    @ApiModelProperty("商户类型 wechat:微信 alipay:支付宝")
    private String merchantType;

    @ApiModelProperty("商户密钥")
    private String merchantKey;

    @ApiModelProperty("商户证书路径")
    private String merchantCertKey;

    @NotNull(message="参与者支付编号不能为空")
    @ApiModelProperty("参与者支付编号")
    private String id;

}