package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.form.PartyExpressFeeQueryForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressSettingForm;

import java.math.BigDecimal;

/**
 * <p>
 * 快递 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyExpressService {

    /**
     * 参与者快递设置
     * @param request
     */
    void setPartyAttr(PartyExpressSettingForm request);

    /**
     * 参与者快递查询
     * @return
     */
    PartyExpressSettingForm getPartyAttr();

    /**
     * 参与者快递费查询
     * @param request
     * @return
     */
    BigDecimal queryPartyExpressFee(PartyExpressFeeQueryForm request);
}
