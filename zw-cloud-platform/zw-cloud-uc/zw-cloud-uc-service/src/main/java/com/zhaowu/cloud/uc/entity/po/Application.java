package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * <p>
 * 应用
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Application extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 父应用编号
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 参与者编号
     */
    @TableField("party_id")
    private String partyId;

    /**
     * 状态 : 1-可用，0-禁用 -1-锁定
     */
    private String status;

    /**
     * 名称
     */
    private String name;

    /**
     * 生效时间
     */
    @TableField(value = "effective_time")
    private Date effectiveTime;

    /**
     * 失效时间
     */
    @TableField(value = "expire_time")
    private Date expireTime;

    /**
     * 版本名称
     */
    private String versionName;
    /**
     * 版本
     */
    private String version;

    private String logo;

}
