package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.Application;

/**
 * <p>
 * 域应用 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2020-10-26
 */
public interface ApplicationMapper extends BaseMapper<Application> {

}
