package com.zhaowu.cloud.uc.controller;


import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.form.PartyShipAddressForm;
import com.zhaowu.cloud.uc.entity.po.PartyShipAddress;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.uc.service.PartyShipAddressService;
import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 参与者收货地址 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@RestController
@RequestMapping("/partyShipAddress/v1")
@Api(tags = "参与者收货地址 接口")
public class PartyShipAddressController extends BaseController {

        @Autowired
        private PartyShipAddressService partyShipAddressService;

        /**
         * 获取参与者收货地址列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取参与者收货地址列表")
        public Result<List<PartyShipAddress>> list() {
            return this.success(partyShipAddressService.selectList());
        }

        /**
         * 新增参与者收货地址
         */
        @PostMapping(value = "/add")
        @ApiOperation("新增参与者收货地址")
        public Result<String> add(@Valid @RequestBody PartyShipAddressForm partyShipAddressForm) {
            partyShipAddressService.add(partyShipAddressForm.toPo(PartyShipAddress.class));
            return this.success();
        }

        /**
         * 删除参与者收货地址
         */
        @PostMapping(value = "/delete/{id}")
        @ApiOperation("删除参与者收货地址")
        public Result<String> delete(@PathVariable("id") String id) {
            partyShipAddressService.delete(id);
            return this.success();
        }

        /**
         * 修改参与者收货地址
         */
        @PostMapping(value = "/update")
        @ApiOperation("修改参与者收货地址")
        public Result<String> update(@RequestBody PartyShipAddressForm partyShipAddressForm) {
            partyShipAddressService.modify(partyShipAddressForm.toPo(PartyShipAddress.class));
            return this.success();
        }

        /**
         * 获取参与者收货地址
         */
        @GetMapping(value = "/getById")
        @ApiOperation("获取参与者收货地址")
        public Result<PartyShipAddress> getById(@Valid @RequestParam("id") String id) {
            return this.success(partyShipAddressService.selectById(id));
        }
}

