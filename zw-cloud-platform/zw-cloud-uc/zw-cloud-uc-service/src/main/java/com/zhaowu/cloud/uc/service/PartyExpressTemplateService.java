package com.zhaowu.cloud.uc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressTemplateSetForm;
import com.zhaowu.cloud.uc.entity.param.PartyExpressTemplateQueryParam;
import com.zhaowu.cloud.uc.entity.vo.PartyExpressTemplateVo;

/**
 * <p>
 * 参与者快递模版 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyExpressTemplateService{

    /**
     * 参与者快递模版设置
     * @param request request
     */
    void setPartyExpressTemplate(PartyExpressTemplateSetForm request);

    /**
     * 参与者快递模版删除
     * @param templateId templateId
     */
    void deletePartyExpressTemplate(String templateId);

    /**
     * 参与者快递模版查询
     * @param queryForm
     * @return
     */
    IPage<PartyExpressTemplateVo> queryPartyExpressTemplate(BaseQueryForm<PartyExpressTemplateQueryParam> queryForm);

    /**
     * 参与者快递模版查询
     * @param partyId
     * @param templateId
     * @return
     */
    PartyExpressTemplateVo queryPartyExpressTemplate(String partyId, String templateId);
}
