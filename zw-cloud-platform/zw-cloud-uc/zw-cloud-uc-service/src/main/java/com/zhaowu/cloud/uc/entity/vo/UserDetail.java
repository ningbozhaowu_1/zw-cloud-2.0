package com.zhaowu.cloud.uc.entity.vo;


import com.zhaowu.cloud.uc.entity.po.Account;
import com.zhaowu.cloud.uc.entity.po.Person;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xxp
 **/
@Data
public class UserDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String status;

    private String partyId;

    private String applicationParty;

    private String username;

    private String userType;
    /**
     * 手机号
     */
    private String mobile;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 简介
     */
    private String reserve;

    /**
     * 头像
     */
    private String avatar;

    private Person person;

    private List<Account> accountList;

}
