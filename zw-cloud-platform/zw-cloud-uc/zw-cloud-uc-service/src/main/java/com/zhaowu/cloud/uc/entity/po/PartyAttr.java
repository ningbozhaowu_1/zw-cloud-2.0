package com.zhaowu.cloud.uc.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 应用属性
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_attr")
public class PartyAttr extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 参与者编号
     */
    private String partyId;

    /**
     * 属性类型 功能开关等
     */
    @TableField("attr_type")
    private String attrType;

    /**
     * 属性code
     */
    @TableField("attr_code")
    private String attrCode;

    /**
     * 属性值
     */
    @TableField("attr_value")
    private String attrValue;

    /**
     * 属性描述
     */
    @TableField("attr_desc")
    private String attrDesc;

}
