package com.zhaowu.cloud.uc.controller;


import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.*;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.entity.vo.ApplicationVO;
import com.zhaowu.cloud.uc.entity.vo.PartyChannelVO;
import com.zhaowu.cloud.uc.entity.vo.PartyChannelVOO;
import com.zhaowu.cloud.uc.entity.vo.PartyPayVO;
import com.zhaowu.cloud.uc.service.*;
import com.zhaowu.cloud.wx.third.entity.form.CreateForm;
import com.zhaowu.cloud.wxThird.client.WxThirdClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 参与者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/app/admin/v1")
@Api(tags = "应用管理接口")
public class ApplicationAdminController extends BaseController {
        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        @Autowired
        private PartyAssoService partyAssoService;


        @Autowired
        private PartyPayService partyPayService;

        @Autowired
        private ApplicationService applicationService;

        @Autowired
        private PartyChannelService partyChannelService;

        @Autowired
        private PartyApplyInfoService partyApplyInfoService;

        @Autowired
        private WxThirdClient wxThirdClient;

        @Autowired
        private PartyChannelService applicationChannelService;

        @Autowired
        private OrganizationService organizationService;

        @Autowired
        private PartyChannelPayService partyChannelPayService;


        /**
         * 获取应用基础信息
         */
        @GetMapping(value = "/get")
        @ApiOperation("获取应用详细信息")
        public Result<ApplicationVO> get(String id) {
                Application application =  applicationService.selectById(id);
                ApplicationVO applicationVO = new ApplicationVO();
                BeanUtils.copyProperties(application, applicationVO);
                return this.success(applicationVO);
        }

        /**
         * 修改应用基础信息
         */
        @PostMapping(value = "/modify")
        @ApiOperation("修改应用基础信息")
        public Result modify(@Valid @RequestBody ModifyApplicationForm modifyApplicationForm) {
                Application application =  applicationService.selectById(modifyApplicationForm.getId());

                if(application==null){
                        return this.failure("应用不存在");
                }

                if(!StringUtils.isEmpty(modifyApplicationForm.getLogo())){
                        application.setLogo(modifyApplicationForm.getLogo());
                }

                if(!StringUtils.isEmpty(modifyApplicationForm.getName())) {
                        application.setName(modifyApplicationForm.getName());
                }
                applicationService.modify(application);
                return this.success();
        }

        /**
         * 获取应用基础信息
         */
        @GetMapping(value = "/getByApplicationParty")
        @ApiOperation("获取应用详细信息")
        public Result<ApplicationVO> getByApplicationParty(@Valid @RequestParam("applicationParty") String applicationParty) {
                Application application =  applicationService.selectByPartyId(applicationParty);
                ApplicationVO applicationVO = new ApplicationVO();
                BeanUtils.copyProperties(application, applicationVO);
                return this.success(applicationVO);
        }

        /**
         * 获取应用发布渠道列表
         */
        @GetMapping(value = "/getPartyChannelList")
        @ApiOperation("获取应用发布渠道列表 wechatMA-微信小程序")
        public Result<List<PartyChannelVOO>> getPartyChannelList(@RequestParam(value="applicationParty", required=false) String applicationParty, @Valid @RequestParam("type") String type) {

                List<PartyChannel> partyChannelList = partyChannelService.getPartyChannelList(applicationParty==null?UserContextHolder.getInstance().getApplicationParty():applicationParty, type);

                List<PartyChannelVOO> partyChannelVOList = JSON.parseArray(JSON.toJSONString(partyChannelList), PartyChannelVOO.class);

                return this.success(partyChannelVOList);
        }

        @RequestMapping(value = "/getPartyPay", method = RequestMethod.GET)
        @ApiOperation("应用渠道支付查询")
        public Result<List<PartyPayVO>> getPartyPay(@Valid @RequestParam String partyChannelId){

                List<PartyPayVO> partyPayVOList = partyPayService.selectByChannelId(partyChannelId, UserContextHolder.getInstance().getApplicationParty(), null);
                return this.success(partyPayVOList);
        }

        /**
         * 应用渠道绑定支付
         */
        @PostMapping(value = "/partyChannelBindPay")
        @ApiOperation("应用渠道绑定支付")
        @Transactional
        public Result partyChannelBindPay(@Valid @RequestBody PartyChannelPayForm partyChannelPayForm) {

                PartyChannelPay partyChannelPay = partyChannelPayForm.toPo(PartyChannelPay.class);
                partyChannelPay.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
                this.partyChannelPayService.add(partyChannelPay);
                return this.success();
        }

        /**
         * 应用渠道绑定支付
         */
        @PostMapping(value = "/partyChannelUnBindPay")
        @ApiOperation("应用渠道解绑支付")
        @Transactional
        public Result partyChannelUnBindPay(@Valid @RequestBody PartyChannelPayForm partyChannelPayForm) {
                partyChannelPayService.delete(partyChannelPayForm.getPartyChannelId(), partyChannelPayForm.getPartyPayId());
                return this.success();
        }

        /**
         * 应用渠道发布
         */
        @PostMapping(value = "/partyChannelPublish")
        @ApiOperation("应用渠道发布")
        @Transactional
        public Result partyChannelPublish(@Valid @RequestBody PartyChannelForm partyChannelForm) {

                this.logger.info("应用渠道发布partyChannelForm:{} ", partyChannelForm);

                PartyChannel partyChannel = new PartyChannel();
                BeanUtils.copyProperties(partyChannelForm, partyChannel);
                partyChannel.setStatus(CommonConstant.Status.P);
                partyChannel.setPartyId(UserContextHolder.getInstance().getApplicationParty());
                partyChannel = partyChannelService.add(partyChannel);
                this.logger.info("应用渠道发布partyChannel结果:{} ", partyChannel);

                PartyApplyInfo partyChannelApplyInfo = new PartyApplyInfo();
                BeanUtils.copyProperties(partyChannelForm, partyChannelApplyInfo);
                partyChannelApplyInfo.setPartyChannelId(partyChannel.getId());
                partyChannelApplyInfo.setUserParty(UserContextHolder.getInstance().getPartyId());
                partyChannelApplyInfo.setStatus(CommonConstant.ApplyStatus.A);
                partyChannelApplyInfo.setApplyType(CommonConstant.PartyApplyType.CHANNEL);
                this.logger.info("应用渠道发布partyChannel:{} ", partyChannel);
                partyApplyInfoService.add(partyChannelApplyInfo);
                this.logger.info("应用渠道发布partyChannel结果:{} ", partyChannel);
                if(CommonConstant.ApplicationType.SOCIAL_TYPE_WECHAT_MINIAP.equalsIgnoreCase(partyChannelForm.getType())){
                        CreateForm createForm  = new CreateForm();
                        BeanUtils.copyProperties(partyChannelForm, createForm);
                        wxThirdClient.creat(createForm);
                }else{
                        Assert.isTrue(false,"目前只支持微信小程序渠道,其他正在构建中");
                }

                return this.success();
        }

        @GetMapping(value = "/list")
        @ApiOperation("查询应用列表")
        public Result<List<ApplicationVO>> list() {
                List<PartyAsso> partyAssoList = this.partyAssoService.getListByAssoType(CommonConstant.AssoType.OWNER, UserContextHolder.getInstance().getPartyId());

                List<ApplicationVO> applicationVOList = new LinkedList<>();
                for(PartyAsso partyAsso : partyAssoList){
                        Application application =  applicationService.selectByPartyId(partyAsso.getAssoParty());
                        ApplicationVO applicationVO = new ApplicationVO();
                        BeanUtils.copyProperties(application, applicationVO);
                        Organization organization = organizationService.selectById(application.getPartyId());
                        if(organization!=null){
                                applicationVO.setWebPage(organization.getWebPage());
                        }
                        applicationVOList.add(applicationVO);
                }

                return this.success(applicationVOList);
        }

        @RequestMapping(value = "/getPartyPayList", method = RequestMethod.GET)
        @ApiOperation("查询支付列表")
        public Result<PartyPay> getPartyPayList(@Valid @RequestParam("applicationParty") String applicationParty, @Valid @RequestParam("partyChannelId") String partyChannelId){

                List<PartyPay> applicationPayList = partyPayService.selectByPartyId(null, applicationParty);
                return this.success(applicationPayList);
        }

        @RequestMapping(value = "/addPartyPay", method = RequestMethod.POST)
        @ApiOperation("新增应用支付")
        public Result addPartyPay(@Valid @RequestBody PartyPayForm partyPayForm){

                partyPayService.add(partyPayForm.toPo(PartyPay.class));
                return this.success();
        }

        @RequestMapping(value = "/modifyPartyPay", method = RequestMethod.POST)
        @ApiOperation("修改应用支付")
        public Result modifyPartyPay(@Valid @RequestBody ModifyPartyPayForm modifyPartyPayForm){
                partyPayService.modify(modifyPartyPayForm.toPo(PartyPay.class));
                return this.success();
        }

        @RequestMapping(value = "/delPartyPay/{id}", method = RequestMethod.DELETE)
        @ApiOperation("删除应用支付")
        public Result delApplicationPay(@Valid @PathVariable("id") String id){
                this.partyPayService.delete(id);
                return this.success();
        }

        @RequestMapping(value = "/partyPayApply", method = RequestMethod.POST)
        @ApiOperation("申请平台特约商户")
        public Result partyPayApply(){

                return this.success();
        }

        @RequestMapping(value = "/getPartyChannel", method = RequestMethod.POST)
        @ApiIgnore
        public Result<PartyChannel> getPartyChannel(@Valid @RequestBody PartyChannelQueryForm partyChannelQueryForm){
                return this.success(applicationChannelService.selectById(partyChannelQueryForm.getPartyChannelId()));
        }

        @RequestMapping(value = "/getPartyPay", method = RequestMethod.POST)
        @ApiIgnore
        public Result<List<PartyPayVO>> getPartyPay(@Valid @RequestBody PartyPayQueryForm applicationPayQueryForm){

                List<PartyPayVO> partyPayVOList = partyPayService.selectByChannelId(applicationPayQueryForm.getApplicationChannelId(), applicationPayQueryForm.getApplicationParty(), applicationPayQueryForm.getMercahtType());
                return this.success(partyPayVOList);
        }

        @RequestMapping(value = "/getPartyChannelByAuthorizerAppId", method = RequestMethod.POST)
        @ApiIgnore
        public Result<PartyChannelVO> getPartyChannelByAuthorizerAppId(@Valid @RequestBody PartyChannelQueryForm applicationChannelQueryForm){

                PartyChannel applicationChannel = applicationChannelService.selectByAppId(applicationChannelQueryForm.getApplicationParty(), applicationChannelQueryForm.getAuthorizerAppId());
                PartyChannelVO applicationChannelVO = new PartyChannelVO();
                BeanUtils.copyProperties(applicationChannel, applicationChannelVO);
                return this.success(applicationChannelVO);
        }
        @RequestMapping(value = "/updateIntroductionByAuthorizerAppId", method = RequestMethod.POST)
        @ApiIgnore
        public Result<PartyChannelVO> updateIntroductionByAuthorizerAppId(@Valid @RequestBody PartyChannelUpdateForm partyChannelUpdateForm){
                this.logger.info("应用更新简介:{} ", partyChannelUpdateForm);
                PartyChannel old = applicationChannelService.selectByAppId(partyChannelUpdateForm.getApplicationParty(), partyChannelUpdateForm.getAuthorizerAppId());
                old.setIntroduction(partyChannelUpdateForm.getIntroduction());
                PartyChannel applicationChannel = applicationChannelService.modify(old);
                PartyChannelVO applicationChannelVO = new PartyChannelVO();
                BeanUtils.copyProperties(applicationChannel, applicationChannelVO);
                this.logger.info("应用更新简介结束:{} ", applicationChannelVO);
                return this.success(applicationChannelVO);
        }

        @RequestMapping(value = "/partyChannelAuthUpdate", method = RequestMethod.POST)
        @Transactional
        @ApiIgnore
        public Result partyChannelAuthUpdate(@Valid @RequestBody PartyChannelAuthorizeForm authorizeApplicationChannelForm){
                this.logger.info("应用更新:{} ", authorizeApplicationChannelForm);

                PartyApplyInfo partyChannelApplyInfoExsit = partyApplyInfoService.selectByCode(authorizeApplicationChannelForm.getCodeType(), authorizeApplicationChannelForm.getCode(), CommonConstant.PartyApplyType.CHANNEL);

                if(partyChannelApplyInfoExsit == null){
                        return this.success();
                }

                PartyChannel applicationChannelExsit =  applicationChannelService.selectById(partyChannelApplyInfoExsit.getPartyChannelId());

                applicationChannelExsit.setAuthorizerAppId(authorizeApplicationChannelForm.getAuthorizerAppid());
                applicationChannelExsit.setAccessToken(authorizeApplicationChannelForm.getAccessToken());
                applicationChannelExsit.setRefreshToken(authorizeApplicationChannelForm.getRefreshToken());
                applicationChannelExsit.setAuthCode(authorizeApplicationChannelForm.getAuthCode());
                applicationChannelExsit.setStatus(CommonConstant.Status.A);//已授权
                applicationChannelExsit.setName(authorizeApplicationChannelForm.getNickName());
                applicationChannelExsit.setIntroduction(authorizeApplicationChannelForm.getIntroduction());
                applicationChannelExsit.setAvatar(authorizeApplicationChannelForm.getAvatar());
                applicationChannelService.modify(applicationChannelExsit);

                partyChannelApplyInfoExsit.setStatus(CommonConstant.ApplyStatus.P);//更新为已通过
                partyApplyInfoService.modify(partyChannelApplyInfoExsit);

                return this.success();
        }

        @RequestMapping(value = "/partyChannelAdd", method = RequestMethod.POST)
        @Transactional
        @ApiIgnore
        public Result partyChannelAdd(@Valid @RequestBody PartyChannelAuthorizeForm authorizeApplicationChannelForm){
                this.logger.info("应用新增:{} ", authorizeApplicationChannelForm);
                PartyChannel applicationChannelExsit = applicationChannelService.selectByAppId(authorizeApplicationChannelForm.getApplicationParty(), authorizeApplicationChannelForm.getAuthorizerAppid());

                if(applicationChannelExsit!=null){
                        applicationChannelExsit.setAccessToken(authorizeApplicationChannelForm.getAccessToken());
                        applicationChannelExsit.setRefreshToken(authorizeApplicationChannelForm.getRefreshToken());
                        applicationChannelExsit.setAuthCode(authorizeApplicationChannelForm.getAuthCode());
                        applicationChannelExsit.setType(authorizeApplicationChannelForm.getType());
                        applicationChannelExsit.setStatus(CommonConstant.Status.A);
                        applicationChannelExsit.setName(authorizeApplicationChannelForm.getNickName());
                        applicationChannelExsit.setIntroduction(authorizeApplicationChannelForm.getIntroduction());
                        applicationChannelExsit.setAvatar(authorizeApplicationChannelForm.getAvatar());
                        applicationChannelService.modify(applicationChannelExsit);
                        return this.success();
                }

                if(!CommonConstant.ApplicationType.SOCIAL_TYPE_WECHAT_MINIAP.equalsIgnoreCase(authorizeApplicationChannelForm.getType())){
                        return this.failure("目前只支持微信小程序渠道,其他正在构建中");
                }

                PartyChannel partyChannel = new PartyChannel();
                partyChannel.setPartyId(authorizeApplicationChannelForm.getApplicationParty());
                partyChannel.setAuthorizerAppId(authorizeApplicationChannelForm.getAuthorizerAppid());
                partyChannel.setAccessToken(authorizeApplicationChannelForm.getAccessToken());
                partyChannel.setRefreshToken(authorizeApplicationChannelForm.getRefreshToken());
                partyChannel.setAuthCode(authorizeApplicationChannelForm.getAuthCode());
                partyChannel.setType(authorizeApplicationChannelForm.getType());
                partyChannel.setStatus(CommonConstant.Status.A);//已授权
                partyChannel.setName(authorizeApplicationChannelForm.getNickName());
                partyChannel.setIntroduction(authorizeApplicationChannelForm.getIntroduction());
                partyChannel.setAvatar(authorizeApplicationChannelForm.getAvatar());
                applicationChannelService.add(partyChannel);
                return this.success();
        }

}

