package com.zhaowu.cloud.uc.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.po.User;
import com.zhaowu.cloud.uc.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p> 注册控制器 </p>
 * @author xxp @since 2021-01-20
 */
@RestController
@RequestMapping("/public/register/v1")
@Api(tags = "注册 接口")
public class RegisterController extends BaseController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/check", method = {RequestMethod.GET})
    @ApiOperation("查询注册状态")
    public Result<Boolean> check(@Valid @RequestParam String mobileNo,
                                         @Valid @RequestParam String applicationParty) {

        User userExsit = this.userService.findUsersByMobileNo(mobileNo, applicationParty);
        if(userExsit!=null){
            return this.success(true);
        }else{
            return this.success(false);
        }
    }
}
