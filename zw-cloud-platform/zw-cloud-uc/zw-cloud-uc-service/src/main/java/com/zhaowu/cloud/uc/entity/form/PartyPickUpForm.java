package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyPickUp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class PartyPickUpForm extends BaseForm<PartyPickUp> {


    @NotNull(message="自提点名称不能为空")
    @ApiModelProperty("自提点名称")
    private String name;

    @NotNull(message="自提点联系电话不能为空")
    @ApiModelProperty("自提点联系电话")
    private String componentPhone;

    @NotNull(message="自提点不能为空")
    @ApiModelProperty("自提点联系电话")
    private String picture;

    @NotNull(message="自提点门店标记不能为空")
    @ApiModelProperty(value = "自提点门店标记", allowableValues = "Y,N")
    private String isShop;

    @NotNull(message="自提点开门时间不能为空")
    @ApiModelProperty("自提点开门时间")
    private String openTime;

    @NotNull(message="自提点关门时间不能为空")
    @ApiModelProperty("自提点关门时间")
    private String closeTime;

    @NotNull(message="自提点所在省份不能为空")
    @ApiModelProperty("自提点所在省份")
    private String province;

    @NotNull(message="自提点所在城市不能为空")
    @ApiModelProperty("自提点所在城市")
    private String city;

    @NotNull(message="自提点所在街道不能为空")
    @ApiModelProperty("自提点所在街道")
    private String district;

    @NotNull(message="自提点详细地址不能为空")
    @ApiModelProperty("自提点详细地址")
    private String address;

}