package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.po.PartyDelivery;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 配送 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyDeliveryService{

    void add(PartyDelivery partyDelivery);

    PartyDelivery get(String partyId);
}
