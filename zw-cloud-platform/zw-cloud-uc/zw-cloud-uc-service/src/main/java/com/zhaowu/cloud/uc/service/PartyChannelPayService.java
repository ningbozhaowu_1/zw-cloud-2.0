package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.po.PartyChannelPay;

public interface PartyChannelPayService {

    void add(PartyChannelPay partyChannelPay);

    void delete(String partyChannelId, String partyPayId);

    void modify(PartyChannelPay partyChannelPay);
}
