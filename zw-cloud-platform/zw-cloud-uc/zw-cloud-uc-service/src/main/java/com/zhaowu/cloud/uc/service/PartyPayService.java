package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.PartyPay;
import com.zhaowu.cloud.uc.entity.vo.PartyPayVO;

import java.util.List;

public interface PartyPayService {

    List<PartyPay> selectByPartyId(String merchantType, String partyId);

    @Cached(name="applicationPayCache::", key = "#merchantId+ '::' + #partyId", expire = 3600)
    @CachePenetrationProtect
    PartyPay selectByMerchantId(String merchantId, String partyId);

    List<PartyPayVO> selectByChannelId(String partyChannelId, String partyId, String merchantType);

//    List<PartyPay> getApplicationPayList(String partyChannelId, String partyId);

    @CacheInvalidate(name = "applicationPayCache::", key = "#partyPay.partyChannelId + '::' + #partyPay.partyId")
    void add(PartyPay partyPay);

    @CacheInvalidate(name = "applicationPayCache::", key = "#partyPay.partyChannelId + '::' + #partyPay.partyId")
    void modify(PartyPay partyPay);

    @CacheInvalidate(name = "applicationPayCache::", key = "#partyPay.partyChannelId + '::' + #partyPay.partyId")
    void delete(String id);
}
