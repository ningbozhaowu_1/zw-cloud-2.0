package com.zhaowu.cloud.uc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.form.PartyPickUpPageQueryForm;
import com.zhaowu.cloud.uc.entity.param.PartyPickUpQueryParam;
import com.zhaowu.cloud.uc.entity.po.PartyPickUp;

import java.util.List;

/**
 * <p>
 * 提货点 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
public interface PartyPickUpService{

    void add(PartyPickUp partyPickUp);

    IPage<PartyPickUp> pageList(Page<PartyPickUp> page, PartyPickUpQueryParam partyPickUpQueryParam);

    void delete(String partyId, String id);

    void modify(PartyPickUp partyPickUp);

    PartyPickUp selectById(String id);

    List<PartyPickUp> selectByCity(String cityName, String partyId);
}
