package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.po.PartyShipAddress;
import com.zhaowu.cloud.uc.mapper.PartyShipAddressMapper;
import com.zhaowu.cloud.uc.service.PartyShipAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参与者收货地址 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@Service
public class PartyShipAddressServiceImpl extends ServiceImpl<PartyShipAddressMapper,PartyShipAddress> implements PartyShipAddressService {

    @Override
    public PartyShipAddress selectById(String id) {

        QueryWrapper<PartyShipAddress> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id", UserContextHolder.getInstance().getPartyId());
        queryWrapper.eq("id", id);
        return this.getOne(queryWrapper);
    }

    @Override
    public List<PartyShipAddress> selectList() {
        QueryWrapper<PartyShipAddress> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id", UserContextHolder.getInstance().getPartyId());
        return this.list(queryWrapper);
    }

    @Override
    public void add(PartyShipAddress partyShipAddress) {

        this.save(partyShipAddress);
    }

    @Override
    public void delete(String id) {

        QueryWrapper<PartyShipAddress> queryWrapper = new QueryWrapper();
        queryWrapper.eq("party_id", UserContextHolder.getInstance().getPartyId());
        queryWrapper.eq("id", id);
        this.remove(queryWrapper);
    }

    @Override
    public void modify(PartyShipAddress partyShipAddress) {
        partyShipAddress.setPartyId(UserContextHolder.getInstance().getPartyId());
        this.updateById(partyShipAddress);
    }
}
