package com.zhaowu.cloud.uc.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 用户渠道
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@Accessors(chain = true)
@TableName("user_channel")
public class UserChannel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 参与者编号
     */
    @TableField("application_party")
    private String applicationParty;

    /**
     * 渠道编号
     */
    @TableField("party_channel_id")
    private String partyChannelId;

    /**
     * 授权状态
     */
    private String status;

    /**
     * openid
     */
    @TableField("open_id")
    private String openId;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;
}
