package com.zhaowu.cloud.uc.password;

/**
 * @author xxp
 **/
public interface PasswordGenerator {

    /**
     * 生成密码
     * @param param
     * @return
     */
    String generate(Object param);
}
