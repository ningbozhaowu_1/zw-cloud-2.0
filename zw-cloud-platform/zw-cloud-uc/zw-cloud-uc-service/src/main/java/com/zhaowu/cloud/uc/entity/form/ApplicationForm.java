package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.Application;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ApplicationForm extends BaseForm<Application> {

    @NotNull(message="应用名称不能为空")
    @Size(max= 256, message="应用名称长度必须在{max}个字符内")
    @ApiModelProperty("应用名称")
    private String name;
}
