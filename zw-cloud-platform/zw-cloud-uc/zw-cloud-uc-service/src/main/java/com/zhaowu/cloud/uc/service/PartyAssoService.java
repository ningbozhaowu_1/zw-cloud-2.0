package com.zhaowu.cloud.uc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.uc.entity.po.PartyAsso;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhaowu.cloud.uc.entity.po.User;

import java.util.List;

/**
 * <p>
 * 参与者关联 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
public interface PartyAssoService{

    void add(PartyAsso partyAsso);

    List<PartyAsso> getListByAssoType(String assoType, String partyId);

    List<PartyAsso> getListByAssoParty(String assoType, String assoParty);

    Page<PartyAsso> getPageListByAssoParty(Page<PartyAsso> page, String assoType, String assoParty);

    PartyAsso getByAssoType(String assoType, String partyId, String assoParty);

    void delByAssoType(String assoType, String partyId, String assoParty);
}
