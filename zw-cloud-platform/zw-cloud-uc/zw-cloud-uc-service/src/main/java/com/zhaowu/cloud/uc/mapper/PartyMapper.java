package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.Party;

/**
 * <p>
 * 参与者 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PartyMapper extends BaseMapper<Party> {

}
