package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.Party;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyForm extends BaseForm<Party> {

    @NotNull(message="申请类型不能为空")
    @ApiModelProperty(value = "申请类型", allowableValues = "SHOP,ENT")
    private String applyType;


}