package com.zhaowu.cloud.uc.entity.form;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyApplyInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyApplyForm extends BaseForm<PartyApplyInfo> {

    @NotNull(message="申请类型不能为空")
    @ApiModelProperty(value = "申请类型", allowableValues = "SHOP,ENT")
    private String applyType;

    @NotNull(message="企业名称不能为空")
    @ApiModelProperty(value = "企业名称")
    private String name;

    @NotNull(message="申请类型不能为空")
    @ApiModelProperty(value = "证件类型 1：统一社会信用代码（18 位）2：组织机构代码(9 位) 3：营业执照注册号(15 位)", allowableValues = "1,2,3")
    private String codeType;

    @NotNull(message="企业证件号不能为空")
    @ApiModelProperty(value = "企业证件号")
    private String code;

    @NotNull(message="法人姓名不能为空")
    @ApiModelProperty(value = "法人姓名")
    private String legalPersonaName;

    @NotNull(message="联系人手机号不能为空")
    @ApiModelProperty(value = "联系人手机号")
    private String componentPhone;

    @NotNull(message="token不能为空")
    @ApiModelProperty(value = "token")
    private String token;

    @NotNull(message="验证码不能为空")
    @ApiModelProperty(value = "验证码")
    private String mobileToken;
}