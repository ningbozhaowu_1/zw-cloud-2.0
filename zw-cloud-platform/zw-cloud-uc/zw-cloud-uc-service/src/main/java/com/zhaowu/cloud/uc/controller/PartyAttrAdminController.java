package com.zhaowu.cloud.uc.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.*;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 参与者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/party/admin/v1")
@Api(tags = "参与者属性管理 接口")
public class PartyAttrAdminController extends BaseController {

        @Autowired
        private PartyAttrService partyAttrService;

        @RequestMapping(value = "/addPartyAttr", method = {RequestMethod.POST})
        @ApiOperation("参与者属性新增")
        public Result addPartyAttr(@Valid @RequestBody PartyAttrForm partyAttrForm) {

                PartyAttr partyAttr = partyAttrForm.toPo(PartyAttr.class);
                partyAttr.setPartyId(UserContextHolder.getInstance().getApplicationParty());
                partyAttrService.add(partyAttr);
                return this.success();
        }

        @RequestMapping(value = "/getList", method = {RequestMethod.GET})
        @ApiOperation("参与者属性列表查询")
        public Result getList(@Valid @RequestParam("attrType") String attrType) {

                return this.success(partyAttrService.getListByAttrType(UserContextHolder.getInstance().getApplicationParty(), attrType));
        }

        @RequestMapping(value = "/modifyPartyAttr", method = {RequestMethod.POST})
        @ApiOperation("参与者属性修改")
        public Result modifyPartyAttr(@Valid @RequestBody ModifyPartyAttrForm modifyPartyAttrForm) {

                PartyAttr partyAttr = modifyPartyAttrForm.toPo(PartyAttr.class);
                partyAttr.setPartyId(UserContextHolder.getInstance().getApplicationParty());
                partyAttrService.modify(partyAttr);
                return this.success();
        }

        @ApiOperation(value = "删除参与者属性", notes = "删除参与者属性")
        @ApiImplicitParam(paramType = "path", name = "id", value = "属性ID", required = true, dataType = "string")
        @DeleteMapping(value = "/{id}")
        public Result delete(@PathVariable String id) {

                PartyAttr partyAttr = partyAttrService.findById(UserContextHolder.getInstance().getApplicationParty(), id);
                if(partyAttr == null){
                        return this.failure("编号不存在");
                }
                partyAttrService.delete(partyAttr);
                return this.success();
        }

}

