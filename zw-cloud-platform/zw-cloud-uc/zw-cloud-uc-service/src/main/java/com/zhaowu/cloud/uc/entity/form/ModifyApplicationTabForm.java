package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.zhaowu.cloud.uc.entity.po.ApplicationTab;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ModifyApplicationTabForm extends BaseForm<ApplicationTab> {

    @NotNull(message="编号不能为空")
    @ApiModelProperty(value = "编号")
    private String id;

    @Size(max= 256, message="tab名称必须在{max}个字符内")
    @ApiModelProperty("tab名称")
    private String name;

    @ApiModelProperty("页面名称")
    private Integer ordNum;

    @Size(max= 3, message="是否显示必须在{max}个字符内")
    @ApiModelProperty(value = "是否显示",allowableValues = "Y,N")
    private String isShow;

    @ApiModelProperty("页面编号")
    private String applicationHtmlId;

    @Size(max= 256, message="选中tab名称必须在{max}个字符内")
    @ApiModelProperty("选中tab名称")
    private String nameChoosed;

    @Size(max= 2048, message="图标必须在{max}个字符内")
    @ApiModelProperty("图标")
    private String icon;

    @Size(max= 2048, message="选中图标必须在{max}个字符内")
    @ApiModelProperty("选中图标")
    private String iconChoosed;

    @Size(max= 256, message="tab路径必须在{max}个字符内")
    @ApiModelProperty("路径")
    private String path;
}
