package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.PartyPayApplyment;
import com.zhaowu.cloud.uc.entity.vo.PayApplymentVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartyPayApplymentMapper  extends BaseMapper<PartyPayApplyment> {
    List<PayApplymentVO> selectByApplicationParty(@Param("applicationParty") String applicationParty);

}
