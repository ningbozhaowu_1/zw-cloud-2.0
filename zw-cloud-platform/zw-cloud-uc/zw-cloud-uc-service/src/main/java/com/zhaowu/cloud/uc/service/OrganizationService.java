package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.Organization;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhaowu.cloud.uc.entity.po.Party;

/**
 * <p>
 * 组织者 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
public interface OrganizationService{

    void add(Organization organization);

    @CacheInvalidate(name="organizationCache::",key = "#organization.partyId")
    void modify(Organization organization);

    @Cached(name="organizationCache::", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Organization selectById(String id);

    Organization selectByHost(String host);
}
