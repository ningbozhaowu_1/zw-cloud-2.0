//package com.zhaowu.cloud.uc.service.impl;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.zhaowu.cloud.uc.entity.po.ApplicationApply;
//import com.zhaowu.cloud.uc.mapper.ApplicationApplyMapper;
//import com.zhaowu.cloud.uc.service.ApplicationApplyService;
//import org.springframework.stereotype.Service;
//
//@Service
//public class ApplicationApplyServiceImpl extends ServiceImpl<ApplicationApplyMapper, ApplicationApply> implements ApplicationApplyService {
//
//    @Override
//    public void add(ApplicationApply applicationApply) {
//        this.save(applicationApply);
//    }
//
//    @Override
//    public ApplicationApply selectByCode(String codeType, String code) {
//
//        QueryWrapper<ApplicationApply> wrapper = new QueryWrapper<ApplicationApply>();
//        wrapper.eq("code", code);
//        wrapper.eq("code_type", codeType);
//        wrapper.eq("status", "0");
//        return this.getOne(wrapper);
//    }
//
//    @Override
//    public ApplicationApply selectByChannelId(Long channelId) {
//
//        QueryWrapper<ApplicationApply> wrapper = new QueryWrapper<ApplicationApply>();
//        wrapper.eq("application_channel_id", channelId);
//        return this.getOne(wrapper);
//    }
//
//    @Override
//    public void modify(ApplicationApply applicationApply) {
//        this.updateById(applicationApply);
//    }
//}
