package com.zhaowu.cloud.uc.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.PartyPickUpForm;
import com.zhaowu.cloud.uc.entity.form.PartyPickUpPageQueryForm;
import com.zhaowu.cloud.uc.entity.param.PartyPickUpQueryParam;
import com.zhaowu.cloud.uc.entity.po.PartyPickUp;
import com.zhaowu.cloud.uc.entity.vo.PartyPickUpVO;
import com.zhaowu.cloud.framework.map.MapService;
import com.zhaowu.cloud.framework.map.entity.GeocoderResult;
import com.zhaowu.cloud.framework.map.entity.Location;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.uc.service.PartyPickUpService;

import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 提货点 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@RestController
@RequestMapping("/partyPickUp/v1")
@Api(tags = "提货点 接口")
public class PartyPickUpController extends BaseController {

        @Autowired
        private PartyPickUpService partyPickUpService;

        @Autowired
        private MapService mapService;

        @RequestMapping(value = "/addPickUp", method = {RequestMethod.POST})
        @ApiOperation("添加自提点")
        public Result addPickUp(@Valid @RequestBody PartyPickUpForm partyPickUpForm) {

                PartyPickUp partyPickUp = partyPickUpForm.toPo(PartyPickUp.class);
                partyPickUp.setPartyId(UserContextHolder.getInstance().getMerchantPartyId()==null?UserContextHolder.getInstance().getApplicationParty():UserContextHolder.getInstance().getMerchantPartyId());
                //获取门店经纬度
                Location location = new Location();
                BeanUtils.copyProperties(partyPickUp, location);
                GeocoderResult geocoderResult = mapService.geocoder(location);
                partyPickUp.setLongitude(geocoderResult.getLongitude());
                partyPickUp.setLatitude(geocoderResult.getLatitude());
                partyPickUpService.add(partyPickUp);

                return this.success();
        }

        @RequestMapping(value = "/list", method = {RequestMethod.POST})
        @ApiOperation("参与者自提点列表")
        public Result<Page<PartyPickUp>> list(@Valid @RequestBody PartyPickUpPageQueryForm partyPickUpPageQueryForm) {

                if(StringUtils.isEmpty(partyPickUpPageQueryForm.getPartyId())){
                        partyPickUpPageQueryForm.setPartyId(UserContextHolder.getInstance().getApplicationParty());
                }
                return this.success(partyPickUpService.pageList(partyPickUpPageQueryForm.getPage(), partyPickUpPageQueryForm.toParam(PartyPickUpQueryParam.class)));
        }

        @RequestMapping(value = "/listByCity", method = {RequestMethod.GET})
        @ApiOperation("收货地城市参与者自提点列表")
        public Result<List<PartyPickUpVO>> listByCity(@Valid @RequestParam String cityName,
                                                      @Valid @RequestParam String partyId) {

                List<PartyPickUp> partyPickUpList = partyPickUpService.selectByCity(cityName, partyId);
                List<PartyPickUpVO> partyPickUpVOList = JSON.parseArray(JSON.toJSONString(partyPickUpList), PartyPickUpVO.class);
                return this.success(partyPickUpVOList);
        }

        @RequestMapping(value = "/getById", method = {RequestMethod.GET})
        @ApiOperation("收货地城市参与者自提点列表")
        public Result<PartyPickUpVO> getById(@Valid @RequestParam String id) {

                PartyPickUp partyPickUp = partyPickUpService.selectById(id);
                PartyPickUpVO partyPickUpVO = new PartyPickUpVO();
                BeanUtils.copyProperties(partyPickUp, partyPickUpVO);
                return this.success(partyPickUpVO);
        }
}

