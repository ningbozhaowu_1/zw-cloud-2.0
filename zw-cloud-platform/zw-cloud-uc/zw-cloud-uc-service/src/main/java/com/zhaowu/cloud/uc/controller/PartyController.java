package com.zhaowu.cloud.uc.controller;

import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.po.*;
import com.zhaowu.cloud.uc.entity.vo.PartyDeliveryVO;
import com.zhaowu.cloud.uc.entity.vo.PartyVO;
import com.zhaowu.cloud.uc.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 参与者 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@RestController
@RequestMapping("/party/v1")
@Api(tags = "参与者 接口")
public class PartyController extends BaseController {

        @Autowired
        private PartyDeliveryService partyDeliveryService;

        @Autowired
        private PartyService partyService;

        @RequestMapping(value = "/getDelivery", method = {RequestMethod.GET})
        @ApiOperation("获取配送设置")
        public Result<PartyDeliveryVO> getDelivery(@Valid @RequestParam String partyId) {

                PartyDelivery partyDelivery = partyDeliveryService.get(partyId);
                PartyDeliveryVO partyDeliveryVO = new PartyDeliveryVO();
                BeanUtils.copyProperties(partyDelivery,partyDeliveryVO);
                return this.success(partyDeliveryVO);
        }

        @RequestMapping(value = "/getPartyById", method = {RequestMethod.GET})
        @ApiOperation("获取参与者信息")
        public Result<PartyVO> getPartyById(@Valid @RequestParam String id) {

                Party party = partyService.selectById(id);
                PartyVO partyVO = new PartyVO();
                BeanUtils.copyProperties(party,partyVO);
                return this.success(partyVO);
        }

}

