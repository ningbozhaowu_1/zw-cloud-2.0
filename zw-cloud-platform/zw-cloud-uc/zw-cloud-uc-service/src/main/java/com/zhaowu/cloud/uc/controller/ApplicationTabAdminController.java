package com.zhaowu.cloud.uc.controller;


import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.ApplicationTabForm;
import com.zhaowu.cloud.uc.entity.form.ModifyApplicationTabForm;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.zhaowu.cloud.uc.entity.po.ApplicationTab;
import com.zhaowu.cloud.uc.entity.vo.ApplicationHtmlVO;
import com.zhaowu.cloud.uc.entity.vo.ApplicationTabVO;
import com.zhaowu.cloud.uc.service.ApplicationHtmlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zhaowu.cloud.uc.service.ApplicationTabService;

import java.util.List;


import org.springframework.web.bind.annotation.RestController;
import com.zhaowu.cloud.framework.base.controller.BaseController;

import javax.validation.Valid;

/**
 * <p>
 * 应用tab 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-04-01
 */
@RestController
@RequestMapping("/applicationTab/admin/v1")
@Api(tags = "应用tab 接口")
public class ApplicationTabAdminController extends BaseController {

        @Autowired
        private ApplicationTabService applicationTabService;

        @Autowired
        private ApplicationHtmlService applicationHtmlService;

        /**
         * 获取应用tab列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取应用tab列表")
        public Result<List<ApplicationTabVO>> list() {
            List<ApplicationTab> applicationTabList = applicationTabService.getList();
            List<ApplicationTabVO> applicationHtmlVOList = JSON.parseArray(JSON.toJSONString(applicationTabList), ApplicationTabVO.class);
            return this.success(applicationHtmlVOList);
        }

        /**
         * 新增应用tab
         */
        @PostMapping(value = "/add")
        @ApiOperation("新增应用tab")
        public Result add(@Valid @RequestBody ApplicationTabForm applicationTabForm) {

            ApplicationTab applicationTab = applicationTabForm.toPo(ApplicationTab.class);
            if(!StringUtils.isEmpty(applicationTabForm.getApplicationHtmlId())){
                ApplicationHtml applicationHtml = applicationHtmlService.findById(UserContextHolder.getInstance().getApplicationParty(), applicationTabForm.getApplicationHtmlId());
                if(applicationHtml==null){
                    return this.failure("页面不存在");
                }
                applicationTab.setPath(applicationHtml.getPath());
            }

            applicationTab.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
            applicationTabService.add(applicationTab);
            return this.success();
        }

        /**
         * 删除应用tab
         */
        @PostMapping(value = "/delete/{id}")
        @ApiOperation("删除应用tab")
        public Result delete(@PathVariable("id") String id) {

            applicationTabService.delete(UserContextHolder.getInstance().getApplicationParty(), id);

            return this.success();
        }

        /**
         * 修改应用tab
         */
        @PostMapping(value = "/update")
        @ApiOperation("修改应用tab")
        public Result update(@Valid @RequestBody ModifyApplicationTabForm modifyApplicationTabForm) {

            ApplicationTab applicationTab = modifyApplicationTabForm.toPo(ApplicationTab.class);
            if(!StringUtils.isEmpty(modifyApplicationTabForm.getApplicationHtmlId())){
                ApplicationHtml applicationHtml = applicationHtmlService.findById(UserContextHolder.getInstance().getApplicationParty(), modifyApplicationTabForm.getApplicationHtmlId());
                if(applicationHtml==null){
                    return this.failure("页面不存在");
                }
                applicationTab.setPath(applicationHtml.getPath());
            }
            applicationTabService.modify(applicationTab);
            return this.success();
        }
}

