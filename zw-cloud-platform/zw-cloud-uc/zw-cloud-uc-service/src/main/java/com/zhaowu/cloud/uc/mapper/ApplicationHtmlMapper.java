package com.zhaowu.cloud.uc.mapper;

import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用页面DIY Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-03-18
 */
public interface ApplicationHtmlMapper extends BaseMapper<ApplicationHtml> {

}
