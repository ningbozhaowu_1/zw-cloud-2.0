package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyShipAddress;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyShipAddressForm extends BaseForm<PartyShipAddress> {

    @ApiModelProperty(value = "参与者编号")
    private String partyId;

    @ApiModelProperty(value = "收货地址编号")
    private String id;

    @NotNull(message="省份名称不能为空")
    @ApiModelProperty(value = "省份名称")
    private String provinceName;

    @NotNull(message="城市名称不能为空")
    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @NotNull(message="区域名称不能为空")
    @ApiModelProperty(value = "区域名称")
    private String districtName;

    @NotNull(message="详细地址不能为空")
    @ApiModelProperty(value = "详细地址")
    private String address;

    @NotNull(message="省份编号不能为空")
    @ApiModelProperty(value = "省份编号")
    private String province;

    @NotNull(message="城市编号不能为空")
    @ApiModelProperty(value = "城市编号")
    private String city;

    @NotNull(message="区域编号不能为空")
    @ApiModelProperty(value = "区域编号")
    private String district;

    @NotNull(message="收货人姓名不能为空")
    @ApiModelProperty(value = "收货人姓名")
    private String name;

    @NotNull(message="收货人手机号不能为空")
    @ApiModelProperty(value = "收货人手机号")
    private String phone;

    @NotNull(message="默认地址不能为空")
    @ApiModelProperty(value = "是否默认地址", allowableValues = "Y,N")
    private String isDefault;

}
