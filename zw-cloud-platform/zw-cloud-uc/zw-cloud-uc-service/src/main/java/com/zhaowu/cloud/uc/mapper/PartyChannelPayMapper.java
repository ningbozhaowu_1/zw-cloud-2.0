package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.PartyChannelPay;

public interface PartyChannelPayMapper extends BaseMapper<PartyChannelPay> {
}
