package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.PartyChannelPay;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PartyChannelPayForm extends BaseForm<PartyChannelPay> {

    @NotNull(message="渠道编号不能为空")
    @ApiModelProperty("渠道编号")
    private String partyChannelId;

    @NotNull(message="支付编号不能为空")
    @ApiModelProperty("支付编号")
    private String partyPayId;
}
