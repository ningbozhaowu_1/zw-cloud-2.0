package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.uc.entity.po.UserChannel;
import com.zhaowu.cloud.uc.mapper.UserChannelMapper;
import com.zhaowu.cloud.uc.service.UserChannelService;
import org.springframework.stereotype.Service;

@Service
public class UserChannelServiceImpl extends ServiceImpl<UserChannelMapper, UserChannel> implements UserChannelService {

    @Override
    public void add(UserChannel userChannel) {
        this.save(userChannel);
    }

    @Override
    public UserChannel findUserChannelByOpenId(String openId, String applicationParty) {

        QueryWrapper<UserChannel> wrapper = new QueryWrapper<UserChannel>();
        wrapper.eq("open_id", openId);
        wrapper.eq("application_party", applicationParty);
        return this.getOne(wrapper);
    }
}
