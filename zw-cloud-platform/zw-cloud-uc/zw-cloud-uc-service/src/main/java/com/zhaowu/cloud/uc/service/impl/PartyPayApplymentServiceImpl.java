package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.po.PartyMediaAttachement;
import com.zhaowu.cloud.uc.entity.po.PartyPay;
import com.zhaowu.cloud.uc.entity.po.PartyPayApplyment;
import com.zhaowu.cloud.uc.entity.vo.PayApplymentVO;
import com.zhaowu.cloud.uc.mapper.PartyMediaAttachementMapper;
import com.zhaowu.cloud.uc.mapper.PartyPayApplymentMapper;
import com.zhaowu.cloud.uc.service.PartyPayApplymentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@Service
public class PartyPayApplymentServiceImpl  implements PartyPayApplymentService {
    @Resource
    private PartyPayApplymentMapper partyPayApplymentMapper;
    @Resource
    private PartyMediaAttachementMapper partyMediaAttachementMapper;

    @Override
    public void saveOrUpdate(PartyPayApplyment partyPayApplyment) {
        QueryWrapper<PartyPayApplyment> wrapper = new QueryWrapper<PartyPayApplyment>();
        wrapper.eq("business_code", partyPayApplyment.getBusinessCode());
        wrapper.eq("application_party", partyPayApplyment.getApplicationParty());
        PartyPayApplyment old = this.partyPayApplymentMapper.selectOne(wrapper);
        if(old !=null && StringUtils.isNotBlank(old.getId())){
            partyPayApplyment.setId(old.getId());
            this.partyPayApplymentMapper.updateById(partyPayApplyment);
        }else {
            this.partyPayApplymentMapper.insert(partyPayApplyment);
        }
    }

    @Override
    public PartyPayApplyment select(String businessCode,String applicationParty) {
        QueryWrapper<PartyPayApplyment> wrapper = new QueryWrapper<PartyPayApplyment>();
        wrapper.eq("business_code", businessCode);
        wrapper.eq("application_party", applicationParty);
        return this.partyPayApplymentMapper.selectOne(wrapper);
    }

    @Override
    public List<PayApplymentVO> list(String applicationParty) {
        return this.partyPayApplymentMapper.selectByApplicationParty(applicationParty);
    }

    @Override
    public void mediaSave(String mediaId, String fullUrlPath) {
        PartyMediaAttachement partyMediaAttachement = new PartyMediaAttachement();
        partyMediaAttachement.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
        partyMediaAttachement.setCreateBy(UserContextHolder.getInstance().getUserId());
        partyMediaAttachement.setCreateTime(new Date());
        partyMediaAttachement.setMediaId(mediaId);
        partyMediaAttachement.setFullUrlPath(fullUrlPath);
        this.partyMediaAttachementMapper.insert(partyMediaAttachement);
    }

    @Override
    public PartyMediaAttachement findMediaUrl(String mediaId) {
        QueryWrapper<PartyMediaAttachement> wrapper = new QueryWrapper<PartyMediaAttachement>();
        wrapper.eq("media_id", mediaId);
        wrapper.eq("application_party", UserContextHolder.getInstance().getApplicationParty());
        return this.partyMediaAttachementMapper.selectOne(wrapper);
    }

}
