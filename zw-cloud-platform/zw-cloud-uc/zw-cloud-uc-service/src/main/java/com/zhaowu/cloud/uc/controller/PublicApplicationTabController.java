package com.zhaowu.cloud.uc.controller;


import com.alibaba.fastjson.JSON;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.zhaowu.cloud.uc.entity.po.ApplicationTab;
import com.zhaowu.cloud.uc.entity.vo.ApplicationHtmlVO;
import com.zhaowu.cloud.uc.entity.vo.ApplicationTabVO;
import com.zhaowu.cloud.uc.entity.vo.ApplicationVO;
import com.zhaowu.cloud.uc.service.ApplicationTabService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 应用tab 控制器
 * </p>
 *
 * @author xxp
 * @since 2021-04-01
 */
@RestController
@RequestMapping("/public/applicationTab/v1")
@Api(tags = "应用tab 接口")
public class PublicApplicationTabController extends BaseController {

        @Autowired
        private ApplicationTabService applicationTabService;

        /**
         * 获取应用tab列表
         */
        @GetMapping(value = "/list")
        @ApiOperation("获取应用tab列表")
        public Result<List<ApplicationTabVO>> list(@Valid @RequestParam("applicationParty") String applicationParty) {

                List<ApplicationTab> applicationTabList = applicationTabService.getList(applicationParty);
                List<ApplicationTabVO> applicationHtmlVOList = JSON.parseArray(JSON.toJSONString(applicationTabList), ApplicationTabVO.class);
                return this.success(applicationHtmlVOList);
        }
}

