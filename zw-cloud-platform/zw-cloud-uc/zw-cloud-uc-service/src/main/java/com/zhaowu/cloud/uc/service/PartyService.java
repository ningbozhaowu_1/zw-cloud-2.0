package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.Party;

/**
 * <p>
 * 参与者 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PartyService{

    void add(Party party);

    @Cached(name="partyCache::", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Party selectById(String id);
}
