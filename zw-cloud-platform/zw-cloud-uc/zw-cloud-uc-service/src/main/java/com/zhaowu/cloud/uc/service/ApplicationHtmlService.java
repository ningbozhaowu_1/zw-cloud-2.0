package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.ApplicationHtml;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 应用页面DIY 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-03-18
 */
public interface ApplicationHtmlService{

    void add(ApplicationHtml applicationHtml);

    ApplicationHtml findById(String applicationParty, String id);

    @CacheInvalidate(name = "applicationHtmlCache::", key = "#applicationHtml.path + '::' + #applicationHtml.applicationParty")
    void delete(ApplicationHtml applicationHtml);

    @CacheInvalidate(name = "applicationHtmlCache::", key = "#applicationHtml.path + '::' + #applicationHtml.applicationParty")
    void modify(ApplicationHtml applicationHtml);

    List<ApplicationHtml> getList(String applicationParty);

    @Cached(name="applicationHtmlCache::", key = "#path + '::' + #applicationParty", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    ApplicationHtml getByPath(String path, String applicationParty);
}
