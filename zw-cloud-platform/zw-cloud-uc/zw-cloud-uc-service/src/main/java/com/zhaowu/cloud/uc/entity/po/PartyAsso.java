package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 参与者关联
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@Accessors(chain = true)
public class PartyAsso implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者编号
     */
    @TableField(value = "party_id", fill = FieldFill.INSERT)
    private String partyId;

    /**
     * 关联类型
     */
    @TableField("asso_type")
    private String assoType;

    /**
     * 关联参与者
     */
    @TableField("asso_party")
    private String assoParty;

    /**
     * 属性值
     */
    @TableField("asso_code")
    private String assoCode;

    /**
     * 状态
     */
    private String status;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;


}
