package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.uc.entity.param.EntPartyQueryParam;
import com.zhaowu.cloud.uc.entity.param.PartyApplyQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EntPartyQueryForm extends BaseQueryForm<EntPartyQueryParam> {

    @NotNull(message="查询类型不能为空")
    @ApiModelProperty(value = "申请类型 SHOP:商铺,ENT:企业用户", allowableValues = "SHOP,ENT")
    private String userType;

    @ApiModelProperty(value = "企业名称")
    private String name;

}