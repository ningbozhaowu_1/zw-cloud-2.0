package com.zhaowu.cloud.uc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.uc.entity.po.Party;
import com.zhaowu.cloud.uc.entity.po.PartyUser;

/**
 * <p>
 * 参与者 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PartyUserMapper extends BaseMapper<PartyUser> {

}
