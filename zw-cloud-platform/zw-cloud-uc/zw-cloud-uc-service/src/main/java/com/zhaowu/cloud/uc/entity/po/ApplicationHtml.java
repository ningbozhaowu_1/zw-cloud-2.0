package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 应用页面DIY
 * </p>
 *
 * @author xxp
 * @since 2021-03-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ApplicationHtml extends BasePo {

    private static final long serialVersionUID = 1L;

    @TableField("application_party")
    private String applicationParty;

    /**
     * 页面path
     */
    private String path;

    /**
     * 页面标题
     */
    private String title;

    /**
     * 页面名称
     */
    private String name;

    /**
     * 主题色
     */
    private String color;

    /**
     * 背景色
     */
    private String backColor;

    /**
     * 头部文字颜色
     */
    private String headTextColor;

    /**
     * tab标识
     */
//    private String isTab;

    /**
     * 数据
     */
    private String data;

}
