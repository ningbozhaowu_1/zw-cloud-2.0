package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
public class ShopQueryForm extends BaseQueryForm {

    @ApiModelProperty("应用编号 管理端不用送")
    private String applicationParty;

}
