package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.po.Account;
import com.zhaowu.cloud.uc.mapper.AccountMapper;
import com.zhaowu.cloud.uc.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 账户 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Override
    public void add(Account account) {
        this.save(account);
    }

    @Override
    public void modify(Account account) {
        this.updateById(account);
    }

    @Override
    public Account getByAcType(String acType, String partyId) {

        QueryWrapper<Account> wrapper = new QueryWrapper<Account>();
        wrapper.eq("party_id", partyId);
        wrapper.eq("ac_type", acType);
        return this.getOne(wrapper);
    }

    @Override
    public List<Account> getAccountList() {
        QueryWrapper<Account> wrapper = new QueryWrapper<Account>();
        wrapper.eq("party_id", UserContextHolder.getInstance().getPartyId());
        return this.list(wrapper);
    }
}
