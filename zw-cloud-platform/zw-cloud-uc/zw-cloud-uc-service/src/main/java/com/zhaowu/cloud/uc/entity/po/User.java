package com.zhaowu.cloud.uc.entity.po;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 用户表
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("users")
public class User extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 参与者编号
     */
    @TableField("party_id")
    private String partyId;

    /**
     * 应用参与者
     */
    @TableField("application_party")
    private String applicationParty;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户类型
     */
    @TableField("user_type")
    private String userType;

    /**
     * 用户状态
     */
    private String status;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 简介
     */
    private String reserve;

    /**
     * 头像
     */
    private String avatar;

}
