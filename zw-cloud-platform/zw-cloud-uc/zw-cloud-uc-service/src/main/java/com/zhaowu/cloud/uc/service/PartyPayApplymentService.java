package com.zhaowu.cloud.uc.service;

import com.zhaowu.cloud.uc.entity.po.PartyMediaAttachement;
import com.zhaowu.cloud.uc.entity.po.PartyPayApplyment;
import com.zhaowu.cloud.uc.entity.vo.PayApplymentVO;
import org.springframework.integration.support.management.LifecycleMessageSourceManagement;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 支付进件草稿服务
 * </p>
 *
 * @author shen
 * @since 2021-10-26
 */
public interface PartyPayApplymentService {

    void saveOrUpdate(PartyPayApplyment partyPayApplyment);

    PartyPayApplyment select(String businessCode,String applicationParty);
    List<PayApplymentVO> list(String applicationParty);

    void mediaSave(String mediaId, String fullUrlPath);

    PartyMediaAttachement findMediaUrl(String mediaId);

}
