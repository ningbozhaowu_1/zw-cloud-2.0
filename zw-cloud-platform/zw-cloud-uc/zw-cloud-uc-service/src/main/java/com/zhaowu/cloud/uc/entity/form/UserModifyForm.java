package com.zhaowu.cloud.uc.entity.form;

import com.zhaowu.cloud.common.web.entity.form.BaseForm;
import com.zhaowu.cloud.uc.entity.po.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class UserModifyForm extends BaseForm<User> {

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("用户头像")
    private String avatar;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("用户密码")
    private String password;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "验证码")
    private String mobileToken;
}
