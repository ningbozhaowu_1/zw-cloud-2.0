package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_media_attachement")
public class PartyMediaAttachement extends BasePo {
    private static final long serialVersionUID = 1L;
    /**
     * 当前应用ID
     */
    @TableField("application_party")
    private String applicationParty;
    /**
     * mediaId
     */
    @TableField("media_id")
    private String mediaId;

    /**
     * 文件路径
     */
    @TableField("full_url_path")
    private String fullUrlPath;
}
