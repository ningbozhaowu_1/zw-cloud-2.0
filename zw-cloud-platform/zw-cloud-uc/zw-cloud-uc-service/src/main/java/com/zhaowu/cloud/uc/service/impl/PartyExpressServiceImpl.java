package com.zhaowu.cloud.uc.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.PartyExpressFeeQueryForm;
import com.zhaowu.cloud.uc.entity.form.PartyExpressSettingForm;
import com.zhaowu.cloud.uc.entity.po.PartyAttr;
import com.zhaowu.cloud.uc.entity.form.GoodsFeeForm;
import com.zhaowu.cloud.uc.entity.vo.PartyExpressTemplateVo;
import com.zhaowu.cloud.uc.entity.form.UserAddressForm;
import com.zhaowu.cloud.uc.entity.vo.ValuationRuleVo;
import com.zhaowu.cloud.uc.exception.UcErrorStatus;
import com.zhaowu.cloud.uc.mapper.PartyAttrMapper;
import com.zhaowu.cloud.uc.service.PartyExpressService;
import com.zhaowu.cloud.uc.service.PartyExpressTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class PartyExpressServiceImpl implements PartyExpressService {

    @Autowired
    private PartyAttrMapper partyAttrMapper;
    @Autowired
    private PartyExpressTemplateService partyExpressTemplateService;

    @Transactional
    @Override
    public void setPartyAttr(PartyExpressSettingForm request) {
        String partyId = UserContextHolder.getInstance().getPartyId();

        //设置功能开关做检验
        if (request.getIsExpress()!=null||request.getIsLocal()!=null||request.getIsSelf()!=null) {
            //至少需要开启快递发货、同城配送、上门自提中的一种配送方式，店铺才能正常经营。
            QueryWrapper<PartyAttr> wrapper = new QueryWrapper<PartyAttr>();
            wrapper.eq("party_id", partyId);
            wrapper.eq("attr_type", CommonConstant.PartyAttrType.EXPRESS);
            List<PartyAttr> attrs = partyAttrMapper.selectList(wrapper);
            Boolean isExpress = false;
            Boolean isLocal = false;
            Boolean isSelf = false;

            if (attrs.size()==0) {
                isExpress = request.getIsExpress()!=null?request.getIsExpress():false;
                isLocal = request.getIsLocal()!=null?request.getIsLocal():false;
                isSelf = request.getIsSelf()!=null?request.getIsSelf():false;
            } else {
                for (PartyAttr attr: attrs ) {
                    if (CommonConstant.PartyAttrCodeEXPRESS.IS_EXPRESS.equals(attr.getAttrCode())) {
                        isExpress = Boolean.valueOf(attr.getAttrValue());
                    } else if (CommonConstant.PartyAttrCodeEXPRESS.IS_LOCAL.equals(attr.getAttrCode())) {
                        isLocal = Boolean.valueOf(attr.getAttrValue());
                    } else if (CommonConstant.PartyAttrCodeEXPRESS.IS_SELF.equals(attr.getAttrCode())) {
                        isSelf = Boolean.valueOf(attr.getAttrValue());
                    }
                }
                isExpress = request.getIsExpress()!=null?(request.getIsExpress()&&isExpress):isExpress;
                isLocal = request.getIsLocal()!=null?(request.getIsLocal()&&isLocal):isLocal;
                isSelf = request.getIsSelf()!=null?(request.getIsSelf()&&isSelf):isSelf;
            }
            if (!(isExpress||isLocal||isSelf)) {
                throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_ERROR.getCode(),
                        UcErrorStatus.INVALID_PARTY_EXPRESS_ERROR.getMessage());
            }
        }

        Map<String, String> map = null;
        try {
            map = BeanUtils.describe(request);
            map.remove("class");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("转换失败",e);
        }
        Iterator<Map.Entry<String, String>> it=map.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry<String, String> entry=it.next();
            if (entry.getValue()== null) {
                continue;
            }
            PartyAttr partyAttr = new PartyAttr();
            partyAttr.setPartyId(partyId);
            partyAttr.setAttrType(CommonConstant.PartyAttrType.EXPRESS);
            partyAttr.setAttrCode(entry.getKey());
            partyAttr.setAttrValue(entry.getValue());

            QueryWrapper<PartyAttr> wrapper = new QueryWrapper<PartyAttr>();
            wrapper.eq("party_id", partyId);
            wrapper.eq("attr_type", CommonConstant.PartyAttrType.EXPRESS);
            wrapper.eq("attr_code", entry.getKey());
            int count = partyAttrMapper.selectCount(wrapper);
            if (count!=0){
                partyAttrMapper.update(partyAttr, wrapper);
            }else {
                partyAttrMapper.insert(partyAttr);
            }
        }
    }

    @Override
    public PartyExpressSettingForm getPartyAttr() {
        String partyId = UserContextHolder.getInstance().getPartyId();
        QueryWrapper<PartyAttr> wrapper = new QueryWrapper<PartyAttr>();
        wrapper.eq("party_id", partyId);
        wrapper.eq("attr_type", CommonConstant.PartyAttrType.EXPRESS);
        List<PartyAttr> attrs = partyAttrMapper.selectList(wrapper);
        if (attrs.size()==0) {
            return null;
        }
        Map<String, Object> temp = new HashMap<String, Object>();
        for (PartyAttr attr:attrs) {
            temp.put(attr.getAttrCode(), attr.getAttrValue());
        }
        PartyExpressSettingForm settingRequest = new PartyExpressSettingForm();
        try {
            BeanUtils.populate(settingRequest, temp);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("转换失败", e);
        }

        return settingRequest;
    }

    @Override
    public BigDecimal queryPartyExpressFee(PartyExpressFeeQueryForm request) {
        if (request.getPartyId()==null) {
            log.info("partyId为空");
            request.setPartyId(UserContextHolder.getInstance().getPartyId());
        }

        //获取party的计费方式: 1 按商品累加运费 2 组合运费（推荐使用）
        QueryWrapper<PartyAttr> wrapper = new QueryWrapper<PartyAttr>();
        wrapper.eq("party_id", request.getPartyId());
        wrapper.eq("attr_type", CommonConstant.PartyAttrType.EXPRESS);
        wrapper.eq("attr_code", CommonConstant.PartyAttrCodeEXPRESS.CALC_TYPE);
        String calcType = partyAttrMapper.selectOne(wrapper).getAttrValue();
//        calcType = "2";
        if ("1".equals(calcType)) {
            return calculateFeeByTotal(request.getPartyId(), request.getGoodList(), request.getAddress());
        } else if ("2".equals(calcType)) {
            return calculateFeeByGroup(request.getPartyId(), request.getGoodList(), request.getAddress());
        }
        return null;
    }
    /**
         1.1 按商品累加运费
         说明：先将使用统一运费和运费模板的商品分开计算，将两者累加，作为最终运费。
         - 统一运费取最大值：A、B、C 统一运费分别为1元，2元和3元，同时购买A+B+C，则统一运费为：3元。
         - 运费模板：分别计算每个运费模板规则的应收运费（从首费开始），再累加计算合计运费。
         商品名称   快递运费设置                           购买数量（商品重量/件数）   运费结算金额
         苹果1     运费模板：首重：2元/1kg、续重：1元/1kg    1.4kg                   最后金额：10元 = 3元（模板）+5元（模板）+2元（统一运费）
         苹果2     运费模板：首件：5元/1件，续件：3元/1件     1件
         苹果3     统一运费：2元                          1件
         1.获取统一运费最大值
         2.计算每个商品运费模板对应的运费
         3.运费 = 统一运费最大值 + 每个商品运费模板对应的运费
     * @param partyId
     * @param goodVos
     * @param addressVo
     * @return
     */
    private BigDecimal calculateFeeByTotal(String partyId, List<GoodsFeeForm> goodVos, UserAddressForm addressVo) {
        //最后需支付的费用
        BigDecimal fee = BigDecimal.ZERO;
        //统一运费最大值
        BigDecimal maxUnionFee = BigDecimal.ZERO;

        for (GoodsFeeForm goodVo : goodVos) {
            //计费方式 1按件数 2按重量 3统一运费
            if ("Y".equals(goodVo.getIsUnionFee())) {
                if (StringUtils.isEmpty(goodVo.getUnionFee())) {
                    log.info(goodVo.toString());
                    throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_CALCULATE_ERROR.getCode(),
                            "商品：" + goodVo.getGoodsName() + "的统一运费不能为空");
                }
                maxUnionFee = maxUnionFee.max(new BigDecimal(goodVo.getUnionFee()));
            } else {
                //查询商品的快递模板信息
                if (StringUtils.isEmpty(goodVo.getTemplateId())) {
                    log.info(goodVo.toString());
                    throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_CALCULATE_ERROR.getCode(),
                            "商品：" + goodVo.getGoodsName() + "的运费模板不能为空");
                }
                PartyExpressTemplateVo templateVo =
                        partyExpressTemplateService.queryPartyExpressTemplate(partyId, goodVo.getTemplateId());
                int valuationRulesIndex = -1;
                //查询地址对应的计费规则
                Map<String, Integer> province = templateVo.getRegionRules().get("province");
                Map<String, Integer> city = templateVo.getRegionRules().get("city");
                Map<String, Integer> county = templateVo.getRegionRules().get("county");
                if (province.containsKey(addressVo.getProvince() + "")) {
                    valuationRulesIndex = province.get(addressVo.getProvince()).intValue();
                } else if (city.containsKey(addressVo.getCity() + "")) {
                    valuationRulesIndex = city.get(addressVo.getCity()).intValue();
                } else if (county.containsKey(addressVo.getCounty() + "")) {
                    valuationRulesIndex = county.get(addressVo.getCounty()).intValue();
                } else {
                    log.info(goodVo.toString());
                    log.info(addressVo.toString());
                    throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_CALCULATE_ERROR.getCode(),
                            "商品：" + goodVo.getGoodsName() + "无客户地址对应的配送区域信息");
                }

                ValuationRuleVo valuationRuleVo = templateVo.getValuationRules().get(valuationRulesIndex);
                log.info("区域规则：" + valuationRuleVo.toString());
                //数量 1000=1kg =1个
                BigDecimal count = BigDecimal.ZERO;
                if("2".equals(templateVo.getValuationType())){//重量
                    count = new BigDecimal(goodVo.getWeight());
                }
                if("1".equals(templateVo.getValuationType())){
                    count = new BigDecimal(goodVo.getCount());
                }
                //该商品的运费 首件 + 续件
                BigDecimal feeForGood = new BigDecimal(valuationRuleVo.getFirstFee());
                //首重
                BigDecimal firstAmount = new BigDecimal(valuationRuleVo.getFirstAmount());
                if (count.compareTo(firstAmount)>0) {
                    //(总量-首重)/续重*续费
                    feeForGood = ((count.subtract(firstAmount)).divide(new BigDecimal(valuationRuleVo.getAdditionalAmount()), RoundingMode.UP)
                            .multiply(new BigDecimal(valuationRuleVo.getAdditionalFee())).add(feeForGood));
                }
                log.info(goodVo.toString() + "的运费为：" + feeForGood.toString());
                fee = fee.add(feeForGood);
            }
        }

        fee = fee.add(maxUnionFee);
        return fee;
    }

    /**
        2.1 组合运费（推荐使用）
        说明：先将使用统一运费和运费模板的商品分开计算，再取二者较大的值，作为最终运费。
        - 统一运费取最大值：A、B、C 统一运费分别为1元，2元和3元，同时购买A+B+C，则统一运费为：3元。
        - 运费模板运费：x = x1首费最贵的模板（首费+续费） + x2其他模板（从续费开始算）
        商品名称    快递运费设置                           购买数量（商品重量/件数）   运费结算金额
        苹果1      运费模板：首重：2元/1kg、续重：1元/1kg    1.4kg                   最后金额：7元 = 5元（首费）+1元*2（续费）
        苹果2      运费模板：首件：5元/1件，续件：3元/1件     1件
        苹果3      统一运费：2元                          1件
        1.获取统一运费最大值
        2.获取商品中的首费最贵的模板（首费+续费）
        3.计算其他商品运费模板对应的运费（从续费开始算）
        4.运费 = 统一运费最大值 + 商品中的首费最贵的模板（首费+续费） + 其他商品运费模板对应的运费（从续费开始算）
     * @param partyId
     * @param goodVos
     * @param addressVo
     * @return
     */
    private BigDecimal calculateFeeByGroup(String partyId, List<GoodsFeeForm> goodVos, UserAddressForm addressVo) {
        //最后需支付的费用
        BigDecimal fee = BigDecimal.ZERO;
        //统一运费最大值
        BigDecimal maxUnionFee = BigDecimal.ZERO;
        //商品中的首费最贵的模板
        ValuationRuleVo maxValuationRuleVo = null;
        //商品中的首费最贵的模板的首费+续费
        BigDecimal maxFirstFeeAmount = BigDecimal.ZERO;
        //商品中的首费最贵的模板对应的商品的数量
        BigDecimal maxFeeForGood = BigDecimal.ZERO;
        //是否需要减去首重
        boolean flag = false;

        for (GoodsFeeForm goodVo : goodVos) {
            //计费方式 1按件数 2按重量 3统一运费
            if ("Y".equals(goodVo.getIsUnionFee())) {
                if (StringUtils.isEmpty(goodVo.getUnionFee())) {
                    log.info(goodVo.toString());
                    throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_CALCULATE_ERROR.getCode(),
                            "商品：" + goodVo.getGoodsName() + "的统一运费不能为空");
                }
                maxUnionFee = maxUnionFee.max(new BigDecimal(goodVo.getUnionFee()));
            } else {
                //查询商品的快递模板信息
                if (StringUtils.isEmpty(goodVo.getTemplateId())) {
                    log.info(goodVo.toString());
                    throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_CALCULATE_ERROR.getCode(),
                            "商品：" + goodVo.getGoodsName() + "的运费模板不能为空");
                }
                PartyExpressTemplateVo templateVo =
                        partyExpressTemplateService.queryPartyExpressTemplate(partyId, goodVo.getTemplateId());
                int valuationRulesIndex = -1;
                //查询地址对应的计费规则
                Map<String, Integer> province = templateVo.getRegionRules().get("province");
                Map<String, Integer> city = templateVo.getRegionRules().get("city");
                Map<String, Integer> county = templateVo.getRegionRules().get("county");
                if (province.containsKey(addressVo.getProvince() + "")) {
                    valuationRulesIndex = province.get(addressVo.getProvince()).intValue();
                } else if (city.containsKey(addressVo.getCity() + "")) {
                    valuationRulesIndex = city.get(addressVo.getCity()).intValue();
                } else if (county.containsKey(addressVo.getCounty() + "")) {
                    valuationRulesIndex = county.get(addressVo.getCounty()).intValue();
                } else {
                    log.info(goodVo.toString());
                    log.info(addressVo.toString());
                    throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_CALCULATE_ERROR.getCode(),
                            "商品：" + goodVo.getGoodsName() + "无客户地址对应的配送区域信息");
                }

                ValuationRuleVo valuationRuleVo = templateVo.getValuationRules().get(valuationRulesIndex);
                log.info("区域规则：" + valuationRuleVo.toString());
                //数量 1000=1kg =1个
                BigDecimal count = BigDecimal.ZERO;
                if("2".equals(templateVo.getValuationType())){//重量
                    count = new BigDecimal(goodVo.getWeight());
                }
                if("1".equals(templateVo.getValuationType())){
                    count = new BigDecimal(goodVo.getCount());
                }
                //数量/续重*续费
                BigDecimal feeForGood = count
                        .divide(new BigDecimal(valuationRuleVo.getAdditionalAmount()), RoundingMode.UP)
                        .multiply(new BigDecimal(valuationRuleVo.getAdditionalFee()));
                log.info(goodVo.toString() + "的运费为：" + feeForGood.toString());
                //比较首费+续费
                BigDecimal firstFeeAmount = new BigDecimal(valuationRuleVo.getFirstFee())
                        .add(new BigDecimal(valuationRuleVo.getAdditionalFee()));
                if (firstFeeAmount.compareTo(maxFirstFeeAmount)>0) {
                    maxValuationRuleVo = valuationRuleVo;
                    maxFirstFeeAmount = firstFeeAmount;
                    flag = count.compareTo(new BigDecimal(valuationRuleVo.getFirstAmount()))>0;
                    maxFeeForGood = feeForGood;
                }
                fee = fee.add(feeForGood);
            }
        }
        //首重
        BigDecimal firstAmount = new BigDecimal(maxValuationRuleVo.getFirstAmount());
        //续重
        BigDecimal additionalAmount = new BigDecimal(maxValuationRuleVo.getAdditionalAmount());
        //续费
        BigDecimal additionalFee = new BigDecimal(maxValuationRuleVo.getAdditionalFee());

        BigDecimal temp = null;
        if (flag) {
            temp = firstAmount.divide(additionalAmount, RoundingMode.DOWN).multiply(additionalFee);
        } else {
            temp = maxFeeForGood;
        }

        fee = fee.add(new BigDecimal(maxValuationRuleVo.getFirstFee())).subtract(temp);
        log.info("运费模板的商品的运费为：" + fee.toString() + ", 统一费用的商品的运费为：" + maxUnionFee.toString());
        if (maxUnionFee.compareTo(fee)>0) {
            fee = maxUnionFee;
        }
        return fee;
    }

}
