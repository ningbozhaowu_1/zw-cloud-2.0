package com.zhaowu.cloud.uc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.util.FastJsonUtils;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.uc.entity.form.PartyExpressTemplateSetForm;
import com.zhaowu.cloud.uc.entity.param.PartyExpressTemplateQueryParam;
import com.zhaowu.cloud.uc.entity.po.PartyExpressTemplate;
import com.zhaowu.cloud.uc.entity.vo.PartyExpressTemplateVo;
import com.zhaowu.cloud.uc.entity.vo.ValuationRuleVo;
import com.zhaowu.cloud.uc.exception.UcErrorStatus;
import com.zhaowu.cloud.uc.mapper.PartyExpressTemplateMapper;
import com.zhaowu.cloud.uc.service.PartyExpressTemplateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 参与者快递模版 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Service
public class PartyExpressTemplateServiceImpl extends ServiceImpl<PartyExpressTemplateMapper,PartyExpressTemplate>
        implements PartyExpressTemplateService {

    @Override
    public void setPartyExpressTemplate(PartyExpressTemplateSetForm request) {
        String partyId = UserContextHolder.getInstance().getPartyId();
        PartyExpressTemplate template = new PartyExpressTemplate();
        template.setPartyId(partyId);
        template.setIsFree(request.getIsFreeDelivery());
        template.setName(request.getName());
        template.setValuationType(request.getValuationType());

        //获取区域规则
        Map<String, Map<String, Integer>> regionRulesMap = new HashMap<String, Map<String, Integer>>();
        Map<String, Integer> province = new HashMap<String, Integer>();
        Map<String, Integer> city = new HashMap<String, Integer>();
        Map<String, Integer> county = new HashMap<String, Integer>();

        regionRulesMap.put("province", province);
        regionRulesMap.put("city", city);
        regionRulesMap.put("county", county);

        int size = request.getValuationRules().size();
        for (int i=0;i<size;i++) {
            ValuationRuleVo valuationRuleVo = request.getValuationRules().get(i);
            for (String region : valuationRuleVo.getRegions()) {
                if (region.endsWith("0000")) {
                    province.put(region, i);
                } else if (region.endsWith("00")) {
                    city.put(region, i);
                } else {
                    county.put(region, i);
                }
            }
        }
        String regionRules = FastJsonUtils.map2json(regionRulesMap);
        template.setRegionRules(regionRules);

        String valuationRules = FastJsonUtils.object2json(request.getValuationRules());
        template.setValuationRules(valuationRules);

        this.save(template);
    }

    @Override
    public void deletePartyExpressTemplate(String templateId) {
        String partyId = UserContextHolder.getInstance().getPartyId();
        PartyExpressTemplate partyExpressTemplate = this.getById(templateId);
        if (!partyExpressTemplate.getPartyId().equals(partyId)) {
            throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_PARTY_ERROR.getCode(),
                    UcErrorStatus.INVALID_PARTY_EXPRESS_PARTY_ERROR.getMessage());
        }
        if (partyExpressTemplate.getUseCount()>0) {
            throw new ServiceException(UcErrorStatus.INVALID_PARTY_EXPRESS_TEMPLATE_USED_ERROR.getCode(),
                    UcErrorStatus.INVALID_PARTY_EXPRESS_TEMPLATE_USED_ERROR.getMessage());
        }
        this.removeById(templateId);
    }

    @Override
    public IPage<PartyExpressTemplateVo> queryPartyExpressTemplate(BaseQueryForm<PartyExpressTemplateQueryParam> queryForm) {
        String partyId = UserContextHolder.getInstance().getPartyId();
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        QueryWrapper<PartyExpressTemplate> wrapper = new QueryWrapper<>();
        wrapper.eq("party_id", partyId);
        IPage<PartyExpressTemplate> page = this.page(queryForm.getPage(), wrapper);
        List<PartyExpressTemplateVo> list = new ArrayList<>();
        for (PartyExpressTemplate template: page.getRecords()) {
            PartyExpressTemplateVo vo = mapper.map(template, PartyExpressTemplateVo.class);
            vo.setRegionRules(FastJsonUtils.json2map(template.getRegionRules()));
            vo.setValuationRules(FastJsonUtils.json2list(template.getValuationRules(), ValuationRuleVo.class));
            list.add(vo);
        }

        IPage<PartyExpressTemplateVo> page2 = mapper.map(page, IPage.class);
        page2.setRecords(list);
        return page2;
    }

    @Override
    public PartyExpressTemplateVo queryPartyExpressTemplate(String partyId, String templateId) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        QueryWrapper<PartyExpressTemplate> wrapper = new QueryWrapper<>();
        wrapper.eq("party_id", partyId);
        wrapper.eq("id", templateId);
        PartyExpressTemplate template = this.getOne(wrapper);

        if (template == null) {
            return null;
        }

        PartyExpressTemplateVo vo = mapper.map(template, PartyExpressTemplateVo.class);
        vo.setRegionRules(FastJsonUtils.json2map(template.getRegionRules()));
        vo.setValuationRules(FastJsonUtils.json2list(template.getValuationRules(), ValuationRuleVo.class));

        return vo;
    }
}
