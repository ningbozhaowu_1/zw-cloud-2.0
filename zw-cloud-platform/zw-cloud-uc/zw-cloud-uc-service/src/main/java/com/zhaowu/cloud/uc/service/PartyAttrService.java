package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.PartyAttr;
import com.zhaowu.cloud.uc.entity.po.PartyUser;

import java.util.List;

/**
 * <p>
 * 参与者属性 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-20
 */
public interface PartyAttrService {

    @CacheInvalidate(name = "partyAttrListCache::", key = "#partyAttr.partyId+ '::' + #partyAttr.attrType")
    void add(PartyAttr partyAttr);

    PartyAttr findById(String partyId, String id);

    @CacheInvalidate(name = "partyAttrListCache::", key = "#partyAttr.partyId + '::' + #partyAttr.attrType")
    void modify(PartyAttr partyAttr);

    @CacheInvalidate(name = "partyAttrListCache::", key = "#partyAttr.partyId+ '::' + #partyAttr.attrType")
    void delete(PartyAttr partyAttr);

    @Cached(name="partyAttrListCache::", key = "#partyId + '::' + #attrType", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    List<PartyAttr> getListByAttrType(String partyId, String attrType);
}
