package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 参与者收货地址
 * </p>
 *
 * @author xxp
 * @since 2021-02-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class PartyShipAddress extends BasePo {

    private static final long serialVersionUID = 1L;

    @TableField(value = "party_id", fill = FieldFill.INSERT)
    private String partyId;

    private String provinceName;

    private String cityName;

    private String districtName;

    private String address;

    private String province;

    private String city;

    private String district;

    private String name;

    private String phone;

    private String isDefault;
}
