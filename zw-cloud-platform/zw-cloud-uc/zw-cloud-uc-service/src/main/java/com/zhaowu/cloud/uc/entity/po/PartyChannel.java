package com.zhaowu.cloud.uc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 参与者渠道
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_channel")
public class PartyChannel extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 参与者编号
     */
    @TableField(value = "party_id", fill = FieldFill.INSERT)
    private String partyId;

    /**
     * 渠道appid 外部应用编号
     */
    @TableField("authorizer_app_id")
    private String authorizerAppId;

    /**
     * 渠道类型 微信小程序支付宝小程序等
     */
    private String type;

    private String status;

    /**
     * 名称
     */
    private String name;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 名称
     */
    private String avatar;

    private String secret;

    private String token;

    private String aesKey;

    @TableField("auth_code")
    private String authCode;

    @TableField("access_token")
    private String accessToken;

    @TableField("refresh_token")
    private String refreshToken;
}
