package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 参与者快递设置
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@ApiModel
public class PartyExpressSettingForm {

    /**快递发货功能是否开启*/
    @ApiModelProperty("快递发货功能是否开启")
    private Boolean isExpress;
    /**同城配送功能是否开启*/
    @ApiModelProperty("同城配送功能是否开启")
    private Boolean isLocal;
    /**同城配送定时达功能是否开启*/
    @ApiModelProperty("同城配送定时达功能是否开启")
    private Boolean isLocalInTime;
    /**买家上门自提功能是否开启*/
    @ApiModelProperty("买家上门自提功能是否开启")
    private Boolean isSelf;
    /**计费方式: 1 按商品累加运费 2 组合运费（推荐使用）*/
    @ApiModelProperty("计费方式: 1 按商品累加运费 2 组合运费（推荐使用）")
    private String calcType;

}
