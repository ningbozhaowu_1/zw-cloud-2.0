package com.zhaowu.cloud.uc.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.uc.entity.po.ApplicationTab;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 应用tab 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-04-01
 */
public interface ApplicationTabService{

    List<ApplicationTab> getList();

    @Cached(name="applicationTabCache::", key = "#applicationParty", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    List<ApplicationTab> getList(String applicationParty);

    @CacheInvalidate(name = "applicationTabCache::", key = "#applicationTab.applicationParty")
    void add(ApplicationTab applicationTab);

    @CacheInvalidate(name = "applicationTabCache::", key = "#applicationParty")
    void delete(String applicationParty, String id);

    @CacheInvalidate(name = "applicationTabCache::", key = "#applicationTab.applicationParty")
    void modify(ApplicationTab applicationTab);
}
