package com.zhaowu.cloud.uc.entity.po;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 参与者渠道申请记录
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("party_apply_info")
public class PartyApplyInfo extends BasePo {

    private static final long serialVersionUID = 1L;
    /**
     * 参与者编号
     */
    @TableField(value = "application_party", fill = FieldFill.INSERT)
    private String applicationParty;

    /**
     * 申请人参与者编号
     */
    @TableField(value = "user_party", fill = FieldFill.INSERT)
    private String userParty;

    /**
     * 参与者渠道编号
     */
    @TableField(value = "party_channel_id", fill = FieldFill.INSERT)
    private String partyChannelId;

    @TableField("apply_type")
    private String applyType;
    /**
     * 企业名称
     */
    private String name;

    /**
     * 企业代码
     */
    private String code;

    /**
     * 企业代码类型
     * 1：统一社会信用代码（18 位）
     * 2：组织机构代码（9 位 xxxxxxxx-x）
     * 3：营业执照注册号(15 位)
     */
    @TableField("code_type")
    private String codeType;

    /**
     * 法人微信号
     */
    @TableField("legal_persona_wechat")
    private String legalPersonaWechat;

    /**
     * 法人姓名
     */
    @TableField("legal_persona_name")
    private String legalPersonaName;

    /**
     * 联系电话
     */
    @TableField("component_phone")
    private String componentPhone;

    /**
     * 申请状态
     */
    private String status;

    private String remark;

}
