package com.zhaowu.cloud.uc.entity.po;

import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


/**
 * <p>
 * 参与者快递运费规则
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ExpressTemplateRule extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者编号
     */
    private String expressTemplateId;

    /**
     * 起始数量
     */
    private BigDecimal firstAmount;

    /**
     * 起始价格
     */
    private BigDecimal firstFee;

    /**
     * 超出数量
     */
    private BigDecimal additionalAmount;

    /**
     * 超出价格
     */
    private BigDecimal additionalFee;

}
