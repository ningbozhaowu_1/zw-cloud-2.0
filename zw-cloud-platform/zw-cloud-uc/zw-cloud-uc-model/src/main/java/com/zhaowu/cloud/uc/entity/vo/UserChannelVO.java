package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
public class UserChannelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyChannelId;

    private String openId;

    private String type;
}
