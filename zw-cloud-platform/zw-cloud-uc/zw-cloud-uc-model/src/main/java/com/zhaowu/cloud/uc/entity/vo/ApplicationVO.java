package com.zhaowu.cloud.uc.entity.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xxp
 **/
@Data
public class ApplicationVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String partyId;

    private String parentId;

    private String name;

    private String status;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date effectiveTime;



    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date expireTime;

    /**
     * 版本名称
     */
    private String versionName;

    /**
     * 版本
     */
    private String version;

    private String logo;

    private String webPage;
}
