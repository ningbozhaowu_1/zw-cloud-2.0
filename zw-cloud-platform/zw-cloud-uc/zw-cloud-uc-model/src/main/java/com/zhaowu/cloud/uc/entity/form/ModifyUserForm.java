package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class ModifyUserForm implements Serializable {

    @Size(max = 256,message = "用户名必须在{max}个字符内")
    @ApiModelProperty(value = "用户名")
    private String username;

    @Size(max = 20,message = "真实姓名必须在{max}个字符内")
    @ApiModelProperty(value = "真实姓名")
    private String name;

    @Email(message = "邮箱格式错误")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Size(max = 2048,message = "头像必须在{max}个字符内")
    @ApiModelProperty(value = "头像")
    private String avatar;

    @Size(max = 255,message = "语言必须在{max}个字符内")
    @ApiModelProperty(value = "语言")
    private String language;

    @Size(max = 255,message = "省份必须在{max}个字符内")
    @ApiModelProperty(value = "省份")
    private String province;

    @Size(max = 255,message = "国家必须在{max}个字符内")
    @ApiModelProperty(value = "国家")
    private String country;

    @Max(value = 2,message = "性别必须在0,1,2之间")
    @ApiModelProperty(value = "性别")
    private int sex;

    @Size(max = 64,message = "昵称必须在{max}个字符内")
    @ApiModelProperty(value = "昵称")
    private String nickName;

    @Size(max = 255,message = "城市必须在{max}个字符内")
    @ApiModelProperty(value = "城市")
    private String city;
}
