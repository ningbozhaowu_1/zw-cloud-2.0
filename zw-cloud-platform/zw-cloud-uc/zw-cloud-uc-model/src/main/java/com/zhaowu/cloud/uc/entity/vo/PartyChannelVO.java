package com.zhaowu.cloud.uc.entity.vo;


import lombok.Data;

import java.io.Serializable;

/**
 * @author xxp
 **/
@Data
public class PartyChannelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String partyId;

    private String authorizerAppId;

    private String name;

    private String introduction;

    private String avatar;

    private String type;

    private String secret;

    private String token;

    private String aesKey;

    private String accessToken;

    private String refreshToken;

    private String authToken;

}
