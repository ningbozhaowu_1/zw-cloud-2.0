package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

@Data
public class PartyChannelQueryForm {

    private String partyChannelId;

    private String applicationParty;

    private String authorizerAppId;
}
