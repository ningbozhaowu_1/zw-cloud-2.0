package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <p>
 * 提货点
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@Accessors(chain = true)
public class PartyPickUpVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自提点编号
     */
    private String id;

    /**
     * 自提点名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String componentPhone;
    /**
     * 是否门店
     */
    private String isShop;

    /**
     * 开门时间
     */
    private String openTime;

    /**
     * 关门时间
     */
    private String closeTime;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 街道
     */
    private String district;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    private BigDecimal distance;

}
