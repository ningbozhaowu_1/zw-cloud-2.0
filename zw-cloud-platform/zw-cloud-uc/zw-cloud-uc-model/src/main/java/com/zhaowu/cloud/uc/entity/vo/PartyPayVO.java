package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PartyPayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String merchantId;

    private String appId;

    private String applicationParty;

    private String applicationChannelId;

    private String merchantKey;

    private String merchantCertKey;

    private String merchantType;
}
