package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

@Data
public class PartyPayQueryForm {

    private String applicationParty;

    private String applicationChannelId;

    private String mercahtType;

    private String merchantId;
}
