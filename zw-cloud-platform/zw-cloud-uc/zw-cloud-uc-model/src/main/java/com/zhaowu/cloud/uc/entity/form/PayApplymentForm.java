package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

import java.io.Serializable;

@Data
public class PayApplymentForm implements Serializable {

    /**
     * 业务申请编号
     */
    private String businessCode;

    /**
     * 商户名称
     */
    private String merchantName;

    /**
     * 微信支付分配的申请单号
     */
    private String applymentId;
    /**
     * 申请状态
     */
    private Integer status;

    /**
     * 超级管理员信息
     */
    private String contactInfo;

    /**
     * 主体资料
     */
    private String subjectInfo;

    /**
     * 经营资料
     */
    private String businessInfo;

    /**
     * 结算规则
     */
    private String settlementInfo;

    /**
     * 结算银行账户
     */
    private String bankAccountInfo;

    /**
     * 补充材料
     */
    private String additionInfo;
}
