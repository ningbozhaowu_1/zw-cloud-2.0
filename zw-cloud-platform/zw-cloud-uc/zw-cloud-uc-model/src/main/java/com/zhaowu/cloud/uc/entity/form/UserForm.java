package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class UserForm implements Serializable {

    @NotNull(message = "编号不能为空")
    @Size(max = 20,message = "编码长度必须在{max}个字符内")
    private String code;

    @NotNull(message = "编号不能为空")
    @Size(max = 50,message = "名称长度必须在{max}个字符内")
    private String name;

    @Size(max = 2000,message = "描述长度必须在{max}个字符内")
    private String description;
}
