package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 参与者快递费查询
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@ApiModel
public class PartyExpressFeeQueryForm {
    /**商品信息*/
    @NotNull(message = "商品信息不能为空")
    @ApiModelProperty("商品信息")
    private List<GoodsFeeForm> goodList;
    /**参与者id*/
    @ApiModelProperty("参与者id")
    private String partyId;
    /**地址信息*/
    @NotNull(message = "地址信息不能为空")
    @ApiModelProperty("地址信息")
    private UserAddressForm address;
}
