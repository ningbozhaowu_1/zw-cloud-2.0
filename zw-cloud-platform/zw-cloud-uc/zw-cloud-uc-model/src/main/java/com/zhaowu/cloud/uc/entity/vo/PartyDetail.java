package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PartyDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyId;

    private String partyName;

    /**
     * 联系人
     */
    private String contactName;

    /**
     * 联系人联系方式
     */
    private String contactContact;

    /**
     * 备用联系方式1
     */
    private String contactContact1;

    /**
     * 联系方式2
     */
    private String contactContact2;

    /**
     * 联系人联系地址
     */
    private String contactAddr;

    /**
     * 企业性质
     */
    private String characters;

    /**
     * 企业类型
     */
    private String industry;

    /**
     * 企业logo
     */
    private String logo;

    /**
     * 主页
     */
    private String webPage;

    private String homePage;

    /**
     * 企业地址
     */
    private String orgAddr;

    private String mail;

    private String contactWechat;
}
