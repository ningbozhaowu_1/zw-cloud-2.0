package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApplicationHtmlVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String applicationParty;

    /**
     * 页面path
     */
    private String path;

    /**
     * 页面标题
     */
    private String title;

    /**
     * 页面名称
     */
    private String name;

    /**
     * 主题色
     */
    private String color;

    /**
     * 背景色
     */
    private String backColor;

    /**
     * 头部文字颜色
     */
    private String headTextColor;

    /**
     * tab标识
     */
//    private String isTab;

    /**
     * 数据
     */
    private String data;
}
