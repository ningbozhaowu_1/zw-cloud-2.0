package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

import java.util.Collection;

@Data
public class BatchUserQueryForm {

    private Collection<String> userIds;

    private Collection<String> mobileNos;

    /**
     * 为空时返回所有渠道
     */
    private String partyChannelId;

}
