package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

@Data
public class PartyChannelUpdateForm {
    private String partyChannelId;

    private String applicationParty;

    private String authorizerAppId;

    private String introduction;
}
