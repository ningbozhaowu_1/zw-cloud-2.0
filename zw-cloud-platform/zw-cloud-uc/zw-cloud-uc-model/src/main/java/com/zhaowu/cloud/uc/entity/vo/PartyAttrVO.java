package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PartyAttrVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyId;

    /**
     * 属性类型 功能开关等
     */
    private String attrType;

    /**
     * 属性code
     */
    private String attrCode;

    /**
     * 属性值
     */
    private String attrValue;

    /**
     * 属性描述
     */
    private String attrDesc;
}
