package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

@Data
public class UserQueryForm {

    private String mobileNo;

    private String openId;

    private String applicationParty;

    private String partyChannelId;

    private String userId;

    private String username;

    private String scene;
}
