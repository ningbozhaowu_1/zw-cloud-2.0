package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApplicationTabVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String applicationParty;

    /**
     * 页面编号
     */
    private String applicationHtmlId;


    private String path;


    /**
     * 顺序
     */
    private Integer ordNum;

    /**
     * 是否显示
     */
    private String isShow;

    /**
     * tab名称
     */
    private String name;

    private String nameChoosed;

    private String icon;

    private String iconChoosed;
}
