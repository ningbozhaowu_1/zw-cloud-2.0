package com.zhaowu.cloud.uc.entity.form;

import lombok.Data;

@Data
public class PartyChannelAuthorizeForm {

    private String codeType;

    private String code;

    private String authorizerAppid;

    private String accessToken;

    private String refreshToken;

    private String authCode;

    private String applicationParty;

    private String type;

    private String nickName;

    private String introduction;

    private String avatar;
}
