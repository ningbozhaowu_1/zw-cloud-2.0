package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class PartyShipAddressVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyId;

    private String provinceName;

    private String cityName;

    private String districtName;

    private String address;

    private String province;

    private String city;

    private String district;

    private String name;

    private String phone;

    private String isDefault;
}
