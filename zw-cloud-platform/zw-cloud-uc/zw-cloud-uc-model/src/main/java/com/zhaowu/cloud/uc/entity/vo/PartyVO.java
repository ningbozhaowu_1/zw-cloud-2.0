package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PartyVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyId;

    private String partyName;

    private OrganizationVO organizationVO;

    private UserVO userVO;
}
