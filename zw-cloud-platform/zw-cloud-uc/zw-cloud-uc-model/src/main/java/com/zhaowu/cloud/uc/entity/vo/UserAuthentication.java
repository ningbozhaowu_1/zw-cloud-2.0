package com.zhaowu.cloud.uc.entity.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

/**
 * 认证主体
 *
 * @author xxp
 */
@Data
public class UserAuthentication implements Serializable {

    private String id;

    private String partyId;

    private String username;

    private String password;

    private String mobile;

    private String status;

    private String applicationParty;

    private String openId;

    private Integer pwdErrorCount;

    private String merchantPartyId;

    private Collection<String> roleIds;
}
