package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 地址
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@ToString
public class UserAddressForm {
    /**省份编码*/
    @NotNull(message = "省份编码不能为空")
    @ApiModelProperty("省份编码")
    private String province;
    /**城市编码*/
    @NotNull(message = "城市编码不能为空")
    @ApiModelProperty("城市编码")
    private String city;
    /**区域编码*/
    @NotNull(message = "区域编码不能为空")
    @ApiModelProperty("区域编码")
    private String county;
    /**街道编码*/
    @ApiModelProperty("街道编码")
    private String street;
    /**详细地址*/
    @NotNull(message = "详细地址不能为空")
    @ApiModelProperty("详细地址")
    private String address;
}
