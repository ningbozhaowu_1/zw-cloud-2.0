package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class PartyChannelForm {

    @NotNull(message="渠道类型不能为空")
    @Size(max= 100, message="渠道类型长度必须在{max}个字符内")
    @ApiModelProperty("渠道类型 wecahtMA:微信小程序")
    private String type;

    @ApiModelProperty("企业名")
    private String name;

//    @NotNull(message="应用参与者编号不能为空")
//    @ApiModelProperty("应用参与者编号")
//    private String applicationParty;

    @ApiModelProperty("企业代码")
    private String code;

    @ApiModelProperty("企业代码类型（1：统一社会信用代码， 2：组织机构代码，3：注册号）")
    private String codeType;

    @ApiModelProperty("法人微信")
    private String legalPersonaWechat;

    @ApiModelProperty("法人姓名")
    private String legalPersonaName;

    @ApiModelProperty("联系电话")
    private String componentPhone;
}
