package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PartyDeliveryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyId;
    /**
     * 配送方式
     */
    private String deliveryType;

    /**
     * 配送范围名称
     */
    private String name;

    /**
     * 配送范围介绍
     */
    private String description;

    /**
     * 免配送费订单金额
     */
    private BigDecimal deliveryFreeAmount;

    /**
     * 免配送费配送范围（km）
     */
    private BigDecimal deliveryFreeDistance;

    /**
     * 起送金额
     */
    private BigDecimal deliveryBeginAmount;

    /**
     * 超出距离
     */
    private String additionalDistance;

    /**
     * 超出距离价格
     */
    private BigDecimal additionalFee;

    /**
     * 超出距离价格
     */
    private BigDecimal fee;

    /**
     * 最大配送距离
     */
    private BigDecimal maxDistance;
}
