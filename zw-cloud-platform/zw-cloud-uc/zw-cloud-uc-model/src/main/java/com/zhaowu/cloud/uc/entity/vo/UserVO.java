package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
public class UserVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String partyId;

    private String username;

    private String nickname;

    private String mobile;

    private String status;

    private String applicationParty;

    private Collection<String> assoTypes;

    private Collection<UserChannelVO> userChannelVOs;
}
