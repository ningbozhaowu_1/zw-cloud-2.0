package com.zhaowu.cloud.uc.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 产品
 * </p>
 *
 * @author xxp
 * @since 2021-01-30
 */
@Data
@ToString
@ApiModel
public class GoodsFeeForm {
    /**商品id*/
    @NotNull(message = "商品id不能为空")
    @ApiModelProperty("商品id")
    private String id;
    /**商品名称*/
    @NotNull(message = "商品名称不能为空")
    @ApiModelProperty("商品名称")
    private String goodsName;
    /**快递模板ID*/
    @ApiModelProperty("快递模板ID")
    private String templateId;
    /**计费方式 1按件数 2按重量*/
    @NotNull(message = "计费方式不能为空")
    @ApiModelProperty("统一运费计费方式 Y N")
    private String isUnionFee;
    /**数量 件数 总量kg*/
    @NotNull(message = "数量不能为空")
    @ApiModelProperty("数量 1个")
    private String count;

    @NotNull(message = "重量不能为空")
    @ApiModelProperty("重量 1000=1kg")
    private String weight;
    /**统一运费*/
    @ApiModelProperty("统一运费 100=1元")
    private String unionFee;
}
