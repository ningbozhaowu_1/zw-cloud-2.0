package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 组织者
 * </p>
 *
 * @author xxp
 * @since 2021-01-27
 */
@Data
@Accessors(chain = true)
public class OrganizationVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者
     */
    private String partyId;

    /**
     * 企业代码
     */
    private String code;

    /**
     * 企业代码类型 1：统一社会信用代码
2：组织机构代码
3：营业执照注册
     */
    private String codeType;

    /**
     * 成立时间
     */
    private Date openday;

    /**
     * 法人姓名
     */
    private String legalName;

    /**
     * 法人证件类型
     */
    private String legalIdtype;

    /**
     * 法人证件号码
     */
    private String legalIdno;

    /**
     * 法人联系方式
     */
    private String legalContacts;

    /**
     * 联系人
     */
    private String contactName;

    /**
     * 联系人证件类型
     */
    private String contactIdtype;

    /**
     * 联系人证件号码
     */
    private String contactIdno;

    /**
     * 联系人联系方式
     */
    private String contactContact;

    /**
     * 备用联系方式1
     */
    private String contactContact1;

    /**
     * 联系方式2
     */
    private String contactContact2;

    /**
     * 联系人联系地址
     */
    private String contactAddr;

    /**
     * 企业性质
     */
    private String characters;

    /**
     * 企业类型
     */
    private String industry;

    /**
     * 企业logo
     */
    private String logo;

    /**
     * 主页
     */
    private String homePage;

    /**
     * 子域名
     */
    private String webPage;

    /**
     * 企业地址
     */
    private String orgAddr;

    private String mail;

    private String contactWechat;
}
