package com.zhaowu.cloud.uc.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partyId;

    private String partyName;
    /**
     * 联系人
     */
    private String contactName;

    /**
     * 联系人联系方式
     */
    private String contactContact;

    /**
     * 门店地址
     */
    private String orgAddr;

    /**
     * 企业类型
     */
    private String industry;

}
