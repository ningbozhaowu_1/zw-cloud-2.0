package com.zhaowu.cloud.notification.client;

import com.zhaowu.cloud.notification.model.Notification;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author xxp
 * @date 2020-9-22
 **/
@Component
public class NotificationClient {

    @Autowired
    private RabbitTemplate template;

    @Value("${zw.notification.queue.name:notification-queue}")
    private String notificationQueue;

    public void send(Notification notification){
        template.convertAndSend(notificationQueue,notification);
    }
}
