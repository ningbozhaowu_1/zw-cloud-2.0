package com.zhaowu.cloud.notification.service;

import com.github.qcloudsms.SmsSingleSender;
import com.zhaowu.cloud.framework.mail.MailSender;
import com.zhaowu.cloud.framework.mail.config.MailAutoConfiguration;
import com.zhaowu.cloud.framework.sms.config.SmsAutoConfiguration;
import com.zhaowu.cloud.notification.service.exchanger.EmailNoficationExchanger;
import com.zhaowu.cloud.notification.service.exchanger.SmsNotificationExchanger;
import com.zhaowu.cloud.notification.service.exchanger.WebSocketNotificationExchanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xxp
 * @date 2020-09-22
 **/
@Configuration
@EnableAutoConfiguration
@AutoConfigureAfter({SmsAutoConfiguration.class, MailAutoConfiguration.class})
public class NoticationAutoConfiguration {

    @Autowired(required = false)
    private SmsSingleSender smsSender;

    @Autowired(required = false)
    private MailSender mailSender;

    @Bean
    public SmsNotificationExchanger smsNotifcationExchanger(){
        return new SmsNotificationExchanger(smsSender);
    }

    @Bean
    public EmailNoficationExchanger emailNoficationExchanger(){
        return new EmailNoficationExchanger(mailSender);
    }

    @Bean
    @ConditionalOnProperty(name="zw.notification.websocket.enable",matchIfMissing = false)
    public WebSocketNotificationExchanger webSocketNotificationExchanger(){
        return new WebSocketNotificationExchanger();
    }
}
