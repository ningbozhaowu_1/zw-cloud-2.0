package com.zhaowu.cloud.notification.service.events;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.notification.model.SmsNotification;
import com.zhaowu.cloud.notification.service.NotificationDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class BusReceiver {

    @Autowired
    private NotificationDispatcher dispatcher;

    public void handleMessage(BusMessage message) throws IOException {

        log.info("Received Message:<{}>", message);
        if(CommonConstant.InfoType.SMS.equals(message.getInfoType())){
            JSONObject jsonObject = message.getJsonObject();
            SmsNotification smsNotification = JSON.parseObject(jsonObject.toJSONString(), SmsNotification.class);
            this.dispatcher.dispatch(smsNotification);
        }
    }
}