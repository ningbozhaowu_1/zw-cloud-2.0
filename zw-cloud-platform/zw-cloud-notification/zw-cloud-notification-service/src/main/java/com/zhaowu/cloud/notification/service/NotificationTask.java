package com.zhaowu.cloud.notification.service;

import com.zhaowu.cloud.notification.model.Notification;
import com.zhaowu.cloud.notification.service.exchanger.NotificationExchanger;

import java.util.concurrent.Callable;

/**
 * @author xxp
 * @date 2020-09-22
 **/
public class NotificationTask implements Callable<Boolean> {

    private NotificationExchanger notificationExchanger;

    private Notification notification;

    public NotificationTask(NotificationExchanger notificationExchanger,Notification notification){
        this.notificationExchanger = notificationExchanger;
        this.notification = notification;
    }

    @Override
    public Boolean call() throws Exception {
        return notificationExchanger.exchange(notification);
    }
}
