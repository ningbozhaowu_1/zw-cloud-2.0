package com.zhaowu.cloud.notification.service.exchanger;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import com.zhaowu.cloud.framework.sms.SmsParameter;
import com.zhaowu.cloud.notification.model.Notification;
import com.zhaowu.cloud.notification.model.SmsNotification;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.Arrays;

/**
 * 短信通知转发器，可以配置<b>zw.notification.sms.sign-name</b>设置默认的短信签名
 * @author xxp
 * @date 2020-12-10
 **/
@Slf4j
public class SmsNotificationExchanger implements NotificationExchanger {

    private SmsSingleSender smsSender;

    private final static String STATUS_OK = "OK";

    public SmsNotificationExchanger(SmsSingleSender smsSender) {
        if (smsSender != null) {
            log.info("初始化短信通知组件");
        }
        this.smsSender = smsSender;
    }

    @Value("zw.notification.sms.sign-name")
    private String signName;

    @Override
    public boolean support(Object notification) {
        return notification.getClass().equals(SmsNotification.class);
    }

    @Override
    public boolean exchange(Notification notification) {

        Assert.notNull(smsSender, "短信接口没有初始化");

        SmsNotification smsNotification = (SmsNotification) notification;
        SmsParameter parameter = new SmsParameter();
        parameter.setPhoneNumbers(Arrays.asList(smsNotification.getPhoneNumber()));
        parameter.setTemplateCode(Integer.valueOf(smsNotification.getTemplateCode()));

        if(StringUtils.isEmpty(smsNotification.getSignName())){
            smsNotification.setSignName(this.signName);
        }

        Assert.notNull(smsNotification.getSignName(),"短信签名不能为空");

        parameter.setSignName(smsNotification.getSignName());

//        String[] params = {smsNotification.getContent(),String.valueOf(smsNotification.getExpire())};

        SmsSingleSenderResult smsSendResult = null;
        try {
            smsSendResult = smsSender.sendWithParam
                    ("86", parameter.getPhoneNumbers().get(0),
                            parameter.getTemplateCode(), smsNotification.getContents().split(","), parameter.getSignName(), "", "");
            log.info(smsSendResult.toString());
        } catch (IOException e) {
            log.error(e.getMessage());
        } catch (HTTPException e) {
            log.error(e.getMessage());
        }

        return STATUS_OK.equals(smsSendResult.result);
    }
}
