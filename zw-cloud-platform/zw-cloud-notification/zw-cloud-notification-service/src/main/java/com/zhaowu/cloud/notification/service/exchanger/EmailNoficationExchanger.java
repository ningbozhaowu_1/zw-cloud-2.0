package com.zhaowu.cloud.notification.service.exchanger;

import com.zhaowu.cloud.framework.mail.MailSender;
import com.zhaowu.cloud.notification.model.EmailNotification;
import com.zhaowu.cloud.notification.model.Notification;

/**
 * @author xxp
 * @date 2020-9-22
 **/
public class EmailNoficationExchanger implements NotificationExchanger{

    private MailSender mailSender;

    public EmailNoficationExchanger(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public boolean support(Object notification) {
        return notification.getClass().equals(EmailNotification.class);
    }

    @Override
    public boolean exchange(Notification notification) {
        return false;
    }
}
