package com.zhaowu.cloud.notification.service;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * @author xxp
 * @date 2020-09-22
 **/
@ComponentScan(basePackages = {"com.zhaowu.cloud", "com.zhaowu.aspect"})
@SpringBootApplication
@EnableApolloConfig
public class NotificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotificationApplication.class,args);
    }
}
