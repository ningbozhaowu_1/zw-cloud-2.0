package com.zhaowu.cloud.notification.service.rest;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.util.HWUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@RestController
@RequestMapping("/notify")
public class NotifyController extends BaseController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private static final String ACCESS_KEY = "07f30e5d-7f70-4bc5-ba47-a77968903f2d";

	@RequestMapping(value = "/hw",method={RequestMethod.POST, RequestMethod.GET})
	public void hw(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HWUtils.verificateRequestParams(request.getParameterMap(), ACCESS_KEY);

		String activity = request.getParameter("activity");
		JSONObject jsonObject = new JSONObject();
		if("newInstance".equals(activity)){


			jsonObject.put("resultCode", "000000");
			jsonObject.put("resultMsg","success");
			jsonObject.put("instanceId",request.getParameter("businessId"));
			jsonObject.put("encryptType","1");

			JSONObject appInfo = new JSONObject();
			appInfo.put("frontEndUrl","www.zhaowu.cc");
			appInfo.put("adminUrl","www.zhaowu.cc");
			appInfo.put("userName",HWUtils.generateSaaSUsernameOrPwd("test",ACCESS_KEY,"1"));
			appInfo.put("password",HWUtils.generateSaaSUsernameOrPwd("111111",ACCESS_KEY,"1"));
			jsonObject.put("appInfo",appInfo);
		}else {
			jsonObject.put("resultCode", "000000");
			jsonObject.put("resultMsg","success");
		}

		String encryptBody = HWUtils.generateResponseBodySignature(ACCESS_KEY, jsonObject.toJSONString());
		System.out.println("body签名:" + encryptBody);
		String bodySign = "sign_type=\"HMAC-SHA256\", signature=\""+encryptBody+"\"";
		response.setHeader("Body-Sign", bodySign);
		responseJson(response, jsonObject);
	}

	/**
	 * 返回JSON数据
	 * @param response
	 * @param obj
	 * @throws Exception
	 */
	public static void responseJson(HttpServletResponse response, Object obj) throws Exception {
		response.setContentType("application/json; charset=utf-8");
		PrintWriter writer = response.getWriter();
		writer.print(JSONObject.toJSONString(obj, SerializerFeature.WriteMapNullValue,
				SerializerFeature.WriteDateUseDateFormat));
		writer.close();
		response.flushBuffer();
	}
}
