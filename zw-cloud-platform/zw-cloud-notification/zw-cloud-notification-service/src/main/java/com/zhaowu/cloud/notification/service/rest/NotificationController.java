package com.zhaowu.cloud.notification.service.rest;

import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.notification.model.EmailNotification;
import com.zhaowu.cloud.notification.model.SmsNotification;
import com.zhaowu.cloud.notification.service.NotificationDispatcher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author xxp
 * @date 2020-09-22
 **/
@RestController("/notification")
@Api(value="notification",tags = "通知中心")
public class NotificationController{

    @Autowired
    private NotificationDispatcher dispatcher;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @ApiOperation("/短信通知")
    @PostMapping("/send/sms")
    public Result<String> sendSms(SmsNotification smsNotification) throws IOException {
        this.dispatcher.dispatch(smsNotification);
        return Result.buildSuccess("");
    }

    @ApiOperation("/邮件通知")
    @PostMapping("/send/email")
    public Result<String> sendEmail(EmailNotification emailNotification) throws IOException {
        this.dispatcher.dispatch(emailNotification);
        return Result.buildSuccess("");
    }

}
