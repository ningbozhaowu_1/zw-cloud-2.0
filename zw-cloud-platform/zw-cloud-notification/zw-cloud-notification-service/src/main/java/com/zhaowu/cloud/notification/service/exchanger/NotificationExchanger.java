package com.zhaowu.cloud.notification.service.exchanger;

import com.zhaowu.cloud.notification.model.Notification;

/**
 * @author xxp
 * @date 2020-09-22
 **/
public interface NotificationExchanger {

    boolean support(Object notification);

    boolean exchange(Notification notification);
}
