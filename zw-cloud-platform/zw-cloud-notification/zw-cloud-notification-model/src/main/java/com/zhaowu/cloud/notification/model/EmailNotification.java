package com.zhaowu.cloud.notification.model;

import lombok.Data;

/**
 * @author xxp
 * @date 2020-9-22
 **/
@Data
public class EmailNotification extends Notification{

    private String receiver;

    private String title;

    private String content;

}
