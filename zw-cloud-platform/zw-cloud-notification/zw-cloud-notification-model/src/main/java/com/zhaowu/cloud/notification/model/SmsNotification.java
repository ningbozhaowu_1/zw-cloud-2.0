package com.zhaowu.cloud.notification.model;

import lombok.Data;

import java.util.List;

/**
 * @author xxp
 * @date 2020-9-22
 **/
@Data
public class SmsNotification extends Notification{

    private static final long serialVersionUID = 1L;

    private String phoneNumber;

    private String templateCode;

//    private Map<String,Object> params;

    private String signName;

    private Long expire;

    private String contents;

}
