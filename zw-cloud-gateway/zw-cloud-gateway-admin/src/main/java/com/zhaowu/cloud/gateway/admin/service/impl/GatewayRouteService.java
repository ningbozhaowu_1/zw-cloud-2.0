package com.zhaowu.cloud.gateway.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import com.zhaowu.cloud.gateway.admin.config.BusConfig;
import com.zhaowu.cloud.gateway.admin.dao.GatewayRouteMapper;
import com.zhaowu.cloud.gateway.admin.entity.param.GatewayRouteQueryParam;
import com.zhaowu.cloud.gateway.admin.entity.po.GatewayRoute;
import com.zhaowu.cloud.gateway.admin.service.IGatewayRouteService;
import com.zhaowu.cloud.gateway.admin.entity.ov.GatewayRouteVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GatewayRouteService extends ServiceImpl<GatewayRouteMapper, GatewayRoute> implements IGatewayRouteService {

    private static final String GATEWAY_ROUTES = "gateway_routes::";

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BusSender busSender;

    @Value("${rabbitmq.routing_key}")
    private String routingKey;

    @Override
    public boolean add(GatewayRoute gatewayRoute) {
        boolean isSuccess = this.save(gatewayRoute);
        // 转化为gateway需要的类型，缓存到redis, 并事件通知各网关应用
        RouteDefinition routeDefinition = gatewayRouteToRouteDefinition(gatewayRoute);
        stringRedisTemplate.opsForValue().setIfAbsent(GATEWAY_ROUTES+gatewayRoute.getRouteId(), JSON.toJSONString(routeDefinition));
//        gatewayRouteCache.put(gatewayRoute.getRouteId(), routeDefinition);
        busSender.send(CommonConstant.RouteKey.GATEWAYWEB, routeDefinition);
        return isSuccess;
    }

    @Override
    public boolean delete(String id) {
        GatewayRoute gatewayRoute = this.getById(id);
        // 删除redis缓存, 并事件通知各网关应用
//        gatewayRouteCache.remove(gatewayRoute.getRouteId());
        stringRedisTemplate.delete(GATEWAY_ROUTES+gatewayRoute.getRouteId());
        busSender.send(CommonConstant.RouteKey.GATEWAYWEB, gatewayRouteToRouteDefinition(gatewayRoute));
        return this.removeById(id);
    }

    @Override
    public boolean update(GatewayRoute gatewayRoute) {
        boolean isSuccess = this.updateById(gatewayRoute);
        // 转化为gateway需要的类型，缓存到redis, 并事件通知各网关应用
        RouteDefinition routeDefinition = gatewayRouteToRouteDefinition(gatewayRoute);
        stringRedisTemplate.opsForValue().set(GATEWAY_ROUTES+gatewayRoute.getRouteId(), JSON.toJSONString(routeDefinition));
//        gatewayRouteCache.put(gatewayRoute.getRouteId(), routeDefinition);
        busSender.send(CommonConstant.RouteKey.GATEWAYWEB, routeDefinition);
        return isSuccess;
    }

    /**
     * 将数据库中json对象转换为网关需要的RouteDefinition对象
     *
     * @param gatewayRoute
     * @return RouteDefinition
     */
    private RouteDefinition gatewayRouteToRouteDefinition(GatewayRoute gatewayRoute) {
        RouteDefinition routeDefinition = new RouteDefinition();
        routeDefinition.setId(gatewayRoute.getRouteId());
        routeDefinition.setOrder(gatewayRoute.getOrders());
        routeDefinition.setUri(URI.create(gatewayRoute.getUri()));
        try {
            List<PredicateDefinition> predicateDefinitionList = JSON.parseArray(gatewayRoute.getPredicates(), PredicateDefinition.class);
            List<FilterDefinition> filterDefinitionList = JSON.parseArray(gatewayRoute.getFilters(), FilterDefinition.class);
            routeDefinition.setFilters(filterDefinitionList);
            routeDefinition.setPredicates(predicateDefinitionList);
        } catch (Exception e) {
            log.error("网关路由对象转换失败", e);
        }
        return routeDefinition;
    }

    @Override
    public GatewayRoute get(String id) {
        return this.getById(id);
    }

    @Override
    public List<GatewayRouteVo> query(GatewayRouteQueryParam gatewayRouteQueryParam) {
        QueryWrapper<GatewayRoute> queryWrapper = gatewayRouteQueryParam.build();
        queryWrapper.eq(StringUtils.isNotBlank(gatewayRouteQueryParam.getUri()), "uri", gatewayRouteQueryParam.getUri());
        return this.list(queryWrapper).stream().map(GatewayRouteVo::new).collect(Collectors.toList());
    }

    @Override
    @PostConstruct
    public boolean overload() {
        List<GatewayRoute> gatewayRoutes = this.list(new QueryWrapper<>());
        gatewayRoutes.forEach(gatewayRoute ->
                stringRedisTemplate.opsForValue().set(GATEWAY_ROUTES+gatewayRoute.getRouteId(), JSON.toJSONString(gatewayRouteToRouteDefinition(gatewayRoute)))
//                gatewayRouteCache.put(gatewayRoute.getRouteId(), gatewayRouteToRouteDefinition(gatewayRoute))
        );
        log.info("全局初使化网关路由成功!");
        return true;
    }
}
