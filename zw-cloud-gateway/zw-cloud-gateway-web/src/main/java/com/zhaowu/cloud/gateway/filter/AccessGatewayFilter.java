package com.zhaowu.cloud.gateway.filter;//package com.zhaowu.cloud.gateway.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhaowu.cloud.auth.client.service.IAuthService;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.exception.BaseException;
import com.zhaowu.cloud.gateway.exception.GatewayErrorStatus;
import com.zhaowu.cloud.gateway.service.IPermissionService;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.vo.ApplicationVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.ByteArrayDecoder;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.DecoderHttpMessageReader;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 请求url权限校验
 */
@Configuration
@ComponentScan(basePackages = "com.zhaowu.cloud.auth.client")
@Slf4j
public class AccessGatewayFilter implements GlobalFilter {

    private static final String X_CLIENT_TOKEN_USER = "zw-client-token-user";
    private static final String X_CLIENT_TOKEN = "zw-client-token";
    private static final String X_CLIENT_HOST = "zw-client-host";

    /**
     * 由authentication-client模块提供签权的feign客户端
     */
    @Autowired
    private IAuthService authService;

    @Autowired
    private UserClient userClient;

    @Autowired
    private IPermissionService permissionService;

//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;

    private static final String BEARER = "Bearer ";

    /**
     * 1.首先网关检查token是否有效，无效直接返回401，不调用签权服务
     * 2.调用签权服务器看是否对该请求有权限，有权限进入下一个filter，没有权限返回401
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String authentication = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        String method = request.getMethodValue();
        String url = request.getPath().value();
        log.debug("url:{},method:{},headers:{}", url, method, request.getHeaders());

        ServerHttpRequest.Builder builder = request.mutate();
        builder.header(X_CLIENT_HOST, request.getHeaders().getFirst(HttpHeaders.HOST));
        //不需要网关签权的url
        if (authService.ignoreAuthentication(url)) {
            return chain.filter(exchange);
        }

//        if (!StringUtils.isBlank(authentication) && authentication.startsWith(BEARER)) {
//            if(!stringRedisTemplate.hasKey("auth:"+StringUtils.substring(authentication, BEARER.length()))){
//                throw new BaseException(GatewayErrorStatus.TOKEN_EXPIRED);
//            }
//        }

        // 如果请求未携带token信息, 直接权限
        if (StringUtils.isBlank(authentication) || !authentication.startsWith(BEARER)) {
            log.error("user token is null");
            throw new BaseException(GatewayErrorStatus.ACCESS_DENIED);
        }

        //token是否有效，在网关进行校验，无效/过期等
        if (authService.invalidJwtAccessToken(authentication)) {
            throw new BaseException(GatewayErrorStatus.TOKEN_EXPIRED);
        }

        //调用签权服务看用户是否有权限，若有权限进入下一个filter
        if (permissionService.permission(authentication, url, method)) {
            //TODO 转发的请求都加上服务间认证token
            builder.header(X_CLIENT_TOKEN, "TODO添加服务间简单认证");
            //将jwt token中的用户信息传给服务
            String token = getUserToken(authentication);
            try {
                String applicationParty = (String) new ObjectMapper().readValue(token, Map.class).get("application_party");
                validateApplicationStatus(applicationParty);
            } catch (IOException e) {
                log.error(e.getMessage());
            }
            builder.header(X_CLIENT_TOKEN_USER, token);
//            refreshTokenExpire(StringUtils.substring(authentication, BEARER.length()));
            return chain.filter(exchange.mutate().request(builder.build()).build());
        }
        throw new BaseException(GatewayErrorStatus.ACCESS_DENIED);
    }

//    void refreshTokenExpire(String token){
//        stringRedisTemplate.expire("auth:" + token,86400, TimeUnit.SECONDS);
//    }
    /**
     * 提取jwt token中的数据，转为json
     *
     * @param authentication
     * @return
     */
    private String getUserToken(String authentication) {
        String token = "{}";
        try {
            token = new ObjectMapper().writeValueAsString(authService.getJwt(authentication).getBody());
            return token;
        } catch (JsonProcessingException e) {
            log.error("token json error:{}", e.getMessage());
        }
        return token;
    }

    private void validateApplicationStatus(String applicationParty){
        ApplicationVO applicationVO = userClient.getByApplicationParty(applicationParty).getData();
        if(!CommonConstant.Status.A.equals(applicationVO.getStatus())
            || DateUtils.truncatedCompareTo(new Date(), applicationVO.getExpireTime(), Calendar.DATE)==1){
            throw new BaseException(GatewayErrorStatus.APP_INVALID);
        }
    }

    /**
     * 网关拒绝，返回401
     *
     * @param
     */
    private Mono<Void> unauthorized(ServerWebExchange serverWebExchange) {
        serverWebExchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        DataBuffer buffer = serverWebExchange.getResponse()
                .bufferFactory().wrap(HttpStatus.UNAUTHORIZED.getReasonPhrase().getBytes());
        return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
    }
}
