package com.zhaowu.cloud.gateway.exception;

import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import lombok.Getter;

@Getter
public enum GatewayErrorStatus implements ErrorStatus {

    TOKEN_EXPIRED("999999", "登录超时"),
    ACCESS_DENIED("040030", "拒绝访问"),
    APP_INVALID("040031", "应用过期"),
    APP_ERROR("040032", "应用不存在");

    /**
     * 错误类型码
     */
    private String code;
    /**
     * 错误类型描述信息
     */
    private String message;

    GatewayErrorStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
