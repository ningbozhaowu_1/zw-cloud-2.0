package com.zhaowu.cloud.vcc.service;

import com.zhaowu.cloud.vcc.entity.SmsTemplate;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * 验证码服务接口
 * @author xxp
 * @date 2020-9-21
 */
public interface VerificationCodeService {

    /**
     * 获取验证码token
     * @param size  验证码长度
     * @param expire 过期时间
     * @param type 类型 0：数字验证码 1：混合验证码
     * @return
     * 验证码长度和过期时间采用默认配置
     * 验证码长度配置项：zw.vc.size 默认值：4<br/>
     * 验证码过期时间配置项：zw.vc.expire 默认值：180000(3分钟)
     */
    String getToken(String paramType, Integer size, Long expire, String type, String subject);

    /**
     * 刷新验证码有效期
     * 验证码过期时间配置项：zw.vc.expire 默认值：180000(3分钟)
     * @param token 验证码token
     */
    void refresh(String token);

    /**
     * 刷新验证码
     * @param token 验证码token
     * @param expire 验证码过期时间
     */
    void refresh(String token, long expire);

    /**
     * 发送短信验证码
     * @param token 验证码TOKEN
     * @param phoneNumber 手机号码
     * 短信模板配置项：zw.vc.sms.templateCode<br/>
     * 短信签名配置项：zw.vc.sms.signName<br/>
     * 验证码参数名配置项：zw.vc.sms.codeParamName<br/>
     * 可以对上述三个配置项进行配置全局的发送短信参数
     */
    void sendSms(String paramType, SmsTemplate smsTemplate, String token, String phoneNumber);

    /**
     * 发送短信验证码
     * @param token 验证码TOKEN
     * @param templateCode 短信模板
     * @param signName 短信签名
     * @param phoneNumber 手机号码
     * @param codeParamName 短信验证码参数名
     * @param params 短信参数
     */
    void sendSms(String paramType, Long expire, String token, String templateCode, String signName, String phoneNumber, String codeParamName, Map<String, Object> params);
/*
    String getToken(int size, String type);

    *//**
     * 获取验证码
     * @param type
     * @return
     *//*
    String getToken(String type);

    *//**
     * 获取验证码token
     * @param size 验证码长度
     * @return
     *//*
    String getToken(int size);

    *//**
     * 获取验证码token
     * @return
     * 验证码长度和过期时间采用默认配置
     * 验证码长度配置项：zw.vc.size 默认值：4<br/>
     * 验证码过期时间配置项：zw.vc.expire 默认值：180000(3分钟)
     *//*
    String getToken();*/

    /**
     * 渲染验证码图片
     * @param token
     * @param outputStream
     * @throws IOException
     */
    void renderImage(String token, OutputStream outputStream) throws IOException;

    /**
     * 判断验证码是否匹配，默认忽略大小写
     * @param token 验证码TOKEN
     * @param code 验证码值
     * @return
     */
    boolean validate(String paramType, String token, String code, String phoneNumber);

    /**
     * 判断验证是否匹配
     * @param token 验证码TOKEN
     * @param code 验证码值
     * @param ignoreCase 忽略大小写
     * @return
     */
    boolean validate(String paramType, String token, String code, String phoneNumber, boolean ignoreCase);
}
