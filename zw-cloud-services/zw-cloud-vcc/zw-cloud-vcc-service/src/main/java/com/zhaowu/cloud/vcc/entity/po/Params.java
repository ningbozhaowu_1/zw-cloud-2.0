package com.zhaowu.cloud.vcc.entity.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * <p>
 * 系统参数
 * </p>
 *
 * @author xxp
 * @since 2021-01-26
 */
@Data
@Accessors(chain = true)
public class Params {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 参数类型
     */
    @TableField("param_type")
    private String paramType;

    /**
     * 参数值
     */
    private String params;

    /**
     * 父参数编号
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 参数描述
     */
    private String description;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;


    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

}
