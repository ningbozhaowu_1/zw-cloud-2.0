package com.zhaowu.cloud.vcc.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

@Data
public class SmsTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    private String templateCode;

    private String codeParamName;

    private String signName;

    private Integer size;

    private Long smsSendInterval;//分

    private Long expire;//分

    /**
     * 0 数字 1数字+字母
     */
    private String type;
}
