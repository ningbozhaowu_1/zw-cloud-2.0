package com.zhaowu.cloud.vcc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.vcc.entity.po.Params;

/**
 * <p>
 * 系统参数 Mapper 接口
 * </p>
 *
 * @author xxp
 * @since 2021-01-26
 */
public interface ParamsMapper extends BaseMapper<Params> {

}
