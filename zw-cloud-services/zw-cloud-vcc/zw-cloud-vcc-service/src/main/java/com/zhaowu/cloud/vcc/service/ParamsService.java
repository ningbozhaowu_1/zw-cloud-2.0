package com.zhaowu.cloud.vcc.service;

import com.zhaowu.cloud.vcc.entity.po.Params;

/**
 * <p>
 * 系统参数 服务类
 * </p>
 *
 * @author xxp
 * @since 2021-01-26
 */
public interface ParamsService {

    Params getParamsById(String paramsType);
}
