package com.zhaowu.cloud.vcc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.anno.CreateCache;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import com.zhaowu.cloud.vcc.entity.SmsTemplate;
import com.zhaowu.cloud.vcc.service.VerificationCodeService;
import com.zhaowu.cloud.vcc.utils.VerificationCodeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 系统验证码服务类
 *
 * @author xxp
 * @date 2020-9-21
 */
@Service
public class VerificationCodeServiceImpl implements VerificationCodeService {

    @CreateCache(name = "verificationCodeCache:", expire = 3600)
    private com.alicp.jetcache.Cache<String, String> verificationCodeCache;

    @CreateCache(name = "verificationCodeSmsSendCache:", expire = 3600)
    private com.alicp.jetcache.Cache<String, String> verificationCodeSmsSendCache;

    @Autowired
    private BusSender busSender;

    /**
     * 默认验证码长度
     */
    @Value("${zw.vc.size:4}")
    private Integer verificationCodeSize;

    /**
     * 发送验证码短信模板
     */
    @Value("${zw.vc.sms.template-code:}")
    private String templateCode;

    /**
     * 验证码参数名称
     */
    @Value("${zw.vc.sms.code-param-name:}")
    private String codeParamName;

    /**
     * 验证码参数名称
     */
    @Value("${zw.vc.sms.sign-name:}")
    private String signName;

    /**
     * 默认验证码失效时间
     */
    @Value("${zw.vc.expire:180}")
    private Long verificationCodeExpireTime;

    /**
     * 短信验证码发送间隔,默认为1分钟
     */
    @Value("${zw.vc.sms.interval:1}")
    private Long smsSendInterval;

    /**
     * 数字验证码
     */
    private static final String NUMBER_VRIFICATION_CODE = "0";

    /**
     * 数字英文混合验证码
     */
    private static final String MIXED_VRIFICATION_CODE = "1";

    @Override
    public String getToken(String paramType, Integer size, Long expire, String type,String subject){
        if(size == null) {
            size = this.verificationCodeSize;
        }
        if(expire == null){
            expire = this.verificationCodeExpireTime;
        }
        String code = "";
        if(NUMBER_VRIFICATION_CODE.equals(type)){
            code = VerificationCodeUtils.generateNumberVerifyCode(size);
        }else{
            code = VerificationCodeUtils.generateVerifyCode(size);
        }
        String token = UUID.randomUUID().toString().replaceAll("-","");
        String realToken = token;
        if(StringUtils.isNotEmpty(subject)){
            realToken = paramType+ "@" +subject + "@" + token;
        }
        this.putCache(realToken, code,expire);
        return token;
    }

    @Override
    public void refresh(String token){
        this.refresh(token,verificationCodeExpireTime);
    }

    @Override
    public void refresh(String token,long expire){
        String code = this.getCode(token);
        this.updateCache(token,code,expire);
    }

    @Override
    public void sendSms(String paramType, SmsTemplate smsTemplate, String token, String phoneNumber) {
        this.sendSms(paramType, smsTemplate.getExpire(), token, smsTemplate.getTemplateCode(), smsTemplate.getSignName(), phoneNumber, smsTemplate.getCodeParamName(), null);
    }

    @Override
    public void sendSms(String paramType, Long expire, String token, String templateCode, String signName, String phoneNumber, String codeParamName, Map<String, Object> params) {
        Assert.isTrue(StringUtils.isNotEmpty(templateCode), "短信模板没有配置");
        Assert.isTrue(StringUtils.isNotEmpty(signName), "短信模板没有没有配置");
        Assert.isTrue(StringUtils.isNotEmpty(codeParamName), "验证码参数不能为空");

        token = paramType+ "@" +phoneNumber + "@" + token;

        //验证是否已经过期
        boolean isExpired = this.verificationCodeCache.get(token)==null;
        if (isExpired) {
            this.evictCache(token);
        }
        Assert.isTrue(!isExpired, "验证码已过期");

        //判断短信验证码的发送间隔
        validateSmsSendInterval(phoneNumber);
        String value = this.getCode(token);
        //通过通知中心发送通知
        BusMessage message = new BusMessage();
        message.setStandalone("Y");
        message.setInfoType(CommonConstant.InfoType.SMS);
        JSONObject jsonObject = new JSONObject();
        message.setJsonObject(jsonObject);
        jsonObject.put("phoneNumber", phoneNumber);
        jsonObject.put("signName", signName);
        jsonObject.put("templateCode", templateCode);
        jsonObject.put("contents", value +"," + expire.toString());
        busSender.send("notification-route",message);
        setSmsSendTimeStamp(phoneNumber);
    }

    /**
     * 设置短信发送的时间戳
     * @param phoneNumber
     */
    private void setSmsSendTimeStamp(String phoneNumber){
        this.verificationCodeSmsSendCache.put(phoneNumber,String.valueOf(System.currentTimeMillis()),smsSendInterval, TimeUnit.MINUTES);
    }

    /**
     * 验证短信发送的间隔
     * @param phoneNumber
     */
    private void validateSmsSendInterval(String phoneNumber){
       String timeStamp = this.verificationCodeSmsSendCache.get(phoneNumber);
       Assert.isTrue(StringUtils.isEmpty(timeStamp),"短信发送间隔时间太短，请"+smsSendInterval+"分钟后再试");
    }

    @Override
    public void renderImage(String token, OutputStream outputStream) throws IOException {
        // 生成随机字串
        String value = this.getCode(token);
        // 生成图片
        int w = 100, h = 30;
        VerificationCodeUtils.outputImage(w, h, outputStream, value);
    }

    @Override
    public boolean validate(String paramType, String token, String code,String subject) {
        return this.validate(paramType, token, code, subject,true);
    }

    @Override
    public boolean validate(String paramType, String token, String code, String subject,boolean ignoreCase) {
        if(StringUtils.isEmpty(code)){
            return false;
        }
        if(StringUtils.isNotEmpty(subject)){
            token = paramType+ "@" +subject + "@" + token;
        }
        String value = this.verificationCodeCache.get(token);

        if(StringUtils.isEmpty(value)){
            return false;
        }

        boolean result = false;

        result = ignoreCase ? code.equalsIgnoreCase(value) : code.equals(value);

        if(result){
            this.evictCache(token);
        }

        return result;
    }

    public String getCode(String token){
       return this.verificationCodeCache.get(token);
    }

    /**
     * 放入缓存
     * @param token
     * @param code
     */
    private void putCache(String token,String code,long expire){
        verificationCodeCache.put(token,code,expire,TimeUnit.MINUTES);
    }

    /**
     * 删除缓存
     * @param token
     */
    private void evictCache(String token){
        verificationCodeCache.remove(token);
    }

    /**
     * 更新缓存
     * @param token
     * @param code
     * @param expire
     */
    private void updateCache(String token,String code,long expire){
        this.evictCache(token);
        verificationCodeCache.put(token,code,expire,TimeUnit.MINUTES);
    }
}
