package com.zhaowu.cloud.vcc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.vcc.entity.po.Params;
import com.zhaowu.cloud.vcc.mapper.ParamsMapper;
import com.zhaowu.cloud.vcc.service.ParamsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统参数 服务实现类
 * </p>
 *
 * @author xxp
 * @since 2021-01-26
 */
@Service
public class ParamsServiceImpl extends ServiceImpl<ParamsMapper, Params> implements ParamsService {

    @Override
    public Params getParamsById(String paramsType) {
        QueryWrapper<Params> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("param_type", paramsType);
        return this.getOne(queryWrapper);
    }
}
