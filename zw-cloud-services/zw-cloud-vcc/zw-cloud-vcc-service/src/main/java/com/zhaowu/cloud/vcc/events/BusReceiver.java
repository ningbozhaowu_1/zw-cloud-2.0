package com.zhaowu.cloud.vcc.events;

import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BusReceiver {


    public void handleMessage(BusMessage message) {
        log.info("Received Message:<{}>", message);
    }
}