//package com.zhaowu.cloud.vcc.events;
//import com.zhaowu.cloud.vcc.config.BusConfig;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.amqp.support.converter.MessageConverter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//
//@Component
//@Slf4j
//public class BusSender {
//
//    public static final String EXCHANGE_NAME = "spring-zhaowu-exchange";
//
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    @Autowired
//    private MessageConverter messageConverter;
//
//    @PostConstruct
//    public void init() {
//        rabbitTemplate.setMessageConverter(messageConverter);
//    }
//
//    public void send(String routingKey, Object object) {
//        log.info("routingKey:{}=>message:{}", routingKey, object);
//        rabbitTemplate.convertAndSend(BusConfig.EXCHANGE_NAME, routingKey, object);
//    }
//}
