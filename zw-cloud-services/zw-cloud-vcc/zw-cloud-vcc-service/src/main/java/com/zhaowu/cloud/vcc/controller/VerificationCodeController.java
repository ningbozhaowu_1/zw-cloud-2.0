package com.zhaowu.cloud.vcc.controller;

import com.alibaba.fastjson.JSON;
import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import com.google.common.collect.Maps;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.vcc.entity.SmsTemplate;
import com.zhaowu.cloud.vcc.entity.po.Params;
import com.zhaowu.cloud.vcc.service.ParamsService;
import com.zhaowu.cloud.vcc.service.VerificationCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

/**
 * @author xxp
 * @date 2020-9-20
 **/
@RestController
@RequestMapping("/vcc")
@Api(tags = "验证码中心接口")
public class VerificationCodeController extends BaseController {

    @Autowired
    private VerificationCodeService verificationCodeService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private ParamsService paramsService;

    @GetMapping("/public/token")
    @ApiOperation("登录前 获取短信验证码")
    public Result<String> getLoginToken(@Valid @RequestParam("paramType") String paramType,
                                        @Valid @RequestParam("mobile") String mobile,
                                        @Valid @RequestParam("captchaVerification") String captchaVerification){

        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(captchaVerification);
        ResponseModel response = captchaService.verification(captchaVO);
        if(response.isSuccess() == false){
            //验证码校验失败，返回信息告诉前端
            //repCode  0000  无异常，代表成功
            //repCode  9999  服务器内部异常
            //repCode  0011  参数不能为空
            //repCode  6110  验证码已失效，请重新获取
            //repCode  6111  验证失败
            //repCode  6112  获取验证码失败,请联系管理员
            return this.failure("用户行为二次验证失败");
        }

        Params params = this.paramsService.getParamsById(paramType);

        if(params==null){
            return this.failure("短信模版未配置: "+"paramType");
        }

        SmsTemplate smsTemplate = JSON.parseObject(params.getParams(), SmsTemplate.class);
        Object token = this.verificationCodeService.getToken(paramType, smsTemplate.getSize(),smsTemplate.getExpire(),smsTemplate.getType(),mobile);
        this.verificationCodeService.sendSms(paramType, smsTemplate, (String)token,mobile);
        return this.success(token);
    }

    @GetMapping("/token")
    @ApiOperation("登录后 获取短信验证码")
    public Result<String> getToken(@Valid @RequestParam("paramType") String paramType, @Valid @RequestParam("mobile") String mobile){

        Params params = this.paramsService.getParamsById(paramType);
        SmsTemplate smsTemplate = JSON.parseObject(params.getParams(), SmsTemplate.class);
        Object token = this.verificationCodeService.getToken(paramType, smsTemplate.getSize(),smsTemplate.getExpire(),smsTemplate.getType(),mobile);
        this.verificationCodeService.sendSms(paramType, smsTemplate, (String)token, mobile);
        return this.success(token);
    }

    @GetMapping("/validate")
    @ApiOperation("校验验证码")
    public Result<Boolean> validate(@Valid @RequestParam("paramType") String paramType,
                                    @Valid @RequestParam("token") String token,
                                    @Valid @RequestParam("code") String code,
                                    @Valid @RequestParam("subject") String subject){
        return this.success(this.verificationCodeService.validate(paramType, token,code,subject));
    }

    @GetMapping("/image")
    @ApiOperation("生成验证码图片")
    public void validate(String token, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        this.verificationCodeService.renderImage(token,response.getOutputStream());
    }

    @GetMapping("/captchaVerificate")
    @ApiOperation("用户行为二次验证")
    public Result captchaVerificate(@RequestParam("captchaVerification") String captchaVerification) {
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(captchaVerification);
        ResponseModel response = captchaService.verification(captchaVO);
        if(response.isSuccess() == false){
            //验证码校验失败，返回信息告诉前端
            //repCode  0000  无异常，代表成功
            //repCode  9999  服务器内部异常
            //repCode  0011  参数不能为空
            //repCode  6110  验证码已失效，请重新获取
            //repCode  6111  验证失败
            //repCode  6112  获取验证码失败,请联系管理员
            return this.failure("用户行为二次验证失败");
        }else{
            return this.success();
        }

    }

}
