package com.zhaowu.cloud.vcc.client;

import com.zhaowu.cloud.framework.base.protocol.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author xxp
 */
@FeignClient(name = "common-service")
public interface VerificationCodeClient {

    @RequestMapping(value = "/vcc/sendSms", method = RequestMethod.GET)
    Result<String> sendSms(@RequestParam("paramType") String paramType, @RequestParam("mobile") String mobile, @RequestParam("content") String[] content);

    @RequestMapping(value = "/vcc/validate", method = RequestMethod.GET)
    Result<Boolean> validate(@RequestParam("paramType") String paramType, @RequestParam("token") String token, @RequestParam("code") String code, @RequestParam("subject") String subject);

    @RequestMapping(value = "/vcc/captchaVerificate", method = RequestMethod.GET)
    Result captchaVerificate(@RequestParam("captchaVerification") String captchaVerification);

}
