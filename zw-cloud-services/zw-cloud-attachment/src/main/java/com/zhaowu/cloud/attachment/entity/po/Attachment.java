package com.zhaowu.cloud.attachment.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author xxp
 */
@Data
@Accessors
@TableName("attachment")
public class Attachment extends BasePo {

	/**
	 * 应用编号
	 */
	@TableField("application_party")
	private String applicationParty;

    /**
     * 文件上传时的名称
     */
	@TableField("file_name")
	private String fileName;
    /**
     * 文件mime类型
     */
	@TableField("content_type")
	private String contentType;
    /**
     * 文件存储路径
     */
	@TableField("url_path")
	private String urlPath;

	/**
	 * 文件访问路径
	 */
	@TableField("full_url_path")
	private String fullUrlPath;

    /**
     * 文件大小,单位字节
     */
	private Long size;
    /**
     * 业务类型
     */
	@TableField("biz_id")
	private String bizId;
    /**
     * 业务标识
     */
	@TableField("biz_code")
	private String bizCode;
}
