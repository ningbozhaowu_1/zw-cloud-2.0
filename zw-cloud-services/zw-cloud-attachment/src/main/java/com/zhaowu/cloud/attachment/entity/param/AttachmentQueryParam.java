package com.zhaowu.cloud.attachment.entity.param;

import com.zhaowu.cloud.attachment.entity.po.Attachment;
import com.zhaowu.cloud.common.web.entity.param.BaseParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentQueryParam extends BaseParam<Attachment> {
    private String bizId;
    private String bizCode;
}
