package com.zhaowu.cloud.attachment.service;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.attachment.entity.param.AttachmentQueryParam;
import com.zhaowu.cloud.attachment.entity.po.Attachment;

import java.io.InputStream;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xxp
 */
public interface AttachmentService{


    /**
     * 保存并且清除原来的附件
     * @param input
     * @param fileName
     * @param contentType
     * @param bizId
     * @param bizCode
     * @param size
     */
    String saveAndClear(InputStream input, String fileName, String contentType, String bizId, String bizCode, Long size);

    /**
     * 上传附件
     * @param input 输入流
     * @param fileName 文件名称
     * @param contentType contentType
     * @param bizId 业务表ID
     * @param bizCode 业务代码
     * @param size 文件大小
     */
    Attachment save(InputStream input, String fileName, String contentType, String bizId, String bizCode, Long size);


    /**
     * 通过文件KEY获取附件输入流
     * @param key
     * @return
     */
    public InputStream getInputStream(String key);

    /**
     * 根据业务单据ID和业务代码获取单个文件流
     * @param bizId
     * @param bizCode
     * @return
     */
    InputStream getInputStream(Long domainId, Long applicaitonId, String bizId, String bizCode);

    /**
     * 删除附件
     * @param id
     */
    @CacheInvalidate(name="attachmentCache:" , key="#id")
    void delete(String id);

    /**
     * 根据KEY获取附件对象
     * @param key
     * @return
     */
    Attachment getByKey(String key);

    /**
     * 根据业务单据ID和业务代码获取附件列表
     * @param attachmentQueryParam
     * @return
     */
    IPage<Attachment> listByBizIdAndBizCode(Page<Attachment> page, AttachmentQueryParam attachmentQueryParam);
    /**
     * 根据业务单据ID和业务代码获取单个附件
     * @param bizId
     * @param bizCode
     * @return
     */
    Attachment getByBizIdAndBizCode(Long domainId, Long applicaitonId, String bizId, String bizCode);
    /**
     * 根据ID获取单个附件
     * @param id
     * @return
     */
    @Cached(name="attachmentCache:", key="#id", expire = 3600, cacheType = CacheType.BOTH)
    @CachePenetrationProtect
    Attachment selectById(String id);
}
