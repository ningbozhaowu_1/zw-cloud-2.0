package com.zhaowu.cloud.attachment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhaowu.cloud.attachment.entity.po.Attachment;
import com.zhaowu.cloud.attachment.service.AttachmentService;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import com.zhaowu.cloud.framework.storage.FileStorage;
import com.zhaowu.cloud.attachment.entity.param.AttachmentQueryParam;
import com.zhaowu.cloud.attachment.mapper.AttachmentMapper;
import com.zhaowu.cloud.attachment.validator.FileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.io.InputStream;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xxp
 */
@Service
public class AttachmentServiceImpl extends ServiceImpl<AttachmentMapper, Attachment> implements AttachmentService {

    @Autowired
    FileValidator fileValidator;

    @Autowired
    FileStorage fileStorage;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String saveAndClear(InputStream input, String fileName, String contentType, String bizId, String bizCode, Long size){
        //清除原来的附件
        QueryWrapper<Attachment> wrapper = new QueryWrapper<Attachment>();
        wrapper.eq("biz_id",bizId).eq("biz_code",bizCode);
        this.remove(wrapper);
        return this.save(input, fileName, contentType, bizId, bizCode, size).getFullUrlPath();
    }

    @Override
    @Transactional
    public Attachment save(InputStream input, String fileName, String contentType, String bizId, String bizCode, Long size) {
        //校验文件合法性
        fileValidator.validate(size, fileName, contentType);

        String urlPath = String.format("%s/%s/%s/%s",UserContextHolder.getInstance().getApplicationParty() ,bizId,bizCode,UUID.randomUUID().toString());;

        String postfix = fileName.substring(fileName.lastIndexOf("."));

        urlPath = urlPath+"."+postfix;
        //        try {
//            key = CryptUtils.encryptMD5(bizCode + "-" + bizId + "-" + UUID.randomUUID().toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException("加密附件KEY失败");
//        }

        String host = "https://zw-cloud-1255468730.cos.ap-shanghai.myqcloud.com";

        String fullUrlPath = String.format("%s/%s",host,urlPath);

        //保存到数据库记录
        Attachment attachment = new Attachment();
//        attachment.setChannelId(UserContextHolder.getInstance().getChannelId());
        attachment.setApplicationParty(UserContextHolder.getInstance().getApplicationParty());
        attachment.setUrlPath(urlPath);
        attachment.setSize(size);
        attachment.setFileName(fileName);
        attachment.setBizCode(bizCode);
        attachment.setBizId(bizId);
        attachment.setContentType(contentType);
        attachment.setFullUrlPath(fullUrlPath);

        this.save(attachment);
        //存储上传
        fileStorage.store(input, urlPath);
        return attachment;

    }

    @Override
    public InputStream getInputStream(String key) {
        return fileStorage.getInputStream(key);
    }

    @Override
    public InputStream getInputStream(Long domainId, Long applicaitonId, String bizId,String bizCode){
        Attachment attachment = this.getByBizIdAndBizCode(domainId, applicaitonId, bizId,bizCode);
//        if(attachment != null){
//            return this.getInputStream(attachment.getAttachmentKey());
//        }
        return null;
    }

    @Override
    public void delete(String id){
        Attachment attachment = this.selectById(id);
        Assert.notNull(attachment,"附件不存在");
        this.fileStorage.remove(attachment.getUrlPath());
        this.removeById(id);
    }

    @Override
    public Attachment getByKey(String key){
        QueryWrapper<Attachment> wrapper = new QueryWrapper<Attachment>();
        wrapper.eq("attachment_key",key);
        return this.getOne(wrapper);
    }

    @Override
    public IPage<Attachment> listByBizIdAndBizCode(Page<Attachment> page, AttachmentQueryParam attachmentQueryParam) {
        QueryWrapper<Attachment> queryWrapper = attachmentQueryParam.build();
        queryWrapper.select("id");
        queryWrapper.eq("application_party", UserContextHolder.getInstance().getApplicationParty());
        queryWrapper.eq("biz_id", attachmentQueryParam.getBizId());
        queryWrapper.eq("biz_code", attachmentQueryParam.getBizCode());
        return this.page(page, queryWrapper);
    }

    @Override
    public Attachment getByBizIdAndBizCode(Long domainId, Long applicaitonId, String bizId, String bizCode) {
        QueryWrapper<Attachment> wrapper = new QueryWrapper<>();
        wrapper.eq("biz_id",bizId).eq("biz_code",bizCode).eq("application_party",applicaitonId);
        return this.getOne(wrapper);
    }

    @Override
    public Attachment selectById(String id) {
        return this.getById(id);
    }
}
