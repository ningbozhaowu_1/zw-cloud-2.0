package com.zhaowu.cloud.attachment.rest;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhaowu.cloud.attachment.entity.po.Attachment;
import com.zhaowu.cloud.attachment.service.AttachmentService;
import com.zhaowu.cloud.common.web.audit.EnableAudit;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.attachment.entity.form.AttachmentQueryForm;
import com.zhaowu.cloud.attachment.entity.param.AttachmentQueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 文件上传控制器
 *
 * @author xxp
 */
@EnableAudit
@RestController
@RequestMapping("/attachment")
@Api(value="/attachment",tags="附件服务")
public class AttachmentController extends BaseController {

    @Autowired
    private AttachmentService attachmentService;

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    @ApiOperation("上传附件")
    public Result<Attachment> upload(@RequestPart("file") MultipartFile file, String bizId, String bizCode) {
        Attachment attachment = null;
        try {
            attachment = this.attachmentService.save(file.getInputStream(),file.getOriginalFilename(),file.getContentType(),bizId,bizCode,file.getSize());
        } catch (IOException e) {
            throw new RuntimeException("存储文件失败" + e);
        }
        return this.success(attachment);
    }

    /**
     * 删除附件
     * @param id
     * @return
     */
    @PostMapping("/delete/{id}")
    @ApiOperation("删除附件")
    public Result<String> delete(@PathVariable String id){
        this.attachmentService.delete(id);
        return this.success();
    }

    /**
     * 根据业务单据ID和业务代码获取附件列表
     * @param attachmentQueryForm
     * @return
     */
    @PostMapping("/list")
    @ApiOperation("获取附件列表")
    public Result<Page<Attachment>> list(@Valid @RequestBody AttachmentQueryForm attachmentQueryForm){

        IPage<Attachment> page = this.attachmentService.listByBizIdAndBizCode(attachmentQueryForm.getPage(), attachmentQueryForm.toParam(AttachmentQueryParam.class));

        for(Attachment attachment : page.getRecords()){
            BeanUtils.copyProperties(this.attachmentService.selectById(attachment.getId()), attachment);
        }

        return this.success(page);
    }

    /**
     * 下载文件
     */
    @GetMapping("/download/{id}")
    @ApiOperation("下载附件")
    public void download(@PathVariable String id, HttpServletResponse response) {
        Attachment attachment = this.attachmentService.selectById(id);
        response.addHeader("Content-Type",attachment.getContentType());
        String encodedfileName = null;
        try {
            encodedfileName = new String(attachment.getFileName().getBytes(), "ISO8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"" + encodedfileName + "\"");
        response.setHeader("Content-Length",String.valueOf(attachment.getSize()));
        try {
            IOUtils.copy(this.attachmentService.getInputStream(attachment.getUrlPath()),response.getOutputStream());
        } catch (IOException e) {
           throw new RuntimeException("下载文件失败" + e);
        }
    }

//    /**
//     * 下载文件
//     */
//    @GetMapping("/image/{key}")
//    @ApiOperation("下载附件")
//    public void image(@PathVariable String key, HttpServletResponse response) {
//        Attachment attachment = this.attachmentService.getByKey(key);
//        String contentType = attachment.getContentType();
//        if(!contentType.startsWith("image")){
//            throw new ServiceException("该附件不是图片文件");
//        }
//
//        try {
//            response.setContentType(attachment.getContentType());
//            IOUtils.copy(this.attachmentService.getInputStream(key),response.getOutputStream());
//        } catch (IOException e) {
//            throw new ServiceException("下载图片失败");
//        }
//    }

}
