package com.zhaowu.cloud.attachment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhaowu.cloud.attachment.entity.po.Attachment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xxp
 */
public interface AttachmentMapper extends BaseMapper<Attachment> {
    List<Attachment> getAttachmentIdByBizIdAndBizCode(@Param("domainId") Long domainId, @Param("applicationParty") String applicationParty, @Param("bizId") Long bizId, @Param("bizCode") String bizCode);
}