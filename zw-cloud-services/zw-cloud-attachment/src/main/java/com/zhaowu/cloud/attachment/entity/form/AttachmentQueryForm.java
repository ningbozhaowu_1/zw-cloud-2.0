package com.zhaowu.cloud.attachment.entity.form;

import com.zhaowu.cloud.attachment.entity.param.AttachmentQueryParam;
import com.zhaowu.cloud.common.web.entity.form.BaseQueryForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel
@Data
public class
AttachmentQueryForm extends BaseQueryForm<AttachmentQueryParam> {

    @NotNull(message = "业务类型不能为空")
    @Size(max = 100,message = "业务类型必须在{max}个字符内")
    @ApiModelProperty(value = "业务编号")
    private String bizId;

    @NotNull(message = "业务编码不能为空")
    @Size(max = 100,message = "业务编码必须在{max}个字符内")
    @ApiModelProperty(value = "业务编码")
    private String bizCode;

}
