package com.zhaowu.cloud.framework.sms;

import java.util.List;

/**
 * 短信
 * @author xxp
 *
 */
public class SmsParameter {

	private int templateCode;
	private String signName;
	private List<String> phoneNumbers;
	private String params;
	public int getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(int templateCode) {
		this.templateCode = templateCode;
	}
	public String getSignName() {
		return signName;
	}
	public void setSignName(String signName) {
		this.signName = signName;
	}
	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
}
