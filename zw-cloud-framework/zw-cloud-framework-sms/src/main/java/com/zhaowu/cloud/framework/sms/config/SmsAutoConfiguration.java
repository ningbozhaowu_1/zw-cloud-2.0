package com.zhaowu.cloud.framework.sms.config;

import com.github.qcloudsms.SmsSingleSender;
import com.zhaowu.cloud.framework.sms.properties.QCloudSmsProperties;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
@EnableConfigurationProperties({QCloudSmsProperties.class})
public class SmsAutoConfiguration {
	
	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private QCloudSmsProperties QCloudSmsProperties;

	@Bean
	@ConditionalOnClass({ SmsSingleSender.class })
	@ConditionalOnProperty(name = "zw.sms.type", havingValue = "qcloud")
	public SmsSingleSender smsSender() {
		SmsSingleSender sender = new SmsSingleSender(QCloudSmsProperties.getAccessKeyId(),QCloudSmsProperties.getAccessKeySecret());
		BeanUtils.copyProperties(QCloudSmsProperties, sender);
		return sender;
	}

}
