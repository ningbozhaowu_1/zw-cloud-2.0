package com.zhaowu.cloud.framework.sms.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="zw.sms.qcloud")
public class QCloudSmsProperties {

	private int accessKeyId ;
	private String accessKeySecret;
	public int getAccessKeyId() {
		return accessKeyId;
	}
	public void setAccessKeyId(int accessKeyId) {
		this.accessKeyId = accessKeyId;
	}
	public String getAccessKeySecret() {
		return accessKeySecret;
	}
	public void setAccessKeySecret(String accessKeySecret) {
		this.accessKeySecret = accessKeySecret;
	}
	
	
}
