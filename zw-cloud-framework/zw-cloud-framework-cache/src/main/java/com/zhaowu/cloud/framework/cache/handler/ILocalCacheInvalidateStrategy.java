package com.zhaowu.cloud.framework.cache.handler;

import com.alicp.jetcache.anno.support.CacheContext;
import com.alicp.jetcache.support.CacheMessage;
import com.rabbitmq.client.Channel;

public interface ILocalCacheInvalidateStrategy {

    void invalidateLocalCache(CacheContext cacheContext, String area, String cacheName, CacheMessage cacheMessage);
}
