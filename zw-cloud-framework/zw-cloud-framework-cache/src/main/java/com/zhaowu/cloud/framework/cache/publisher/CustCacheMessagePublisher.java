package com.zhaowu.cloud.framework.cache.publisher;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.support.CacheMessage;
import com.alicp.jetcache.support.CacheMessagePublisher;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.cache.entity.CacheMessageWithName;
import com.zhaowu.cloud.framework.cache.events.BusSender;
import org.bouncycastle.util.encoders.Base64Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Base64UrlNamingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class CustCacheMessagePublisher implements CacheMessagePublisher {

    public Logger logger = LoggerFactory.getLogger(CustCacheMessagePublisher.class);

    @Value("${server.port}")
    private String serverPort;

    @Value("${rabbitmq.routing_key}")
    private String routeKey;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${standalone:Y}")
    private String standalone;

    @Autowired
    private BusSender busSender;

    public final static String UUID = new Base64UrlNamingStrategy().generateName();

    @Override
    public void publish(String area, String cacheName, CacheMessage cacheMessage) {

        if(!"Y".equals(standalone)){
            BusMessage busMessage = new BusMessage();
            busMessage.setStandalone("N");
            busMessage.setInfoType(CommonConstant.InfoType.CACHE_UPDATE);

            CacheMessageWithName cacheMessageWithName = new CacheMessageWithName();
            cacheMessageWithName.setArea(area);
            cacheMessageWithName.setCacheName(cacheName);
            cacheMessageWithName.setCacheMessage(cacheMessage);
            cacheMessageWithName.setServerPort(this.serverPort);
            cacheMessageWithName.setFrom(appName+"."+UUID);
            JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(cacheMessageWithName));
            busMessage.setJsonObject(jsonObject);

            busSender.send(routeKey, busMessage);
            logger.info(String.format("发送缓存更新消息：message:%s",cacheMessageWithName));
        }
    }
}
