package com.zhaowu.cloud.framework.cache.entity;

import com.alicp.jetcache.support.CacheMessage;
import lombok.Data;

import java.io.Serializable;

@Data
public class CacheMessageWithName implements Serializable {



    private String area;
    private String cacheName;
    private CacheMessage cacheMessage;
    private String serverPort;
    private String from;
}
