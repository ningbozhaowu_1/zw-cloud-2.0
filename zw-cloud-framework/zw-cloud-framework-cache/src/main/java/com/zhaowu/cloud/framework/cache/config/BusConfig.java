//package com.zhaowu.cloud.framework.cache.config;
//
//import com.fasterxml.jackson.annotation.JsonAutoDetect;
//import com.fasterxml.jackson.annotation.PropertyAccessor;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
//import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
//import org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter;
//import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
//import org.springframework.amqp.support.converter.MessageConverter;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//@Slf4j
//public class BusConfig {
//
//    @Value("${rabbitmq.exchange_name}")
//    public String exchangeName;
//
//    @Value("${rabbitmq.routing_key}")
//    public String routingKey;
//
//    @Value("${spring.application.name}")
//    private String appName;
//
//    @Bean
//    Queue queue() {
//        log.info("queue name:{}", appName);
//        return new Queue(appName, false);
//    }
//
//    @Bean
//    TopicExchange exchange() {
//        log.info("exchange:{}", exchangeName);
//        return new TopicExchange(exchangeName);
//    }
//
//    @Bean
//    Binding binding(Queue queue, TopicExchange exchange) {
//        log.info("binding {} to {} with {}", queue, exchange, routingKey);
//        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
//    }
//
//    @Bean
//    public MessageConverter messageConverter() {
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        return new ContentTypeDelegatingMessageConverter(new Jackson2JsonMessageConverter(objectMapper));
//    }
//
//}
