package com.zhaowu.cloud.framework.cache.events;

import com.alicp.jetcache.anno.support.ConfigProvider;
import com.zhaowu.cloud.framework.cache.entity.CacheMessageWithName;
import com.zhaowu.cloud.framework.cache.handler.CacheTask;
import com.zhaowu.cloud.framework.cache.handler.CaffeineInvalidateStrategy;
import com.zhaowu.cloud.framework.cache.publisher.CustCacheMessagePublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleUnresolved;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class BusCacheReceiver {

    private ExecutorService executorService;

    private CaffeineInvalidateStrategy localCacheInvalidateStrategy = new CaffeineInvalidateStrategy();

    @Autowired
    ConfigProvider configProvider;

    @Value("${spring.application.name}")
    private String appName;

    public BusCacheReceiver(){
        Integer availableProcessors = Runtime.getRuntime().availableProcessors();
        Integer numOfThreads = availableProcessors * 2;
        executorService = new ThreadPoolExecutor(numOfThreads,numOfThreads,0, TimeUnit.MILLISECONDS,new LinkedBlockingDeque<>());
        log.info("Init Notification ExecutorService , numOfThread : " + numOfThreads);
    }

    public void handleMessage(CacheMessageWithName message) {

        log.info("Received Message:<{}>", message);

        String me = appName+"."+ CustCacheMessagePublisher.UUID;
        if(me.equals(message.getFrom())){
            throw new RuntimeException("本机器缓存事件不消费");
        }
        //添加到线程池进行处理
        executorService.submit(new CacheTask(localCacheInvalidateStrategy, message, configProvider.getCacheContext()));

    }
}
