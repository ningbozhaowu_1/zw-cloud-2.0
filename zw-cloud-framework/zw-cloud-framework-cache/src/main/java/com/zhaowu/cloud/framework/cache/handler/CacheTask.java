package com.zhaowu.cloud.framework.cache.handler;

import com.alicp.jetcache.anno.support.CacheContext;
import com.rabbitmq.client.Channel;
import com.zhaowu.cloud.framework.cache.entity.CacheMessageWithName;

import java.util.concurrent.Callable;

/**
 * @author xxp
 * @date 2020-09-22
 **/
public class CacheTask implements Callable<Boolean> {

    private ILocalCacheInvalidateStrategy hander;

    private CacheMessageWithName message;

    private CacheContext cacheContext;

    public CacheTask(ILocalCacheInvalidateStrategy hander, CacheMessageWithName message, CacheContext cacheContext){
        this.hander = hander;
        this.message = message;
        this.cacheContext = cacheContext;
    }

    @Override
    public Boolean call() throws Exception {

        hander.invalidateLocalCache(this.cacheContext, message.getArea(), message.getCacheName(), message.getCacheMessage());

        return true;
    }
}
