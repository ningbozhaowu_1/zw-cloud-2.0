package com.zhaowu.cloud.framework.cache.handler;

import com.alibaba.fastjson.JSON;
import com.alicp.jetcache.anno.support.CacheContext;
import com.alicp.jetcache.anno.support.ConfigProvider;
import com.alicp.jetcache.support.CacheMessage;
import com.alicp.jetcache.support.FastjsonKeyConvertor;
import com.github.benmanes.caffeine.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author xxp
 * 二级缓存准实时更新处理类
 */
public class CaffeineInvalidateStrategy implements ILocalCacheInvalidateStrategy{

    public Logger logger = LoggerFactory.getLogger(CaffeineInvalidateStrategy.class);

    @Autowired
    ConfigProvider configProvider;

    @Override
    public void invalidateLocalCache(CacheContext cacheContext, String area, String cacheName, CacheMessage cacheMessage) {

        logger.info(String.format("执行失效本地缓存策略: area:%s；cacheName:%s;cacheMessage:%s", area, cacheName, JSON.toJSONString(cacheMessage)));

        Cache localCache = cacheContext.getCache(area, cacheName).unwrap(Cache.class);

        try{

            int type = cacheMessage.getType();

            switch (type) {
                case CacheMessage.TYPE_PUT: {
                    Object[] keys = cacheMessage.getKeys();
                    updateLocalCaches(localCache, keys);
                    break;
                }

                case CacheMessage.TYPE_REMOVE: {
                    Object[] keys = cacheMessage.getKeys();
                    invalidateLocalCaches(localCache, keys);
                    break;
                }
                case CacheMessage.TYPE_REMOVE_ALL: {
                    localCache.invalidateAll();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        logger.info(String.format("失效本地缓存: area:%s；cacheName:%s;cacheMessage:%s", area, cacheName, JSON.toJSONString(cacheMessage)));
    }

    private void updateLocalCaches(Cache localCache, Object[] keys) {
        invalidateLocalCaches(localCache, keys);//失效
    }

    private void invalidateLocalCaches(Cache localCache, Object[] keys) {
        for (Object key : keys) {
            Object fastJsonKey = FastjsonKeyConvertor.INSTANCE.apply(key);
            Object valueBefore = localCache.getIfPresent(fastJsonKey);
            if (null != valueBefore) {
                invalidateKey(localCache, fastJsonKey);
            }
        }
    }

    private void invalidateKey(Cache localCache, Object fastJsonKey) {
        Object valueBefore = localCache.getIfPresent(fastJsonKey);
        logger.info("local cache value before invalidate:" + JSON.toJSONString(valueBefore));
        localCache.invalidate(fastJsonKey);
        logger.info("invalidate local cache key:" + fastJsonKey);
        Object valueAfter = localCache.getIfPresent(fastJsonKey);
        logger.info("local cache value after invalidate:" + JSON.toJSONString(valueAfter));
    }
}
