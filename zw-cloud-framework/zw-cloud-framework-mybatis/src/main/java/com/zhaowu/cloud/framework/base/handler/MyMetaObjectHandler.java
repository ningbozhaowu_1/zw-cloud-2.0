package com.zhaowu.cloud.framework.base.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zhaowu.cloud.framework.util.UserContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Date;

@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 获取当前交易的用户，为空返回默认system
     *
     * @return
     */
    private String getCurrentUserId() {

        if(UserContextHolder.getInstance().getUserId()!=null){
            return UserContextHolder.getInstance().getUserId();
        }else{
            return "system";
        }

    }

    private String getCurrentPartyId() {

        if(UserContextHolder.getInstance().getPartyId()!=null){
            return UserContextHolder.getInstance().getPartyId();
        }else{
            return "1";
        }

    }

    private String getCurrentApplicationParty() {

        if(UserContextHolder.getInstance().getApplicationParty()!=null){
            return UserContextHolder.getInstance().getApplicationParty();
        }
        return "1";
    }

    private String getCurrentApplicationChannelId() {

        if(UserContextHolder.getInstance().getChannelId()!=null){
            return UserContextHolder.getInstance().getChannelId();
        }
        return "1";
    }


    @Override
    public void insertFill(MetaObject metaObject) {

        log.info("start insert fill ....");
        this.strictInsertFill(metaObject, "createBy", String.class, getCurrentUserId());
        this.strictInsertFill(metaObject, "applicationParty", String.class, getCurrentApplicationParty());
        this.strictInsertFill(metaObject, "partyChannelId", String.class, getCurrentApplicationChannelId());
        this.strictInsertFill(metaObject, "partyId", String.class, getCurrentPartyId());
        this.strictInsertFill(metaObject, "userParty", String.class, getCurrentPartyId());
        this.strictInsertFill(metaObject, "userId", String.class, getCurrentUserId());
        this.strictInsertFill(metaObject, "createTime", Date.class, Date.from(ZonedDateTime.now().toInstant())); // 起始版本 3.3.0(推荐使用)
    }

    @Override
    public void updateFill(MetaObject metaObject) {

        log.info("start update fill ....");
        this.strictUpdateFill(metaObject, "updateBy", String.class, getCurrentUserId());
        this.strictUpdateFill(metaObject, "updateTime", Date.class, Date.from(ZonedDateTime.now().toInstant())); // 起始版本 3.3.0(推荐)
    }
}
