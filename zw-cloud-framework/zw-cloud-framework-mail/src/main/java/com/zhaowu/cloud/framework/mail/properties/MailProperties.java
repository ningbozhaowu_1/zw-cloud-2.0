package com.zhaowu.cloud.framework.mail.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 邮件服务的配置
 *
 * @author xxp
 * @date 2020-09-22
 */
@Component
@ConfigurationProperties(prefix = "spring.mail")
public class MailProperties {

    private String name;
    
    private String from;

    private boolean preferIPv4Stack;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getPreferIPv4Stack() {
		return preferIPv4Stack;
	}

	public void setPreferIPv4Stack(boolean preferIPv4Stack) {
		this.preferIPv4Stack = preferIPv4Stack;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
}
