package com.zhaowu.cloud.framework.pay.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "zw.wxpay")
public class WxPayBean {
    /**
     * 服务商appid
     */
    private String appId;
    /**
     * 公众号/小程序appID
     */
    private String subAppId;
    /**
     * 公众号/小程序密钥
     */
    private String appSecret;
    /**
     * 服务商商户号
     */
    private String mchId;
    /**
     * 支付子商户号
     */
    private String subMchId;
    /**
     * 支付密钥 mchKey
     */
    private String partnerKey;
    /**
     * 证书地址
     */
    private String certPath;
    /**
     * 域名
     */
    private String domain;
    /**
     * apiv3 模式下所需配置
     *  <privateKeyPath>apiclient_key.pem证书文件的绝对路径或者以classpath:开头的类路径.</privateKeyPath>
     *  <privateCertPath>apiclient_cert.pem证书文件的绝对路径或者以classpath:开头的类路径.</privateCertPath>
     *  <apiV3Key> apiV3 秘钥值.</apiV3Key>
     *  <certSerialNo>apiV3 证书序列号值</certSerialNo>
     *
     */

    private String privateKeyPath;
    private String privateCertPath;
    private String apiV3Key;
    private String certSerialNo;

	@Override
	public String toString() {
		return "WxPayBean [appId=" + appId + ", subAppId=" + subAppId + ", subMchId=" + subMchId + ",appSecret=" + appSecret + ", mchId=" + mchId + ", partnerKey="
				+ partnerKey + ", certPath=" + certPath + ", domain=" + domain + ",privateKeyPath=" + privateKeyPath + ",privateCertPath=" + privateCertPath + ",apiV3Key=" + apiV3Key + ",certSerialNo=" + certSerialNo + "]";
	}
}
