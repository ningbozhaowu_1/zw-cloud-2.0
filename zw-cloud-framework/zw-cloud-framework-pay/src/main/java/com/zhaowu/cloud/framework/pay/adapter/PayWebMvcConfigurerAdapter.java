package com.zhaowu.cloud.framework.pay.adapter;

import com.zhaowu.cloud.framework.pay.interceptor.AliPayInterceptor;
import com.zhaowu.cloud.framework.pay.interceptor.WxPayInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class PayWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new AliPayInterceptor()).addPathPatterns("/order/v1/createOrder","/order/v1/payOrder");
        registry.addInterceptor(new WxPayInterceptor()).addPathPatterns("/order/v1/createOrder","/order/v1/payOrder");
        super.addInterceptors(registry);
    }
}

