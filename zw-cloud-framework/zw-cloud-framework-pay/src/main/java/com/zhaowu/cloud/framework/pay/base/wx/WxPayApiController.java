package com.zhaowu.cloud.framework.pay.base.wx;

import com.jpay.weixin.api.WxPayApiConfig;

public abstract class WxPayApiController {
	public abstract WxPayApiConfig getApiConfig();
}