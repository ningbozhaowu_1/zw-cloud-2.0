package com.zhaowu.cloud.framework.pay.base.alipay;

import com.jpay.alipay.AliPayApiConfig;

public abstract class AliPayApiController {
	public abstract  AliPayApiConfig getAliPayApiConfig();
}