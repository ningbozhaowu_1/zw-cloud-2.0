package com.zhaowu.cloud.framework.pay.base.wx;

import com.jpay.weixin.api.WxPayApiConfig;

public interface WxPayBaseController {

    WxPayApiConfig getWxPayApiConfig();
}
