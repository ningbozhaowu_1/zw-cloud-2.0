package com.zhaowu.cloud.framework.pay.base.alipay;

import com.jpay.alipay.AliPayApiConfig;

public interface AliPayBaseController {

    AliPayApiConfig getAliPayApiConfig();
}
