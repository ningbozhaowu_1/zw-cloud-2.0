package com.zhaowu.aspect.converter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 敏感信息加密
 */
@Component
public class SensitiveDataConverter {

    /**
     * 是否转换
     */
    @Value("${sensitive.converter.enable:true}")
    private boolean converterCanRun;

    /**
     * 敏感信息列表
     */
    @Value("${sensitive.data.keys:idcard,username,bankcard,mobile,password,pwd,authorization}")
    private String[] keyArray;
    /**
     * 表达式
     */
    private static Pattern pattern = Pattern.compile("[0-9a-zA-Z]]");

    /**
     * 转换信息
     * @param oriMsg
     * @return String
     */
    public String invokeMsg(String oriMsg) {
        if (StringUtils.isEmpty(oriMsg)) {
            return oriMsg;
        }
        String tempMsg = oriMsg;
        if (this.converterCanRun) {
            String[] var3 = this.keyArray;
            int var4 = var3.length;
            for (int var5 = 0;var5<var4;++var5) {
                String key = var3[var5];
                int index = -1;
                do {
                    index = tempMsg.indexOf(key, index+1);
                    if (index!=-1 && !this.isWorkChar(tempMsg, key, index)) {
                        int valueStart = this.getValueStartIndex(tempMsg, index + key.length());
                        int valueEnd = this.getValueEndIndex(tempMsg, valueStart);

                        String subStr = tempMsg.substring(valueStart, valueEnd);
                        subStr = this.tuomin(subStr, key);
                        tempMsg = tempMsg.substring(0, valueStart) + subStr + tempMsg.substring(valueEnd);
                    }
                } while(index !=-1);
            }
        }
        return tempMsg;
    }

    /**
     * 脱敏
     * @param submsg
     * @param key
     * @return
     */
    private String tuomin(String submsg, String key) {
        if ("idcard".equals(key)) {
            return SensitiveInfoUtils.idCardNum(submsg);
        } else if ( "name".equals(key)) {
            return  SensitiveInfoUtils.chineseName(submsg);
        } else if ( "bankcard".equals(key)) {
            return SensitiveInfoUtils.bankCard(submsg);
        } else {
            return "mobile".equals(key)? SensitiveInfoUtils.mobilePhone(submsg):"******";
        }
    }

    private int getValueStartIndex(String msg, int valueStart) {
        while (true) {
            char ch = msg.charAt(valueStart);
            if ( ch == ':' || ch == '=') {
                ++valueStart;

                for (ch = msg.charAt(valueStart); ch == ' ';ch = msg.charAt(valueStart)) {
                    ++valueStart;
                }
                if (ch == '"') {
                    ++valueStart;
                }

                return valueStart;
            }
            ++valueStart;
        }
    }

    private int getValueEndIndex(String msg, int valueEnd) {
        while (true) {
            if (valueEnd != msg.length()) {
                char ch = msg.charAt(valueEnd);
                if (ch == '"') {
                    char nextCh = msg.charAt(valueEnd+1);
                    if (valueEnd + 1 != msg.length() && nextCh != '\n') {
                        if (nextCh !=';' && nextCh !=',' && nextCh != '}') {
                            ++valueEnd;
                            continue;
                        }
                        while (valueEnd > 0) {
                            char preCh = msg.charAt(valueEnd - 1);
                            if (preCh != '\\') {
                                break;
                            }
                            --valueEnd;
                        }
                    }
                } else if (ch != ';' && ch != ',' && ch != '}') {
                    ++valueEnd;
                    continue;
                }
            }
            return valueEnd;
        }
    }

    /**
     * 转换
     * @param msg
     * @param key
     * @param index
     * @return
     */
    private boolean isWorkChar(String msg, String key, int index) {
        char nextCh;
        Matcher matcher;
        if (index!=0) {
            nextCh = msg.charAt(index-1);
        } else {
            nextCh = msg.charAt(index + key.length());
        }

        matcher = pattern.matcher(nextCh + "");
        return matcher.matches();
    }

}
