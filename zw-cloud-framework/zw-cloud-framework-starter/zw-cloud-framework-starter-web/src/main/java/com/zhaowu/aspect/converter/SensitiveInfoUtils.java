package com.zhaowu.aspect.converter;

import org.apache.commons.lang3.StringUtils;

public class SensitiveInfoUtils {

    public static String chineseName(String fullName) {
        if (StringUtils.isBlank(fullName)) {
            return "";
        } else {
            String name = StringUtils.left(fullName, 1);
            return StringUtils.rightPad(name, StringUtils.length(fullName), "*");
        }
    }

    public static String idCardNum(String idCardNum) {
        return StringUtils.isBlank(idCardNum)?"": StringUtils.left(idCardNum, 6).concat(
                StringUtils.removeStart(
                        StringUtils.leftPad(StringUtils.right(idCardNum, 4), StringUtils.length(idCardNum), "*"),
                        "******"));
    }

    public static String mobilePhone(String num) {
        return StringUtils.isBlank(num)?"": StringUtils.left(num, 3).concat(
                StringUtils.removeStart(
                        StringUtils.leftPad(StringUtils.right(num, 4), StringUtils.length(num), "*"),
                        "***"));
    }

    public static String bankCard(String cardNum) {
        return StringUtils.isBlank(cardNum)?"": StringUtils.left(cardNum, 4).concat(
                StringUtils.removeStart(
                        StringUtils.leftPad(StringUtils.right(cardNum, 4), StringUtils.length(cardNum), "*"),
                        "****"));
    }

}
