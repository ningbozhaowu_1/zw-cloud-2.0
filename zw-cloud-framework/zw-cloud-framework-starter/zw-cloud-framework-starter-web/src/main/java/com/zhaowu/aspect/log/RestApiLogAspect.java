package com.zhaowu.aspect.log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.zhaowu.aspect.converter.SensitiveDataConverter;
import org.apache.commons.io.IOUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.TimeZone;

/**
 * 日志切面
 */
@Configuration
@Aspect
public class RestApiLogAspect {

    /**
     * 日志
     */
    private final Logger log = LoggerFactory.getLogger(RestApiLogAspect.class);
    /**
     * 开始时间
     */
    private ThreadLocal<Long> startTime = new ThreadLocal();

    /**
     * 敏感信息加密
     */
    @Autowired
    private SensitiveDataConverter sc;

    /**
     * 日志拦截
     */
    @Pointcut("(execution(public * com.zhaowu.cloud.*.controller.*.*(..)))")
    public void log() {
        log.info("logging");
    }

    @Before("log()")
    public void before(JoinPoint joinPoint) {
        try {
            this.startTime.set(System.currentTimeMillis());
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            log.info("uri=[{}]", request.getRequestURI());
            log.info("header===>[{}]", this.sc.invokeMsg(this.printToLogStringByRequestHeader(request)));
            log.info("param===>[{}]", this.sc.invokeMsg(this.printToLogStringByQueryString(request.getQueryString())));
            log.info("body===>{}", this.sc.invokeMsg(this.printToLogStringByBody(request)));
            log.info("class=[{}], method=[{}]", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        } catch (Exception e) {
            log.error("日志输出错误", e);
        }
    }

    @AfterReturning(returning = "obj", pointcut = "log()")
    public void afterReturning(Object obj) {
        try {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            log.info("uri=[{}] end of execution cost [{}] ms", request.getRequestURI(),
                    System.currentTimeMillis()-(Long)this.startTime.get());
//            log.info("response=[{}]", this.sc.invokeMsg(this.bean2json(obj)));
        } catch (Exception e) {
            log.error("日志输出错误", e);
        }
    }

    @AfterThrowing(throwing = "ex", pointcut = "log()")
    public void afterThrowing(Throwable ex) {
        try {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            log.info("uri=[{}] end of execution cost [{}] ms", request.getRequestURI(),
                    System.currentTimeMillis()-(Long)this.startTime.get());
            log.info("catch unexcepted : {}", ex.getMessage());
        } catch (Exception e) {
            log.error("日志输出错误", e);
        }
    }



    private String bean2json(Object bean) {
        try {
            if (bean == null) {
                return "null";
            }
            ObjectMapper objectMapper = this.objectMapper();
            ObjectWriter writer = objectMapper.writer();
            return  writer.writeValueAsString(bean);
        } catch (JsonProcessingException e) {
            log.error("bean to json is error!", e);
            return "null";
        }
    }

    private ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
        objectMapper.setTimeZone(TimeZone.getTimeZone("UTF-8"));
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper;
    }


    private String printToLogStringByBody(HttpServletRequest request) {
        String body = null;
        ServletInputStream inputStream = null;

        try {
            inputStream = request.getInputStream();
            body = IOUtils.toString(inputStream, "UTF-8");
            return body;
        } catch (IOException e) {
            log.error("body to json is error!", e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return null;
    }

    private String printToLogStringByQueryString(String param) {
        if (param != null && !"".equals(param.trim())) {
            String[] ps = param.split("&");
            StringBuffer sb = new StringBuffer("");
            String[] var4 = ps;
            int var5 = ps.length;
            for (int var6 = 0;var6<var5;++var6) {
                String s = var4[var6];
                sb.append(s).append(";");
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    private String printToLogStringByRequestHeader(HttpServletRequest request) {
        Enumeration er = request.getHeaderNames();
        StringBuffer sb = new StringBuffer("");

        while (er.hasMoreElements()) {
            String name = (String) er.nextElement();
            sb.append(name).append("=").append(request.getHeader(name)).append(";");
        }
        return sb.toString();
    }


}
