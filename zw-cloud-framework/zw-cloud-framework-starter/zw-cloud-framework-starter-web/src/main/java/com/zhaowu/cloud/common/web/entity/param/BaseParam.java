package com.zhaowu.cloud.common.web.entity.param;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhaowu.cloud.common.web.entity.po.BasePo;
import lombok.Data;

import java.util.Date;

@Data
public class BaseParam<T extends BasePo> {

    private Long channelId;
    private String applicationParty;
    private String partyId;
    private String userId;
    private Date createdTimeStart;
    private Date createdTimeEnd;

    public QueryWrapper<T> build() {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge(null != this.createdTimeStart, "create_time", this.createdTimeStart)
                .le(null != this.createdTimeEnd, "create_time", this.createdTimeEnd);
        return queryWrapper;
    }
}
