package com.zhaowu.cloud.common.web.exception;

import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import com.zhaowu.cloud.framework.base.constant.SystemErrorStatus;
import com.zhaowu.cloud.framework.base.exception.BaseException;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartException;

@Slf4j
public class DefaultGlobalExceptionHandlerAdvice {

    @Value("${spring.profiles.active:dev}")
    public String activeProfile;

    private static final String DEV_MODE = "dev";
    private static final String TEST_MODE = "test";


    /**
     * 构建返回错误
     * @param status
     * @param exception
     * @return
     */
    private Result failure(ErrorStatus status, Exception exception){
        return failure(status,exception,null);
    }

    /**
     * 构建返回错误
     * @param status
     * @param exception
     * @param data
     * @return
     */
    private Result failure(ErrorStatus status,Exception exception,Object data){
        String meesage = status.getMessage();
        if(exception != null){
            meesage = this.getMessage(exception,status);
        }
        return Result.buildFailure(status.getCode(),meesage,data);
    }

    private String getMessage(Exception exception,ErrorStatus status){
        if(activeProfile.equalsIgnoreCase(DEV_MODE) || activeProfile.equalsIgnoreCase(TEST_MODE)){
            return exception.getMessage() != null ? exception.getMessage() : status.getMessage();
        }else{
            return status.getMessage();
        }
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public Result missingServletRequestParameterException(MissingServletRequestParameterException ex) {
        log.error(SystemErrorStatus.ARGUMENT_NOT_VALID.getMessage() + ":" + ExceptionUtils.getStackTrace(ex));
        return Result.buildFailure(SystemErrorStatus.ARGUMENT_NOT_VALID);
    }

    @ExceptionHandler(value = {MultipartException.class})
    public Result uploadFileLimitException(MultipartException ex) {
        log.error(SystemErrorStatus.UPLOAD_FILE_SIZE_LIMIT.getMessage() + ":" + ExceptionUtils.getStackTrace(ex));
        return Result.buildFailure(SystemErrorStatus.UPLOAD_FILE_SIZE_LIMIT);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public Result serviceException(MethodArgumentNotValidException ex) {
        log.error(SystemErrorStatus.ARGUMENT_NOT_VALID.getMessage() + ":" + ExceptionUtils.getStackTrace(ex));
        return Result.buildFailure(SystemErrorStatus.ARGUMENT_NOT_VALID, ex.getBindingResult().getFieldError().getDefaultMessage());
    }

    @ExceptionHandler(value = {DuplicateKeyException.class})
    public Result duplicateKeyException(DuplicateKeyException ex) {
        log.error(SystemErrorStatus.DUPLICATE_PRIMARY_KEY.getMessage() + ":" + ExceptionUtils.getStackTrace(ex));
        return Result.buildFailure(SystemErrorStatus.DUPLICATE_PRIMARY_KEY);
    }

    @ExceptionHandler(value = {BaseException.class})
    public Result baseException(BaseException ex) {
        log.error(ex.getMessage() + ":" + ExceptionUtils.getStackTrace(ex));
        return Result.buildFailure(ex.getErrorStatus());
    }

    @ExceptionHandler(value = {ServiceException.class})
    public Result serviceException(ServiceException ex) {
        log.error(ex.getMessage() + ":" + ExceptionUtils.getStackTrace(ex));
        return Result.buildFailure(ex.getMessage());
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result exception(Exception e) {
        log.error(SystemErrorStatus.SYSTEM_BUSY.getMessage() + ":" + ExceptionUtils.getStackTrace(e));
        return Result.buildFailure(SystemErrorStatus.SYSTEM_BUSY);
    }

    @ExceptionHandler(value = {Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result throwable() {
        return Result.buildFailure(SystemErrorStatus.SYSTEM_BUSY);
    }

    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.OK)
    public Result handleIllegalArgumentException(IllegalArgumentException e) {
        log.error(SystemErrorStatus.ILLEGAL_ARGUMENT.getMessage() + ":" + ExceptionUtils.getStackTrace(e));
        return failure(SystemErrorStatus.ILLEGAL_ARGUMENT,e);
    }
}