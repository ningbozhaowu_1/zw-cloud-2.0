package com.zhaowu.cloud.common.web.entity;//package com.zhaowu.cloud.framework.base.entity;
//
//import lombok.Data;
//
//import java.util.Date;
//
///**
// * @author xxp
// **/
//@Data
//public class BaseEntity {
//    /**
//     * 创建者
//     */
//    private Long createBy;
//    /**
//     * 创建时间
//     */
//    private Date createTime;
//    /**
//     * 修改者
//     */
//    private Long modifyBy;
//    /**
//     * 修改时间
//     */
//    private Date modifyTime;
//    /**
//     * 发生修改IP
//     */
//    private String modifyIp;
//}
