package com.zhaowu.cloud.common.web.audit;

import java.lang.annotation.*;

/**
 * 类SysLog的功能描述:
 * 系统日志注解
 *
 * @auther xxp
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableAudit {

    String value() default "";

}