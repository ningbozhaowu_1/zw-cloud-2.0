package com.zhaowu.cloud.framework.starter.map;

import com.zhaowu.cloud.framework.starter.map.properties.QCloudProperties;
import com.zhaowu.cloud.framework.map.MapService;
import com.zhaowu.cloud.framework.map.impl.QCloudMapServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 地图服务
 *
 * @author xxp
 * @Date 2020/12/11
 */
@Configuration
@EnableConfigurationProperties({QCloudProperties.class})
public class MapAutoConfiguration {

    @Autowired
    private QCloudProperties qCloudProperties;

    @Bean
    @ConditionalOnProperty(prefix = "zw.map", name = "type", havingValue = "qcloud" )
    @ConditionalOnMissingBean(MapService.class)
    @Order(0)
    public QCloudMapServiceImpl qCloudFileStorage() {

        QCloudMapServiceImpl qCloudMapService = new QCloudMapServiceImpl(qCloudProperties.getUrl(), qCloudProperties.getAccessKeyId());
        return qCloudMapService;
    }
}
