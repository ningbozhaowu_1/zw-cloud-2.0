package com.zhaowu.cloud.framework.starter.map.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 腾讯云文件上传属性配置
 *
 * @author xxp
 * @date 2020年12月11日
 */
@ConfigurationProperties(prefix = "zw.map.qcloud" )
public class QCloudProperties {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String accessKeyId;

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }
}
