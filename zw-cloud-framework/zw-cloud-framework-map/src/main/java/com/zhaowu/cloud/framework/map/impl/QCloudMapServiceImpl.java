package com.zhaowu.cloud.framework.map.impl;

import com.zhaowu.cloud.framework.map.MapService;
import com.zhaowu.cloud.framework.map.entity.Location;
import com.zhaowu.cloud.framework.util.DistanceCalculatorUtil;
import com.zhaowu.cloud.framework.util.FastJsonUtils;
import com.zhaowu.cloud.framework.util.HttpClientUtil;
import com.zhaowu.cloud.framework.map.entity.GeocoderResult;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class QCloudMapServiceImpl implements MapService {

    private String url;

    private String key;

    public QCloudMapServiceImpl(String url, String key) {
        this.key = key;
        this.url = url;
    }

    @Override
    public GeocoderResult geocoder(Location location) {

        Map params = new HashMap();
        params.put("key", key);
        params.put("address", String.format("%s%s%s%s", location.getProvince(),location.getCity(),location.getDistrict(),location.getAddress()));
        String result =  HttpClientUtil.get(url.concat("geocoder/v1/"),params);

        log.info(result);
        GeocoderResult geocoderResult = new GeocoderResult();
        Map resultMap = FastJsonUtils.json2map(result);
        String latitude = (((Map)((Map)resultMap.get("result")).get("location")).get("lat")).toString();
        String longitude = (((Map)((Map)resultMap.get("result")).get("location")).get("lng")).toString();
        geocoderResult.setStatus(resultMap.get("status").toString());
        geocoderResult.setMessage((String) resultMap.get("message"));
        geocoderResult.setLatitude(latitude);
        geocoderResult.setLongitude(longitude);
        return geocoderResult;
    }

    @Override
    public Double getDistance(Location location, String latitude, String longitude, String unit) {

        GeocoderResult geocoderResult = geocoder(location);
        String receiverLongitude =  geocoderResult.getLongitude();
        String receiverLatitude =  geocoderResult.getLatitude();
        return DistanceCalculatorUtil.distance(Double.valueOf(latitude),Double.valueOf(longitude),Double.valueOf(receiverLatitude), Double.valueOf(receiverLongitude),unit );
    }
}
