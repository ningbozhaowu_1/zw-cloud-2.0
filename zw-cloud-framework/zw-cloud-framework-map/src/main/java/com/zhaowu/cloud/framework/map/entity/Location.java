package com.zhaowu.cloud.framework.map.entity;

import lombok.Data;

@Data
public class Location {

    private String province;
    
    private String city;

    private String district;

    private String address;

    private String latitude;

    private String longitude;
}
