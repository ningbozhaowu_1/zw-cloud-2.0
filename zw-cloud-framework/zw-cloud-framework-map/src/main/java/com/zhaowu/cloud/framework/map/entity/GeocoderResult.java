package com.zhaowu.cloud.framework.map.entity;

import lombok.Data;

@Data
public class GeocoderResult {

    private String status;

    private String latitude;

    private String longitude;

    private String message;
}
