package com.zhaowu.cloud.framework.map;

import com.alicp.jetcache.anno.CachePenetrationProtect;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.zhaowu.cloud.framework.map.entity.Location;
import com.zhaowu.cloud.framework.map.entity.GeocoderResult;

/**
 * 地图服务
 */
public interface MapService {

    GeocoderResult geocoder(Location location);

    @Cached(name="distanceCache::", expire = 3600, cacheType = CacheType.BOTH, localLimit = 1000)
    @CachePenetrationProtect
    Double getDistance(Location location, String latitude, String longitude, String unit);
}
