package com.zhaowu.cloud.framework.base.protocol;

import lombok.Data;

import java.util.List;

/**
 * @author xxp
 * @date 2020/11/8
 */
@Data
public class PageListResult extends Result{

	/**
	 * page_list 页面配置列表
	 */
	private List<String> pageList;

}
