package com.zhaowu.cloud.framework.base.constant;

/**
 * 平台常量
 **/
public class CommonConstant {

    /** 默认参与者 */
    public static class DefaultParty {
        public static final String PartyId = "1";
    }

    public static final String INDICATOR_YES = "1";
    public static final String INDICATOR_NO = "0";

    /** 状态 */
    public static class Status {
        /** Active-活动 */
        public static final String A = "A";
        /** Invalid-失效 */
        public static final String I = "I";
        /** Pause-暂停 */
        public static final String P = "P";
    }

    /** pin状态 */
    public static class PinStatus extends Status{
        /** Lock- 锁定 */
        public static final String L = "L";
    }

    /** 商品状态 */
    public static class GoodsStatus{
        /** 上架  */
        public static final String A = "A";
        /**  下架  */
        public static final String P = "P";
    }

    /** 商品分类状态 */
    public static class GoodsTypeStatus{
        /** 正常  */
        public static final String A = "A";
        /**  禁用  */
        public static final String P = "P";
    }

    /** 商品分组状态 */
    public static class GoodsGroupStatus{
        /** 正常  */
        public static final String A = "A";
        /**  禁用  */
        public static final String P = "P";
    }

    /** 审核状态 */
    public static class ApplyStatus{
        /** 通过  Pass*/
        public static final String P = "P";
        /**  拒绝 Reject*/
        public static final String R = "R";
        /**  审核中*/
        public static final String A = "A";
    }

    /** 账户类型 */
    public static class AcType {
        /** 余额账户 */
        public static final String BAL = "BAL";

        /** 积分账户 */
        public static final String INTE = "INTE";

        /** 代金卡账户 */
        public static final String CASH = "CASH";
    }

    /** 参与者类型 */
    public static class PartyType {
        /** Per-个人 */
        public static final String P = "P";
        /** Ent-企业 */
        public static final String E = "E";
        /** Mgmt-管理 */
        public static final String M = "M";
        /** App-应用 */
        public static final String A = "A";
    }

    /** 用户类型 */
    public static class UserType {

        /** Per-个人用户 */
        public static final String PER = "PER";
        /** Ent-企业用户 */
        public static final String ENT = "ENT";
        /** MCH-商户 */
        public static final String MCH = "MCH";
        /** AGT-代理商 */
        public static final String AGT = "AGT";
        /** DIS-分销商 */
        public static final String DIS = "DIS";
    }

    /**
     * 角色编码
     */
    public static class RoleCode {

        /**普通用户*/
        public static final String CUSTOMER = "CUSTOMER";

        public static final String APP_ADMIN = "APP_ADMIN";

        public static final String SUPER_ADMIN = "SUPER_ADMIN";

        /**管理员*/
        public static final String ADMIN = "ADMIN";

        /**客服*/
        public static final String SERVICE = "SERVICE";

        /**商户*/
        public static final String MERCHANTS = "MERCHANTS";

        /**核销员*/
        public static final String VERIFIER = "VERIFIER";

        /**配送员*/
        public static final String COURIER = "COURIER";

        public static final String ENT = "ENT";
    }

    /** 角色类型 */
    public static class RoleType {

        /** 系统默认角色 */
        public static final String DF = "DF";

        /** 自定义角色 */
        public static final String CST = "CST";

    }

    /**  参与者申请类型 */
    public static class PartyApplyType {

        /** 商铺 */
        public static final String SHOP = "SHOP";

        /** 企业用户 */
        public static final String ENT = "ENT";

        /** 参与者渠道 */
        public static final String CHANNEL = "CHANNEL";

    }

    /** 菜单类型 */
    public static class MenuType {

        /** 基础菜单 */
        public static final String BM = "BM";

        /** 付费菜单*/
        public static final String PM = "PM";

    }

    /** Party关联关系类型 */
    public static class AssoType {
        /** 推荐人 Referrer */
        public static final String REF = "REF";
        /** 员工 Staff */
        public static final String STAFF = "STAFF";
        /** 人事 Hr */
        public static final String HR = "HR";
        /** 用户 Consumer */
        public static final String CON = "CON";
        /** 签约 Sign */
        public static final String SIGN = "SIGN";
        /** 持有 Owner */
        public static final String OWNER = "OWNER";
    }

    /** PartyAttrType属性类型 */
    public static class PartyAttrType {
        /** 标记 Indicator */
        public static final String IND = "IND";
        /** 支付 PAY */
        public static final String PAY = "PAY";
        /** 快递 EXPRESS */
        public static final String EXPRESS = "EXPRESS";
        /** 普通属性 Attr */
        public static final String THEME = "THEME";
    }


    public static class PartyAttrCodeTheme {

        public static final String BACK_COLOR = "BACK_COLOR";

        public static final String TEXT_COLOR_CHOOSED = "TEXT_COLOR_CHOOSED";

        public static final String TEXT_COLOR = "TEXT_COLOR";

        public static final String THEME_COLOR = "THEME_COLOR";

        public static final String THEME_COLOR2 = "THEME_COLOR2";
    }

    /** PartyAttrCodeEXPRESS关联关系类型 */
    public static class PartyAttrCodeEXPRESS {
        /** 快递发货功能是否开启 isExpress */
        public static final String IS_EXPRESS = "isExpress";
        /**同城配送功能是否开启*/
        public static final String IS_LOCAL = "isLocal";
        /**同城配送定时达功能是否开启*/
        public static final String IS_LOCAL_IN_TIME = "isLocalInTime";
        /**买家上门自提功能是否开启*/
        public static final String IS_SELF = "isSelf";
        /**计费方式: 1 按商品累加运费 2 组合运费（推荐使用）*/
        public static final String CALC_TYPE = "calcType";
    }

    /** Channel类型 */
    public static class ChannelType {
        /** 微信小程序 wechatMA */
        public static final String WECHATMA = "wechatMA";
        /** 支付宝小程序 aliMA */
        public static final String ALIMA = "aliMA";
        /** PC pweb */
        public static final String PWEB = "pweb";
        /** 微信公众号 wechatWma */
        public static final String WECHATWMA = "wechatWMA";
    }

    /** 信息类型 */
    public static class InfoType {

        /**  用户登陆失败 login_fail */
        public static final String LOGIN_FAIL = "LOGIN_FAIL";

        /**  用户登陆成功 login_success */
        public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";

        /**  订单状态 order */
        public static final String ORDER = "ORDER";

        /**  缓存更新 cacheUpdate */
        public static final String CACHE_UPDATE = "CACHE_UPDATE";

        /**  缓存更新 sms */
        public static final String SMS = "SMS";
    }

    /**应用类型*/
    public static class ApplicationType {
        /**微信小程序*/
        public static final String SOCIAL_TYPE_WECHAT_MINIAP = "wechatMA";
    }

    public static class MerchantType {

        /**微信直连商户*/
        public static final String WECHAT_BUSINESS = "wechatBusiness";

        /**微信特约商户*/
        public static final String WECHAT_SERVICE = "wechatService";

        /**支付宝直连商户*/
        public static final String ALIPAY_BUSINESS = "alipayBusiness";

        /**支付宝特约商户*/
        public static final String ALIPAY_SERVICE = "alipayService";
    }

    /** 统一参数Id */
    public static class ParamsId {
        /** 密钥 */
        public static final String EncryptKey = "EncryptKey";
        /** 短信模板 */
        public static final String SMSTemplate = "SMSTemplate";
        /** 邮箱模板 */
        public static final String EMSTemplate = "EMSTemplate";
    }

    /** 统一参数类型 */
    public static class ParamsType {
        /** 登录短信 */
        public static final String LOGIN_SMS = "LOGIN_SMS";

        /**企业入驻*/
        public static final String ENT_REG_SMS = "ENT_REG_SMS";

        /**密码修改*/
        public static final String PWD_MOD_SMS = "PWD_MOD_SMS";
    }

    /** 支付类型 */
    public static class PayType {

        public static final String WECHAT = "WECHAT";

        public static final String ALIPAY = "ALIPAY";

        public static final String BAL = "BAL";

        public static final String INT = "INT";

        public static final String CASH = "CASH";
    }

    /** 支付方式 */
    public static class PayModel {

        public static final String QR = "QR";

        public static final String MA = "MA";

        public static final String WMA = "WMA";

        public static final String APP = "APP";

        public static final String H5 = "H5";
    }

    public static class RouteKey {
        public static final String UC = "uc-route";
        public static final String COMMON = "common-route";
        public static final String notification = "notification-route";
        public static final String GATEWAYWEB = "gateway-web-route";
    }

    public static class Exchange {
        public static final String Default = "EXCHANGE_NAME";
    }

    public static class DistanceUnit {

        /**英里 Miles*/
        public static final String M = "M";
        /**公里 Kilometers*/
        public static final String K = "K";
        /**海里 Nautical Miles*/
        public static final String N = "N";

    }

    public static class DeliveryType {
        /**快递 Express*/
        public static final String EXP = "EXP";
        /**同城 TC*/
        public static final String TC = "TC";
        /**同城 自提*/
        public static final String ZITI = "ZITI";

        public static final String INVALID = "INVALID";

        /**无需配送*/
        public static final String UNNESSASERY = "UNNE";
    }

    public static class DeliveryFreeType {

        public static final String AMOUNT = "AMOUNT";
        public static final String DISTANCE = "DISTANCE";
    }

    /** 内容类型 */
    public static class ContentType {
        /** 公告 notice */
        public static final String NTC = "NTC";
        /** 广告 */
        public static final String AD = "AD";
        /** 热销产品 */
        public static final String HTP = "HTP";
        /** 慈善新闻 charity news */
        public static final String CTN = "CTN";
        /** Banner图 */
        public static final String BAN = "BAN";
        /** 新品推荐 new products */
        public static final String NPD = "NPD";
        /** 活动优惠 activity Discount */
        public static final String ATD = "ATD";
        /** 优惠商户 */
        public static final String MCT = "MCT";
        /** 反馈 */
        public static final String FB = "FB";
        /** 开屏广告图片 */
        public static final String OPADPT = "OPADPT";
        /** 优惠券 */
        public static final String CPN = "CPN";
        /** 客户留言 */
        public static final String CLM = "CLM";
    }

    /** 关联关系类型 */
    public static class ContentAssoType {
        /** 系统级内容 */
        public static final String SYSC = "SYSC";
        /** 订阅 subscribe */
        public static final String SBSE = "SBSE";

        /** 当为系统公告时作为notice type 使用,SYSC为系统普通公告 */
        /** 跑马灯公告 lamp notice */
        public static final String LAP = "LAP";
        /** 紧急公告 urgent notice */
        public static final String URG = "URG";
    }

    /** 资源类型-根据用途分类 */
    public static class ResourceType {
        /** 头像 */
        public static final String Avatar = "Avatar";
        /** 公钥 */
        public static final String PublicKey = "PublicKey";
        /** 广告图片 */
        public static final String AdImage = "AdImage";
        /** 说明书 */
        public static final String Specification = "Specification";
        /** 企业logo */
        public static final String EntLogo = "EntLogo";
        /** 反馈图片 */
        public static final String FbImage = "FbImage";
    }
}
