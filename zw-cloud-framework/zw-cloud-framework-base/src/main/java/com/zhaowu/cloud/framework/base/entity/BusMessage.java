package com.zhaowu.cloud.framework.base.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

@Data
public class BusMessage implements Serializable {

  private static final long serialVersionUID = 1L;

  private String infoType;

  private String from;

  private JSONObject jsonObject;

  private String standalone;
}
