package com.zhaowu.cloud.framework.base.exception;

/**
 * 平台服务层异常，主要是在业务数据或者状态异常时使用
 * @author xxp
 */
public class ServiceException extends RuntimeException{

    private String code;

    public ServiceException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public ServiceException(String code,String message) {
        super(message);
        this.code = code;
    }

    public ServiceException(String message) {
        super(message);
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
