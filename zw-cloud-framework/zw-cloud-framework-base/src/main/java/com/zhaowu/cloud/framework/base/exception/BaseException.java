package com.zhaowu.cloud.framework.base.exception;

import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import com.zhaowu.cloud.framework.base.constant.SystemErrorStatus;
import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {
    /**
     * 异常对应的错误类型
     */
    private final ErrorStatus errorStatus;

    /**
     * 默认是系统异常
     */
    public BaseException() {
        this.errorStatus = SystemErrorStatus.SYSTEM_ERROR;
    }

    public BaseException(ErrorStatus errorStatus) {
        this.errorStatus = errorStatus;
    }

    public BaseException(ErrorStatus errorStatus, String message) {
        super(message);
        this.errorStatus = errorStatus;
    }

    public BaseException(ErrorStatus errorStatus, String message, Throwable cause) {
        super(message, cause);
        this.errorStatus = errorStatus;
    }
}
