package com.zhaowu.cloud.framework.base.constant;

/**
 * @author xxp
 **/
public interface ErrorStatus {


    String getCode();


    String getMessage();

}
