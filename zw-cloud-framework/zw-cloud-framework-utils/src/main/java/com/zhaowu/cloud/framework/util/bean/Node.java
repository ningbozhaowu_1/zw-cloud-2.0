package com.zhaowu.cloud.framework.util.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Node implements Serializable {

    private String herf;
    private String name;
    private String icon;
    private String id;
    private String pid;
    private String status;
    private Long flag;
    private int orderNum;
    private List<Node> children;
}

