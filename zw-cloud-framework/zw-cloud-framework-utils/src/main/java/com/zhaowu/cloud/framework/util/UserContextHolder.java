package com.zhaowu.cloud.framework.util;

import com.google.common.collect.Maps;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 用户上下文
 */
public class UserContextHolder {

    private ThreadLocal<Map<String, Object>> threadLocal;

    private UserContextHolder() {
        this.threadLocal = new ThreadLocal<>();
    }

    /**
     * 创建实例
     *
     * @return
     */
    public static UserContextHolder getInstance() {
        return SingletonHolder.sInstance;
    }

    /**
     * 静态内部类单例模式
     * 单例初使化
     */
    private static class SingletonHolder {
        private static final UserContextHolder sInstance = new UserContextHolder();
    }

    /**
     * 用户上下文中放入信息
     *
     * @param map
     */
    public void setContext(Map<String, Object> map) {
        threadLocal.set(map);
    }

    /**
     * 获取上下文中的信息
     *
     * @return
     */
    public Map<String, Object> getContext() {
        return threadLocal.get();
    }

    /**
     * 获取上下文中的用户名
     *
     * @return
     */
    public String getUsername() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("user_name");
    }

    /**
     * 获取上下文中的域编号
     *
     * @return
     */
    public String getChannelId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("party_channel_id");
    }

    /**
     * 获取上下文中的应用编号
     *
     * @return
     */
    public String getApplicationParty() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("application_party");
    }

    /**
     * 获取上下文中的用户编号
     *
     * @return
     */
    public String getUserId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("user_id");
    }

    /**
     * 获取上下文中的参与者编号
     *
     * @return
     */
    public String getPartyId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("party_id");
    }

    /**
     * 获取上下文中的商户参与者编号
     *
     * @return
     */
    public String getMerchantPartyId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("merchant_party_id");
    }

    /**
     * 获取上下文中的openId
     *
     * @return
     */
    public String getOpenId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("openId");
    }

    public Collection<String> getAuthorities() {
        return (Collection<String>) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("authorities");
    }

    /**
     * 清空上下文
     */
    public void clear() {
        threadLocal.remove();
    }

}
