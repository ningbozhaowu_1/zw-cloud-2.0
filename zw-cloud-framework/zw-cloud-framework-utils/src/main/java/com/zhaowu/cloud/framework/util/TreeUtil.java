package com.zhaowu.cloud.framework.util;

import com.zhaowu.cloud.framework.util.bean.Node;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class TreeUtil {
    public static List data(String root, List<Node> nodes) {
        ArrayList<Node> rootNode = new ArrayList<>();
        for(Node node:nodes){
            if(root.equals(node.getPid())){
                rootNode.add(node);
            }
        }

        sort(rootNode);

        for(Node node:rootNode){
            List<Node> child = getChild(node, nodes);
            if(!CollectionUtils.isEmpty(child)){
                sort(child);
            }

            node.setChildren(child);
        }
        return rootNode;
    }

    private static void sort(List list){
        list.sort(new Comparator<Node>(){

            @Override
            public int compare(Node o1, Node o2) {
                if (o1.getOrderNum() > o2.getOrderNum()){
                    return -1;
                }else{
                    return 1;
                }
            }
        });
    }

    public static List<Node> getChild(Node pnode, List<Node> allNode) {

        boolean endTag = true;

        //存放子菜单的集合
        ArrayList<Node> listChild = new ArrayList<>();
        for (Node node : allNode) {
            if (node.getPid().equals(pnode.getId())) {
                if(node.getFlag()!=null
                        && node.getFlag()==0L){
                    pnode.setFlag(0L);
                }
                listChild.add(node);
            }
        }

        //递归：
        for (Node node : listChild) {
            node.setChildren(getChild(node, allNode));
        }
        if (listChild.size() == 0) {
            return null;
        }
        return listChild;
    }
}
