package com.zhaowu.cloud.framework.util;

import org.apache.commons.codec.binary.Base64;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.security.InvalidKeyException;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.apache.commons.lang.StringUtils;

public class HWUtils {

        // 编码方式
        private static final String CHARSET = "UTF-8";

        // 加密类型 256加密（AES256_CBC_PKCS5Padding）时传1，128加密（AES128_CBC_PKCS5Padding）时传2
        private static final String ENCRYPT_TYPE_256 = "1";

        /**
         * 校验通知消息的合法性
         * @param accessKey 接入码
         * @return 验证结果
         */

        public static boolean verificateRequestParams(Map<String, String[]> paramsMap, String accessKey) {

            String timeStamp = null;

            String authToken = null;

            String[] timeStampArray = paramsMap.get("timeStamp");

            if (null != timeStampArray && timeStampArray.length > 0) {

                timeStamp = timeStampArray[0];

            }

            String[] authTokenArray = paramsMap.get("authToken");

            if (null != authTokenArray && authTokenArray.length > 0) {

                authToken = authTokenArray[0];

            }

// 对剩下的参数进行排序，拼接成加密内容

            Map<String, String[]> sortedMap = new TreeMap<String, String[]>();

            sortedMap.putAll(paramsMap);

            sortedMap.remove("authToken");

            StringBuffer strBuffer = new StringBuffer();

            Set<String> keySet = sortedMap.keySet();

            Iterator<String> iter = keySet.iterator();

            while (iter.hasNext()) {

                String key = iter.next();

                String value = sortedMap.get(key)[0];

                strBuffer.append("&").append(key).append("=").append(value);

            }

// 修正消息体,去除第一个参数前面的&

            String reqParams = strBuffer.toString().substring(1);

            String key = accessKey + timeStamp;

            String signature = null;

            try {

                signature = generateResponseBodySignature(key, reqParams);

            } catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException

                    | UnsupportedEncodingException e) {

// TODO Auto-generated catch block

            }

            return authToken.equals(signature);

        }

        public static String generateResponseBodySignature(String key, String body)

                throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, UnsupportedEncodingException {

            return base_64(hmacSHA256(key, body));

        }

        public static byte[] hmacSHA256(String macKey, String macData) {

            try {

                try {

                    SecretKeySpec secret = new SecretKeySpec(macKey.getBytes(CHARSET), "HmacSHA256");

                    Mac mac = Mac.getInstance("HmacSHA256");

                    mac.init(secret);

                    return mac.doFinal(macData.getBytes(CHARSET));

                } catch (UnsupportedEncodingException e) {

                } catch (InvalidKeyException e) {

                }

            } catch (NoSuchAlgorithmException e) {

            }

            return new byte[0];

        }

// 服务商body签名

        public static String generateSaaSUsernameOrPwd(String isvBody, String decryptAccessKey, String sEncryptType) {

            String iv = getRandomChars(16);

            int iEncryptType = 0;

            try {

                iEncryptType = Integer.parseInt(sEncryptType);

            } catch (NumberFormatException exception) {

                iEncryptType = 1;

            }

            int encryptType;

            if (1 == iEncryptType) {

                encryptType = 256;

            } else {

                encryptType = 128;

            }

            String isvEncryptBody = encryptAESCBCEncode(isvBody, decryptAccessKey, iv, encryptType);

            return iv + isvEncryptBody;

        }

        /**

         * AES CBC 256位加密

         *

         * @param content 加密内容

         * @param key 加密秘钥

         * @param iv 加密盐值

         * @return 加密结果

         */

        public static String encryptAESCBCEncode(String content, String key, String iv, int encryptType) {

            if (StringUtils.isEmpty(content) || StringUtils.isEmpty(key) || StringUtils.isEmpty(iv)) {

                return null;

            }

            try {

                byte[] encrypContent =

                        encryptAESCBC(content.getBytes(CHARSET), key.getBytes(CHARSET), iv.getBytes(CHARSET), encryptType);

                if (null != encrypContent) {

                    return base_64(encrypContent);

                } else {

                    return null;

                }

            } catch (UnsupportedEncodingException e) {

                return null;

            }

        }

        public static byte[] encryptAESCBC(byte[] content, byte[] keyBytes, byte[] iv, int encryptType) {

            try {

                KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");

                SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");

                secureRandom.setSeed(keyBytes);

                keyGenerator.init(encryptType, secureRandom);

                SecretKey key = keyGenerator.generateKey();

                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

                cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));

                return cipher.doFinal(content);

            } catch (Exception e) {

            }

            return null;

        }

        public static String base_64(byte[] bytes) {

            try {

                return new String(Base64.encodeBase64(bytes), CHARSET);

            } catch (UnsupportedEncodingException e) {

                return null;

            }

        }

        static String decryptMobilePhoneOrEMail(String accessKey, String encryptStr, String sEncryptType) {

            String iv = encryptStr.substring(0, 16);

            int iEncryptType = 1;

            try {

                iEncryptType = Integer.parseInt(sEncryptType);

            } catch (NumberFormatException exception) {

                exception.printStackTrace();

            }

            int encryptType;

            if (1 == iEncryptType) {

                encryptType = 256;

            } else {

                encryptType = 128;

            }

            String decryptBody = null;

            try {

                decryptBody = decryptAESCBCEncode(encryptStr.substring(16), accessKey, iv, encryptType);

            } catch (Exception e) {

                e.printStackTrace();

                return decryptBody;

            }

            return decryptBody;

        }

        public static String decryptAESCBCEncode(String content, String key, String iv, int encryptType)

                throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,

                InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

            if (StringUtils.isEmpty(content) || StringUtils.isEmpty(key) || StringUtils.isEmpty(iv)) {

                return null;

            }

            return new String(decryptAESCBC(org.apache.commons.codec.binary.Base64.decodeBase64(content.getBytes()),

                    key.getBytes(), iv.getBytes(), encryptType));

        }

        public static byte[] decryptAESCBC(byte[] content, byte[] keyBytes, byte[] iv, int encryptType)

                throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,

                InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");

            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");

            secureRandom.setSeed(keyBytes);

            keyGenerator.init(encryptType, secureRandom);

            SecretKey key = keyGenerator.generateKey();

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));

            byte[] result = cipher.doFinal(content);

            return result;

        }

        /**

         * 获取随机字符串

         *

         * @param length 字符串长度

         * @return

         * @author d00420944

         */

        public static String getRandomChars(int length) {

            StringBuffer randomCharsBuf = new StringBuffer(1024);

            SecureRandom random = new SecureRandom();

            for (int i = 0; i < length; i++) {

// 字母和数字中随机

                if (random.nextInt(2) % 2 == 0) {

// 输出是大写字母还是小写字母

                    int letterIndex = random.nextInt(2) % 2 == 0 ? 65 : 97;

                    randomCharsBuf.append((char) (random.nextInt(26) + letterIndex));

                } else {

                    randomCharsBuf.append(String.valueOf(random.nextInt(10)));

                }

            }

            String randomChars = randomCharsBuf.toString();

            return randomChars;

        }

    }

