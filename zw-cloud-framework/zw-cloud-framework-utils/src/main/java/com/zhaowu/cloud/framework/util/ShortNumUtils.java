package com.zhaowu.cloud.framework.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ShortNumUtils {

    public static String getOutTradeNo(int length) {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss", Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);
        key = key + System.currentTimeMillis();
        key = key.substring(1, length+1);
        return key;
    }

    public static void main(String[] args) {
        System.out.println(getOutTradeNo(10));
    }
}
