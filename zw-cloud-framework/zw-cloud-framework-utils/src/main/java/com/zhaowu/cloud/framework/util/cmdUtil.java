package com.zhaowu.cloud.framework.util;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

@Slf4j
public class cmdUtil {

	public static String execute(String cmd) {

		log.info("执行命令:"+cmd);
		String[] cmdA = { "/bin/sh", "-c", cmd };
		String returnString = "";
		Process pro = null;
		Runtime runTime = Runtime.getRuntime();
		if (runTime == null) {
			log.error("Create runtime false!");
		}
		try {
			pro = runTime.exec(cmdA);
			BufferedReader input = new BufferedReader(new InputStreamReader(pro.getInputStream()));
			PrintWriter output = new PrintWriter(new OutputStreamWriter(pro.getOutputStream()));
			String line;
			while ((line = input.readLine()) != null) {
				returnString = returnString + line + "\n";
			}
			log.info("返回值:" + returnString);
			input.close();
			output.close();
			pro.destroy();
		} catch (IOException ex) {
			log.error(ex.getMessage());
		}
		return returnString;
	}
}
