package com.zhaowu.cloud.framework.util;


import com.alibaba.fastjson.JSONObject;
import io.goeasy.GoEasy;

/**
 * 使用goeasy第三方推送服务 向相同频道的其它用户推送消息
 */
public class GoeasyUtils {

    private final static String APPKEY = "BC-2f57ea58b4f14bb5883b1d03df2c3ec7";

    public static void send(String channel, JSONObject json){
        GoEasy goEasy = new GoEasy("https://rest-hangzhou.goeasy.io/publish",APPKEY);
        goEasy.publish(channel, json.toJSONString());
    }
}
