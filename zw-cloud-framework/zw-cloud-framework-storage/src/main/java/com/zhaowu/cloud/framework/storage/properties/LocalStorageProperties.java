package com.zhaowu.cloud.framework.storage.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 本地存储属性配置
 *
 * @author xxp
 * @date 2020-12-11
 */
@ConfigurationProperties(prefix = "zw.storage.local" )
public class LocalStorageProperties {
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
