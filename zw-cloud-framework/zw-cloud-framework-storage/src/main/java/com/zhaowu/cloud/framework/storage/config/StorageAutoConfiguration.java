package com.zhaowu.cloud.framework.storage.config;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.region.Region;
import com.zhaowu.cloud.framework.storage.FileStorage;
import com.zhaowu.cloud.framework.storage.properties.LocalStorageProperties;
import com.zhaowu.cloud.framework.storage.properties.QCloudProperties;
import com.zhaowu.cloud.framework.storage.impl.LocalFileStorage;
import com.zhaowu.cloud.framework.storage.impl.QCloudFileStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 文件上传自动配置
 *
 * @author xxp
 * @Date 2020/12/11
 */
@Configuration
@EnableConfigurationProperties({QCloudProperties.class, LocalStorageProperties.class})
public class StorageAutoConfiguration {

    @Autowired
    private QCloudProperties qCloudProperties;

    @Autowired
    private LocalStorageProperties localStorageProperties;

    /**
     * 根据配置文件自动装配腾讯云COS存储组件
     *
     * @return QCloudFileStorage.class
     */
    @Bean
    @ConditionalOnProperty(prefix = "zw.storage", name = "type", havingValue = "qcloud" )
    @ConditionalOnMissingBean(FileStorage.class)
    @Order(0)
    public QCloudFileStorage qCloudFileStorage() {
        COSCredentials cred = new BasicCOSCredentials(qCloudProperties.getAccessKeyId(), qCloudProperties.getAccessKeySecret());
        // 2 设置 bucket 的区域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region(qCloudProperties.getRegion());
        ClientConfig clientConfig = new ClientConfig(region);
        COSClient cosClient = new COSClient(cred, clientConfig);
        String host = String.format("https://%s.cos.%s.myqcloud.com",qCloudProperties.getBucketName(),qCloudProperties.getRegion());
        QCloudFileStorage qcloudFileUpload = new QCloudFileStorage(cosClient ,qCloudProperties.getBucketName(),host);
        return qcloudFileUpload;
    }

    /**
     * 根据配置文件自动装配本地存储组件
     *
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "zw.storage", name = "type", havingValue = "local" )
    @ConditionalOnMissingBean(FileStorage.class)
    @Order(1)
    public LocalFileStorage localFileStorage() {
        return new LocalFileStorage(localStorageProperties.getLocation());
    }

    /**
     * 默认配置用户目录作为存储目录
     *
     * @return
     */
    @Bean
    @Order(2)
    @ConditionalOnMissingBean(FileStorage.class)
    public LocalFileStorage defaultLocalStorage() {
        return new LocalFileStorage(System.getProperty("user.home" ));
    }
}
