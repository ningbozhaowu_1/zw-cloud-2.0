package com.zhaowu.cloud.framework.storage.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 腾讯云文件上传属性配置
 *
 * @author xxp
 * @date 2020年12月11日
 */
@ConfigurationProperties(prefix = "zw.storage.cos" )
public class QCloudProperties {

    /**
     * endpoint地址
     */
    private String endpoint;

    /**
     * oss key
     */
    private String accessKeyId;

    /**
     * oss密钥
     */
    private String accessKeySecret;

    /**
     * bucket名称
     */
    private String bucketName;

    /**
     * 存储桶区域
     */
    private String region;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
