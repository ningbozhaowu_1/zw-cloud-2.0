package com.zhaowu.cloud.framework.storage.impl;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.model.*;
import com.zhaowu.cloud.framework.storage.FileStorage;

import java.io.InputStream;

/**
 * 腾讯云文件上传
 *
 * @author xxp
 */
public class QCloudFileStorage implements FileStorage {

    /**
     * cos客户端
     */
    private COSClient cosClient = null;

    /**
     * cos的bucket名称
     */
    private String bucketName;

    public String getHost() {
        return host;
    }

    /**
     * cos的host
     */
    private String host;

    public QCloudFileStorage(COSClient cosClient, String bucketName, String host) {
        this.cosClient = cosClient;
        this.bucketName = bucketName;
        this.host = host;
    }

    @Override
    public void store(byte[] fileBytes, String key) {

    }

    @Override
    public void store(InputStream input, String key) {
        try{
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(input.available());
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, input, objectMetadata);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public byte[] getBytes(String key) {
        return new byte[0];
    }

    @Override
    public void remove(String key) {
        cosClient.deleteObject(bucketName,key);
    }

    @Override
    public InputStream getInputStream(String key) {

        COSObject cosObject = null;
        try{
            cosObject = cosClient.getObject(bucketName, key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return cosObject.getObjectContent();
    }

    @Override
    public String getDownloadUrl(String key) {
        return null;
    }
}
