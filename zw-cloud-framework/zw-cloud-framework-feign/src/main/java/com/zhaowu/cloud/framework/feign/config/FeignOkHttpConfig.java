package com.zhaowu.cloud.framework.feign.config;

import com.google.gson.Gson;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import feign.Feign;
import feign.FeignException;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/****
 *     需要修改成OKHTTP的客户端，需要在配置文件增加
 *     feign.httpclient.enabled=false
	   feign.okhttp.enabled=true
 */
@AutoConfigureBefore(FeignAutoConfiguration.class)
@Configuration
@ConditionalOnClass(Feign.class)
@Slf4j
public class FeignOkHttpConfig {

	private int feignOkHttpReadTimeout = 60;
	private int feignConnectTimeout = 60;
	private int feignWriteTimeout = 120;
	private int maxIdleConnections = 20;
	private int keepAliveDuration = 5;

	@Autowired
	private ObjectFactory<HttpMessageConverters> messageConverters;

	@Bean
	public Encoder encoder() {
		return new SpringFormEncoder(new SpringEncoder(messageConverters));
	}

	@Bean
	public okhttp3.OkHttpClient okHttpClient() {
		return new okhttp3.OkHttpClient.Builder()
				.readTimeout(feignOkHttpReadTimeout, TimeUnit.SECONDS)
				.connectTimeout(feignConnectTimeout, TimeUnit.SECONDS)
				.writeTimeout(feignWriteTimeout, TimeUnit.SECONDS)
				.connectionPool(new ConnectionPool(maxIdleConnections, keepAliveDuration, TimeUnit.MINUTES))   //自定义链接池
				.addInterceptor(new FeignOkHttpClientResponseInterceptor()) 	//自定义拦截器
				.build();
	}

	/**
	 * okHttp响应拦截器
	 */
	public static class FeignOkHttpClientResponseInterceptor implements Interceptor {

		@Override
		public Response intercept(Chain chain) throws IOException {

			Request originalRequest = chain.request();
			Response response = chain.proceed(originalRequest);

			MediaType mediaType = response.body().contentType();
			String content = response.body().string();
			//解析content
			Gson gson = new Gson();
			Result result = null;
			result = gson.fromJson(content,Result.class);
			log.info(result.toString());
			if(result.getSuccess()){
				return response.newBuilder()
						.body(ResponseBody.create(mediaType, content))
						.build();
			}else{
				return response.newBuilder()
						.body(ResponseBody.create(mediaType, content))
						.code(500)
						.build();
			}
		}
	}

}
