package com.zhaowu.cloud.framework.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Feign统一Token拦截器
 */
@SuppressWarnings("Duplicates")
@Component
@ConditionalOnClass(RequestInterceptor.class)
public class FeignTokenInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {

        if (null == getHttpServletRequest()) {
            // 此处省略日志记录
            return;
        }
        // 将获取Token对应的值往下面传
        Map<String, String> headersMap = getHeaders(getHttpServletRequest());
        if (headersMap != null && headersMap.size() > 0) {
            Set<Map.Entry<String, String>> entrySet = headersMap.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                String key = entry.getKey();
                String value = entry.getValue();
                // Authorization //headersMap.get("Authorization")
                requestTemplate.header(key, value);
            }

        }

    }

    private HttpServletRequest getHttpServletRequest() {
        try {
            return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Feign拦截器拦截请求获取Token对应的值
     *
     * @param request
     * @return
     */
    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> map = new LinkedHashMap<>();
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            String value = request.getHeader(key);
            if ("content-length".equals(key)) {
                continue;
            }
            map.put(key, value);
        }
        return map;
    }
}
