package com.zhaowu.cloud.framework.feign.exception;

import com.google.gson.Gson;
import com.zhaowu.cloud.framework.base.exception.BaseException;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@Slf4j
public class FeignExceptionErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {

        Gson gson = new Gson();

        Result result = null;

        try{
            result = gson.fromJson(response.body().asReader(),Result.class);
            log.info(result.toString());
        } catch (IOException e) {
            log.error("decode error:" + e);
        }

        return new ServiceException(result.getCode(), result.getMsg());
    }
}
