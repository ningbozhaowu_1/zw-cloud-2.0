package com.zhaowu.cloud.auth.authorization.exception;

import com.zhaowu.cloud.common.web.exception.DefaultGlobalExceptionHandlerAdvice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class AuthorizationExceptionHandlerAdvice extends DefaultGlobalExceptionHandlerAdvice {

}