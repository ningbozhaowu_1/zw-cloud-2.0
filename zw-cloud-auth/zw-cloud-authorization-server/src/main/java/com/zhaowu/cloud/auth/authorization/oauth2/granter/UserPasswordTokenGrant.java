package com.zhaowu.cloud.auth.authorization.oauth2.granter;

import com.alibaba.fastjson.JSONObject;
import com.zhaowu.cloud.auth.authorization.events.BusSender;
import com.zhaowu.cloud.auth.authorization.exception.AuthErrorStatus;
import com.zhaowu.cloud.auth.authorization.exception.CustomOauthException;
import com.zhaowu.cloud.auth.authorization.oauth2.token.UserPasswordAuthenticationToken;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class UserPasswordTokenGrant extends ResourceOwnerPasswordTokenGranter {

    private static final String GRANT_TYPE = "_password";

    private AuthenticationManager authenticationManager;

    private Integer errorCount;

    private BusSender busSender;

    public UserPasswordTokenGrant(AuthenticationManager authenticationManager, Integer errorCount, BusSender busSender,
                                AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
                                OAuth2RequestFactory requestFactory) {
        super(authenticationManager, tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
        this.errorCount = errorCount;
        this.busSender = busSender;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String username = parameters.get("username");
        String password = parameters.get("password");
        parameters.remove("password");

        //设置集成登录信息
        IntegrationAuthentication integrationAuthentication = new IntegrationAuthentication();
        integrationAuthentication.setAuthParameters(parameters);
        IntegrationAuthenticationContext.set(integrationAuthentication);

        Authentication userAuth = new UsernamePasswordAuthenticationToken(username, password);
        UserPasswordAuthenticationToken userPasswordAuthenticationToken = new UserPasswordAuthenticationToken(userAuth);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        try {
            userAuth = this.authenticationManager.authenticate(userPasswordAuthenticationToken);
        } catch (AccountStatusException ase) {
            log.error(ase.getMessage());
            throw new CustomOauthException(AuthErrorStatus.USER_ACCOUNT_IS_LOCKED);
        } catch (BadCredentialsException e) {
            log.error(e.getMessage());
            BusMessage message = new BusMessage();
            message.setStandalone("Y");
            message.setInfoType(CommonConstant.InfoType.LOGIN_FAIL);
            JSONObject jsonObject = new JSONObject();
            message.setJsonObject(jsonObject);
            jsonObject.put("userId", integrationAuthentication.getAuthParameter("userId"));
            Integer pwdErrorCount = IntegrationAuthenticationContext.get().getPwdErrorCount();
            jsonObject.put("pwdErrorCount", pwdErrorCount);
            jsonObject.put("errorCountBalance", errorCount-pwdErrorCount-1);
            busSender.send(CommonConstant.RouteKey.UC,message);
            throw new CustomOauthException(AuthErrorStatus.PASSWORD_ERROR.getCode(),String.format(AuthErrorStatus.PASSWORD_ERROR.getMessage(),errorCount-pwdErrorCount-1));
        }finally {
            IntegrationAuthenticationContext.clear();
        }
        if (userAuth != null && userAuth.isAuthenticated()) {
            BusMessage message = new BusMessage();
            message.setStandalone("Y");
            message.setInfoType(CommonConstant.InfoType.LOGIN_SUCCESS);
            JSONObject jsonObject = new JSONObject();
            message.setJsonObject(jsonObject);
            jsonObject.put("userId", integrationAuthentication.getAuthParameter("userId"));
            busSender.send("uc-route",message);
            OAuth2Request storedOAuth2Request = this.getRequestFactory().createOAuth2Request(client, tokenRequest);
            return new OAuth2Authentication(storedOAuth2Request, userAuth);
        } else {
            throw new InvalidGrantException("Could not authenticate user: " + username);
        }
    }
}
