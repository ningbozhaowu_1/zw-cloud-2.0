package com.zhaowu.cloud.auth.authorization.oauth2.provider;

import com.zhaowu.cloud.auth.authorization.oauth2.token.WechatAppAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 微信小程序登陆provider
 */
public class WecatMAAuthenticationProvider extends DaoAuthenticationProvider {

    public WecatMAAuthenticationProvider(UserDetailsService userDetailsService) {
        super.setUserDetailsService(userDetailsService);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return WechatAppAuthenticationToken.class.isAssignableFrom(authentication);
    }
}