package com.zhaowu.cloud.auth.authorization.exception;

import com.zhaowu.cloud.framework.base.constant.SystemErrorStatus;
import com.zhaowu.cloud.framework.base.exception.BaseException;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;

@Slf4j
public class CustomWebResponseExceptionTranslator extends DefaultWebResponseExceptionTranslator {

    /**
     * @param e spring security内部异常
     * @return 经过处理的异常信息
     * @throws Exception 通用异常
     */
    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
        log.error("Grant Error : " + e);
        if(e instanceof CustomOauthException){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body((CustomOauthException)e);
        } else if (e instanceof BaseException) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new CustomOauthException(((BaseException) e).getErrorStatus()));
        }else if (e instanceof UsernameNotFoundException) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new CustomOauthException(AuthErrorStatus.USERNAME_NOT_FOUND_INVALID));
        }else if(e instanceof LockedException){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new CustomOauthException(AuthErrorStatus.USER_ACCOUNT_IS_LOCKED));
        }else if(e instanceof BadCredentialsException){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new CustomOauthException(AuthErrorStatus.PASSWORD_ERROR));
        }else if(e instanceof InternalAuthenticationServiceException) {

            if(e.getCause() instanceof  ServiceException){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(new CustomOauthException(((ServiceException) e.getCause()).getCode(), ((ServiceException) e.getCause()).getMessage()));
            }else{
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(new CustomOauthException(SystemErrorStatus.SYSTEM_ERROR));
            }
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new CustomOauthException(SystemErrorStatus.SYSTEM_ERROR));
        }
    }
}