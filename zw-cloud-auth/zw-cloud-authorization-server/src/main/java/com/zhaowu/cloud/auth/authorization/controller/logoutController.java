package com.zhaowu.cloud.auth.authorization.controller;

import com.zhaowu.cloud.auth.authorization.entity.Token;
import com.zhaowu.cloud.framework.base.controller.BaseController;
import com.zhaowu.cloud.framework.base.protocol.Result;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "登出 接口")
public class logoutController extends BaseController {

    @Autowired
    @Qualifier("consumerTokenServices")
    private ConsumerTokenServices tokenServices;

    @DeleteMapping("/signout")
    public Result logout(@RequestBody Token token) {
        if (tokenServices.revokeToken(token.getAccessToken())) {
            return this.success();
        } else {
            return this.failure();
        }
    }
}
