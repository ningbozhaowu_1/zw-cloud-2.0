package com.zhaowu.cloud.auth.authorization.oauth2.enhancer;

import com.google.common.collect.Maps;
import com.zhaowu.cloud.auth.authorization.entity.userdetail.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.Map;

/**
 * 自定义token携带内容
 */
public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        Map<String, Object> additionalInfo = Maps.newHashMap();

        if(authentication.getUserAuthentication()!=null){
            User Principal = ((User)authentication.getUserAuthentication().getPrincipal());
            additionalInfo.put("user_id", Principal.getId());
            additionalInfo.put("application_party", Principal.getApplicationParty());
            additionalInfo.put("party_id", Principal.getPartyId());
            additionalInfo.put("party_channel_id", Principal.getPartyChannelId());
            additionalInfo.put("openId", Principal.getOpenId());
            additionalInfo.put("roleIds", Principal.getRole());
            additionalInfo.put("merchant_party_id", Principal.getMerchantPartyId());
        }
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}