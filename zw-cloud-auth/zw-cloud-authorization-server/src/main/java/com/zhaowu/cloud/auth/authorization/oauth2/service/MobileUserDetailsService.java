package com.zhaowu.cloud.auth.authorization.oauth2.service;

import com.zhaowu.cloud.auth.authorization.entity.userdetail.User;
import com.zhaowu.cloud.auth.authorization.exception.AuthErrorStatus;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.UserQueryForm;
import com.zhaowu.cloud.uc.entity.vo.UserAuthentication;
import com.zhaowu.cloud.vcc.client.VerificationCodeClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * 手机验证码登陆, 用户相关获取
 */
@Slf4j
@Service("mobileUserDetailsService")
public class MobileUserDetailsService extends CustomUserDetailsService {

    @Autowired
    private UserClient userClient;

    @Autowired
    private VerificationCodeClient verificationCodeClient;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public UserDetails loadUserByUsername(String uniqueId) {

        IntegrationAuthentication integrationAuthentication = IntegrationAuthenticationContext.get();

        Result<Boolean> sms = verificationCodeClient.validate(CommonConstant.ParamsType.LOGIN_SMS,integrationAuthentication.getAuthParameter("token"),integrationAuthentication.getAuthParameter("password"),uniqueId);

        if(!sms.getData()){
            throw  new ServiceException(AuthErrorStatus.TOKEN_INVALID.getCode(), AuthErrorStatus.TOKEN_INVALID.getMessage());
        }

        String applicationParty = integrationAuthentication.getAuthParameter("application_party");
        UserQueryForm userQueryForm = new UserQueryForm();
        userQueryForm.setMobileNo(uniqueId);
        userQueryForm.setScene("2");
        userQueryForm.setApplicationParty(applicationParty);

        Result<UserAuthentication> result = userClient.findUser4Login(userQueryForm);
        log.info("load user by username :{}", result);

        UserAuthentication userAuthentication = result.getData();

        User userAuthrization = new User();
        userAuthrization.setId(userAuthentication.getId());
        userAuthrization.setApplicationParty(userAuthentication.getApplicationParty());
        userAuthrization.setOpenId(userAuthentication.getOpenId());
        userAuthrization.setPassword(encoder.encode(integrationAuthentication.getAuthParameter("password")));
        userAuthrization.setUsername(uniqueId);
        userAuthrization.setStatus(userAuthentication.getStatus());
        userAuthrization.setRole(userAuthentication.getRoleIds());
        userAuthrization.setPartyId(userAuthentication.getPartyId());
        userAuthrization.setPartyChannelId(integrationAuthentication.getAuthParameter("party_channel_id"));
        userAuthrization.setMerchantPartyId(userAuthentication.getMerchantPartyId());
        return userAuthrization;
    }
}
