package com.zhaowu.cloud.auth.authorization.oauth2.granter;

import com.google.common.collect.Maps;
import com.zhaowu.cloud.auth.authorization.exception.AuthErrorStatus;
import com.zhaowu.cloud.auth.authorization.exception.CustomOauthException;
import com.zhaowu.cloud.auth.authorization.oauth2.token.MobileAuthenticationToken;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class MobileTokenGranter extends ResourceOwnerPasswordTokenGranter {

    private static final String GRANT_TYPE = "mobile";

    private AuthenticationManager authenticationManager;

    public MobileTokenGranter(AuthenticationManager authenticationManager,
                              AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
                              OAuth2RequestFactory requestFactory) {
        super(authenticationManager, tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String token = parameters.get("token");

        if(StringUtils.isEmpty(token)){
            throw new CustomOauthException(AuthErrorStatus.SMS_TOKEN_INVALID);
        }

        String username = parameters.get("username");
        String password = parameters.get("password");
//        parameters.remove("password");

        //设置集成登录信息
        IntegrationAuthentication integrationAuthentication = new IntegrationAuthentication();
        integrationAuthentication.setAuthParameters(parameters);
        IntegrationAuthenticationContext.set(integrationAuthentication);

        Authentication userAuth = new UsernamePasswordAuthenticationToken(username, password);
        MobileAuthenticationToken mobileAuthenticationToken = new MobileAuthenticationToken(userAuth);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        try {
            userAuth = this.authenticationManager.authenticate(mobileAuthenticationToken);
        } catch (AccountStatusException ase) {
            log.error(ase.getMessage());
            throw ase;
        } catch (BadCredentialsException e) {
            log.error(e.getMessage());
            throw new CustomOauthException(AuthErrorStatus.TOKEN_INVALID);
        }finally {
            IntegrationAuthenticationContext.clear();
        }
        if (userAuth != null && userAuth.isAuthenticated()) {
            OAuth2Request storedOAuth2Request = this.getRequestFactory().createOAuth2Request(client, tokenRequest);
            return new OAuth2Authentication(storedOAuth2Request, userAuth);
        } else {
            throw new InvalidGrantException("Could not authenticate user: " + username);
        }
    }
}
