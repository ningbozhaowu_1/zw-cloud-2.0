package com.zhaowu.cloud.auth.authorization.oauth2.granter;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhaowu.cloud.auth.authorization.exception.AuthErrorStatus;
import com.zhaowu.cloud.auth.authorization.exception.CustomOauthException;
import com.zhaowu.cloud.auth.authorization.oauth2.token.UserPasswordAuthenticationToken;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import com.zhaowu.cloud.auth.client.service.IAuthService;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.entity.BusMessage;
import com.zhaowu.cloud.framework.base.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MyRefreshTokenGranter extends AbstractTokenGranter {

    private static final String GRANT_TYPE = "_refresh_token";

    private StringRedisTemplate stringRedisTemplate;

    private IAuthService authService;

    public MyRefreshTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
                                 OAuth2RequestFactory requestFactory, StringRedisTemplate stringRedisTemplate,
                                 IAuthService authService) {
        this(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE, stringRedisTemplate, authService);
    }

    protected MyRefreshTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
                                    OAuth2RequestFactory requestFactory, String grantType,
                                    StringRedisTemplate stringRedisTemplatem,IAuthService authService) {
        super(tokenServices, clientDetailsService, requestFactory, grantType);
        this.stringRedisTemplate = stringRedisTemplate;
        this.authService = authService;
    }

//    private String getUserToken(String authentication) {
//        String token = "{}";
//        try {
//            token = new ObjectMapper().writeValueAsString(authService.getJwt(authentication).getBody());
//            return token;
//        } catch (JsonProcessingException e) {
//            log.error(e.getMessage());
//        }
//        return token;
//    }

    protected OAuth2AccessToken getAccessToken(ClientDetails client, TokenRequest tokenRequest) {

        String refreshToken = tokenRequest.getRequestParameters().get("refresh_token");

        if(authService.invalidJwtRefreshToken(refreshToken)){
            throw new BaseException(AuthErrorStatus.TOKEN_EXPIRED);
        }

        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
//        String token = getUserToken(refreshToken);
//
//        String username = "";
//        try {
//            username = (String) new ObjectMapper().readValue(token, Map.class).get("user_name");
//            parameters.put("username", username);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //设置集成登录信息
        IntegrationAuthentication integrationAuthentication = new IntegrationAuthentication();
        integrationAuthentication.setAuthParameters(parameters);
        IntegrationAuthenticationContext.set(integrationAuthentication);

        try {
            OAuth2AccessToken auth2AccessToken =  this.getTokenServices().refreshAccessToken(refreshToken, tokenRequest);
            return auth2AccessToken;
        } catch (AccountStatusException var5) {
            throw new InvalidGrantException(var5.getMessage());
        } catch (UsernameNotFoundException var6) {
            throw new InvalidGrantException(var6.getMessage());
        }

    }
}