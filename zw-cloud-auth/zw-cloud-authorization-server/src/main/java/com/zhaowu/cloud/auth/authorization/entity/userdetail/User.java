package com.zhaowu.cloud.auth.authorization.entity.userdetail;

import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author xxp
 **/
public class User implements UserDetails, CredentialsContainer {

    private static final long serialVersionUID = 1L;
    private String id;
    private String username;
    private String password;
    private String status;
    private String type;
    private String phoneNumber;
    private String email;
    private String name;
    private Collection<String> resources = new ArrayList<>();
    private Collection<String> roles = new ArrayList<>();
    private Collection<GrantedAuthority> grantedAuthorities;
    private String partyChannelId;
    private String partyId;
    private String applicationParty;
    private String merchantPartyId;
    private String openId;

    public String getMerchantPartyId() {
        return merchantPartyId;
    }

    public void setMerchantPartyId(String merchantPartyId) {
        this.merchantPartyId = merchantPartyId;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPartyChannelId() {
        return partyChannelId;
    }

    public void setPartyChannelId(String partyChannelId) {
        this.partyChannelId = partyChannelId;
    }

    public String getApplicationParty() {
        return applicationParty;
    }

    public void setApplicationParty(String applicationParty) {
        this.applicationParty = applicationParty;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (grantedAuthorities == null) {
            this.grantedAuthorities = this.getRole().stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
        }
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return CommonConstant.Status.A.equals(status);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return CommonConstant.Status.A.equals(status);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Collection<String> getRole() {
        return roles;
    }

    public void setRole(Collection<String> roles) {
        this.roles = roles;
    }

    public Collection<String> getResources() {
        return resources;
    }

    public void setResources(Collection<String> resources) {
        this.resources = resources;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void eraseCredentials() {
        this.password = null;
    }
}
