package com.zhaowu.cloud.auth.authorization.oauth2.service;

import com.zhaowu.cloud.auth.authorization.entity.userdetail.User;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import com.zhaowu.cloud.auth.authorization.service.IUserService;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.UserQueryForm;
import com.zhaowu.cloud.uc.entity.vo.UserAuthentication;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * 微信小程序jscode登陆, 用户相关获取
 */
@Slf4j
@Service("wechatMiniUserDetailsService")
public class WechatMAUserDetailsService extends CustomUserDetailsService {

    @Autowired
    private UserClient userClient;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public UserDetails loadUserByUsername(String uniqueId) {

        IntegrationAuthentication integrationAuthentication = IntegrationAuthenticationContext.get();
        String applicationParty = integrationAuthentication.getAuthParameter("application_party");
        String mobile = integrationAuthentication.getAuthParameter("mobile");
        UserQueryForm userQueryForm = new UserQueryForm();
        userQueryForm.setMobileNo(mobile);
        userQueryForm.setScene("3");
        userQueryForm.setApplicationParty(applicationParty);
        userQueryForm.setOpenId(uniqueId);
        userQueryForm.setPartyChannelId(integrationAuthentication.getAuthParameter("party_channel_id"));

        Result<UserAuthentication> result = userClient.findUser4Login(userQueryForm);
        log.info("load user by username :{}", result);

        UserAuthentication userAuthentication = result.getData();

        User userAuthrization = new User();
        userAuthrization.setId(userAuthentication.getId());
        userAuthrization.setApplicationParty(userAuthentication.getApplicationParty());
        userAuthrization.setOpenId(uniqueId);
        userAuthrization.setPassword(encoder.encode(integrationAuthentication.getAuthParameter("password")));
        userAuthrization.setUsername(uniqueId);
        userAuthrization.setStatus(userAuthentication.getStatus());
        userAuthrization.setRole(userAuthentication.getRoleIds());
        userAuthrization.setPartyId(userAuthentication.getPartyId());
        userAuthrization.setPartyChannelId(integrationAuthentication.getAuthParameter("party_channel_id"));
        userAuthrization.setMerchantPartyId(userAuthentication.getMerchantPartyId());
        return userAuthrization;
    }
}
