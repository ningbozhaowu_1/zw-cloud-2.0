package com.zhaowu.cloud.auth.authorization.entity;

import lombok.Data;

/**
 * @author xxp
 **/
@Data
public class WechatMiniAppToken {

    private String openId;
    private String unionId;
    private String sessionKey;
    private String username;
    private String encryptedData;
    private String iv;

    public WechatMiniAppToken(String openId, String unionId, String sessionKey) {
        this.openId = openId;
        this.unionId = unionId;
        this.sessionKey = sessionKey;
    }
}
