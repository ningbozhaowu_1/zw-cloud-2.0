package com.zhaowu.cloud.auth.authorization.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SmsForm implements Serializable {

    @NotNull(message="手机号不能为空")
    @ApiModelProperty("手机号")
    private String phoneNumber;
}
