package com.zhaowu.cloud.auth.authorization.oauth2.token;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 * 自定义用户名密码登陆Token认证类
 */
public class UserPasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public UserPasswordAuthenticationToken(Authentication authenticationToken) {
        super(authenticationToken.getPrincipal(), authenticationToken.getCredentials());
    }
}
