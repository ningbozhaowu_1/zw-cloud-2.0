package com.zhaowu.cloud.auth.authorization.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import com.zhaowu.cloud.framework.base.protocol.Result;
import lombok.EqualsAndHashCode;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

@EqualsAndHashCode(callSuper = true)
@JsonSerialize(using = CustomOauthExceptionSerializer.class)
public class CustomOauthException extends OAuth2Exception {

    private final Result result;

    public Result getResult() {
        return result;
    }

    CustomOauthException(OAuth2Exception oAuth2Exception) {
        super(oAuth2Exception.getSummary(), oAuth2Exception);
        this.result = Result.buildFailure(AuthErrorStatus.valueOf(oAuth2Exception.getOAuth2ErrorCode().toUpperCase()), oAuth2Exception);
    }

    public CustomOauthException(ErrorStatus status) {
        super(status.getMessage());
        this.result = Result.buildFailure(status.getCode(),status.getMessage(),null);
    }

    public CustomOauthException(String code, String message) {
        super(message);
        this.result = Result.buildFailure(code,message,null);
    }

    public CustomOauthException(Exception exception) {
        super(exception.getMessage());
        this.result = Result.buildFailure(exception.getMessage());
    }
}