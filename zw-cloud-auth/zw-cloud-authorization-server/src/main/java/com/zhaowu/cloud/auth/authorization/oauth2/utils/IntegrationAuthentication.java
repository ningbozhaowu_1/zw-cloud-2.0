package com.zhaowu.cloud.auth.authorization.oauth2.utils;

import lombok.Data;

import java.util.Map;

/**
 * @author xxp
 **/
@Data
public class IntegrationAuthentication {

    private String authType;
    private String username;
    private Map<String,String> authParameters;
    private Integer pwdErrorCount;

    public String getAuthParameter(String paramter){
        return this.authParameters.get(paramter);
    }
}
