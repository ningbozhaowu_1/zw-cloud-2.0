//package com.zhaowu.cloud.auth.authorization.provider;
//
//import com.zhaowu.cloud.auth.authorization.entity.Role;
//import com.zhaowu.cloud.auth.authorization.entity.User;
//import com.zhaowu.cloud.framework.base.protocol.Result;
//import org.springframework.stereotype.Component;
//
//import java.util.HashSet;
//import java.util.Set;
//
//@Component
//public class OrganizationProviderFallback implements OrganizationProvider {
//
//    @Override
//    public Result<User> getUserByUniqueId(String uniqueId) {
//
//        User user = new User();
//        user.setUsername("admin");
//        user.setId("1");
//        user.setPassword("$2a$10$Tj8nuSCkRuLHCQZKiHpYju1mo2KwUru271fhUdud9ARduC4VRTVCO");
//        user.setAccountNonExpired(true);
//        user.setAccountNonLocked(true);
//        user.setEnabled(true);
//        user.setCredentialsNonExpired(true);
//
//        return Result.buildSuccess(user);
//    }
//
//    @Override
//    public Result<Set<Role>> queryRolesByUserId(String userId) {
//        return Result.buildSuccess(new HashSet<Role>());
//    }
//}
