package com.zhaowu.cloud.auth.authorization.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 用户密码
 * </p>
 *
 * @author xxp
 * @since 2021-01-21
 */
@Data
@Accessors(chain = true)
@TableName("user_pin")
public class UserPin implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 参与者编号
     */
    @TableField("party_id")
    private String partyId;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 密码状态
     */
    private String status;

    /**
     * 变更类型
     */
    @TableField("expire_type")
    private String expireType;

    /**
     * 起效时间
     */
    @TableField("effect_time")
    private Date effectTime;

    /**
     * 失效时间
     */
    @TableField("expire_time")
    private Date expireTime;

    /**
     * 密码错误次数
     */
    @TableField("pwd_err_count")
    private Integer pwdErrCount;

    /**
     * 密码错误时间
     */
    @TableField("pwd_err_time")
    private Date pwdErrTime;

    /**
     * 最近密码修改时间
     */
    @TableField("pwd_last_update")
    private Date pwdLastUpdate;

    /**
     * 最近密码解锁时间
     */
    @TableField("pwd_last_unlock")
    private Date pwdLastUnlock;

    /**
     * 首次登陆时间
     */
    @TableField("pwd_first_login")
    private Date pwdFirstLogin;

    /**
     * 最近登陆时间
     */
    @TableField("pwd_last_login")
    private Date pwdLastLogin;

    /**
     * 最近登陆地址
     */
    @TableField("pwd_last_client")
    private String pwdLastClient;

    /**
     * 登陆次数
     */
    @TableField("login_count")
    private Integer loginCount;
}
