package com.zhaowu.cloud.auth.authorization.entity.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginSmsForm extends SmsForm {


    @NotNull(message="验证参数不能为空")
    @ApiModelProperty("验证参数")
    private String captchaVerification;
}
