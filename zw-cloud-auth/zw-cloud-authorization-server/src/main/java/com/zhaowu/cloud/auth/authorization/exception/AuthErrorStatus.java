package com.zhaowu.cloud.auth.authorization.exception;

import com.zhaowu.cloud.framework.base.constant.ErrorStatus;
import lombok.Getter;

@Getter
public enum AuthErrorStatus implements ErrorStatus {

    INVALID_REQUEST("040001", "无效请求"),
    INVALID_CLIENT("040002", "无效client_id"),
    INVALID_GRANT("040003", "无效授权"),
    INVALID_SCOPE("040004", "无效scope"),
    INVALID_TOKEN("040005", "无效token"),
    INVALID_APP("040006", "未授权的应用"),
    INSUFFICIENT_SCOPE("040010", "授权不足"),
    REDIRECT_URI_MISMATCH("040020", "redirect url不匹配"),
    ACCESS_DENIED("040030", "拒绝访问"),
    METHOD_NOT_ALLOWED("040040", "不支持该方法"),
    SERVER_ERROR("040050", "权限服务错误"),
    UNAUTHORIZED_CLIENT("040060", "未授权客户端"),
    UNAUTHORIZED("040061", "未授权"),
    UNSUPPORTED_RESPONSE_TYPE("040070", " 支持的响应类型"),
    UNSUPPORTED_GRANT_TYPE("040071", "不支持的授权类型"),
    SMS_TOKEN_INVALID("040131", "短信token参数错误"),
    TOKEN_INVALID("040081", "验证码错误或已过期"),
    USERNAME_NOT_FOUND_INVALID("040091", "用户未注册"),
    USER_ACCOUNT_IS_LOCKED("040100", "用户已锁定"),
    PASSWORD_ERROR("0400110", "密码错误,剩余错误次数%d"),
    WECHAT_APP_ERROR("0400120", "获取微信小程序用户信息失败"),
    TOKEN_EXPIRED("999999", "登录超时");

    /**
     * 错误类型码
     */
    private String code;
    /**
     * 错误类型描述信息
     */
    private String message;

    AuthErrorStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
