package com.zhaowu.cloud.auth.authorization.oauth2.token;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 * 微信小程序登陆Token认证类
 */
public class WechatAppAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public WechatAppAuthenticationToken(Authentication authenticationToken) {
        super(authenticationToken.getPrincipal(), authenticationToken.getCredentials());
    }
}
