package com.zhaowu.cloud.auth.authorization.oauth2.granter;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.config.WxMaInMemoryConfig;
import cn.binarywang.wx.miniapp.util.crypt.WxMaCryptUtils;
import com.google.common.collect.Maps;
import com.google.gson.GsonBuilder;
import com.zhaowu.cloud.auth.authorization.entity.WechatMiniAppToken;
import com.zhaowu.cloud.auth.authorization.exception.AuthErrorStatus;
import com.zhaowu.cloud.auth.authorization.exception.CustomOauthException;
import com.zhaowu.cloud.auth.authorization.oauth2.token.WechatAppAuthenticationToken;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import com.zhaowu.cloud.framework.base.constant.CommonConstant;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.client.UserClient;
import com.zhaowu.cloud.uc.entity.form.PartyChannelQueryForm;
import com.zhaowu.cloud.uc.entity.vo.PartyChannelVO;
import com.zhaowu.cloud.wx.third.entity.form.JscodeLoginForm;
import com.zhaowu.cloud.wx.third.entity.model.xcx.Jscode2SessionResult;
import com.zhaowu.cloud.wxThird.client.WxThirdClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class WechatMATokenGranter extends ResourceOwnerPasswordTokenGranter {

    private AuthenticationManager authenticationManager;

    private WxThirdClient wxThirdClient;

    private UserClient userClient;

    private static final GsonBuilder INSTANCE = new GsonBuilder();

    public WechatMATokenGranter(AuthenticationManager authenticationManager, WxThirdClient wxThirdClient,UserClient userClient,
                                AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
                                OAuth2RequestFactory requestFactory) {
        super(authenticationManager, tokenServices, clientDetailsService, requestFactory, CommonConstant.ChannelType.WECHATMA);
        this.authenticationManager = authenticationManager;
        this.wxThirdClient = wxThirdClient;
        this.userClient = userClient;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
//        String username = parameters.get("username");
        String password = parameters.get("password");
        // Protect from downstream leaks of password
//        parameters.remove("password");

        WxMaJscode2SessionResult session = null;
        String openId = "";
        String unionId = "";
        String sessionKey = "";
        String phoneNumber = null;
        try {
            String partyChannelId = parameters.get("party_channel_id");
            String applicationParty = parameters.get("application_party");
            PartyChannelQueryForm partyChannelQueryForm = new PartyChannelQueryForm();
            partyChannelQueryForm.setPartyChannelId(partyChannelId);
            partyChannelQueryForm.setApplicationParty(applicationParty);
            Result<PartyChannelVO> result = this.userClient.getPartyChannel(partyChannelQueryForm);

            if(!result.getSuccess() || result.getData()==null){
                throw new CustomOauthException(AuthErrorStatus.INVALID_APP);
            }

            PartyChannelVO partyChannelVO = result.getData();

            if(!StringUtils.isEmpty(partyChannelVO.getSecret())){
                WxMaInMemoryConfig config = new WxMaInMemoryConfig();
                config.setAppid(partyChannelVO.getAuthorizerAppId());
                config.setSecret(partyChannelVO.getSecret());
                config.setToken(partyChannelVO.getToken());
                config.setAesKey(partyChannelVO.getAesKey());
                WxMaService service = new WxMaServiceImpl();
                service.setWxMaConfig(config);

                session = service.getUserService().getSessionInfo(password);
                openId = session.getOpenid();
                unionId = session.getUnionid();
                sessionKey = session.getSessionKey();
            }else{// 开放平台登录

                JscodeLoginForm jscodeLoginForm = new JscodeLoginForm();
                jscodeLoginForm.setAuthorizerAppid(partyChannelVO.getAuthorizerAppId());
                jscodeLoginForm.setJsCode(password);
                Result<Jscode2SessionResult> result2 = wxThirdClient.miniProgramJscode2Session(jscodeLoginForm);
                openId = result2.getData().getOpenId();
                sessionKey = result2.getData().getSessionKey();
            }

            log.info("iv>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+parameters.get("iv"));
            log.info("encryptedData>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+parameters.get("encryptedData"));

            if(parameters.get("iv")!=null
                    && parameters.get("encryptedData")!=null){
                WechatMiniAppToken wechatToken = new WechatMiniAppToken(openId, unionId, sessionKey);
                wechatToken.setIv(parameters.get("iv"));
                wechatToken.setEncryptedData(parameters.get("encryptedData"));
                String data = WxMaCryptUtils.decrypt(sessionKey, wechatToken.getEncryptedData(), wechatToken.getIv());
                Map map = INSTANCE.create().fromJson(data, Map.class);
                phoneNumber = (String) map.get("purePhoneNumber");
                parameters.put("mobile", phoneNumber);
            }
        } catch (CustomOauthException e) {
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomOauthException(AuthErrorStatus.WECHAT_APP_ERROR);
        }

        //设置集成登录信息
        IntegrationAuthentication integrationAuthentication = new IntegrationAuthentication();
        integrationAuthentication.setAuthParameters(parameters);
        IntegrationAuthenticationContext.set(integrationAuthentication);

        Authentication userAuth = new UsernamePasswordAuthenticationToken(openId, password);
        WechatAppAuthenticationToken wechatAppAuthenticationToken = new WechatAppAuthenticationToken(userAuth);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        try {
            userAuth = this.authenticationManager.authenticate(wechatAppAuthenticationToken);
        } catch (AccountStatusException ase) {
            log.error(ase.getMessage());
            throw ase;
        } catch (BadCredentialsException e) {
            log.error(e.getMessage());
            throw e;
        }finally {
            IntegrationAuthenticationContext.clear();
        }
        if (userAuth == null || !userAuth.isAuthenticated()) {
            throw new InvalidGrantException("Could not authenticate user: " + openId);
        }

        if (userAuth != null && userAuth.isAuthenticated()) {
            OAuth2Request storedOAuth2Request = this.getRequestFactory().createOAuth2Request(client, tokenRequest);
            return new OAuth2Authentication(storedOAuth2Request, userAuth);
        } else {
            throw new InvalidGrantException("Could not authenticate user: " + openId);
        }
    }
}
