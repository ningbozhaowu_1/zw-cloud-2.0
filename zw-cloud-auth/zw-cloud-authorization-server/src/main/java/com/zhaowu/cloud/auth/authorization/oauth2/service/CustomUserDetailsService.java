package com.zhaowu.cloud.auth.authorization.oauth2.service;

import com.zhaowu.cloud.auth.authorization.entity.userdetail.User;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthentication;
import com.zhaowu.cloud.auth.authorization.oauth2.utils.IntegrationAuthenticationContext;
import com.zhaowu.cloud.framework.base.exception.ServiceException;
import com.zhaowu.cloud.framework.base.protocol.Result;
import com.zhaowu.cloud.uc.entity.form.UserQueryForm;
import com.zhaowu.cloud.uc.entity.vo.UserAuthentication;
import com.zhaowu.cloud.uc.client.UserClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserClient userClient;

    @Override
    public UserDetails loadUserByUsername(String uniqueId) {

        IntegrationAuthentication integrationAuthentication = IntegrationAuthenticationContext.get();
        String applicationParty = integrationAuthentication.getAuthParameter("application_party");
        UserQueryForm userQueryForm = new UserQueryForm();
        userQueryForm.setUsername(uniqueId);
        userQueryForm.setScene("1");
        userQueryForm.setApplicationParty(applicationParty);


        Result<UserAuthentication> result = userClient.findUser4Login(userQueryForm);
        log.info("load user by username :{}", result);
        if(!result.getSuccess()){
            throw  new ServiceException(result.getCode(), result.getMsg());
        }

        UserAuthentication userAuthentication = result.getData();
        integrationAuthentication.setPwdErrorCount(userAuthentication.getPwdErrorCount());//TODO
        integrationAuthentication.getAuthParameters().put("userId", userAuthentication.getId());
        integrationAuthentication.getAuthParameters().put("partyId", userAuthentication.getPartyId());

        User userAuthrization = new User();
        userAuthrization.setId(userAuthentication.getId());
        userAuthrization.setApplicationParty(userAuthentication.getApplicationParty());
        userAuthrization.setOpenId(userAuthentication.getOpenId());
        userAuthrization.setPassword(userAuthentication.getPassword());
        userAuthrization.setUsername(uniqueId);
        userAuthrization.setStatus(userAuthentication.getStatus());
        userAuthrization.setRole(userAuthentication.getRoleIds());
        userAuthrization.setPartyId(userAuthentication.getPartyId());
        userAuthrization.setPartyChannelId(integrationAuthentication.getAuthParameter("party_channel_id"));
        userAuthrization.setMerchantPartyId(userAuthentication.getMerchantPartyId());
        return userAuthrization;
    }
}
