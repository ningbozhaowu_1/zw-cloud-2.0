package com.zhaowu.cloud.auth.authorization.oauth2.provider;

import com.zhaowu.cloud.auth.authorization.oauth2.token.MobileAuthenticationToken;
import com.zhaowu.cloud.auth.authorization.oauth2.token.UserPasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 手机验证码登陆provider
 */
public class UserPasswordAuthenticationProvider extends DaoAuthenticationProvider {

    public UserPasswordAuthenticationProvider(UserDetailsService userDetailsService) {
        super.setUserDetailsService(userDetailsService);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UserPasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}