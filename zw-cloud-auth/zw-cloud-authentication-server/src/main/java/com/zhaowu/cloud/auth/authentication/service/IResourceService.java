package com.zhaowu.cloud.auth.authentication.service;

import com.zhaowu.cloud.organization.entity.vo.ResourceVO;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Service
public interface IResourceService {

    /**
     * 动态新增更新权限
     *
     * @param resource
     */
    void saveResource(ResourceVO resource);

    /**
     * 动态删除权限
     *
     * @param resource
     */
    void removeResource(ResourceVO resource);

    /**
     * 加载权限资源数据
     */
    void loadResource();

    /**
     * 根据url和method查询到对应的权限信息
     *
     * @param authRequest
     * @return
     */
    ConfigAttribute findConfigAttributesByUrl(HttpServletRequest authRequest);

    /**
     * 查询角色对应的资源信息
     *
     * @param roleIds
     * @return
     */
    Collection<ResourceVO> queryByRoleCodes(Collection<String> roleIds);
}
