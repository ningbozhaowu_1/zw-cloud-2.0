package com.zhaowu.cloud.auth.authentication.events;

import com.zhaowu.cloud.auth.authentication.service.impl.ResourceService;
import com.zhaowu.cloud.organization.entity.vo.ResourceVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BusReceiver {

    @Autowired
    private ResourceService resourceService;

    public void handleMessage(ResourceVO resource) {
        log.info("Received Message:<{}>", resource);
        resourceService.saveResource(resource);
    }
}
